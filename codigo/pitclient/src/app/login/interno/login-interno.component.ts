import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Auth} from "../../models/auth";
import {LoginService} from "../login.service";
import {UsuarioService} from "../../gerenciar-usuario/usuario.service";
import {BlockUI, NgBlockUI} from "ng-block-ui";
import {Constants} from "../../shared/utils/constants";
import {finalize} from "rxjs/operators";
import {PageNotificationService} from "@nuvem/primeng-components";
import {NgForm} from "@angular/forms";
import {RecuperarSenhaService} from "../../shared/services/recuperar-senha.service";
import {ActivatedRoute, Router} from "@angular/router";
import {SelectItem} from "primeng";
import {TranslateService} from "@ngx-translate/core";
import {Usuario} from "../../models/usuario";
import {Perfil} from "../../models/perfil";
import {PerfilEnum} from "../../shared/enumerations/perfil.enum";
import {PerfilService} from "../../shared/services/perfil.service";

@Component({
    selector: 'login-interno',
    templateUrl: 'login-interno.component.html',
    styleUrls: ['./login-interno.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class LoginInternoComponent implements OnInit {
    auth;
    @BlockUI() blockUI: NgBlockUI;
    dialogCadastro = false;
    email: string;
    langs: SelectItem[];
    lingua;
    recuperarSenha = false;
    usuario: Usuario;
    perfilEnum = PerfilEnum;
    perfis: Perfil[];

    redirectUri = '';

    static LANGS = ['en', 'pt', 'es'];

    constructor(private router: Router,
                private activatedRoute: ActivatedRoute,
                private loginService: LoginService,
                private perfilService: PerfilService,
                private usuarioService: UsuarioService,
                private translateService: TranslateService,
                private recuperarSenhaService: RecuperarSenhaService,
                private pagenotification: PageNotificationService) {
        translateService.addLangs(LoginInternoComponent.LANGS);
    }

    ngOnInit(): void {
        this.auth = new Auth();
        this.langs = [
            {label: 'Português', value: 'pt'},
            {label: 'English', value: 'en'},
            {label: 'Español', value: 'es'}
        ];
        this.lingua = this.translateService.currentLang;
        this.perfis = [
            {id: 2, nome: 'Empreendedor', descricao: ''},
            {id: 1, nome: 'Investidor', descricao: ''}
        ];
        this.usuario = new Usuario();

        this.activatedRoute.queryParams.subscribe(params => {
            if(params.lang && LoginInternoComponent.LANGS.includes(params.lang)) {
                this.lingua = params.lang;
                this.useLanguage(params.lang)
            }
            if(params.redirectUri) {
                this.redirectUri = params.redirectUri;
            }
        })
    }

    acaoRecuperarSenha() {
        this.auth = new Auth();
        this.recuperarSenha = !this.recuperarSenha;
    }

    buscarPerfil() {
        this.blockUI.start(Constants.CARREGANDO);
        this.perfilService.buscarPerfil(new Perfil()).pipe(finalize(() => this.blockUI.stop())).subscribe(res => {
            this.usuario.perfilEnum = null;
            this.perfis = res;
            this.perfis.splice(this.perfis.indexOf(this.perfis.find(it => it.nome == PerfilEnum.GESTOR)), 1);
            this.verDialogCadastro();
        });
    }

    iconeEmpreendedor(nome) {
        return nome == this.perfilEnum.EMPREENDEDOR;
    }

    iconeInvestidor(nome) {
        return nome == this.perfilEnum.INVESTIDOR;
    }

    login(form) {
        if (!form.valid) {
            return;
        }
        this.blockUI.start(Constants.CARREGANDO);
        this.loginService.login(this.auth).pipe(finalize(() => this.blockUI.stop()))
            .subscribe(() => {
                sessionStorage.setItem('redirectUri', this.redirectUri);
                this.router.navigate(['login-success']);
            });
    }

    novaSenha() {
        this.blockUI.start(Constants.CARREGANDO);
        this.recuperarSenhaService.gerarSenha(this.email).pipe(finalize(() => this.blockUI.stop()))
            .subscribe(() => {
                this.acaoRecuperarSenha();
                this.pagenotification.addSuccessMessage('Operação realizada com sucesso.');
                this.email = '';
            });
    }

    selecionarPerfil(perfil) {
        this.usuario.perfilEnum = perfil;
    }

    useLanguage(lang): void {
        this.translateService.setDefaultLang(lang);
        this.translateService.use(lang);
        localStorage.setItem('language', lang);
    }

    validarRecuperacao(form: NgForm) {
        return !form.valid;
    }

    verDialogCadastro() {
        this.dialogCadastro = !this.dialogCadastro;
    }

    perfilSelecionado() {
        return !this.usuario.perfilEnum;
    }
}
