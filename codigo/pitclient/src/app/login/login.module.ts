import {NgModule} from '@angular/core';
import {LoginInternoComponent} from "./interno/login-interno.component";
import {loginRoute} from "./login.route";
import {RouterModule} from "@angular/router";
import {PRIMENG_IMPORTS} from "../shared/primeng-imports";
import {ReactiveFormsModule} from "@angular/forms";
import {SharedModule} from "../shared/shared.module";
import {ErrorModule} from "@nuvem/angular-base";
import {ErrorStackModule} from "@nuvem/primeng-components";
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
    imports: [
        RouterModule.forChild(loginRoute),
        PRIMENG_IMPORTS,
        ReactiveFormsModule,
        SharedModule,
        ErrorStackModule,
        ErrorModule,
        TranslateModule
    ],
    exports: [],
    declarations: [
        LoginInternoComponent
    ],
    providers: [],
})
export class LoginModule {
}
