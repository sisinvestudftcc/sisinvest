import {Injectable} from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Auth} from "../models/auth";
import {map} from "rxjs/operators";

@Injectable({
    providedIn: 'root'
})
export class LoginService {

    baseUrl = environment.pitsegurancaUrl;

    constructor(private http: HttpClient) {
    }

    login(auth: Auth): Observable<any>{
        return this.http.post(`${this.baseUrl}/login`, auth);
    }
}
