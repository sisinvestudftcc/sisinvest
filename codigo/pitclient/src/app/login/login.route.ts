import {Routes} from "@angular/router";
import {LoginInternoComponent} from "./interno/login-interno.component";

export const loginRoute: Routes = [
    {
        path: '',
        component: LoginInternoComponent
    }
];
