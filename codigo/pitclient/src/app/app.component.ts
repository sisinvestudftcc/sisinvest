import {Component, OnInit} from '@angular/core';
import {NavigationEnd, NavigationStart, Router} from "@angular/router";
import {TranslateService} from "@ngx-translate/core";
import {GoogleAnalyticsService} from "./shared/services/google-analytics.service";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: []
})
export class AppComponent implements OnInit{

    constructor(private router: Router, private translateService: TranslateService, protected googleAnalyticsService: GoogleAnalyticsService) {
        this.router.events.subscribe((val) => {
            if(val instanceof NavigationEnd) {
                if (val.url === '/login-success') {
                    const redirectUri = sessionStorage.getItem('redirectUri');
                    if (redirectUri) {
                        sessionStorage.removeItem('redirectUri');
                        this.router.navigate(redirectUri.split('/'));
                    }
                }

                this.googleAnalyticsService.dispararAcesso(val.urlAfterRedirects);
            }
        })
    }

    ngOnInit() {
        if(localStorage.getItem('language')) {
            this.translateService.setDefaultLang(localStorage.getItem('language'));
            this.translateService.use(localStorage.getItem('language'));
        } else {
            this.translateService.setDefaultLang('pt');
            this.translateService.use('pt');
        }
    }

}
