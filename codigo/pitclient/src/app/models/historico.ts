import {CamposHistorico} from "./campos-historico";
import {TipoAcaoHistoricoEnum} from "../shared/enumerations/tipo-acao-historico.enum";

export class Historico {

    tipoAcao: TipoAcaoHistoricoEnum;
    usuarioAcao: string;
    dataHoraAcao: Date;
    campos: CamposHistorico[];
}
