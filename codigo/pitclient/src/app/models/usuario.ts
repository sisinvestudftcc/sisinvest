import {Telefone} from "./telefone";
import {StatusEnum} from '../shared/enumerations/status.enum';
import {PerfilEnum} from '../shared/enumerations/perfil.enum';
import {Endereco} from "./endereco";

export class Usuario {
    id: number;
    nome: string;
    email: string;
    telefones: Telefone[];
    perfilEnum: PerfilEnum;
    status: StatusEnum;
    listStatus: StatusEnum[];
    telefone: string;
    passaporte: string;
    cnpj: string;
    cpf: string;
    rne: string;
    informacaoAdicional: string;
    areaInteresseIds: number[];
    linkedin: string;
    perfilId: number;
    endereco: Endereco;
    justificativa: string;
}
