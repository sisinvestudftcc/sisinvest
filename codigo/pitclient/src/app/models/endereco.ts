export class Endereco {
    id: number;
    pais: string;
    estado: string;
    cidade: string;
    codigoPostal: string;
}
