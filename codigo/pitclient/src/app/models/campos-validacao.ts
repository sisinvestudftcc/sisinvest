import {CamposValidacaoEnum} from "../shared/enumerations/campos-validacao.enum";

export class CampoValidacao {
    valor: string;
    tipo: CamposValidacaoEnum;
    id: number;

    constructor(valor, tipo, id) {
        this.valor = valor;
        this.tipo = tipo;
        this.id = id;
    }
}
