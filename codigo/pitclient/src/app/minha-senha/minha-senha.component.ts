import {Component, OnInit} from '@angular/core';
import {BreadcrumbService, PageNotificationService} from "@nuvem/primeng-components";
import {SenhaUsuario} from "../models/senha-usuario";
import {BlockUI, NgBlockUI} from "ng-block-ui";
import {Constants} from "../shared/utils/constants";
import {UsuarioService} from "../gerenciar-usuario/usuario.service";
import {finalize} from "rxjs/operators";
import {Router} from "@angular/router";
import {TranslateService} from "@ngx-translate/core";

@Component({
    selector: 'app-minha-senha',
    templateUrl: 'minha-senha.component.html'
})

export class MinhaSenhaComponent implements OnInit {
    @BlockUI() blockUI: NgBlockUI;
    mensagemSenha = Constants.MENSAGEM_SENHA;
    senhaUsuario: SenhaUsuario;

    constructor(private router: Router,
                private usuarioService: UsuarioService,
                private breadcrumbService: BreadcrumbService,
                private pageNotificationService: PageNotificationService,
                private translateService: TranslateService) {
    }

    ngOnInit() {
        this.breadcrumbService.setItems([{label: this.translateService.instant('ALTERAR_SENHA')}]);
        this.senhaUsuario = new SenhaUsuario();
    }

    salvar() {
        this.blockUI.start(Constants.SALVANDO);
        this.usuarioService.alterarSenha(this.senhaUsuario).pipe(finalize(() => this.blockUI.stop()))
            .subscribe(() => {
                this.voltar();
                this.pageNotificationService.addSuccessMessage('Operação realizada com sucesso.');
            });
    }

    validarSenha() {
        this.blockUI.start(Constants.CARREGANDO);
        this.usuarioService.verificarSenha(this.senhaUsuario.senhaAtual)
            .pipe(finalize(() => this.blockUI.stop())).subscribe();
    }

    validarNovaSenha(campo) {
        const {novaSenha, confirmarSenha} = this.senhaUsuario;
        if (novaSenha && confirmarSenha && novaSenha != confirmarSenha) {
            return this.pageNotificationService.addErrorMessage('A informação inserida no campo Nova Senha e Confirmar Nova Senha devem ser idênticas.');
        }
        this.blockUI.start(Constants.CARREGANDO);
        this.usuarioService.verificarNovaSenha(campo).pipe(finalize(() => this.blockUI.stop())).subscribe();
    }

    voltar() {
        this.router.navigate(['']);
    }
}
