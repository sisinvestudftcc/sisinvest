import {Routes} from "@angular/router";
import {MinhaSenhaComponent} from "./minha-senha.component";

export const MinhaSenhaRoute: Routes = [
    {
        path: '',
        component: MinhaSenhaComponent
    }
]
