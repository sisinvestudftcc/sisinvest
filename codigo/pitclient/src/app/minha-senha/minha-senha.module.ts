import {NgModule} from '@angular/core';
import {MinhaSenhaComponent} from './minha-senha.component';
import {RouterModule} from "@angular/router";
import {PRIMENG_IMPORTS} from "../shared/primeng-imports";
import {SharedModule} from "../shared/shared.module";
import {MinhaSenhaRoute} from "./minha-senha.route";
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
    imports: [
        RouterModule.forChild(MinhaSenhaRoute),
        PRIMENG_IMPORTS,
        SharedModule,
        TranslateModule
    ],
    exports: [],
    declarations: [MinhaSenhaComponent],
    providers: [],
})
export class MinhaSenhaModule {
}
