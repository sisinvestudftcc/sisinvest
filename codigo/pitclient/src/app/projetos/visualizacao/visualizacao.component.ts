import {Component, OnInit, ViewEncapsulation} from "@angular/core";
import {BlockUI, NgBlockUI} from "ng-block-ui";
import {ProjetosService} from "../projetos.service";
import {PageNotificationService} from "@nuvem/primeng-components";
import {TranslateService} from "@ngx-translate/core";
import {ActivatedRoute, Router} from "@angular/router";
import {ProjetoApp} from "../../shared/models/app.models";
import {UsuarioService} from "../../gerenciar-usuario/usuario.service";
import {MessageDialogService} from "../../shared/message-dialog.service";
import {ProjetosApresentacaoService} from "../projetos-apresentacao.service";
import * as _ from "lodash";
import {DateTime} from "luxon";
import {Usuario} from "../../models/usuario";
import {PerfilEnum} from "../../shared/enumerations/perfil.enum";
import {DominioService} from "../dominio.service";
import {FormGroup} from "@angular/forms";
import {FormUtilsService} from "../../shared/services/form-utils.service";
import {finalize} from "rxjs/operators";
import {CentralComunicacaoService} from "../../central-comunicacao/central-comunicacao.service";
import Projeto = ProjetoApp.Projeto;
import ProjetoLinhaTemporal = ProjetoApp.ProjetoLinhaTemporal;
import TipoArquivo = ProjetoApp.TipoArquivo;
import ProjetoInternacionalizacao = ProjetoApp.ProjetoInternacionalizacao;
import Acao = ProjetoApp.Acao;
import Status = ProjetoApp.Status;
import Mensagem = ProjetoApp.Mensagem;
import TipoMensagem = ProjetoApp.TipoMensagem;
import {GoogleAnalyticsService} from "../../shared/services/google-analytics.service";

@Component({
    selector: 'app-projetos-visualizacao',
    templateUrl: 'visualizacao.component.html',
    styleUrls: ['visualizacao.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ProjetosVisualizacaoComponent implements OnInit {

    @BlockUI() blockUI: NgBlockUI;

    projeto: Projeto;

    projetoLinhaTemporal: ProjetoLinhaTemporal[] = [];

    mensagemList: Mensagem[] = [];

    internacionalizacao: ProjetoInternacionalizacao;

    usuarios = [];

    usuariosMensagem = [];

    optionsGmap: any;

    overlaysGmap: any[];

    usuarioLogado: Usuario;

    camposFaltando: boolean = true;

    visualizarPublicacao: boolean = false;

    modalComentario: boolean = false;

    formComentario: FormGroup;

    modalSolicitarEsclarecimento: boolean = false;

    formSolicitarEsclarecimento: FormGroup;

    modalResponderEsclarecimento: boolean = false;

    formResponderEsclarecimento: FormGroup;

    modalConversarEmpreendedor: boolean = false;

    formConversarEmpreendedor: FormGroup;

    mensagemEdicao: Mensagem = null;

    resposta: Mensagem = null;

    podeAprovar: boolean = false;
    podeReprovar: boolean = false;
    podeComentar: boolean = false;
    podeFavoritar: boolean = false;
    podeDesfavoritar: boolean = false;
    usuarioFavoritou: boolean = false;
    podeRestaurar: boolean = false;
    podeSolicitarEsclarecimento: boolean = false;
    podeResponderEsclarecimento: boolean = false;
    podeConversarEmpreendedor: boolean = false;

    mensagensAlerta: {severity: string, summary?: string, detail?: string}[] = [];

    constructor(
        protected pageNotificationService: PageNotificationService,
        protected formUtilsService: FormUtilsService,
        protected translateService: TranslateService,
        protected projetosService: ProjetosService,
        protected dominioService: DominioService,
        protected projetosApresentacaoService: ProjetosApresentacaoService,
        protected usuarioService: UsuarioService,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected messageDialogService: MessageDialogService,
        protected centralComunicacaoService: CentralComunicacaoService,
        protected googleAnalyticsService: GoogleAnalyticsService
    ) {

        this.activatedRoute.params.subscribe(val => {
            if (val.id) {

                this.buscarProjeto(val.id);
            }
        })
    }


    private buscarUsuarioLogado() {
        this.usuarioService.buscarUsuarioLogado().subscribe(response => {
            this.usuarioLogado = response;
            this.usuarioLogado.perfilEnum = response.perfilEnum.toString();

            this.projetosService.filtrar({usuarioFavoritado: this.usuarioLogado.id, projetoId: this.projeto.id}, {
                page: 0,
                size: 1
            }).subscribe(response => {
                this.usuarioFavoritou = false;

                if (response.content.length) {
                    this.usuarioFavoritou = true;
                }

                this.validaPodeFavoritar();
                this.validaPodeDesfavoritar();

                if (this.usuarioLogado.perfilEnum !== PerfilEnum.INVESTIDOR) {
                    this.buscarLinhaTemporal(this.projeto.id);
                }

                this.buscarMensagensProjeto(this.projeto.id);
            })

            this.validaBotoes();
        });
    }

    private buscarProjeto(projetoId) {
        this.blockUI.start(this.translateService.instant('CARREGANDO'));
        this.projetosService.buscarPorId(projetoId).subscribe(
            response => {

                this.buscarUsuarioLogado();

                this.blockUI.stop();
                this.projeto = response;

                if(this.projeto.statusId === Status.RASCUNHO) {
                    this.mensagensAlerta.push({
                        severity: 'warn',
                        summary: this.translateService.instant('PROJETO.SALVO_RASCUNHO')
                    })
                }

                this.buscaIdiomaPadrao();

                this.validaMapa(_.last(this.projeto.localizacoes));
            }
        )
    }

    private buscarLinhaTemporal(projetoId) {
        this.blockUI.start(this.translateService.instant('CARREGANDO'));
        this.projetosService.buscarLinhaTemporalPorId(projetoId, {
            page: 0,
            size: 9999,
            sort: 'id,ASC'
        }).pipe(finalize(() => this.blockUI.stop())).subscribe(response => {
            this.projetoLinhaTemporal = [];
            this.usuarios = [];
            this.buscaDadosUsuarioHistorico(response.content);
        })
    }

    private buscarMensagensProjeto(projetoId) {
        this.blockUI.start(this.translateService.instant('CARREGANDO'));

        let perfil: any = this.usuarioLogado.perfilEnum;

        this.centralComunicacaoService.buscarPorId(projetoId, perfil == PerfilEnum.INVESTIDOR ? this.usuarioLogado.id : null).pipe(finalize(() => this.blockUI.stop())).subscribe(response => {
            this.mensagemList = [];
            this.usuariosMensagem = [];
            this.buscaDadosUsuarioMensagem(response.content);
        })
    }

    ngOnInit() {

    }

    buscaDadosUsuarioHistorico(projetoLinhaTemporal: ProjetoLinhaTemporal[]) {
        if (projetoLinhaTemporal.length) {
            let usuariosBuscados = [];
            let usuarios = [];

            projetoLinhaTemporal.forEach(linhaTemporal => {
                if (!usuariosBuscados.includes(linhaTemporal.usuario)) {
                    usuariosBuscados.push(linhaTemporal.usuario);
                    this.usuarioService.buscarPorId(linhaTemporal.usuario).pipe(finalize(() => this.blockUI.stop())).subscribe(usuario => {

                        usuarios.push({id: linhaTemporal.usuario, nome: usuario.nome});

                        this.usuarios = usuarios;

                    });
                }
            });

            this.projetoLinhaTemporal = projetoLinhaTemporal;
        }
    }

    buscaDadosUsuarioMensagem(mensagemList: Mensagem[]) {
        if (mensagemList.length) {
            let usuariosBuscados = [];
            let usuarios = [];

            mensagemList.forEach(mensagem => {
                if (!usuariosBuscados.includes(mensagem.usuario)) {
                    usuariosBuscados.push(mensagem.usuario);
                    this.usuarioService.buscarPorId(mensagem.usuario).pipe(finalize(() => this.blockUI.stop())).subscribe(usuario => {

                        usuarios.push({id: mensagem.usuario, nome: usuario.nome});

                        this.usuariosMensagem = usuarios;

                    });
                }

                mensagem.respostaList.forEach(resposta => {
                    if (!usuariosBuscados.includes(resposta.usuario)) {
                        usuariosBuscados.push(resposta.usuario);
                        this.usuarioService.buscarPorId(resposta.usuario).pipe(finalize(() => this.blockUI.stop())).subscribe(usuario => {

                            usuarios.push({id: resposta.usuario, nome: usuario.nome});

                            this.usuariosMensagem = usuarios;

                        });
                    }
                });
            });

            this.mensagemList = mensagemList;
        }
    }

    validaMapa(localizacao) {

        if (localizacao.latitude && localizacao.longitude) {

            this.optionsGmap = {
                center: {
                    lat: parseFloat(localizacao.latitude),
                    lng: parseFloat(localizacao.longitude)
                },
                zoom: 16
            }
            this.overlaysGmap = [
                new google.maps.Marker({
                    position: {
                        lat: parseFloat(localizacao.latitude),
                        lng: parseFloat(localizacao.longitude)
                    }
                })
            ];

        }
    }

    buscaIdiomaPadrao() {
        if (this.projeto.internacionalizacao.length) {
            let internacionalizacaoData = _.first(this.projeto.internacionalizacao.filter(internacionalizacao => {
                return internacionalizacao.idioma.sigla === ProjetosApresentacaoService.INTERNACIONALIZACAO_IDIOMA[this.translateService.getDefaultLang()];
            }));

            if(!internacionalizacaoData) {
                internacionalizacaoData = _.first(this.projeto.internacionalizacao.filter(internacionalizacao => {
                    return internacionalizacao.internacionalizacaoPadrao;
                }));
            }

            this.internacionalizacao = internacionalizacaoData;
        }
    }

    getImagemNome(internacionalizacao) {
        if (internacionalizacao.projetoInternacionalizacaoArquivos.length) {
            let imagem = _.first(internacionalizacao.projetoInternacionalizacaoArquivos.filter(arquivo => {
                return arquivo.tipoArquivo === TipoArquivo.IMAGEM;
            }));

            if (imagem) {
                return imagem.arquivo;
            }

        }
    }

    getImagemId(internacionalizacao) {
        if (internacionalizacao.projetoInternacionalizacaoArquivos.length) {
            let imagem = _.first(internacionalizacao.projetoInternacionalizacaoArquivos.filter(arquivo => {
                return arquivo.tipoArquivo === TipoArquivo.IMAGEM;
            }));

            return imagem.arquivoId;
        }
    }

    formataUrl(url) {
        if (!url.includes('http') && !url.includes('http')) {
            return `http://${url}`;
        }

        return url;
    }

    pegaAcaoMensagem(mensagem: Mensagem) {
        return mensagem.tipoMensagem.id === TipoMensagem.MENSAGEM
            ? this.translateService.instant('CENTRAL_COMUNICACAO.HISTORICO.ENVIOU_MENSAGEM')
            : this.translateService.instant('CENTRAL_COMUNICACAO.HISTORICO.RESPONDEU_MENSAGEM');
    }

    converterData(data) {

        if (data) {
            return DateTime.fromISO(data, {zone: 'utc'}).toFormat("dd/LL/yyyy")
        }

        return '-';
    }

    converterHora(data) {

        if (data) {
            return DateTime.fromISO(data, {zone: 'utc'}).toFormat("HH:mm")
        }

        return '-';
    }

    pegaListaAnexos() {
        return this.internacionalizacao.projetoInternacionalizacaoArquivos.filter(arquivo => arquivo.tipoArquivo === TipoArquivo.ANEXO);
    }

    getUsuarioNome(linhaTemporal: ProjetoLinhaTemporal) {

        let usuario = _.first(this.usuarios.filter(usuario => usuario.id == linhaTemporal.usuario));

        return _.get(usuario, 'nome');
    }

    getUsuarioNomeMensagem(mensagem: Mensagem) {

        let usuario = _.first(this.usuariosMensagem.filter(usuario => usuario.id == mensagem.usuario));

        return _.get(usuario, 'nome');
    }

    pegaLabelStatus(linhaTemporal: ProjetoLinhaTemporal) {

        if (this.usuarioLogado.perfilEnum === PerfilEnum.GESTOR) {
            return `<b>${this.getUsuarioNome(linhaTemporal)}</b> ${this.translateService.instant('PROJETO.ALTEROU_STATUS')}`;
        } else {
            return this.translateService.instant('PROJETO.STATUS_ALTERADO');
        }

    }

    pegaAcaoLinhaTemporal(linhaTemporal: ProjetoLinhaTemporal) {
        let acao = '';

        if (linhaTemporal.acao) {
            switch (linhaTemporal.acao.id) {

                case Acao.ESCLARECIMENTO : {
                    acao = this.translateService.instant('PROJETO.ACOES.ESCLARECIMENTO');
                    break;
                }

                case Acao.COMENTARIO : {
                    acao = this.translateService.instant('PROJETO.ACOES.COMENTARIO');
                    break;
                }

                case Acao.RESPONDER_ESCLARECIMENTO : {
                    acao = this.translateService.instant('PROJETO.ACOES.RESPONDER_ESCLARECIMENTO');
                    break;
                }

                case Acao.APROVAR : {
                    acao = this.translateService.instant('PROJETO.ACOES.APROVAR');
                    break;
                }

                case Acao.REPROVAR : {
                    acao = this.translateService.instant('PROJETO.ACOES.REPROVAR');
                    break;
                }

                case Acao.RESTAURAR : {
                    acao = this.translateService.instant('PROJETO.ACOES.RESTAURAR');
                    break;
                }

                case Acao.DESPUBLICAR : {
                    acao = this.translateService.instant('PROJETO.ACOES.DESPUBLICAR');
                    break;
                }

                case Acao.REMOVER_DESTAQUE : {
                    acao = this.translateService.instant('PROJETO.ACOES.REMOVER_DESTAQUE');
                    break;
                }

            }
        }

        return acao;
    }

    validaComentario(linhaTemporal: ProjetoLinhaTemporal) {
        return linhaTemporal.acao && linhaTemporal.acao.id === Acao.COMENTARIO && this.usuarioLogado.id === linhaTemporal.usuario;
    }

    validaMensagemExclusao(mensagem: Mensagem) {

        let perfil: any = this.usuarioLogado.perfilEnum;

        return ([PerfilEnum.GESTOR].includes(perfil) || (this.usuarioLogado.id === mensagem.usuario)) && !mensagem.respostaList.length;

    }

    removerComentario(linhaTemporalId: number) {
        this.messageDialogService.addModerarBoxSemJustificativa(
            'question',
            this.translateService.instant('PROJETO.REMOVER_COMENTARIO'),
            '<p style="color: red"><b>' + this.translateService.instant('PROJETO.ATENCAO') + '</b></p>' +
            this.translateService.instant('PROJETO.REMOVER_COMENTARIO_MENSAGEM'),
            this.translateService.instant('CONFIRMAR'),
            this.translateService.instant('CANCELAR'),
            () => {
                this.projetosService.removerComentario(linhaTemporalId).pipe(finalize(() => this.blockUI.stop())).subscribe(val => {
                    this.pageNotificationService.addSuccessMessage(this.translateService.instant('PROJETO.OPERACAO_REALIZADA'));
                    this.buscarProjeto(this.projeto.id);
                })
            }
        )
    }

    excluirMensagem(mensagemId: number) {
        this.messageDialogService.addModerarBoxSemJustificativa(
            'question',
            this.translateService.instant('PROJETO.REMOVER_MENSAGEM'),
            '<p style="color: red"><b>' + this.translateService.instant('PROJETO.ATENCAO') + '</b></p>' +
            this.translateService.instant('PROJETO.REMOVER_MENSAGEM_MENSAGEM'),
            this.translateService.instant('CONFIRMAR'),
            this.translateService.instant('CANCELAR'),
            () => {
                this.blockUI.start(this.translateService.instant('CARREGANDO'));
                this.centralComunicacaoService.excluir(mensagemId).pipe(finalize(() => this.blockUI.stop())).subscribe(val => {
                    this.pageNotificationService.addSuccessMessage(this.translateService.instant('PROJETO.OPERACAO_REALIZADA'));
                    this.buscarProjeto(this.projeto.id);
                })
            }
        )
    }

    excluirResposta(mensagemId: number) {
        this.messageDialogService.addModerarBoxSemJustificativa(
            'question',
            this.translateService.instant('PROJETO.REMOVER_RESPOSTA'),
            '<p style="color: red"><b>' + this.translateService.instant('PROJETO.ATENCAO') + '</b></p>' +
            this.translateService.instant('PROJETO.REMOVER_RESPOSTA_MENSAGEM'),
            this.translateService.instant('CONFIRMAR'),
            this.translateService.instant('CANCELAR'),
            () => {
                this.blockUI.start(this.translateService.instant('CARREGANDO'));
                this.centralComunicacaoService.excluir(mensagemId).pipe(finalize(() => this.blockUI.stop())).subscribe(val => {
                    this.pageNotificationService.addSuccessMessage(this.translateService.instant('PROJETO.OPERACAO_REALIZADA'));
                    this.buscarProjeto(this.projeto.id);
                })
            }
        )
    }

    validaBotoes() {

        if (![Status.APROVADO].includes(this.projeto.statusId) && this.usuarioLogado.perfilEnum.toString() == PerfilEnum.INVESTIDOR) {
            this.pageNotificationService.addErrorMessage(this.translateService.instant('PROJETO.SEM_ACESSO'));
            this.router.navigate(['/projetos']);
        }

        this.validaTodosCamposPreenchidos();
        this.validaPodeReprovar();
        this.validaPodeAprovar();
        this.validaPodeComentar();
        this.validaPodeRestaurar();
        this.validaPodeSolicitarEsclarecimento();
        this.validaPodeResponderEsclarecimento();
        this.validaPodeConversarEmpreendedor();
    }

    validaTodosCamposPreenchidos() {

        this.camposFaltando = true;

        const detalhe = _.first(_.get(this.projeto, 'detalhes', []));
        const empresa = _.first(_.get(this.projeto, 'empresas', []));
        const segmentos = _.first(_.get(this.projeto, 'segmentos', []));
        const localizacao = _.first(_.get(this.projeto, 'localizacoes', []));
        const internacionalizacao = _.get(this.projeto, 'internacionalizacao', []);

        [
            'latitudeLongitude',
            'latitude',
            'longitude',
            'cidade',
            'estado',
        ].forEach((campo) => {
            if (!_.get(localizacao, campo, null)) {
                this.camposFaltando = false;
            }
        });

        [
            'razaoSocial',
            'cnpj',
            'pais',
            'proprietario',
            'responsavel',
            'site',
            'email',
            'telefoneList'
        ].forEach((campo) => {
            if (!_.get(empresa, campo, null)) {
                this.camposFaltando = false;
            }
        });

        [
            'dataInicio',
            'dataConclusao',
            'naturezaInvestimento',
            'modeloContrato',
            'valorProjeto',
            'empregosProjeto',
            'areaConstruida',
            'areaTotal',
            'grupoOperador'
        ].forEach((campo) => {
            if (!_.get(detalhe, campo, null)) {
                this.camposFaltando = false;
            }
        });

        internacionalizacao.forEach(internacionalizacaoIdioma => {
            [
                'nome',
                'descricao',
                'imagem'
            ].forEach((campo) => {
                if (!_.get(internacionalizacaoIdioma, campo, null)) {
                    this.camposFaltando = false;
                }
            })
        });

    }

    validaPodeComentar() {

        let perfil: any = this.usuarioLogado.perfilEnum;

        this.podeComentar = [PerfilEnum.GESTOR].includes(perfil) && ![Status.REPROVADO, Status.ESCLARECIMENTO].includes(this.projeto.statusId);

    }

    validaPodeRestaurar() {

        let perfil: any = this.usuarioLogado.perfilEnum;

        this.podeRestaurar = [PerfilEnum.GESTOR].includes(perfil) && !this.camposFaltando && this.projeto.statusId == Status.REPROVADO;

    }

    validaPodeFavoritar() {

        let perfil: any = this.usuarioLogado.perfilEnum;

        this.podeFavoritar = [PerfilEnum.INVESTIDOR].includes(perfil) && !this.usuarioFavoritou;

    }

    validaPodeDesfavoritar() {

        let perfil: any = this.usuarioLogado.perfilEnum;

        this.podeDesfavoritar = [PerfilEnum.INVESTIDOR].includes(perfil) && this.usuarioFavoritou;

    }

    validaPodeSolicitarEsclarecimento() {

        let perfil: any = this.usuarioLogado.perfilEnum;

        this.podeSolicitarEsclarecimento = [PerfilEnum.GESTOR].includes(perfil) && !this.camposFaltando && ![Status.RASCUNHO, Status.REPROVADO, Status.ESCLARECIMENTO].includes(this.projeto.statusId);

    }

    validaPodeAprovar() {

        let perfil: any = this.usuarioLogado.perfilEnum;

        this.podeAprovar = [PerfilEnum.GESTOR].includes(perfil) && this.projeto.statusId === Status.SOB_ANALISE && !this.camposFaltando;

    }

    validaPodeReprovar() {

        let perfil: any = this.usuarioLogado.perfilEnum;

        this.podeReprovar = [PerfilEnum.GESTOR].includes(perfil) && !this.camposFaltando && ![Status.REPROVADO, Status.RASCUNHO].includes(this.projeto.statusId);

    }

    validaPodeResponderEsclarecimento() {

        let perfil: any = this.usuarioLogado.perfilEnum;

        this.podeResponderEsclarecimento = [PerfilEnum.GESTOR, PerfilEnum.EMPREENDEDOR].includes(perfil)
            && this.projeto.usuario === this.usuarioLogado.id
            && this.projeto.statusId === Status.ESCLARECIMENTO
            && !this.camposFaltando;

    }

    validaPodeConversarEmpreendedor() {

        const perfil: any = this.usuarioLogado.perfilEnum;

        this.podeConversarEmpreendedor = [Status.APROVADO].includes(this.projeto.statusId)
            && [PerfilEnum.GESTOR, PerfilEnum.INVESTIDOR].includes(perfil);

    }

    podeEditarMensagem(mensagem: Mensagem) {

        const perfil: any = this.usuarioLogado.perfilEnum;

        return (
                PerfilEnum.GESTOR == perfil
                || (
                    PerfilEnum.INVESTIDOR == perfil
                    && mensagem.usuario === this.usuarioLogado.id
                )
            )
            && mensagem.status.id === Status.NAO_RESPONDIDO
            && !mensagem.respostaList.length;

    }

    podeEditarResposta(mensagem: Mensagem) {

        const perfil: any = this.usuarioLogado.perfilEnum;

        return (
            PerfilEnum.GESTOR == perfil
            || (
                PerfilEnum.EMPREENDEDOR == perfil && mensagem.usuario === this.usuarioLogado.id
            )
        );

    }

    podeResponderMensagem(mensagem: Mensagem) {

        const perfil: any = this.usuarioLogado.perfilEnum;

        return [PerfilEnum.GESTOR, PerfilEnum.EMPREENDEDOR].includes(perfil)
            && mensagem.status.id === Status.NAO_RESPONDIDO
            && !mensagem.respostaList.length;

    }

    aprovar() {
        this.messageDialogService.addModerarBoxJustificativa(
            'question',
            this.translateService.instant('PROJETO.APROVAR'),
            '',
            this.translateService.instant('CONFIRMAR'),
            this.translateService.instant('CANCELAR'),
            (justificativa: string) => {
                this.blockUI.start(this.translateService.instant('CARREGANDO'));
                this.projetosService.aprovar({
                    id: this.projeto.id,
                    justificativa,
                    usuario: this.usuarioLogado.id
                }).pipe(finalize(() => this.blockUI.stop())).subscribe(val => {
                    this.pageNotificationService.addSuccessMessage(this.translateService.instant('PROJETO.OPERACAO_REALIZADA'));
                    this.buscarProjeto(this.projeto.id);
                })
            }
        )
    }

    reprovar() {
        this.messageDialogService.addModerarBoxJustificativa(
            'question',
            this.translateService.instant('PROJETO.REPROVAR'),
            '',
            this.translateService.instant('CONFIRMAR'),
            this.translateService.instant('CANCELAR'),
            (justificativa: string) => {
                this.blockUI.start(this.translateService.instant('CARREGANDO'));
                this.projetosService.reprovar({
                    id: this.projeto.id,
                    justificativa,
                    usuario: this.usuarioLogado.id
                }).pipe(finalize(() => this.blockUI.stop())).subscribe(val => {
                    this.pageNotificationService.addSuccessMessage(this.translateService.instant('PROJETO.OPERACAO_REALIZADA'));
                    this.buscarProjeto(this.projeto.id);
                })
            }
        )
    }

    restaurar() {
        this.messageDialogService.addModerarBoxJustificativa(
            'question',
            this.translateService.instant('PROJETO.RESTAURAR_PROJETO'),
            '<p style="color: red"><b>' + this.translateService.instant('PROJETO.ATENCAO') + '</b></p>' +
            this.translateService.instant('PROJETO.RESTAURAR_PROJETO_MENSAGEM'),
            this.translateService.instant('CONFIRMAR'),
            this.translateService.instant('CANCELAR'),
            (justificativa: string) => {
                this.blockUI.start(this.translateService.instant('CARREGANDO'));
                this.projetosService.restaurar({
                    id: this.projeto.id,
                    justificativa,
                    usuario: this.usuarioLogado.id
                }).pipe(finalize(() => this.blockUI.stop())).subscribe(val => {
                    this.pageNotificationService.addSuccessMessage(this.translateService.instant('PROJETO.OPERACAO_REALIZADA'));
                    this.buscarProjeto(this.projeto.id);
                })
            }
        )
    }

    publicar() {
        this.messageDialogService.addModerarBoxSemJustificativa(
            'question',
            this.translateService.instant('PROJETO.PUBLICAR'),
            '<p style="color: red"><b>' + this.translateService.instant('PROJETO.ATENCAO') + '</b></p>' +
            this.translateService.instant('PROJETO.PUBLICAR_MENSAGEM'),
            this.translateService.instant('CONFIRMAR'),
            this.translateService.instant('CANCELAR'),
            () => {
                this.blockUI.start(this.translateService.instant('CARREGANDO'));
                this.projetosService.publicar({
                    id: this.projeto.id,
                    usuario: this.usuarioLogado.id
                }).pipe(finalize(() => this.blockUI.stop())).subscribe(val => {
                    this.pageNotificationService.addSuccessMessage(this.translateService.instant('PROJETO.OPERACAO_REALIZADA'));
                    this.buscarProjeto(this.projeto.id);
                    this.googleAnalyticsService.dispararAcesso(`/projetos/${this.projeto.id}/publicar`);

                    setTimeout(() => {
                        this.googleAnalyticsService.dispararAcesso(`/projetos/${this.projeto.id}`);
                    }, 1000)
                })
            }
        )
    }

    despublicar() {
        this.messageDialogService.addModerarBoxJustificativa(
            'question',
            this.translateService.instant('PROJETO.DESPUBLICAR'),
            '<p style="color: red"><b>' + this.translateService.instant('PROJETO.ATENCAO') + '</b></p>' +
            this.translateService.instant('PROJETO.DESPUBLICAR_MENSAGEM'),
            this.translateService.instant('CONFIRMAR'),
            this.translateService.instant('CANCELAR'),
            (justificativa: string) => {
                this.blockUI.start(this.translateService.instant('CARREGANDO'));
                this.projetosService.despublicar({
                    id: this.projeto.id,
                    justificativa: justificativa,
                    usuario: this.usuarioLogado.id
                }).pipe(finalize(() => this.blockUI.stop())).subscribe(val => {
                    this.pageNotificationService.addSuccessMessage(this.translateService.instant('PROJETO.OPERACAO_REALIZADA'));
                    this.buscarProjeto(this.projeto.id);
                })
            }
        )
    }

    comentar() {
        this.modalComentario = true;
    }

    conversarEmpreendedor() {
        this.modalConversarEmpreendedor = true;
    }

    realizarComentario() {
        if (this.formUtilsService.validate(this.formComentario)) {
            this.blockUI.start(this.translateService.instant('CARREGANDO'));
            let value = _.cloneDeep(this.formComentario.value);

            this.projetosService.comentar({
                id: this.projeto.id,
                justificativa: value.justificativa,
                usuario: this.usuarioLogado.id,
                anexoList: value.anexoList
            }).pipe(finalize(() => this.blockUI.stop())).subscribe(val => {
                this.modalComentario = false;
                this.formComentario = null;
                this.pageNotificationService.addSuccessMessage(this.translateService.instant('PROJETO.OPERACAO_REALIZADA'));
                this.buscarProjeto(this.projeto.id);
            })
        }
    }

    solicitarEsclarecimento() {
        this.modalSolicitarEsclarecimento = true;
    }

    realizarSolicitarEsclarecimento() {
        if (this.formUtilsService.validate(this.formSolicitarEsclarecimento)) {
            this.blockUI.start(this.translateService.instant('CARREGANDO'));
            let value = _.cloneDeep(this.formSolicitarEsclarecimento.value);

            this.projetosService.solicitarEsclarecimento({
                id: this.projeto.id,
                justificativa: value.justificativa,
                usuario: this.usuarioLogado.id,
                anexoList: value.anexoList
            }).pipe(finalize(() => this.blockUI.stop())).subscribe(val => {
                this.modalSolicitarEsclarecimento = false;
                this.formSolicitarEsclarecimento = null;
                this.pageNotificationService.addSuccessMessage(this.translateService.instant('PROJETO.OPERACAO_REALIZADA'));
                this.buscarProjeto(this.projeto.id);
            })
        }
    }

    responderEsclarecimento() {
        this.modalResponderEsclarecimento = true;
    }

    realizarResponderEsclarecimento() {
        if (this.formUtilsService.validate(this.formResponderEsclarecimento)) {
            this.blockUI.start(this.translateService.instant('CARREGANDO'));
            let value = _.cloneDeep(this.formResponderEsclarecimento.value);

            this.projetosService.responderEsclarecimento({
                id: this.projeto.id,
                justificativa: value.justificativa,
                usuario: this.usuarioLogado.id,
                anexoList: value.anexoList
            }).pipe(finalize(() => this.blockUI.stop())).subscribe(val => {
                this.modalResponderEsclarecimento = false;
                this.formResponderEsclarecimento = null;
                this.pageNotificationService.addSuccessMessage(this.translateService.instant('PROJETO.OPERACAO_REALIZADA'));
                this.buscarProjeto(this.projeto.id);
            })
        }
    }

    realizarConversarEmpreendedor() {
        if (this.formUtilsService.validate(this.formConversarEmpreendedor)) {
            this.blockUI.start(this.translateService.instant('CARREGANDO'));
            let value = _.cloneDeep(this.formConversarEmpreendedor.value);

            const form = {
                id: this.projeto.id,
                justificativa: value.justificativa,
                usuario: this.usuarioLogado.id,
                anexoList: value.anexoList,
                anexosRemovidos: value.anexosRemovidos
            };

            let observable = this.centralComunicacaoService.comentar(form);

            if (this.mensagemEdicao != null) {
                form.id = this.mensagemEdicao.id;
                observable = this.centralComunicacaoService.editar(form);
            }

            if (this.resposta != null) {
                form.id = this.resposta.id;
                observable = this.centralComunicacaoService.responder(form);
            }

            observable.pipe(finalize(() => this.blockUI.stop())).subscribe(val => {
                this.modalConversarEmpreendedor = false;
                this.resposta = null;
                this.mensagemEdicao = null;
                this.formConversarEmpreendedor = null;
                this.pageNotificationService.addSuccessMessage(this.translateService.instant('PROJETO.OPERACAO_REALIZADA'));
                this.buscarProjeto(this.projeto.id);
            });
        }
    }

    limparModalMensagem() {
        this.resposta = null;
        this.mensagemEdicao = null;
    }

    formComentarioCriado(formGroup: FormGroup) {
        this.formComentario = formGroup;
    }

    formSolicitarEsclarecimentoCriado(formGroup: FormGroup) {
        this.formSolicitarEsclarecimento = formGroup;
    }

    formResponderEsclarecimentoCriado(formGroup: FormGroup) {
        this.formResponderEsclarecimento = formGroup;
    }

    formConversarEmpreendedorCriado(formGroup: FormGroup) {
        this.formConversarEmpreendedor = formGroup;
    }

    editarMensagem(mensagem: Mensagem) {
        this.resposta = null;
        this.mensagemEdicao = mensagem;
        this.modalConversarEmpreendedor = true;
    }

    responderMensagem(mensagem: Mensagem) {
        this.mensagemEdicao = null;
        this.resposta = mensagem;
        this.modalConversarEmpreendedor = true;
    }

    baixarArquivo(arquivoId) {
        this.dominioService.urlBaixarArquivo(arquivoId);
    }

    formataCNPJ(cnpj) {
        return cnpj.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g, "\$1.\$2.\$3\/\$4\-\$5");
    }

    favoritar() {
        this.blockUI.start(this.translateService.instant('CARREGANDO'));
        this.projetosService.favoritar(this.projeto.id, this.usuarioLogado.id).pipe(finalize(() => this.blockUI.stop())).subscribe(val => {
            this.pageNotificationService.addSuccessMessage(this.translateService.instant('PROJETO.OPERACAO_REALIZADA'));
            this.buscarUsuarioLogado();
        })
    }

    desfavoritar() {
        this.messageDialogService.addModerarBoxSemJustificativa(
            'question',
            this.translateService.instant('PROJETO.DESFAVORITAR'),
            '<p style="color: red"><b>' + this.translateService.instant('PROJETO.ATENCAO') + '</b></p>' +
            this.translateService.instant('PROJETO.DESFAVORITAR_MENSAGEM'),
            this.translateService.instant('CONFIRMAR'),
            this.translateService.instant('CANCELAR'),
            () => {
                this.blockUI.start(this.translateService.instant('CARREGANDO'));
                this.projetosService.desfavoritar(this.projeto.id, this.usuarioLogado.id).pipe(finalize(() => this.blockUI.stop())).subscribe(val => {
                    this.pageNotificationService.addSuccessMessage(this.translateService.instant('PROJETO.OPERACAO_REALIZADA'));
                    this.buscarUsuarioLogado();
                })
            }
        )
    }

}
