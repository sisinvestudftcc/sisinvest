import {Component, EventEmitter, Input, OnInit, Output, ViewChild, ViewEncapsulation} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ProjetoApp} from "../../../shared/models/app.models";
import Projeto = ProjetoApp.Projeto;
import {MessageDialogService} from "../../../shared/message-dialog.service";
import {FileUpload} from "primeng";
import {TranslateService} from "@ngx-translate/core";
import Mensagem = ProjetoApp.Mensagem;
import * as _ from "lodash";

@Component({
    selector: 'app-projetos-visualizacao-modal-arquivos',
    templateUrl: 'modal-arquivos.component.html',
    styleUrls: ['modal-arquivos.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ProjetosVisualizacaoModalArquivosComponent implements OnInit {

    @ViewChild('fileUploadAttachmentComponent') fileUploadAttachmentComponent: FileUpload;

    @Output() formCriado: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();

    @Input() mensagem: Mensagem = null;

    form: FormGroup;

    anexoMimeType = Projeto.FORMATO_ANEXO_MIME_TYPE.join(',');

    constructor(
        protected formBuilder: FormBuilder,
        protected translateService: TranslateService,
        protected messageDialogService: MessageDialogService
    ) {

    }

    ngOnInit() {
        this.initForm();
    }

    initForm() {
        let form = {};

        if(this.mensagem) {
            form = this.pegaFormMensagem(this.mensagem);
        }

        this.form = this.formBuilder.group({
            justificativa: [_.get(form, 'justificativa'), [Validators.required]],
            anexoList: [_.get(form, 'anexoList', [])],
            anexosRemovidos: [[]]
        });

        this.formCriado.emit(this.form);
    }

    pegaFormMensagem(mensagem: Mensagem)  {
        let anexoList = [];

        mensagem.arquivoList.forEach(mensagemArquivo => {
            anexoList.push({
                id: mensagemArquivo.arquivo.id,
                name: mensagemArquivo.arquivo.nome
            })
        });

        return {
            justificativa: mensagem.descricao,
            anexoList: anexoList
        };
    }

    enviarArquivoAnexo(event) {

        if (event.files.length) {
            let arquivo = event.files[0];

            if (arquivo.size <= Projeto.TAMANHO_MAXIMO_ANEXO) {
                let arquivos = this.form.get('anexoList').value;

                arquivos.push(arquivo);

                this.form.get('anexoList').setValue(arquivos);
            } else {
                this.messageDialogService.addErrorMessage(
                    this.translateService.instant('PROJETO.ARQUIVO_INVALIDO'),
                    this.translateService.instant('PROJETO.TAMANHO_ANEXO')
                );
            }
        }

        this.fileUploadAttachmentComponent.clear();

    }

    removerAnexo(index) {
        let arquivos = this.form.get('anexoList').value;

        let arquivo = arquivos.splice(index, 1);

        if(_.get(arquivo, '0.id')) {
            this.form.get('anexosRemovidos').value.push(arquivo[0].id);
        }

        this.form.get('anexoList').setValue(arquivos);
    }

}
