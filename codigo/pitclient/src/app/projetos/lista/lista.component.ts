import {Component, OnInit} from "@angular/core";
import {UsuarioService} from "../../gerenciar-usuario/usuario.service";
import {finalize} from "rxjs/operators";
import {BlockUI, NgBlockUI} from "ng-block-ui";
import {Constants} from "../../shared/utils/constants";
import {FormGroup} from "@angular/forms";
import {GoogleAnalyticsService} from "../../shared/services/google-analytics.service";
import {ProjetosService} from "../projetos.service";

@Component({
    selector: 'app-projetos-lista',
    templateUrl: 'lista.component.html'
})
export class ProjetosListaComponent implements OnInit {

    @BlockUI() blockUI: NgBlockUI;

    usuarioId: number;
    perfilUsuario: string;
    consultarModal: boolean = false;

    form: FormGroup;

    filter: any = null;

    constructor(
        protected usuarioService: UsuarioService,
        protected googleAnalyticsService: GoogleAnalyticsService,
        protected projetosService: ProjetosService
    ) {

    }

    ngOnInit() {

        this.initUsuario();

    }

    formCriado(form: FormGroup) {
        this.form = form;
    }

    initUsuario() {
        this.usuarioId = null;
        this.perfilUsuario = null;

        this.blockUI.start(Constants.CARREGANDO);
        this.usuarioService.buscarUsuarioLogado().pipe(finalize(() => this.blockUI.stop()))
            .subscribe(res => {
                this.usuarioId = res.id;
                this.perfilUsuario = res.perfilEnum.toString();
            });

    }

    resetarFiltros() {
        this.form = null;
        this.consultarModal = false;
        this.filter = null;
        this.initUsuario();
        this.googleAnalyticsService.dispararAcesso('/projetos');
    }

    consultar() {
        this.filter = this.form.value;
    }

    ativarConsultar() {
        this.consultarModal = true;
        this.googleAnalyticsService.dispararAcesso('/projetos/consultar');
    }

}
