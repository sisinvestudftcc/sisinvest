import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {ProjetoApp} from "../../../../shared/models/app.models";
import Status = ProjetoApp.Status;
import {MessageDialogService} from "../../../../shared/message-dialog.service";
import {TranslateService} from "@ngx-translate/core";
import {ProjetosService} from "../../../projetos.service";
import {PageNotificationService} from "@nuvem/primeng-components";


@Component({
    selector: 'app-projetos-lista-investidor-acoes',
    templateUrl: 'acoes.component.html'
})
export class ProjetosListaInvestidorAcoesComponent implements OnInit {

    @Input()
    item: any;

    @Input()
    index: number;

    @Input()
    usuarioId: number;

    @Input()
    quantidadeRegistros: number;

    @Output()
    acaoRealizada: EventEmitter<void> = new EventEmitter<void>();

    constructor(
        protected projetosService: ProjetosService,
        protected translateService: TranslateService,
        protected messageDialogService: MessageDialogService,
        protected pageNotificationService: PageNotificationService
    ) {
    }

    ngOnInit() {

    }

}
