import {Component, EventEmitter, Input, OnInit, Output, ViewChild, ViewEncapsulation} from "@angular/core";
import {Observable} from "rxjs";
import {Coluna} from "../../../shared/models/coluna";
import {ProjetosService} from "../../projetos.service";
import {DateTime} from "luxon";
import {ProjetoApp} from "../../../shared/models/app.models";
import {UsuarioService} from "../../../gerenciar-usuario/usuario.service";
import {GridComponent} from "../../../shared/views/grid/grid.component";
import {Router} from "@angular/router";
import {ProjetosApresentacaoService} from "../../projetos-apresentacao.service";
import Projeto = ProjetoApp.Projeto;
import Status = ProjetoApp.Status;
import Idioma = ProjetoApp.Idioma;
import {DominioService} from "../../dominio.service";
import {TranslateService} from "@ngx-translate/core";


@Component({
    selector: 'app-projetos-lista-investidor',
    templateUrl: 'investidor.component.html',
    styleUrls: ['./investidor.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ProjetosListaInvestidorComponent implements OnInit {

    @ViewChild('gridComponent') gridComponent: GridComponent;

    @Input() usuarioId;

    _filterConsultar: any = null;

    @Input() set filterConsultar(filtro: any) {
        this._filterConsultar = filtro;

        if (filtro == null) {
            this.filtroFavoritos = {
                idioma: Idioma.SIGLA_PADRAO,
                usuarioFavoritado: this.usuarioId,
                statusList: [Status.APROVADO]
            };
        } else {
            if(filtro.segmentoList && filtro.segmentoList.length) {
                filtro.segmentoList = filtro.segmentoList.map(segmento => segmento.value);
            }
            filtro.statusList = [Status.APROVADO];
            this.filtroFavoritos = filtro;
        }

        if (this.gridComponent) {
            this.gridComponent.reset()
        }
    };

    get filterConsultar() {
        return this._filterConsultar;
    }

    @Output() itemSelecionadoEvent: EventEmitter<any> = new EventEmitter<any>();

    callbackBuscarDadosFavoritos: (projeto) => Observable<any>;

    filtroFavoritos: any = {
        idioma: Idioma.SIGLA_PADRAO
    };

    paginacao = {};

    itensSelecionados = [];

    colunasFavoritos: Coluna[] = [
        {
            campo: 'dataAtualizacao',
            label: this.translateService.instant('PROJETOS.GRID.ULTIMA_ATUALIZACAO'),
            getResultado: (item: Projeto) => {
                return this.projetosApresentacaoService.converterDataAtualizacao(item.dataAtualizacao)
            }
        },
        {
            campo: 'dataCriacao',
            label: this.translateService.instant('PROJETOS.GRID.DATA_CRIACAO'),
            getResultado: (item: Projeto) => {
                return DateTime.fromISO(item.dataCriacao, {zone: 'utc'}).toFormat("dd/LL/yyyy");
            }
        },
        {
            campo: 'protocolo',
            label: this.translateService.instant('PROJETOS.GRID.PROTOCOLO'),
            getResultado: (item: Projeto) => {
                return item.protocolo;
            }
        },
        {
            campo: 'projetoInternacionalizacaoList.nome',
            label: this.translateService.instant('PROJETOS.GRID.NOME'),
            getResultado: (item: Projeto) => {
                return item.internacionalizacao.filter(traducao => traducao.idioma.sigla === Idioma.SIGLA_PADRAO)
                    .map(traducao => traducao.nome);
            }
        },
        {
            campo: 'projetoEmpresaList.razaoSocial',
            label: this.translateService.instant('PROJETOS.GRID.EMPRESA'),
            getResultado: (item: Projeto) => {
                return item.empresas.map(empresa => empresa.razaoSocial);
            }
        },
        {
            campo: 'segmento.nome',
            label: this.translateService.instant('PROJETOS.GRID.SEGMENTO'),
            tipo: 'html',
            getResultado: (item: Projeto) => {
                return item.segmentos.map(segmento => {
                    return `<span class="badge-segmento">${segmento.segmento.nome}</span>`;
                }).join('');
            }
        },
        {
            campo: 'segmento',
            label: this.translateService.instant('PROJETOS.GRID.PUBLICADO'),
            getResultado: (item: Projeto) => {
                return [Status.APROVADO].includes(item.statusId)
                    ? 'pi pi-check-circle icon-success'
                    : 'pi pi-times-circle icon-error';
            },
            tipo: 'icone',
            ordenavel: false
        }
    ];

    constructor(
        protected projetosService: ProjetosService,
        protected usuarioService: UsuarioService,
        protected dominioService: DominioService,
        protected router: Router,
        protected projetosApresentacaoService: ProjetosApresentacaoService,
        protected translateService: TranslateService
    ) {

    }

    ngOnInit() {
        this.filtroFavoritos.usuarioFavoritado = this.usuarioId;
        this.initCallback();
    }

    initCallback() {
        this.callbackBuscarDadosFavoritos = (evento) => this.projetosService.filtrar(this.filtroFavoritos, evento.paginacao);
    }

    atualizarGrids() {
        let currentUrl = this.router.url;
        this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
            this.router.navigate([currentUrl]);
        });
    }

    itemSelecionado(itemSelecionado){
        this.itensSelecionados = itemSelecionado;

        this.itemSelecionadoEvent.emit(itemSelecionado);
    }

    gerarPortfolio() {
        let idList = this.itensSelecionados.map(item => item.id);

        this.dominioService.portfolio(idList);
    }

}
