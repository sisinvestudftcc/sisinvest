import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from "@angular/core";
import {FormBuilder, FormGroup} from "@angular/forms";
import {DominioService} from "../../dominio.service";
import {ProjetoApp} from "../../../shared/models/app.models";
import Option = ProjetoApp.Option;
import {PerfilEnum} from "../../../shared/enumerations/perfil.enum";
import ProjetoEquipamento = ProjetoApp.ProjetoEquipamento;
import * as _ from "lodash";
import {ProjetosApresentacaoService} from "../../projetos-apresentacao.service";
import {TranslateService} from "@ngx-translate/core";

@Component({
    selector: 'app-projetos-lista-consulta',
    templateUrl: 'consulta.component.html',
    styleUrls: ['consulta.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ProjetosListaConsultaComponent implements OnInit {

    @Input() usuarioId: number;

    @Input() perfil: string;

    @Output() formCriado: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();

    @Output() fecharFiltroEvent: EventEmitter<void> = new EventEmitter<void>();

    @Output() consultarEvent: EventEmitter<void> = new EventEmitter<void>();

    mostrarEquipamento = false;

    form: FormGroup;

    segmentos = [];

    constructor(
        protected formBuilder: FormBuilder,
        protected dominioService: DominioService,
        protected projetosApresentacaoService: ProjetosApresentacaoService,
        protected translateService: TranslateService
    ) {
        this.dominioService.segmentoFiltrar({}, {page: 0, size: 9999}).subscribe(optionList => {

            optionList = optionList.content;

            optionList = optionList.map(option => {
                return new Option(option.id, this.translateService.instant('SEGMENTO.' + option.id));
            });

            this.segmentos = optionList;

        });
    }

    ngOnInit() {
        this.form = this.formBuilder.group({
            protocolo: [null],
            nome: [null],
            estado: [null],
            cidade: [null],
            cnpj: [null],
            pais: [null],
            dataInicio: [null],
            dataConclusao: [null],
            segmentoList: [null],
            grupoEquipamento: [null],
            equipamento: [null],
            naturezaInvestimento: [null],
            modeloContrato: [null],
            valorProjeto: [null],
            status: [null],
            usuarioStatus: [this.perfil === PerfilEnum.INVESTIDOR ? null : this.usuarioId],
            internacionalizacaoPadrao: true
        });

        this.formCriado.emit(this.form);
    }

    fecharFiltro() {
        this.fecharFiltroEvent.emit();
    }

    consultar() {
        this.consultarEvent.emit();
    }

    limpaCidade() {
        this.form.get('cidade').setValue(null);
    }

    validaMostrarEquipamento() {

        this.form.get('equipamento').setValue(null);

        const grupoEquipamento = this.form.get('grupoEquipamento').value;

        this.mostrarEquipamento = false;

        if (this.projetosApresentacaoService.possuiEquipamento(grupoEquipamento)) {
            this.mostrarEquipamento = true;
        }

    }

}
