import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {ProjetoApp} from "../../../../shared/models/app.models";
import Status = ProjetoApp.Status;
import {MessageDialogService} from "../../../../shared/message-dialog.service";
import {TranslateService} from "@ngx-translate/core";
import {ProjetosService} from "../../../projetos.service";
import {PageNotificationService} from "@nuvem/primeng-components";


@Component({
    selector: 'app-projetos-lista-empreendedor-acoes',
    templateUrl: 'acoes.component.html'
})
export class ProjetosListaEmpreendedorAcoesComponent implements OnInit {

    @Input()
    item: any;

    @Input()
    index: number;

    @Input()
    usuarioId: number;

    @Input()
    quantidadeRegistros: number;

    @Output()
    acaoRealizada: EventEmitter<void> = new EventEmitter<void>();

    podeEditar: boolean = false;

    constructor(
        protected projetosService: ProjetosService,
        protected translateService: TranslateService,
        protected messageDialogService: MessageDialogService,
        protected pageNotificationService: PageNotificationService
    ) {
    }

    ngOnInit() {

        this.validaPodeEditar();

    }

    validaPodeEditar() {
        this.podeEditar = [Status.RASCUNHO, Status.ESCLARECIMENTO].includes(this.item.statusId) && this.item.usuario == this.usuarioId;
    }

}
