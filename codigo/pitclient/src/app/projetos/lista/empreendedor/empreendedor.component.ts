import {Component, Input, OnInit, ViewChild} from "@angular/core";
import {Observable} from "rxjs";
import {Coluna} from "../../../shared/models/coluna";
import {ProjetosService} from "../../projetos.service";
import {DateTime} from "luxon";
import {ProjetoApp} from "../../../shared/models/app.models";
import {UsuarioService} from "../../../gerenciar-usuario/usuario.service";
import {GridComponent} from "../../../shared/views/grid/grid.component";
import {Router} from "@angular/router";
import {ProjetosApresentacaoService} from "../../projetos-apresentacao.service";
import Projeto = ProjetoApp.Projeto;
import Status = ProjetoApp.Status;
import Idioma = ProjetoApp.Idioma;
import {TranslateService} from "@ngx-translate/core";


@Component({
    selector: 'app-projetos-lista-empreendedor',
    templateUrl: 'empreendedor.component.html'
})
export class ProjetosListaEmpreendedorComponent implements OnInit {

    @ViewChild('gridComponent') gridComponent: GridComponent;

    callbackBuscarDados: (projeto) => Observable<any>;

    @Input() usuarioId: number;

    _filterConsultar: any = null;

    @Input() set filterConsultar(filtro: any) {
        this._filterConsultar = filtro;

        if (filtro == null) {
            this.filtro = {
                notStatusList: [Status.REPROVADO],
                idioma: Idioma.SIGLA_PADRAO,
                apenasProprios: this.usuarioId
            };
        } else {
            if(filtro.segmentoList && filtro.segmentoList.length) {
                filtro.segmentoList = filtro.segmentoList.map(segmento => segmento.value);
            }
            filtro.apenasProprios = this.usuarioId;
            this.filtro = filtro;
        }

        if (this.gridComponent) {
            this.gridComponent.reset()
        }
    };

    get filterConsultar() {
        return this._filterConsultar;
    }

    filtro: any = {
        notStatusList: [Status.REPROVADO],
        idioma: Idioma.SIGLA_PADRAO
    };

    paginacao = {};

    colunas: Coluna[] = [
        {
            campo: 'dataAtualizacao',
            label: this.translateService.instant('PROJETOS.GRID.ULTIMA_ATUALIZACAO'),
            getResultado: (item: Projeto) => {
                return this.projetosApresentacaoService.converterDataAtualizacao(item.dataAtualizacao)
            }
        },
        {
            campo: 'dataCriacao',
            label: this.translateService.instant('PROJETOS.GRID.DATA_CRIACAO'),
            getResultado: (item: Projeto) => {
                return DateTime.fromISO(item.dataCriacao, {zone: 'utc'}).toFormat("dd/LL/yyyy");
            }
        },
        {
            campo: 'protocolo',
            label: this.translateService.instant('PROJETOS.GRID.PROTOCOLO'),
            getResultado: (item: Projeto) => {
                return item.protocolo;
            }
        },
        {
            campo: 'projetoInternacionalizacaoList.nome',
            label: this.translateService.instant('PROJETOS.GRID.NOME'),
            getResultado: (item: Projeto) => {
                return item.internacionalizacao.filter(traducao => traducao.idioma.sigla === Idioma.SIGLA_PADRAO)
                    .map(traducao => traducao.nome);
            }
        },
        {
            campo: 'statusNome',
            label: this.translateService.instant('PROJETOS.GRID.STATUS'),
            getResultado: (item: Projeto) => {
                return this.translateService.instant(`STATUS.${item.statusId}`);
            }
        },
        {
            campo: 'publicado',
            label: this.translateService.instant('PROJETOS.GRID.PUBLICADO'),
            getResultado: (item: Projeto) => {
                return [Status.APROVADO].includes(item.statusId)
                    ? 'pi pi-check-circle icon-success'
                    : 'pi pi-times-circle icon-error';
            },
            tipo: 'icone',
            ordenavel: false
        }
    ];

    constructor(
        protected projetosService: ProjetosService,
        protected usuarioService: UsuarioService,
        protected router: Router,
        protected projetosApresentacaoService: ProjetosApresentacaoService,
        protected translateService: TranslateService
    ) {

    }

    ngOnInit() {
        this.filtro.usuarioId = this.usuarioId;
        this.initCallback();
    }

    initCallback() {
        this.callbackBuscarDados = (evento) => this.projetosService.filtrar(this.filtro, evento.paginacao);
    }

    atualizarGrids() {
        let currentUrl = this.router.url;
        this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
            this.router.navigate([currentUrl]);
        });
    }

}
