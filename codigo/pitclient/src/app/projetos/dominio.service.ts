import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {Page} from "../shared/page";
import {ProjetoApp} from "../shared/models/app.models";
import Projeto = ProjetoApp.Projeto;
import Idioma = ProjetoApp.Idioma;
import Pais = ProjetoApp.Pais;
import NaturezaInvestimento = ProjetoApp.NaturezaInvestimento;
import ModeloContrato = ProjetoApp.ModeloContrato;
import ValorProjeto = ProjetoApp.ValorProjeto;
import EmpregoProjeto = ProjetoApp.EmpregoProjeto;
import GrupoEquipamento = ProjetoApp.GrupoEquipamento;
import Equipamento = ProjetoApp.Equipamento;
import Segmento = ProjetoApp.Segmento;
import Status = ProjetoApp.Status;
import {FormUtilsService} from "../shared/services/form-utils.service";

@Injectable({providedIn: "root"})
export class DominioService {

    baseUrl = `${environment.pitprojetoUrl}/public/dominio`;
    baseUrlMaps = `${environment.googleMapsUrl}?key=${environment.googleMapsKey}&sensor=true&latlng=`;

    constructor(
        private http: HttpClient
    ) {

    }

    idiomaFiltrar(filtro: any = {}, paginacao: any = {page: 0, size: 30}): Observable<any> {

        return this.http.post<Page<Idioma[]>>(`${this.baseUrl}/idioma`, filtro, {params: paginacao});

    }

    paisFiltrar(filtro: any = {}, paginacao: any = {page: 0, size: 30}): Observable<any> {

        return this.http.post<Page<Pais[]>>(`${this.baseUrl}/pais`, filtro, {params: paginacao});

    }

    naturezaInvestimentoFiltrar(filtro: any = {}, paginacao: any = {page: 0, size: 30}): Observable<any> {

        return this.http.post<Page<NaturezaInvestimento[]>>(`${this.baseUrl}/natureza-investimento`, filtro, {params: paginacao});

    }

    modeloContratoFiltrar(filtro: any = {}, paginacao: any = {page: 0, size: 30}): Observable<any> {

        return this.http.post<Page<ModeloContrato[]>>(`${this.baseUrl}/modelo-contrato`, filtro, {params: paginacao});

    }

    valorProjetoFiltrar(filtro: any = {}, paginacao: any = {page: 0, size: 30}): Observable<any> {

        return this.http.post<Page<ValorProjeto[]>>(`${this.baseUrl}/valor-projeto`, filtro, {params: paginacao});

    }

    empregoProjetoFiltrar(filtro: any = {}, paginacao: any = {page: 0, size: 30}): Observable<any> {

        return this.http.post<Page<EmpregoProjeto[]>>(`${this.baseUrl}/emprego-projeto`, filtro, {params: paginacao});

    }

    grupoEquipamentoFiltrar(filtro: any = {}, paginacao: any = {page: 0, size: 30}): Observable<any> {

        return this.http.post<Page<GrupoEquipamento[]>>(`${this.baseUrl}/grupo-equipamento`, filtro, {params: paginacao});

    }

    equipamentoFiltrar(filtro: any = {}, paginacao: any = {page: 0, size: 30}): Observable<any> {

        return this.http.post<Page<Equipamento[]>>(`${this.baseUrl}/equipamento`, filtro, {params: paginacao});

    }

    statusFiltrar(filtro: any = {}, paginacao: any = {page: 0, size: 30}): Observable<any> {

        return this.http.post<Page<Status[]>>(`${this.baseUrl}/status`, filtro, {params: paginacao});

    }

    estadoFiltrar(): Observable<any> {

        return this.http.post<Page<Status[]>>(`${this.baseUrl}/estado`, {});

    }

    cidadeFiltrar(filtro: any = {}): Observable<any> {

        return this.http.post<Page<Status[]>>(`${this.baseUrl}/cidade`, filtro);

    }

    segmentoFiltrar(filtro: any = {}, paginacao: any = {page: 0, size: 30}): Observable<any> {

        return this.http.post<Page<Segmento[]>>(`${this.baseUrl}/segmento`, filtro, {params: paginacao});

    }

    portfolio(idList): void {

        let urlQueryString = '';
        let query = [];

        idList.forEach(id => {
            query.push(`idlist[]=${id}`);
        });

        urlQueryString += '?' + query.join('&');

        let url = `${this.baseUrl}/portfolio${urlQueryString}`;

        const frame = <any> document.createElement('iframe');
        frame.style = 'display: none';
        frame.src = url;
        document.body.appendChild(frame);

    }

    buscaDadosMapa(latitude: number, longitude: number): any {
        return fetch(`${this.baseUrlMaps}${latitude},${longitude}`);
    }

    urlVisualizarImagem(arquivoId: number) {
        return `${this.baseUrl}/visualizar-arquivo/${arquivoId}`;
    }

    urlBaixarArquivo(arquivoId: number) {
        let url = `${this.baseUrl}/baixar-arquivo/${arquivoId}`;

        const frame = <any> document.createElement('iframe');
        frame.style = 'display: none';
        frame.src = url;
        document.body.appendChild(frame);

    }

}
