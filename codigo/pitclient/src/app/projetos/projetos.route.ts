import {Routes} from "@angular/router";
import {ProjetosListaComponent} from "./lista/lista.component";
import {ProjetosCadastroComponent} from "./cadastro/cadastro.component";
import {ProjetosEdicaoComponent} from "./edicao/edicao.component";
import {ProjetosVisualizacaoComponent} from "./visualizacao/visualizacao.component";

export const ProjetosRoute: Routes = [
    {
        path: '',
        component: ProjetosListaComponent
    },
    {
        path: 'cadastro',
        component: ProjetosCadastroComponent
    },
    {
        path: ':id',
        component: ProjetosVisualizacaoComponent
    },
    {
        path: 'editar/:id',
        component: ProjetosEdicaoComponent
    }
];
