import {Component, OnInit, ViewEncapsulation} from "@angular/core";
import {BlockUI, NgBlockUI} from "ng-block-ui";
import {FormGroup} from "@angular/forms";
import {FormUtilsService} from "../../shared/services/form-utils.service";
import {ProjetosService} from "../projetos.service";
import {PageNotificationService} from "@nuvem/primeng-components";
import {TranslateService} from "@ngx-translate/core";
import {ActivatedRoute, Router} from "@angular/router";
import {ProjetoApp} from "../../shared/models/app.models";
import {UsuarioService} from "../../gerenciar-usuario/usuario.service";
import {Constants} from "../../shared/utils/constants";
import * as _ from "lodash";
import {MessageDialogService} from "../../shared/message-dialog.service";
import {ProjetosApresentacaoService} from "../projetos-apresentacao.service";
import {DominioService} from "../dominio.service";
import Status = ProjetoApp.Status;
import TipoArquivo = ProjetoApp.TipoArquivo;
import Projeto = ProjetoApp.Projeto;
import {finalize} from "rxjs/operators";

@Component({
    selector: 'app-projetos-edicao',
    templateUrl: 'edicao.component.html',
    styleUrls: ['edicao.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ProjetosEdicaoComponent implements OnInit {

    @BlockUI() blockUI: NgBlockUI;

    form: FormGroup;

    internacionalizacaoList = [];

    projeto: Projeto;

    projetoVisualizar: Projeto;

    mensagensAlerta: {severity: string, summary?: string, detail?: string}[] = [];

    constructor(
        public formUtilsService: FormUtilsService,
        protected pageNotificationService: PageNotificationService,
        protected translateService: TranslateService,
        protected projetosService: ProjetosService,
        protected projetosApresentacaoService: ProjetosApresentacaoService,
        protected usuarioService: UsuarioService,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected messageDialogService: MessageDialogService,
        protected dominioService: DominioService
    ) {

        this.activatedRoute.params.subscribe(val => {
            if (val.id) {
                this.projetosService.buscarPorId(val.id).subscribe(
                    response => {
                        this.projeto = response;

                        if(this.projeto.statusId === Status.RASCUNHO) {
                            this.mensagensAlerta.push({
                                severity: 'error',
                                summary: this.translateService.instant('PROJETO.SALVO_RASCUNHO')
                            })
                        }
                    }
                )
            }
        })
    }

    ngOnInit() {
    }

    criacaoForm(form: FormGroup) {
        this.form = form;
    }

    salvar() {
        if (this.formUtilsService.validate(this.form)) {
            this.messageDialogService.addModerarBoxSemJustificativa(
                'question',
                this.translateService.instant('PROJETOS.FORM.CAMPOS.SALVAR_RASCUNHO'),
                '<p style="color: red"><b>' + this.translateService.instant('PROJETO.ATENCAO') + '</b></p>' +
                this.translateService.instant('PROJETO.SALVAR_RASCUNHO'),
                this.translateService.instant('CONFIRMAR'),
                this.translateService.instant('CANCELAR'),
                () => {
                    this.form.get('status').setValue(Status.RASCUNHO);

                    this.salvarAction();
                }
            )
        }

    }

    salvarEnviar() {
        if (this.formUtilsService.validate(this.form)) {
            this.form.get('status').setValue(Status.SOB_ANALISE);

            this.salvarAction();
        }
    }

    private salvarAction() {

        let formData = this.form.value;

        formData.internacionalizacaoList.forEach(internacionalizacao => {

            let texto: string = internacionalizacao.descricao;

            texto = texto.replace(/<br\s*\/?>/gi, '<br\/>');

            internacionalizacao.descricao = texto;

        });

        this.blockUI.start(Constants.CARREGANDO);
        this.usuarioService.buscarUsuarioLogado().subscribe(response => {
            this.form.get('usuarioId').setValue(response.id);
            this.projetosService.salvar(formData).pipe(finalize(() => this.blockUI.stop())).subscribe(
                response => {
                    this.internacionalizacaoList = response.internacionalizacao;
                    this.validaEnvioArquivos();
                }
            )
        });
    }

    validaTodosCamposPreenchidos(formValue: any, validarTermo = true): boolean {

        return this.projetosApresentacaoService.validarCampos(formValue, validarTermo);

    }

    validaEnvioArquivos() {
        if (this.form.value.internacionalizacaoList.length) {
            this.form.value.internacionalizacaoList.forEach(internacionalizacao => {

                let internacionalizacaoIdioma = this.internacionalizacaoList.find(internacionalizacaoSalva => {
                    return internacionalizacaoSalva.idioma.id === internacionalizacao.idiomaId
                });

                if (internacionalizacao.imagem && internacionalizacao.imagem instanceof File) {
                    let formData = this.formUtilsService.convertToFormData({file: internacionalizacao.imagem});
                    this.projetosService.uploadArquivo(
                        internacionalizacaoIdioma.id,
                        formData,
                        TipoArquivo.IMAGEM
                    ).subscribe(response => {
                    });
                }

                if (internacionalizacao.anexoList.length) {
                    let anexosFile = internacionalizacao.anexoList.filter(anexo => anexo instanceof File);

                    let formData = this.formUtilsService.convertToFormData({file: anexosFile});
                    this.projetosService.uploadArquivo(
                        internacionalizacaoIdioma.id,
                        formData,
                        TipoArquivo.ANEXO
                    ).subscribe(response => {
                    });
                }
            });

            this.blockUI.stop();
            this.pageNotificationService.addSuccessMessage(this.translateService.instant('CADASTRO_FINALIZADO'));
            this.router.navigate(['/projetos']);
        }
    }

    cancelar() {
        if (this.form.dirty) {
            this.messageDialogService.addOptionalCancelar(() => this.router.navigate(['/projetos']));
        } else {
            this.router.navigate(['/projetos']);
        }
    }

    projetoEmEsclarecimento() {
        return this.projeto.statusId == Status.ESCLARECIMENTO;
    }

}
