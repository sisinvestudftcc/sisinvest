import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {ButtonModule, TableModule} from "primeng";
import {CommonModule} from "@angular/common";
import {SharedModule} from "../shared/shared.module";
import {PRIMENG_IMPORTS} from "../shared/primeng-imports";
import {ProjetosRoute} from "./projetos.route";
import {ProjetosService} from "./projetos.service";
import {ProjetosListaComponent} from "./lista/lista.component";
import {ProjetosListaGestorComponent} from "./lista/gestor/gestor.component";
import {ProjetosListaGestorAcoesComponent} from "./lista/gestor/acoes/acoes.component";
import {ProjetosCadastroComponent} from "./cadastro/cadastro.component";
import {ProjetosFormularioComponent} from "./formulario/formulario.component";
import {ReactiveFormsModule} from "@angular/forms";
import {ProjetosEdicaoComponent} from "./edicao/edicao.component";
import {ProjetosVisualizacaoComponent} from "./visualizacao/visualizacao.component";
import {ProjetosPublicacaoComponent} from "./publicacao/publicacao.component";
import {ProjetosVisualizacaoModalArquivosComponent} from "./visualizacao/modal-arquivos/modal-arquivos.component";
import {ProjetosListaEmpreendedorComponent} from "./lista/empreendedor/empreendedor.component";
import {ProjetosListaEmpreendedorAcoesComponent} from "./lista/empreendedor/acoes/acoes.component";
import {ProjetosListaInvestidorAcoesComponent} from "./lista/investidor/acoes/acoes.component";
import {ProjetosListaInvestidorComponent} from "./lista/investidor/investidor.component";
import {ProjetosListaConsultaComponent} from "./lista/consulta/consulta.component";
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
    imports: [
        PRIMENG_IMPORTS,
        ButtonModule,
        TableModule,
        CommonModule,
        SharedModule,
        TranslateModule,
        ReactiveFormsModule,
        RouterModule.forChild(ProjetosRoute)
    ],
    exports: [],
    declarations: [
        ProjetosListaComponent,
        ProjetosEdicaoComponent,
        ProjetosCadastroComponent,
        ProjetosFormularioComponent,
        ProjetosPublicacaoComponent,
        ProjetosListaGestorComponent,
        ProjetosVisualizacaoComponent,
        ProjetosListaConsultaComponent,
        ProjetosListaInvestidorComponent,
        ProjetosListaGestorAcoesComponent,
        ProjetosListaEmpreendedorComponent,
        ProjetosListaInvestidorAcoesComponent,
        ProjetosListaEmpreendedorAcoesComponent,
        ProjetosVisualizacaoModalArquivosComponent
    ],
    providers: [
        ProjetosService
    ]
})
export class ProjetosModule {
}
