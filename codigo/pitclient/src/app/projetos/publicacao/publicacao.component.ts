import {Component, Input, OnInit, ViewEncapsulation} from "@angular/core";
import {ProjetoApp} from "../../shared/models/app.models";
import {DominioService} from "../dominio.service";
import * as _ from "lodash";
import {DomSanitizer} from "@angular/platform-browser";
import {DateTime} from "luxon";
import {ProjetosApresentacaoService} from "../projetos-apresentacao.service";
import Projeto = ProjetoApp.Projeto;
import ProjetoInternacionalizacao = ProjetoApp.ProjetoInternacionalizacao;
import Idioma = ProjetoApp.Idioma;
import TipoArquivo = ProjetoApp.TipoArquivo;
import {TranslateService} from "@ngx-translate/core";

@Component({
    selector: 'app-projetos-publicacao',
    templateUrl: 'publicacao.component.html',
    styleUrls: ['./publicacao.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ProjetosPublicacaoComponent implements OnInit {

    @Input() projeto: Projeto;
    @Input() camposEquipamento: string[];

    internacionalizacao: ProjetoInternacionalizacao;

    imagem: number;

    imagemURL: any;

    anexoList: any[] = [];

    optionsGmap: any;
    overlaysGmap: any[];

    constructor(
        protected dominioService: DominioService,
        protected sanitizer: DomSanitizer,
        protected translateService: TranslateService
    ) {
    }

    ngOnInit() {
        if (this.projeto) {
            this.setIdiomaPadrao();
            this.setConfiguracaoMapa();
        }
    }

    setIdiomaPadrao() {
        let internacionalizacaoData = _.first(this.projeto.internacionalizacao.filter(internacionalizacao => {
            return internacionalizacao.idioma.sigla === ProjetosApresentacaoService.INTERNACIONALIZACAO_IDIOMA[this.translateService.getDefaultLang()];
        }));

        if(!internacionalizacaoData) {
            internacionalizacaoData = _.first(this.projeto.internacionalizacao.filter(internacionalizacao => {
                return internacionalizacao.internacionalizacaoPadrao;
            }));
        }

        this.internacionalizacao = internacionalizacaoData;

        this.setImagemId();
        this.setAnexos();
    }

    setConfiguracaoMapa() {
        let localizacao = _.first(_.get(this.projeto, 'localizacoes', []));

        let latitude = _.get(localizacao, 'latitude');
        let longitude = _.get(localizacao, 'longitude');

        this.optionsGmap = {
            center: {
                lat: parseFloat(latitude),
                lng: parseFloat(longitude)
            },
            zoom: 16
        }

        this.overlaysGmap = [
            new google.maps.Marker({
                position: {
                    lat: parseFloat(latitude),
                    lng: parseFloat(longitude)
                }
            })
        ];
    }

    formataUrl(url) {
        if(!url) {
            return '';
        }

        if (!url.includes('http') && !url.includes('http')) {
            return `http://${url}`;
        }

        return url;
    }

    setImagemId() {
        if (this.internacionalizacao.projetoInternacionalizacaoArquivos.length) {
            let imagemObjeto: any = _.first(this.internacionalizacao.projetoInternacionalizacaoArquivos.filter(arquivo => {
                return arquivo.tipoArquivo === TipoArquivo.IMAGEM
            }));

            if (imagemObjeto) {
                if (imagemObjeto.arquivoId instanceof File) {
                    let objeto: any = imagemObjeto.arquivoId;
                    objeto = this.sanitizer.bypassSecurityTrustUrl(objeto.objectURL.changingThisBreaksApplicationSecurity)

                    this.imagemURL = objeto.changingThisBreaksApplicationSecurity;
                } else {
                    this.imagem = imagemObjeto.arquivoId;
                    this.imagemURL = this.dominioService.urlVisualizarImagem(this.imagem);
                }
            }
        }
    }

    setAnexos() {
        if (this.internacionalizacao.projetoInternacionalizacaoArquivos.length) {
            this.anexoList = this.internacionalizacao.projetoInternacionalizacaoArquivos.filter(arquivo => {
                return arquivo.tipoArquivo === TipoArquivo.ANEXO
            });
        }
    }

    formataCNPJ(cnpj) {
        return cnpj ? cnpj.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g, "\$1.\$2.\$3\/\$4\-\$5") : '';
    }

    converterData(data) {

        if (data instanceof Date) {
            return DateTime.fromJSDate(data, {zone: 'utc'}).toFormat("dd/LL/yyyy")
        } else if (data) {
            return DateTime.fromISO(data, {zone: 'utc'}).toFormat("dd/LL/yyyy")
        }

        return '-';
    }

    pegaNomeCampoEquipamento(campo) {
        return this.translateService.instant(`PROJETOS.FORM.CAMPOS.${_.get(ProjetosApresentacaoService.NOME_CAMPO_TRADUZIDO, campo, campo)}`);
    }

    baixarArquivo(arquivo) {
        if (arquivo.blob && arquivo.blob instanceof File) {

            var blobUrl = URL.createObjectURL(arquivo.blob);
            var a = document.createElement("a");
            a.href = blobUrl;
            a.download = arquivo.blob.name;
            document.body.appendChild(a);
            a.click();
            URL.revokeObjectURL(blobUrl);

            return '';
        }

        return this.dominioService.urlBaixarArquivo(arquivo.arquivoId);
    }

    pegaNomeArquivo(arquivo) {

        if (arquivo instanceof File) {
            return arquivo.name;
        }

        return arquivo.arquivo;
    }

}
