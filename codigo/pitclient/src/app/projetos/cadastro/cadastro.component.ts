import {Component, OnInit, ViewEncapsulation} from "@angular/core";
import {BlockUI, NgBlockUI} from "ng-block-ui";
import {FormGroup} from "@angular/forms";
import {FormUtilsService} from "../../shared/services/form-utils.service";
import {ProjetosService} from "../projetos.service";
import {PageNotificationService} from "@nuvem/primeng-components";
import {TranslateService} from "@ngx-translate/core";
import {Router} from "@angular/router";
import {ProjetoApp} from "../../shared/models/app.models";
import {UsuarioService} from "../../gerenciar-usuario/usuario.service";
import Status = ProjetoApp.Status;
import TipoArquivo = ProjetoApp.TipoArquivo;
import {Constants} from "../../shared/utils/constants";
import {MessageDialogService} from "../../shared/message-dialog.service";
import {ProjetosApresentacaoService} from "../projetos-apresentacao.service";
import Projeto = ProjetoApp.Projeto;
import * as _ from "lodash";
import {finalize} from "rxjs/operators";

@Component({
    selector: 'app-projetos-cadastro',
    templateUrl: 'cadastro.component.html',
    styleUrls: ['cadastro.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ProjetosCadastroComponent implements OnInit {

    @BlockUI() blockUI: NgBlockUI;

    form: FormGroup;

    internacionalizacaoList = [];

    visualizarPublicacao: boolean = false;

    projetoVisualizar: Projeto;

    constructor(
        public formUtilsService: FormUtilsService,
        protected pageNotificationService: PageNotificationService,
        protected translateService: TranslateService,
        protected projetosService: ProjetosService,
        protected projetosApresentacaoService: ProjetosApresentacaoService,
        protected usuarioService: UsuarioService,
        protected router: Router,
        protected messageDialogService: MessageDialogService
    ) {

    }

    ngOnInit() {

    }

    criacaoForm(form: FormGroup) {
        this.form = form;
    }

    salvar() {
        if (this.formUtilsService.validate(this.form)) {
            this.messageDialogService.addModerarBoxSemJustificativa(
                'question',
                this.translateService.instant('PROJETOS.FORM.CAMPOS.SALVAR_RASCUNHO'),
                '<p style="color: red"><b>' + this.translateService.instant('PROJETO.ATENCAO') + '</b></p>' +
                this.translateService.instant('PROJETO.SALVAR_RASCUNHO'),
                this.translateService.instant('CONFIRMAR'),
                this.translateService.instant('CANCELAR'),
                () => {
                    this.form.get('status').setValue(Status.RASCUNHO);

                    this.salvarAction();
                }
            )
        }
    }

    salvarEnviar() {
        if (this.formUtilsService.validate(this.form)) {
            this.messageDialogService.addOptionalBox(
                () => {

                    this.form.get('status').setValue(Status.SOB_ANALISE);

                    this.salvarAction();
                },
                'info',
                this.translateService.instant('DESEJA_ENVIAR'),
                this.translateService.instant('PROJETO.CONFIRMAR_CADASTRO')
            )
        }
    }

    cancelar() {
        if (this.form.dirty) {
            this.messageDialogService.addOptionalCancelar(() => this.router.navigate(['/projetos']));
        } else {
            this.router.navigate(['/projetos']);
        }
    }

    private salvarAction() {
        let formData = this.form.value;

        formData.internacionalizacaoList.forEach(internacionalizacao => {

            let texto: string = internacionalizacao.descricao;

            texto = texto.replace(/<br\s*\/?>/gi, '<br\/>');

            internacionalizacao.descricao = texto;

        });

        this.blockUI.start(Constants.CARREGANDO);
        this.usuarioService.buscarUsuarioLogado().subscribe(response => {
            this.form.get('usuarioId').setValue(response.id);
            formData.usuarioId = response.id;
            this.projetosService.salvar(formData).pipe(finalize(() => this.blockUI.stop())).subscribe(
                response => {
                    this.internacionalizacaoList = response.internacionalizacao;
                    this.validaEnvioArquivos();
                }
            )
        });
    }

    validaTodosCamposPreenchidos(formValue: any, validarTermo = true): boolean {

        return this.projetosApresentacaoService.validarCampos(formValue, validarTermo);

    }

    validaEnvioArquivos() {
        if (this.form.value.internacionalizacaoList.length) {
            this.form.value.internacionalizacaoList.forEach(internacionalizacao => {

                let internacionalizacaoIdioma = this.internacionalizacaoList.find(internacionalizacaoSalva => {
                    return internacionalizacaoSalva.idioma.id === internacionalizacao.idiomaId
                });

                if (internacionalizacao.imagem) {
                    let formData = this.formUtilsService.convertToFormData({file: internacionalizacao.imagem});
                    this.projetosService.uploadArquivo(
                        internacionalizacaoIdioma.id,
                        formData,
                        TipoArquivo.IMAGEM
                    ).subscribe();
                }

                if (internacionalizacao.anexoList.length) {
                    let formData = this.formUtilsService.convertToFormData({file: internacionalizacao.anexoList});
                    this.projetosService.uploadArquivo(
                        internacionalizacaoIdioma.id,
                        formData,
                        TipoArquivo.ANEXO
                    ).subscribe();
                }
            });

            this.blockUI.stop();
            this.pageNotificationService.addSuccessMessage(this.translateService.instant('CADASTRO_FINALIZADO'));
            this.router.navigate(['/projetos']);
        }
    }

    validaVisualizarPublicacao() {
        let value = _.cloneDeep(this.form.value);

        this.projetoVisualizar = this.projetosApresentacaoService.converterFormPublicacao(value);

        this.visualizarPublicacao = true;
    }

}
