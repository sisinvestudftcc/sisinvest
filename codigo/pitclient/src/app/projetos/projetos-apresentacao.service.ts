import {Injectable} from "@angular/core";
import {ProjetoApp} from "../shared/models/app.models";
import GrupoEquipamento = ProjetoApp.GrupoEquipamento;
import Equipamento = ProjetoApp.Equipamento;
import * as _ from "lodash";
import Projeto = ProjetoApp.Projeto;
import TipoArquivo = ProjetoApp.TipoArquivo;
import {DominioService} from "./dominio.service";
import {DateTime} from "luxon";
import {TranslateService} from "@ngx-translate/core";
import Idioma = ProjetoApp.Idioma;


@Injectable({providedIn: "root"})
export class ProjetosApresentacaoService {

    public static INTERNACIONALIZACAO_IDIOMA = {
        'pt': Idioma.PORTUGUES,
        'en': Idioma.INGLES,
        'es': Idioma.ESPANHOL
    }

    public static NOME_CAMPO_TRADUZIDO = {
        'unidadesHabitacionais': 'UNIDADES_HABITACIONAIS',
        'unidadesImobiliarias': 'UNIDADES_IMOBILIARIAS',
        'capacidadesMesas': 'CAPACIDADES_MESAS',
        'tamanhoProjeto': 'TAMANHO_PROJETO',
        'numeroPavilhoes': 'NUMERO_PAVILHOES',
        'numeroSalasReuniao': 'NUMERO_SALAS_REUNIAO',
        'numeroAnualVisitantes': 'NUMERO_ANUAL_VISITANTES',
        'rendaAnual': 'RENDA_ANUAL',
        'gastoMedioVisitantes': 'GASTO_MEDIO_VISITANTES',
        'capacidadePublico': 'CAPACIDADE_PUBLICO',
        'capacidadeAtracao': 'CAPACIDADE_ATRACAO',
        'outroEquipamento': 'OUTRO_EQUIPAMENTO',
    };

    static POSSUI_SUB_CAMPOS = [
        GrupoEquipamento.MEIO_HOSPEDAGEM,
        GrupoEquipamento.EQUIPAMENTOS_LAZER
    ];

    static POSSUI_CAMPOS_TEXTO = [
        GrupoEquipamento.BAR_ENTRETENIMENTO,
        GrupoEquipamento.BAR_SEM_ENTRETENIMENTO,
        GrupoEquipamento.RESTAURANTE_ENTRETENIMENTO,
        GrupoEquipamento.RESTAURANTE_SEM_ENTRETENIMENTO,
        GrupoEquipamento.CENTRO_CONVENCOES,
        GrupoEquipamento.COMPLEXO_TURISTICO,
        GrupoEquipamento.MUSEU,
        GrupoEquipamento.TEATRO,
        GrupoEquipamento.OUTROS
    ];

    static MEIO_HOSPEDAGEM_EQUIPAMENTOS = [
        Equipamento.HOTEL_BOUTIQUE,
        Equipamento.SPA,
        Equipamento.RESORT,
        Equipamento.RESORT_INTEGRADO,
        Equipamento.HOTEL_LUXO,
        Equipamento.HOTEL_NEGOCIOS,
        Equipamento.HOTEL_ECONOMICO,
        Equipamento.HOTEL_FAZENDA,
        Equipamento.HOTEL_CAMA,
        Equipamento.HOSTEL,
        Equipamento.POUSADA,
        Equipamento.CAMPING,
        Equipamento.GLAMPING,
        Equipamento.EMPREENDIMENTO_TURISTICO
    ];

    static NUMERO_VISITANTES = [
        Equipamento.PARQUE_TEMATICO,
        Equipamento.PARQUE_AQUATICO,
        Equipamento.CLUBE_GOLF,
        Equipamento.PLANETARIO,
        Equipamento.AQUARIO,
        Equipamento.KARTODROMO,
        Equipamento.POLOS_GASTRONOMICOS,
        Equipamento.SHOPPING_CENTER
    ];

    static CAPACIDADE_PUBLICO = [
        Equipamento.CASA_SHOW,
        Equipamento.CONCHA_ACUSTICA,
        Equipamento.ESTADIO_DESPORTIVO,
        Equipamento.CASA_NOTURNA
    ];

    static CAPACIDADE_ATRACAO = [
        Equipamento.MARINA,
        Equipamento.INSTALACAO_PORTUARIA
    ];

    nacionalidadeEmpresa: any[];

    naturezaInvestimento: any[];

    valorProjeto: any[];

    modeloContrato: any[];

    empregosProjeto: any[];

    grupoEquipamento: any[];

    equipamento: any[];

    segmentos: any[];

    constructor(
        protected dominioService: DominioService,
        protected translateService: TranslateService
    ) {
        this.inicializaDominios();
    }

    inicializaDominios() {
        this.dominioService.segmentoFiltrar({}, {page: 0, size: 9999}).subscribe(response => {
            this.segmentos = response.content;
        });
        this.dominioService.paisFiltrar({}, {page: 0, size: 9999}).subscribe(response => {
            this.nacionalidadeEmpresa = response.content;
        });
        this.dominioService.naturezaInvestimentoFiltrar({}, {page: 0, size: 9999}).subscribe(response => {
            this.naturezaInvestimento = response.content;
        });
        this.dominioService.valorProjetoFiltrar({}, {page: 0, size: 9999}).subscribe(response => {
            this.valorProjeto = response.content;
        });
        this.dominioService.modeloContratoFiltrar({}, {page: 0, size: 9999}).subscribe(response => {
            this.modeloContrato = response.content;
        });
        this.dominioService.empregoProjetoFiltrar({}, {page: 0, size: 9999}).subscribe(response => {
            this.empregosProjeto = response.content;
        });
        this.dominioService.grupoEquipamentoFiltrar({}, {page: 0, size: 9999}).subscribe(response => {
            this.grupoEquipamento = response.content;
        });
        this.dominioService.equipamentoFiltrar({}, {page: 0, size: 9999}).subscribe(response => {
            this.equipamento = response.content;
        });
    }

    public possuiEquipamento(grupo) {
        return ProjetosApresentacaoService.POSSUI_SUB_CAMPOS.includes(grupo);
    }

    public possuiCampoTexto(grupo) {
        return ProjetosApresentacaoService.POSSUI_CAMPOS_TEXTO.includes(grupo);
    }

    public pegaListaCampos(grupo, equipamento) {
        switch (true) {
            case ProjetosApresentacaoService.POSSUI_SUB_CAMPOS.includes(grupo): {
                return this.pegaListaCamposEquipamento(equipamento);
            }
            case ProjetosApresentacaoService.POSSUI_CAMPOS_TEXTO.includes(grupo): {
                return this.pegaListaCamposGrupoEquipamento(grupo);
            }
            default: {
                return [];
            }
        }

    }

    pegaListaCamposGrupoEquipamento(grupo) {
        switch (true) {
            case [
                GrupoEquipamento.BAR_ENTRETENIMENTO,
                GrupoEquipamento.BAR_SEM_ENTRETENIMENTO,
                GrupoEquipamento.RESTAURANTE_ENTRETENIMENTO,
                GrupoEquipamento.RESTAURANTE_SEM_ENTRETENIMENTO
            ].includes(grupo) : {
                return [
                    'capacidadesMesas',
                    'tamanhoProjeto'
                ]
            }

            case [
                GrupoEquipamento.CENTRO_CONVENCOES,
                GrupoEquipamento.COMPLEXO_TURISTICO,
                GrupoEquipamento.MUSEU,
                GrupoEquipamento.TEATRO
            ].includes(grupo) : {
                return [
                    'tamanhoProjeto',
                    'numeroPavilhoes',
                    'numeroSalasReuniao'
                ]
            }

            case [GrupoEquipamento.OUTROS].includes(grupo) : {
                return [
                    'outroEquipamento'
                ]
            }

            default: {
                return [];
            }
        }
    }

    pegaListaCamposEquipamento(equipamento) {
        switch (true) {
            case (ProjetosApresentacaoService.MEIO_HOSPEDAGEM_EQUIPAMENTOS.includes(equipamento)): {
                return [
                    'unidadesHabitacionais',
                    'unidadesImobiliarias'
                ];
            }
            case (ProjetosApresentacaoService.NUMERO_VISITANTES.includes(equipamento)): {
                return [
                    'numeroAnualVisitantes',
                    'rendaAnual',
                    'gastoMedioVisitantes'
                ];
            }
            case (ProjetosApresentacaoService.CAPACIDADE_PUBLICO.includes(equipamento)): {
                return [
                    'capacidadePublico',
                    'rendaAnual',
                    'gastoMedioVisitantes'
                ];
            }
            case (ProjetosApresentacaoService.CAPACIDADE_ATRACAO.includes(equipamento)): {
                return [
                    'capacidadeAtracao',
                    'rendaAnual',
                    'gastoMedioVisitantes'
                ];
            }

            default: {
                return [];
            }
        }
    }

    public validarCampos(formValue: any, validarTermo: boolean = true) {
        let todosPreenchidos = true;

        if (!formValue) {
            return false;
        }

        let camposObrigatorios = [
            'localizacao.latitudeLongitude',
            'localizacao.latitude',
            'localizacao.longitude',
            'localizacao.cidade',
            'localizacao.estado',
            'empresa.razaoSocial',
            'empresa.cnpj',
            'empresa.pais',
            'empresa.proprietario',
            'empresa.responsavel',
            'empresa.site',
            'empresa.email',
            'detalhe.dataInicio',
            'detalhe.dataConclusao',
            'detalhe.naturezaInvestimento',
            'detalhe.modeloContrato',
            'detalhe.valorProjeto',
            'detalhe.empregosProjeto',
            'detalhe.areaConstruida',
            'detalhe.areaTotal',
            'detalhe.grupoOperador'
        ];

        let camposObrigatoriosInternacionalizacao = [
            'nome',
            'descricao',
            'imagem'
        ]

        if (validarTermo) {
            camposObrigatorios.push('declaracaoTermo');
        }

        camposObrigatorios.forEach(campo => {
            if (!_.get(formValue, campo, null)) {
                todosPreenchidos = false;
                return;
            }
        });

        if(!_.get(formValue, 'empresa.telefoneList', []).length) {
            todosPreenchidos = false;
            return;
        }

        formValue.internacionalizacaoList.forEach(internacionalizacao => {
            camposObrigatoriosInternacionalizacao.forEach(campo => {
                if (!_.get(internacionalizacao, campo, null)) {
                    todosPreenchidos = false;
                    return;
                }
            });
        });

        return todosPreenchidos;
    }

    converterFormPublicacao(form) {
        let projetoVisualizar = new Projeto()

        projetoVisualizar.internacionalizacao = form.internacionalizacaoList.map(internacionalizacao => {
            internacionalizacao.idioma = {
                id: internacionalizacao.idiomaId,
                sigla: internacionalizacao.idiomaSigla,
            }

            let arquivo = internacionalizacao.imagem instanceof File
                ? internacionalizacao.imagem
                : internacionalizacao.imagemVisualizacao;

            let anexoList = internacionalizacao.anexoList.map(item => {
                if (item instanceof File) {
                    return {
                        blob: item,
                        arquivo: item.name,
                        tipoArquivo: TipoArquivo.ANEXO
                    }
                }
                return item;
            });

            internacionalizacao.projetoInternacionalizacaoArquivos = [
                {
                    tipoArquivo: TipoArquivo.IMAGEM,
                    arquivoId: arquivo
                },
                ...anexoList
            ];

            return internacionalizacao;
        });

        let empresa = form.empresa;

        empresa.url = empresa.site;

        empresa.pais = _.first(this.nacionalidadeEmpresa.filter(item => {
            return item.id == empresa.pais;
        }));

        let detalhe = form.detalhe;

        detalhe.naturezaInvestimento = _.first(this.naturezaInvestimento.filter(item => {
            return item.id == detalhe.naturezaInvestimento
        }));

        detalhe.valorProjeto = _.first(this.valorProjeto.filter(item => {
            return item.id == detalhe.valorProjeto
        }));

        detalhe.modeloContrato = _.first(this.modeloContrato.filter(item => {
            return item.id == detalhe.modeloContrato
        }));

        detalhe.empregosProjeto = _.first(this.empregosProjeto.filter(item => {
            return item.id == detalhe.empregosProjeto
        }));

        let segmentos = form.segmentoList;

        segmentos = segmentos.map(segmento => {
            let segmentoObjeto = _.first(this.segmentos.filter(segmentoObjeto => {
                return segmentoObjeto.id == segmento
            }));

            return {
                segmento: segmentoObjeto
            }
        });

        let equipamento = form.equipamento;

        equipamento.grupoEquipamento = _.first(this.grupoEquipamento.filter(item => {
            return item.id == equipamento.grupoEquipamento
        }));

        equipamento.equipamento = _.first(this.equipamento.filter(item => {
            return item.id == equipamento.equipamento
        }));

        projetoVisualizar.empresas = [empresa];
        projetoVisualizar.detalhes = [detalhe];
        projetoVisualizar.segmentos = segmentos;
        projetoVisualizar.equipamentos = [equipamento];
        projetoVisualizar.localizacoes = [form.localizacao];

        return projetoVisualizar;
    }

    public converterDataAtualizacao(data) {
        if (parseInt( DateTime.fromISO(data, {zone: 'utc'}).diffNow('days').days.toFixed(0).replace('-', '')) < 1) {
            let horas = parseInt(DateTime.fromISO(data, {zone: 'utc'}).diffNow('hours').hours.toFixed(0).replace('-', ''))

            if (horas < 1) {
                let minutos = parseInt(DateTime.fromISO(data, {zone: 'utc'}).diffNow('minutes').minutes.toFixed(0).replace('-', ''))

                if (minutos < 1) {
                    return this.translateService.instant('PROJETO.GRID.ATUALIZADO_AGORA');
                }

                return this.translateService.instant('PROJETO.GRID.ATUALIZADO_MINUTOS', {minutos: minutos});
            }

            if (horas <= 24) {
                return this.translateService.instant('PROJETO.GRID.ATUALIZADO_HORAS', {horas: horas});
            }
        }

        let dias = DateTime.fromISO(data, {zone: 'utc'}).diffNow('days').days.toFixed(0).replace('-', '');


        return this.translateService.instant('PROJETO.GRID.ATUALIZADO_DIAS', {dias: dias});
    }

}
