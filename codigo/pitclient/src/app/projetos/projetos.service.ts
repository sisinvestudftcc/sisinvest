import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {Page} from "../shared/page";
import {ProjetoApp} from "../shared/models/app.models";
import Projeto = ProjetoApp.Projeto;
import ProjetoLinhaTemporal = ProjetoApp.ProjetoLinhaTemporal;
import {FormUtilsService} from "../shared/services/form-utils.service";

@Injectable({providedIn: "root"})
export class ProjetosService {

    public MAX_RESULTS = 2147483647;

    baseUrl = `${environment.pitprojetoUrl}/projeto`;

    constructor(
        private formUtilsService: FormUtilsService,
        private http: HttpClient
    ) {

    }

    filtrar(filtro: any = {}, paginacao: any = {page: 0, size: 30}): Observable<any> {

        return this.http.post<Page<Projeto[]>>(`${this.baseUrl}/filtrar`, filtro, {params: paginacao});

    }

    gerarRelatorio(): void {

        const frame = <any> document.createElement('iframe');
        frame.style = 'display: none';
        frame.src = `${this.baseUrl}/gerar-relatorio?size=${this.MAX_RESULTS}`;
        document.body.appendChild(frame);

    }

    buscarPorId(id: number): Observable<any> {

        return this.http.get<Projeto>(`${this.baseUrl}/projeto/${id}`);

    }

    buscarLinhaTemporalPorId(id: number, paginacao: any = {page: 0, size: 30}): Observable<any> {

        return this.http.post<Page<ProjetoLinhaTemporal[]>>(`${this.baseUrl}/projeto/${id}/linha-temporal`, {}, {params: paginacao});

    }

    salvar(formulario: any): Observable<any> {

        return this.http.post<Page<Projeto[]>>(`${this.baseUrl}/salvar`, formulario);

    }

    aprovar(form: {id: number, justificativa: string, usuario: number}): Observable<any> {

        return this.http.post<number>(`${this.baseUrl}/aprovar/${form.id}`, form);

    }

    destacar(form: {id: number, usuario: number}): Observable<any> {

        return this.http.post<number>(`${this.baseUrl}/destacar/${form.id}`, form);

    }

    removerDestaque(form: {id: number, justificativa: string, usuario: number}): Observable<any> {

        return this.http.post<number>(`${this.baseUrl}/remover-destaque/${form.id}`, form);

    }

    moverDestaqueDireita(form: {id: number, usuario: number}): Observable<any> {

        return this.http.post<number>(`${this.baseUrl}/mover-destaque-direita/${form.id}`, form);

    }

    moverDestaqueEsquerda(form: {id: number, usuario: number}): Observable<any> {

        return this.http.post<number>(`${this.baseUrl}/mover-destaque-esquerda/${form.id}`, form);

    }

    reprovar(form: {id: number, justificativa: string, usuario: number}): Observable<any> {

        return this.http.post<number>(`${this.baseUrl}/reprovar/${form.id}`, form);

    }

    removerComentario(linhaTemporal: number): Observable<any> {

        return this.http.post<number>(`${this.baseUrl}/remover-comentario/${linhaTemporal}`, {});

    }

    favoritar(id: number, usuarioId: number): Observable<any> {

        return this.http.post<number>(`${this.baseUrl}/favoritar/${id}/${usuarioId}`, {});

    }

    desfavoritar(id: number, usuarioId: number): Observable<any> {

        return this.http.post<number>(`${this.baseUrl}/desfavoritar/${id}/${usuarioId}`, {});

    }

    restaurar(form: {id: number, justificativa: string, usuario: number}): Observable<any> {

        return this.http.post<number>(`${this.baseUrl}/restaurar/${form.id}`, form);

    }

    publicar(form: {id: number, usuario: number}): Observable<any> {

        return this.http.post<number>(`${this.baseUrl}/publicar/${form.id}`, form);

    }

    despublicar(form: {id: number, justificativa: string, usuario: number}): Observable<any> {

        return this.http.post<number>(`${this.baseUrl}/despublicar/${form.id}`, form);

    }

    comentar(form: {id: number, justificativa: string, usuario: number, anexoList: any[]}): Observable<any> {

        let formData = this.formUtilsService.convertToFormData(form);

        return this.http.post<number>(`${this.baseUrl}/comentar/${form.id}`, formData);

    }

    solicitarEsclarecimento(form: {id: number, justificativa: string, usuario: number, anexoList: any[]}): Observable<any> {

        let formData = this.formUtilsService.convertToFormData(form);

        return this.http.post<number>(`${this.baseUrl}/solicitar-esclarecimento/${form.id}`, formData);

    }

    responderEsclarecimento(form: {id: number, justificativa: string, usuario: number, anexoList: any[]}): Observable<any> {

        let formData = this.formUtilsService.convertToFormData(form);

        return this.http.post<number>(`${this.baseUrl}/responder-esclarecimento/${form.id}`, formData);

    }

    uploadArquivo(projetoInternacionalizacaoId: number, form: any, tipo: number) {
        return this.http.post<Page<Projeto[]>>(`${this.baseUrl}/upload-arquivos/${projetoInternacionalizacaoId}/${tipo}`, form);
    }

}
