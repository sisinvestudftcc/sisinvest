import {
    Component,
    EventEmitter,
    Input,
    OnInit,
    Output,
    QueryList,
    ViewChildren,
    ViewEncapsulation
} from "@angular/core";
import {BlockUI, NgBlockUI} from "ng-block-ui";
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ProjetoApp} from "../../shared/models/app.models";
import {DominioService} from "../dominio.service";
import {DomSanitizer} from "@angular/platform-browser";
import {FileUpload} from "primeng";
import {ProjetosApresentacaoService} from "../projetos-apresentacao.service";
import {AppValidators} from "../../shared/shared.module";
import Projeto = ProjetoApp.Projeto;
import Idioma = ProjetoApp.Idioma;
import ModeloContrato = ProjetoApp.ModeloContrato;
import * as _ from "lodash";
import TipoArquivo = ProjetoApp.TipoArquivo;
import {MessageDialogService} from "../../shared/message-dialog.service";
import {TranslateService} from "@ngx-translate/core";

@Component({
    selector: 'app-projetos-formulario',
    templateUrl: 'formulario.component.html',
    styleUrls: ['./formulario.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ProjetosFormularioComponent implements OnInit {

    @ViewChildren('fileUpload') fileUploadComponents: QueryList<FileUpload>;

    @ViewChildren('fileUploadAttachment') fileUploadAttachmentComponents: QueryList<FileUpload>;

    @Input() projeto: Projeto;

    @BlockUI() blockUI: NgBlockUI;

    @Output() criacaoForm: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();

    form: FormGroup;

    idiomas: Idioma[] = [];

    idiomaSiglaPadrao = Idioma.SIGLA_PADRAO;

    optionsGmap: any;

    overlaysGmap: any[];

    mostrarOutroModeloContrato = false;

    segmentoLista: any[] = [];

    exibirTutorialMapa = false;

    anexoMimeType = Projeto.FORMATO_ANEXO_MIME_TYPE.join(',');

    imagemMimeType = Projeto.FORMATO_IMAGEM_MIME_TYPE.join(',');

    constructor(
        protected sanitizer: DomSanitizer,
        protected formBuilder: FormBuilder,
        protected dominioService: DominioService,
        protected projetosApresentacaoService: ProjetosApresentacaoService,
        protected messageDialogService: MessageDialogService,
        public translateService: TranslateService
    ) {
        this.inicializarIdiomas();
    }

    ngOnInit() {

    }

    inicializarIdiomas() {
        this.dominioService.idiomaFiltrar({}, {page: 0, size: 9999}).subscribe(
            response => {
                this.idiomas = response.content;
                this.idiomaSiglaPadrao = ProjetosApresentacaoService.INTERNACIONALIZACAO_IDIOMA[this.translateService.getDefaultLang()];
                this.inicializarFormulario(this.projeto);
                this.inicializarSegmentos();
            }, error => {
                alert(error);
            }
        )
    }

    inicializarFormulario(projeto: Projeto) {

        let idiomasSelecionados = [];

        if (projeto) {

            projeto.internacionalizacao.forEach(internacionalizacao => {
                if (!internacionalizacao.internacionalizacaoPadrao) {
                    idiomasSelecionados.push(internacionalizacao.idioma.id);
                }
            });

        }

        let empresa = _.first(_.get(projeto, 'empresas', []));
        let detalhe = _.first(_.get(projeto, 'detalhes', []));
        let telefoneList = [];
        let segmentoList = [];

        if (_.get(projeto, 'segmentos', []).length) {
            _.get(projeto, 'segmentos', []).forEach(projetoSegmento => {
                segmentoList.push(projetoSegmento.segmento.id);
            })
        }

        if (empresa) {
            empresa.projetoEmpresaTelefoneList.forEach(projetoEmpresaTelefone => {
                telefoneList.push(projetoEmpresaTelefone.telefone);
            })
        }

        let latitudeLongitude = null;

        if (_.get(projeto, 'localizacoes', []).length) {
            let localizacao = _.first(_.get(projeto, 'localizacoes', []));

            if (localizacao.latitude && localizacao.longitude) {
                latitudeLongitude = `${localizacao.latitude}, ${localizacao.longitude}`;
            }
        }

        let dataInicio = null;
        let dataConclusao = null;

        if (_.get(detalhe, 'dataInicio')) {
            dataInicio = new Date(_.get(detalhe, 'dataInicio'));
        }

        if (_.get(detalhe, 'dataConclusao')) {
            dataConclusao = new Date(_.get(detalhe, 'dataConclusao'));
        }

        this.form = this.formBuilder.group({
            id: [_.get(projeto, 'id')],
            idiomasSelecionados: [idiomasSelecionados],
            status: [_.get(projeto, 'statusId')],
            usuarioId: [_.get(projeto, 'usuario')],
            localizacao: this.formBuilder.group({
                latitudeLongitude: [latitudeLongitude],
                latitude: [null],
                longitude: [null],
                cidade: [null],
                estado: [null]
            }),
            empresa: this.formBuilder.group({
                razaoSocial: [_.get(empresa, 'razaoSocial')],
                cnpj: [_.get(empresa, 'cnpj'), [AppValidators.cnpj]],
                pais: [_.get(empresa, 'pais.id')],
                proprietario: [_.get(empresa, 'proprietario')],
                responsavel: [_.get(empresa, 'responsavel')],
                site: [_.get(empresa, 'url'), [AppValidators.url]],
                email: [_.get(empresa, 'email'), [AppValidators.email]],
                telefone: [null],
                telefoneList: [telefoneList]
            }),
            detalhe: this.formBuilder.group({
                dataInicio: [dataInicio],
                dataConclusao: [dataConclusao],
                naturezaInvestimento: [_.get(detalhe, 'naturezaInvestimento.id')],
                modeloContrato: [_.get(detalhe, 'modeloContrato.id')],
                outroModeloContrato: [_.get(detalhe, 'outroModeloContrato')],
                valorProjeto: [_.get(detalhe, 'valorProjeto')],
                empregosProjeto: [_.get(detalhe, 'empregosProjeto')],
                areaConstruida: [_.get(detalhe, 'areaConstruida')],
                areaTotal: [_.get(detalhe, 'areaTotal')],
                grupoOperador: [_.get(detalhe, 'grupoOperador')]
            }),
            segmentoList: [segmentoList],
            internacionalizacaoList: this.formBuilder.array([]),
            declaracaoTermo: [null, [Validators.required]]
        });

        if (this.idiomas.length) {
            let idiomaPadrao = this.idiomas.find(idioma => idioma.sigla === this.idiomaSiglaPadrao);

            if (projeto) {
                const internacionalizacao = _.get(projeto, 'internacionalizacao', []).find(internacionalizacao => {
                    return internacionalizacao.internacionalizacaoPadrao === true;
                });

                this.idiomaSiglaPadrao = _.get(internacionalizacao, 'idioma.sigla', idiomaPadrao);

                idiomaPadrao = this.idiomas.find(idioma => idioma.sigla === _.get(internacionalizacao, 'idioma.sigla', idiomaPadrao));
            }

            this.adicionarIdiomaFormulario(idiomaPadrao, projeto, true);

            if (idiomasSelecionados.length) {
                idiomasSelecionados.forEach(idiomaId => {
                    let idiomaAdicionar = this.idiomas.find(idioma => idioma.id === idiomaId);

                    this.adicionarIdiomaFormulario(idiomaAdicionar, projeto);
                })
            }
        }

        if (latitudeLongitude) {
            this.validaLatitudeLongitude();
        }

        this.criacaoForm.emit(this.form);

    }

    validaIdiomaSelecionado(idioma: Idioma) {
        if (this.form.get('idiomasSelecionados').value.includes(idioma.id)) {
            this.adicionarIdiomaFormulario(idioma, this.projeto);
        } else {
            this.removerIdiomaFormulario(idioma);
        }
    }

    adicionarIdiomaFormulario(idioma: Idioma, projeto: Projeto = null, idiomaPadrao: boolean = false) {

        let internacionalizacao = {};

        if (projeto) {
            internacionalizacao = _.get(projeto, 'internacionalizacao', []).find(internacionalizacao => {
                return internacionalizacao.idioma.id == idioma.id;
            });
        }

        let imagem = null;
        let imagemVisualizacao = null;
        let anexoList = [];

        if (internacionalizacao) {
            _.get(internacionalizacao, 'projetoInternacionalizacaoArquivos', []).forEach(arquivo => {
                if (arquivo.tipoArquivo == TipoArquivo.ANEXO) {
                    anexoList.push(arquivo);
                } else if (arquivo.tipoArquivo == TipoArquivo.IMAGEM) {
                    imagem = arquivo.id;
                    imagemVisualizacao = arquivo.arquivoId;
                }
            });
        }

        (<FormArray>this.form.get('internacionalizacaoList')).push(
            this.formBuilder.group({
                idiomaId: [idioma.id],
                idiomaSigla: [idioma.sigla],
                nome: [_.get(internacionalizacao, 'nome'), [Validators.required]],
                descricao: [_.get(internacionalizacao, 'descricao'), [Validators.required]],
                imagem: [imagem],
                imagemVisualizacao: [imagemVisualizacao],
                anexoList: [anexoList],
                removerImagem: [false],
                internacionalizacaoPadrao: [idiomaPadrao],
                anexosRemovidos: [[]]
            })
        )

    }

    removerIdiomaFormulario(idioma: Idioma) {

        let indexRemover = null;

        (<FormArray>this.form.get('internacionalizacaoList')).controls.forEach((control, index) => {
            if (control.get('idiomaId').value == idioma.id) {
                indexRemover = index;
            }
        })

        if (indexRemover) {
            (<FormArray>this.form.get('internacionalizacaoList')).removeAt(indexRemover);
        }

    }

    inicializarSegmentos() {
        this.dominioService.segmentoFiltrar({}, {page: 0, size: 9999}).subscribe(
            response => this.segmentoLista = response.content,
            error => {
            }
        )
    }

    enviarArquivoImagem(event, formGroup: FormGroup) {

        if (event.files.length) {
            let arquivo = event.files[0];

            const extensao = arquivo.name.split('.').pop();

            if (!Projeto.FORMATO_IMAGEM.includes(extensao.toLowerCase())) {
                this.messageDialogService.addErrorMessage(
                    this.translateService.instant('PROJETO.ARQUIVO_INVALIDO'),
                    this.translateService.instant('PROJETO.FORMATO_IMAGEM')
                );

            } else {
                if (arquivo.size <= Projeto.TAMANHO_MAXIMO_IMAGEM) {
                    if (formGroup.get('imagem').value) {
                        formGroup.get('removerImagem').setValue(true);
                    }
                    formGroup.get('imagem').setValue(arquivo);
                } else {

                    this.messageDialogService.addErrorMessage(
                        this.translateService.instant('PROJETO.ARQUIVO_INVALIDO'),
                        this.translateService.instant('PROJETO.TAMANHO_IMAGEM')
                    )
                }
            }
        }

        this.fileUploadComponents.forEach(componente => {
            componente.clear();
        });

    }

    removerImagem(formGroup: FormGroup) {
        if (!(formGroup.get('imagem').value instanceof File)) {
            formGroup.get('removerImagem').setValue(true);
        }
        formGroup.get('imagem').setValue(null);
    }

    pegaUrlImagem(formGroup: FormGroup) {

        if (formGroup.get('imagem').value) {

            if (formGroup.get('imagem').value instanceof File) {
                return this.sanitizer.bypassSecurityTrustUrl(formGroup.get('imagem').value.objectURL.changingThisBreaksApplicationSecurity)
            } else {
                return this.dominioService.urlVisualizarImagem(formGroup.get('imagemVisualizacao').value);
            }

        }

    }

    validaLatitudeLongitude() {
        if (this.form.get('localizacao').get('latitudeLongitude').value) {

            this.exibirTutorialMapa = false;

            if (this.form.get('localizacao').get('latitudeLongitude').value.includes(', ')) {
                let latitudeLongitude = this.form.get('localizacao').get('latitudeLongitude').value.split(', ');

                let latitude = parseFloat(latitudeLongitude[0]);
                let longitude = parseFloat(latitudeLongitude[1]);

                this.form.get('localizacao').get('latitude').setValue(latitude);
                this.form.get('localizacao').get('longitude').setValue(longitude);

                this.validaMapa();
            }

        } else {
            this.optionsGmap = null;
            this.form.get('localizacao').get('latitude').setValue(null);
            this.form.get('localizacao').get('longitude').setValue(null);
            this.form.get('localizacao').get('estado').setValue(null);
            this.form.get('localizacao').get('cidade').setValue(null);
        }
    }

    validaMapa() {

        this.optionsGmap = null;

        const latitude = this.form.get('localizacao').get('latitude').value;
        const longitude = this.form.get('localizacao').get('longitude').value;

        if (latitude && longitude) {

            this.dominioService.buscaDadosMapa(latitude, longitude)
                .then(res => res.json())
                .then(resposta => {
                    if (resposta.results.length) {
                        const estado = resposta.results[0].address_components.find(componenteEndereco => {
                            return componenteEndereco.types[0] == 'administrative_area_level_1';
                        });
                        const cidade = resposta.results[0].address_components.find(componenteEndereco => {
                            return componenteEndereco.types[0] == 'administrative_area_level_2';
                        });

                        if (estado) {
                            this.form.get('localizacao').get('estado').setValue(estado.long_name);
                        }

                        if (cidade) {
                            this.form.get('localizacao').get('cidade').setValue(cidade.long_name);
                        }
                    }
                });

            this.optionsGmap = {
                center: {
                    lat: parseFloat(latitude),
                    lng: parseFloat(longitude)
                },
                zoom: 16
            };

            this.overlaysGmap = [
                new google.maps.Marker({
                    position: {
                        lat: parseFloat(latitude),
                        lng: parseFloat(longitude)
                    }
                })
            ];

        }
    }

    validaOutroModeloContrato(option: any) {
        this.mostrarOutroModeloContrato = false;

        if (option === ModeloContrato.OUTRO_MODELO_ID) {
            this.mostrarOutroModeloContrato = true;
        } else {
            this.form.get('detalhe').get('outroModeloContrato').setValue(null);
        }
    }

    enviarArquivoAnexo(event, formGroup) {

        if (event.files.length) {
            let arquivo = event.files[0];

            const extensao = arquivo.name.split('.').pop();

            if (!Projeto.FORMATO_ANEXO.includes(extensao.toLowerCase())) {
                this.messageDialogService.addErrorMessage(
                    this.translateService.instant('PROJETO.ARQUIVO_INVALIDO'),
                    this.translateService.instant('PROJETO.FORMATO_ANEXO')
                );

            } else {
                if (arquivo.size <= Projeto.TAMANHO_MAXIMO_ANEXO) {
                    let arquivos = formGroup.get('anexoList').value;

                    arquivos.push(arquivo);

                    formGroup.get('anexoList').setValue(arquivos);
                } else {
                    this.messageDialogService.addErrorMessage(
                        this.translateService.instant('PROJETO.ARQUIVO_INVALIDO'),
                        this.translateService.instant('PROJETO.TAMANHO_ANEXO')
                    )
                }
            }
        }

        this.fileUploadAttachmentComponents.forEach(componente => {
            componente.clear();
        });

    }

    pegaNomeArquivo(anexo) {

        if (anexo instanceof File) {
            return anexo.name;
        } else {
            return anexo.arquivo;
        }

    }

    removerAnexo(index, formGroup) {
        let arquivos = formGroup.get('anexoList').value;

        if (!(formGroup.get('anexoList').value[index] instanceof File)) {
            let anexosRemovidos = formGroup.get('anexosRemovidos').value;

            anexosRemovidos.push(formGroup.get('anexoList').value[index].id);

            formGroup.get('anexosRemovidos').setValue(anexosRemovidos);
        }

        arquivos.splice(index, 1);

        formGroup.get('anexoList').setValue(arquivos);
    }

    pegaControlsInternacionalizacao() {
        return (<FormArray>this.form.get('internacionalizacaoList')).controls;
    }

}
