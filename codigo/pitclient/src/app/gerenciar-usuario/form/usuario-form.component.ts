import {Component, OnInit} from '@angular/core';
import {Perfil} from "../../models/perfil";
import {ActivatedRoute, Router} from "@angular/router";
import {AcaoEnum} from "../../shared/enumerations/acao.enum";
import {UsuarioService} from "../usuario.service";
import {Usuario} from "../../models/usuario";
import {Constants} from "../../shared/utils/constants";
import {finalize} from "rxjs/operators";
import {BlockUI, NgBlockUI} from "ng-block-ui";
import {PerfilEnum} from "../../shared/enumerations/perfil.enum";

@Component({
    selector: 'app-usuario-form',
    templateUrl: 'usuario-form.component.html'
})
export class UsuarioFormComponent implements OnInit {
    acao: string;
    acoes = AcaoEnum;
    @BlockUI() blockUI: NgBlockUI;
    perfil = new Perfil();
    usuario = new Usuario();

    constructor(private router: Router,
                private route: ActivatedRoute,
                private usuarioService: UsuarioService,) {
    }

    ngOnInit(): void {
        this.route.params.subscribe(params => {
            if (params['acao']) {
                this.verificarAcao(params['acao']);
                this.route.params.subscribe(params => this.acao = this.acoes.breadcrumbItem(params['acao']));
                this.buscarUsuario(params['id']);
                return;
            }
            this.verificarPerfil(params['perfil']);
            this.acao = this.acoes.breadcrumbItem('cadastrar');
            this.perfil.nome = params['perfil'].toUpperCase();
            this.usuario.perfilEnum = this.perfil.nome;
        });
    }

    buscarUsuario(id) {
        this.blockUI.start(Constants.CARREGANDO);
        this.usuarioService.buscarPorId(id).pipe(finalize(() => this.blockUI.stop()))
            .subscribe(res => {
                this.usuario = res;
                this.perfil.nome = this.usuario.perfilEnum.toString();
            }, () => this.router.navigate(['']));
    }

    verificarAcao(acao) {
        const listaAcao = [AcaoEnum.CADASTRAR, AcaoEnum.VISUALIZAR, AcaoEnum.ALTERAR];
        if (!listaAcao.includes(acao)) {
            this.router.navigate(['']);
        }
    }

    verificarPerfil(perfil) {
        const listaPerfil = [PerfilEnum.GESTOR, PerfilEnum.EMPREENDEDOR, PerfilEnum.INVESTIDOR];
        if (!listaPerfil.includes(perfil.toUpperCase())) {
            this.router.navigate(['']);
        }
    }
}
