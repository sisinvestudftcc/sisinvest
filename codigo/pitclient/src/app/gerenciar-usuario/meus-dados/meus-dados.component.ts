import {Component, OnInit} from '@angular/core';
import {UsuarioService} from "../usuario.service";
import {BlockUI, NgBlockUI} from "ng-block-ui";
import {Usuario} from "../../models/usuario";
import {Router} from "@angular/router";
import {BreadcrumbService} from "@nuvem/primeng-components";
import {Constants} from "../../shared/utils/constants";
import {finalize} from "rxjs/operators";
import {Perfil} from "../../models/perfil";
import {TranslateService} from "@ngx-translate/core";

@Component({
    selector: 'app-meus-dados',
    templateUrl: 'meus-dados.component.html'
})

export class MeusDadosComponent implements OnInit {
    @BlockUI() blockUI: NgBlockUI;
    perfil = new Perfil();
    usuario = new Usuario();

    constructor(private router: Router,
                private usuarioService: UsuarioService,
                private breadcrumbService: BreadcrumbService,
                private translateService: TranslateService) {
    }

    ngOnInit() {
        this.breadcrumbService.setItems([{label: this.translateService.instant('MEUS_DADOS')}])
        this.buscarUsuario();
    }

    buscarUsuario() {
        this.blockUI.start(Constants.CARREGANDO);
        this.usuarioService.buscarUsuarioLogado().pipe(finalize(() => this.blockUI.stop()))
            .subscribe(res => {
                this.usuario = res;
                this.perfil.nome = this.usuario.perfilEnum.toString();
            });
    }
}
