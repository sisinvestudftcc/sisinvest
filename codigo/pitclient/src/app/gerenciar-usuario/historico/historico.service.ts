import {Injectable} from "@angular/core";
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {HistoricoFiltro} from "../../models/historico-filtro";
import {Historico} from "../../models/historico";


@Injectable({providedIn: 'root'})
export class HistoricoService {
    private baseUrl = `${environment.pitsegurancaUrl}/auditoria`;

    constructor(private http: HttpClient) {
    }

    listarPorEntidade(filtro: HistoricoFiltro) {
        return this.http.post<Historico[]>(`${this.baseUrl}/listar-por-entidade`, filtro);
    }
}
