import {Component, OnInit} from '@angular/core';
import {TipoAcaoHistoricoEnum} from "../../shared/enumerations/tipo-acao-historico.enum";
import {BreadcrumbService} from "@nuvem/primeng-components";
import {HistoricoService} from "./historico.service";
import {HistoricoFiltro} from "../../models/historico-filtro";
import {finalize} from "rxjs/operators";
import {Historico} from "../../models/historico";
import {CamposHistoricoPipe} from "../../shared/pipes/campos-historico.pipe";
import {ValorHistoricoPipe} from "../../shared/pipes/valor-historico.pipe";
import {AcaoEnum} from "../../shared/enumerations/acao.enum";
import {Router} from "@angular/router";
import {HistoricoEnum} from "../../shared/enumerations/historico.enum";
import {Usuario} from "../../models/usuario";
import {UsuarioService} from "../usuario.service";
import {BlockUI, NgBlockUI} from "ng-block-ui";
import {Constants} from "../../shared/utils/constants";
import {PerfilEnum} from "../../shared/enumerations/perfil.enum";

@Component({
    selector: 'app-historico',
    templateUrl: 'historico.component.html'
})
export class HistoricoComponent implements OnInit {
    acao: string;
    acaoVisualizar = AcaoEnum.VISUALIZAR;
    @BlockUI() blockUI: NgBlockUI;
    entidade: string;
    filtro = new HistoricoFiltro;
    historicoEnum = HistoricoEnum;
    historicos: Historico[] = [];
    tipoAcoesHistorico = TipoAcaoHistoricoEnum.items;
    usuario = new Usuario();
    cols = [
        {field: 'campo', header: 'Campo', pipe: new CamposHistoricoPipe()},
        {field: 'valorAnterior', header: 'Antes', pipe: new ValorHistoricoPipe()},
        {field: 'valorAtual', header: 'Depois', pipe: new ValorHistoricoPipe()}
    ];

    constructor(private router: Router,
                private usuarioService: UsuarioService,
                private breadcrumbService: BreadcrumbService,
                private historicoService: HistoricoService) {
    }

    ngOnInit() {
        this.breadcrumbService.setItems([{label: 'Histórico de Usuário'}]);
        this.buscarParametros();
    }

    buscarParametros() {
        const parametro = this.router.url.split('/');
        this.entidade = HistoricoEnum.selecionaEntidade(parametro[1]);
        this.filtro.id = Number(parametro[3]);
        this.buscarUsuario();
        this.filtro.auditoria = HistoricoEnum.selecionaEntidade(parametro[1]);
        this.carregar();
    }

    buscarUsuario() {
        this.blockUI.start(Constants.CARREGANDO);
        this.usuarioService.buscarPorId(this.filtro.id).subscribe(res => this.usuario = res);
    }

    carregar() {
        this.historicoService.listarPorEntidade(this.filtro)
            .pipe(finalize(() => this.blockUI.stop()))
            .subscribe(res => this.historicos = res);
    }

    isAlteracao(tipoAcao) {
        return tipoAcao == TipoAcaoHistoricoEnum.ALTERACAO || tipoAcao == TipoAcaoHistoricoEnum.EDICAO;
    }

    isStatus(tipoAcao) {
        return [TipoAcaoHistoricoEnum.INATIVACAO, TipoAcaoHistoricoEnum.MODERACAO].includes(tipoAcao)
            || this.usuario.perfilEnum == PerfilEnum.EMPREENDEDOR && TipoAcaoHistoricoEnum.ATIVACAO == tipoAcao;
    }
}
