import {Routes} from "@angular/router";
import {UsuarioComponent} from "./usuario.component";
import {UsuarioFormComponent} from "./form/usuario-form.component";
import {MeusDadosComponent} from "./meus-dados/meus-dados.component";
import {AreaInteresseResolver} from "../shared/resolvers/area-interesse.resolver";
import {HistoricoComponent} from "./historico/historico.component";

export const UsuarioRoute: Routes = [
    {
        path: '',
        component: UsuarioComponent
    },
    {
        path: 'cadastrar/:perfil',
        component: UsuarioFormComponent,
        resolve: {
            areasInteresse: AreaInteresseResolver
        }
    },
    {
        path: 'historico/:id',
        component: HistoricoComponent,
    },
    {
        path: 'meus-dados',
        component: MeusDadosComponent,
        resolve: {
            areasInteresse: AreaInteresseResolver
        }
    },
    {
        path: ':acao/:id',
        component: UsuarioFormComponent,
        resolve: {
            areasInteresse: AreaInteresseResolver
        }
    }
];
