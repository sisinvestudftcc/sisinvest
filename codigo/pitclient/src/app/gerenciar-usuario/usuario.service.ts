import {environment} from "../../environments/environment";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {RequestUtil} from "../shared/request-util";
import {Usuario} from "../models/usuario";
import {Page} from "../shared/page";

@Injectable({providedIn: "root"})
export class UsuarioService {

    public MAX_RESULTS = 2147483647;

    baseUrl = `${environment.pitsegurancaUrl}/usuario`;

    constructor(private http: HttpClient) {
    }

    alterarSenha(senha) {
        return this.http.put(`${this.baseUrl}/alterar-senha`, senha);
    }

    alterarStatus(id, justificativa) {
        return this.http.put(`${this.baseUrl}/alterar-status`, null, {params: {id: id, justificativa: justificativa}})
    }

    buscarPorId(id) {
        return this.http.get<Usuario>(`${this.baseUrl}/${id}`);
    }

    buscarUsuarioLogado() {
        return this.http.get<Usuario>(`${this.baseUrl}/buscar-logado`);
    }

    cadastrar(usuario): Observable<any> {
        return this.http.post(this.baseUrl, usuario);
    }

    cadastroExterno(usuario): Observable<any> {
        return this.http.post(`${environment.pitsegurancaUrl}/public/usuario/cadastro-externo`, usuario);
    }

    filtrar(filtro, pageable): Observable<any> {
        return this.http.post<Page<Usuario[]>>(`${this.baseUrl}/filtrar`, filtro, {params: pageable});
    }

    gerarRelatorio(): void {

        const frame = <any> document.createElement('iframe');
        frame.style = 'display: none';
        frame.src = `${this.baseUrl}/gerar-relatorio?size=${this.MAX_RESULTS}`;
        document.body.appendChild(frame);

    }

    moderarUsuario(id, status, justificativa): Observable<any> {
        return this.http.put(`${this.baseUrl}/${id}/moderar`, null, {params: {status: status, justificativa: justificativa}});
    }

    validarCampo(campoValidacao): Observable<any> {
        return this.http.post(`${environment.pitsegurancaUrl}/public/usuario/validar-campo`, campoValidacao);
    }

    verificarSenha(senha) {
        return this.http.get(`${this.baseUrl}/verificar-senha`, {params: {senha: senha}});
    }

    verificarNovaSenha(novaSenha) {
        return this.http.get(`${this.baseUrl}/verificar-nova-senha`, {params: {novaSenha: novaSenha}});
    }
}
