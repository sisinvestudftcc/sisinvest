import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {UsuarioRoute} from './usuario.route';
import {UsuarioComponent} from './usuario.component';
import {ButtonModule, ConfirmationService, KeyFilterModule, TableModule} from "primeng";
import {CommonModule} from "@angular/common";
import {SharedModule} from "../shared/shared.module";
import {UsuarioService} from "./usuario.service";
import {UsuarioFormComponent} from "./form/usuario-form.component";
import {PRIMENG_IMPORTS} from "../shared/primeng-imports";
import {MeusDadosComponent} from "./meus-dados/meus-dados.component";
import {HistoricoComponent} from "./historico/historico.component";
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
    imports: [
        RouterModule.forChild(UsuarioRoute),
        PRIMENG_IMPORTS,
        ButtonModule,
        TableModule,
        CommonModule,
        SharedModule,
        KeyFilterModule,
        TranslateModule,
    ],
    exports: [],
    declarations: [
        UsuarioComponent,
        UsuarioFormComponent,
        MeusDadosComponent,
        HistoricoComponent
    ],
    providers: [
        UsuarioService,
        ConfirmationService
    ]
})
export class UsuarioModule {
}
