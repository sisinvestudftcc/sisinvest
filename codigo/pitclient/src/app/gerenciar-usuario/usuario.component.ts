import {Component, OnInit, ViewChild} from "@angular/core";
import {StatusEnum} from "../shared/enumerations/status.enum";
import {Constants} from "../shared/utils/constants";
import {finalize} from "rxjs/operators";
import {Router} from "@angular/router";
import {BreadcrumbService} from "@nuvem/primeng-components";
import {BlockUI, NgBlockUI} from "ng-block-ui";
import {Perfil} from "../models/perfil";
import {PerfilService} from "../shared/services/perfil.service";
import {Usuario} from "../models/usuario";
import {Table} from "primeng";
import {UsuarioService} from "./usuario.service";
import {Page} from "../shared/page";
import {PerfilEnum} from "../shared/enumerations/perfil.enum";
import {AcaoEnum} from "../shared/enumerations/acao.enum";
import {Endereco} from "../models/endereco";
import {MessageDialogService} from "../shared/message-dialog.service";
import {Coluna} from "../shared/models/coluna";
import {DateTime} from "luxon";
import {GridComponent} from "../shared/views/grid/grid.component";

@Component({
    selector: 'app-usuario',
    templateUrl: 'usuario.component.html'
})
export class UsuarioComponent implements OnInit {
    @ViewChild('gridComponent') gridComponent: GridComponent;
    ativarFiltro = false;
    acao = AcaoEnum;
    @BlockUI() blockUI: NgBlockUI;
    cols = [];
    @ViewChild('dt', {static: true}) table: Table;
    dialogCadastro = false;
    dialogModerar = false;
    dialogStatus = false;
    endereco = new Endereco();
    filtroUsuario = new Usuario();
    mensagemDialog = PerfilEnum;
    perfis: Perfil[];
    perfilEnum = PerfilEnum;
    statusEnum = StatusEnum;
    usuario = new Usuario();
    usuarios = new Page();
    value: any;
    callbackFunction: any;

    colunas: Coluna[] = [
        {
            campo: 'nome',
            label: 'Nome',
            getResultado: (item: any) => {
                return item.nome;
            }
        },
        {
            campo: 'email',
            label: 'E-mail',
            getResultado: (item: any) => {
                return item.email;
            }
        },
        {
            campo: 'perfil',
            label: 'Perfil',
            getResultado: (item: any) => {
                return item.perfilEnum;
            }
        },
        {
            campo: 'status',
            label: 'Status',
            getResultado: (item: any) => {
                return item.status;
            }
        }
    ];

    constructor(private router: Router,
                private perfilService: PerfilService,
                private usuarioService: UsuarioService,
                private messageDialogService: MessageDialogService,
                private breadcrumbService: BreadcrumbService) {
    }

    ngOnInit(): void {
        this.breadcrumbService.setItems([{label: 'Usuários'}]);
        this.filtroUsuario.endereco = new Endereco();
        this.initCallback();
        this.setStatusFiltro();
    }

    initCallback() {
        this.callbackFunction = (evento) => this.usuarioService.filtrar(this.filtroUsuario, evento.paginacao);
    }

    acaoModerar(status) {
        this.blockUI.start(Constants.CARREGANDO)
        this.usuarioService.moderarUsuario(this.usuario.id, status, this.usuario.justificativa)
            .pipe(finalize(() => this.blockUI.stop()))
            .subscribe(() => {
                this.messageDialogService.addSuccessMsg();
                this.dialogModeracao();
            });
    }

    alterarStatus(rowData, props) {
        props.checked = !props.checked;
        this.usuario = rowData;
        this.dialogStatus = true;
    }

    buscarPerfil() {
        this.blockUI.start(Constants.CARREGANDO);
        this.perfilService.buscarPerfil(new Perfil()).pipe(finalize(() => this.blockUI.stop())).subscribe(res => {
            this.perfis = res;
            this.verDialogCadastro();
        });
    }

    cancelarAcao() {
        if (this.usuario.status == StatusEnum.ATIVO || this.usuario.perfilEnum == PerfilEnum.EMPREENDEDOR) {
            this.messageDialogService.addOptionalCancelar(() => this.fecharDialog());
            return;
        }
        this.fecharDialog();
    }

    confirmarAcao() {
        this.blockUI.start(Constants.SALVANDO);
        this.usuarioService.alterarStatus(this.usuario.id, this.usuario.justificativa).pipe(finalize(() => this.blockUI.stop()))
            .subscribe(() => {
                this.messageDialogService.addSuccessMsg();
                this.fecharDialog();
                this.usuario = new Usuario();
            });
    }

    dialogModeracao() {
        this.dialogModerar = false;
        this.gridComponent.reset();
    }

    entradaTelefone() {
        if (this.filtroUsuario.telefone) {
            this.filtroUsuario.telefone = this.filtroUsuario.telefone.replace(/[^+\d]/, '');
        }
    }

    fecharDialog() {
        this.dialogStatus = false;
        this.gridComponent.reset();
    }

    fecharFiltro() {
        this.verFiltro();
        this.filtroUsuario.endereco = new Endereco();
        this.endereco = new Endereco();
        this.setStatusFiltro();
        this.gridComponent.reset();
    }

    filtroStatus() {
        if (!this.filtroUsuario.status) {
            this.filtroUsuario.listStatus = null;
            return;
        }
        this.filtroUsuario.listStatus = [this.filtroUsuario.status];
    }

    filtroConsulta() {
        this.filtroUsuario.endereco = this.endereco;
        this.gridComponent.reset()
    }

    iconePerfil(nome) {
        return PerfilEnum.iconeUsuario.find(item => item.value == nome).label;
    }

    isEmpreendedorInativo() {
        return this.usuario.perfilEnum != PerfilEnum.EMPREENDEDOR && this.statusAtivo(this.usuario.status)
            || this.usuario.perfilEnum == PerfilEnum.EMPREENDEDOR;
    }

    perfilSelecionado() {
        return !this.filtroUsuario.perfilEnum;
    }

    mostrarMensagem() {
        return this.usuario.perfilEnum == PerfilEnum.EMPREENDEDOR && !this.statusAtivo(this.usuario.status);
    }

    setStatusFiltro() {
        this.filtroUsuario.listStatus = [StatusEnum.ATIVO, StatusEnum.SOB_ANALISE];
    }

    statusAtivo(status) {
        return status == this.statusEnum.ATIVO
    }

    statusSobAnalise(status): boolean {
        return status == this.statusEnum.SOB_ANALISE;
    }

    statusSobAnaliseReprovado(status): boolean {
        return this.statusSobAnalise(status)|| this.statusReprovado(status);
    }

    statusReprovado(status): boolean {
        return status == this.statusEnum.REPROVADO;
    }

    selecionarPerfil(perfil) {
        this.filtroUsuario.perfilEnum = perfil;
    }

    setEndereco() {
        this.filtroUsuario.endereco = this.endereco;
    }

    statusUsuario() {
        return this.usuario.status == StatusEnum.ATIVO ? this.statusEnum.INATIVAR : this.statusEnum.ATIVAR;
    }

    verDialogCadastro() {
        this.dialogCadastro = !this.dialogCadastro;
    }

    verFiltro() {
        this.filtroUsuario = new Usuario();
        this.ativarFiltro = !this.ativarFiltro;
    }

    gerarRelatorio() {
        this.usuarioService.gerarRelatorio();
    }

    verificaFiltros() {
        if (!Object.values(this.endereco).some(it => it)) {
            this.filtroUsuario.endereco = null;
        }
        return !Object.values(this.filtroUsuario).some(it => it) && !Object.values(this.endereco).some(it => it);
    }

    verificarStatus() {
        return !this.usuario.justificativa && (this.usuario.status == StatusEnum.ATIVO || this.usuario.perfilEnum == PerfilEnum.EMPREENDEDOR)
    }

    vincularUsuario(rowData) {
        rowData.justificativa = null;
        this.usuario = rowData;
        this.dialogModerar = true;
    }

    fecharModeracao() {
        if (this.usuario.justificativa) {
            this.messageDialogService.addOptionalCancelar(() => this.dialogModerar = false);
            return;
        }
        this.dialogModerar = false;
    }
}
