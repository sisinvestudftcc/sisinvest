import {Component} from '@angular/core';
import {Authentication, User} from '@nuvem/angular-base';
import {MainComponent} from "../main/main.component";
import {TranslateService} from "@ngx-translate/core";

@Component({
    selector: 'app-topbar',
    templateUrl: './app.topbar.component.html'
})
export class AppTopbarComponent {

    idiomaList: { imagem: string, titulo: string, identificador: string }[] = [
        {
            imagem: '/assets/images/idiomas/pt-BR.png',
            titulo: 'Português',
            identificador: 'pt'
        },
        {
            imagem: '/assets/images/idiomas/en-US.png',
            titulo: 'English',
            identificador: 'en'
        },
        {
            imagem: '/assets/images/idiomas/es.png',
            titulo: 'Español',
            identificador: 'es'
        }
    ];

    constructor(public app: MainComponent, private readonly _authentication: Authentication<User>, protected translateService: TranslateService) {
    }

    get usuario() {
        return this._authentication.getUser();
    }

    isAuthenticated() {
        return this._authentication.isAuthenticated();
    }

    mudarIdioma(idioma) {
        localStorage.setItem('language', idioma);
        this.translateService.setDefaultLang(idioma);
        this.translateService.use(idioma);

        window.location.reload();
    }
}
