import {Routes} from "@angular/router";
import {CentralComunicacaoListaComponent} from "./lista/lista.component";

export const CentralComunicacaoRoute: Routes = [
    {
        path: '',
        component: CentralComunicacaoListaComponent
    }
];
