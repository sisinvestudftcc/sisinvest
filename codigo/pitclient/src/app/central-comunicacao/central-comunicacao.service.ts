import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {Page} from "../shared/page";
import {ProjetoApp} from "../shared/models/app.models";
import Projeto = ProjetoApp.Projeto;
import ProjetoLinhaTemporal = ProjetoApp.ProjetoLinhaTemporal;
import {FormUtilsService} from "../shared/services/form-utils.service";
import Mensagem = ProjetoApp.Mensagem;
import TipoMensagem = ProjetoApp.TipoMensagem;

@Injectable({providedIn: "root"})
export class CentralComunicacaoService {

    baseUrl = `${environment.pitprojetoUrl}/mensagem`;

    constructor(
        private formUtilsService: FormUtilsService,
        private http: HttpClient
    ) {

    }

    filtrar(filtro: any = {}, paginacao: any = {page: 0, size: 30}): Observable<any> {

        return this.http.post<Page<Projeto[]>>(`${this.baseUrl}/filtrar`, filtro, {params: paginacao});

    }

    buscarPorId(id: number, usuarioId: number = null, paginacao: any = {page: 0, size: 9999999, sort: 'dataAtualizacao,DESC'}): Observable<any> {

        return this.http.post<Page<Projeto[]>>(`${this.baseUrl}/projeto/${id}`, {projeto: id, tipoMensagem: TipoMensagem.MENSAGEM, usuarioMensagem: usuarioId}, {params: paginacao});

    }

    comentar(form: {id: number, justificativa: string, usuario: number, anexoList: any[], anexosRemovidos: any[]}): Observable<any> {

        let formData = this.formUtilsService.convertToFormData(form);

        return this.http.post<number>(`${this.baseUrl}/comentar/${form.id}`, formData);

    }

    editar(form: {id: number, justificativa: string, usuario: number, anexoList: any[], anexosRemovidos: any[]}): Observable<any> {

        let formData = this.formUtilsService.convertToFormData(form);

        return this.http.post<number>(`${this.baseUrl}/editar/${form.id}`, formData);

    }

    responder(form: {id: number, justificativa: string, usuario: number, anexoList: any[], anexosRemovidos: any[]}): Observable<any> {

        let formData = this.formUtilsService.convertToFormData(form);

        return this.http.post<number>(`${this.baseUrl}/responder/${form.id}`, formData);

    }

    excluir(id: number): Observable<any> {

        return this.http.post<number>(`${this.baseUrl}/excluir/${id}`, {});

    }

}
