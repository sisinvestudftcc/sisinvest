import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from "@angular/core";
import {FormBuilder, FormGroup} from "@angular/forms";
import {ProjetoApp} from "../../../shared/models/app.models";
import Option = ProjetoApp.Option;
import {PerfilEnum} from "../../../shared/enumerations/perfil.enum";
import {DominioService} from "../../../projetos/dominio.service";
import TipoMensagem = ProjetoApp.TipoMensagem;
import {TranslateService} from "@ngx-translate/core";

@Component({
    selector: 'app-central-comunicacao-lista-consulta',
    templateUrl: 'consulta.component.html',
    styleUrls: ['consulta.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class CentralComunicacaoListaConsultaComponent implements OnInit {

    @Input() usuarioId: number;

    @Input() perfil: string;

    @Output() formCriado: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();

    @Output() fecharFiltroEvent: EventEmitter<void> = new EventEmitter<void>();

    @Output() consultarEvent: EventEmitter<void> = new EventEmitter<void>();

    form: FormGroup;

    segmentos = [];

    constructor(
        protected formBuilder: FormBuilder,
        protected dominioService: DominioService,
        protected translateService: TranslateService
    ) {
        this.dominioService.segmentoFiltrar({}, {page: 0, size: 9999}).subscribe(optionList => {

            optionList = optionList.content;

            optionList = optionList.map(option => {
                return new Option(option.id, this.translateService.instant(`SEGMENTO.${option.id}`));
            });

            this.segmentos = optionList;

        });
    }

    ngOnInit() {
        this.form = this.formBuilder.group({
            protocolo: [null],
            nomeProjeto: [null],
            dataInicio: [null],
            dataConclusao: [null],
            status: [null],
            segmentoList: [null],
            usuario: [this.perfil === PerfilEnum.EMPREENDEDOR ? this.usuarioId : null],
            usuarioMensagem: [this.perfil === PerfilEnum.INVESTIDOR ? this.usuarioId : null],
            tipoMensagem: [TipoMensagem.MENSAGEM]
        });

        this.formCriado.emit(this.form);
    }

    fecharFiltro() {
        this.fecharFiltroEvent.emit();
    }

    consultar() {
        this.consultarEvent.emit();
    }

}
