import {Component, OnInit} from "@angular/core";
import {UsuarioService} from "../../gerenciar-usuario/usuario.service";
import {finalize} from "rxjs/operators";
import {BlockUI, NgBlockUI} from "ng-block-ui";
import {Constants} from "../../shared/utils/constants";
import {FormGroup} from "@angular/forms";
import {ProjetoApp} from "../../shared/models/app.models";
import TipoMensagem = ProjetoApp.TipoMensagem;
import {PerfilEnum} from "../../shared/enumerations/perfil.enum";

@Component({
    selector: 'app-central-comunicacao-lista',
    templateUrl: 'lista.component.html'
})
export class CentralComunicacaoListaComponent implements OnInit {

    @BlockUI() blockUI: NgBlockUI;

    usuarioId: number;
    perfilUsuario: string;

    form: FormGroup;

    filter: any = null;

    consultarModal: boolean = false;

    constructor(
        protected usuarioService: UsuarioService
    ) {

    }

    ngOnInit() {

        this.initUsuario();

    }

    formCriado(form: FormGroup) {
        this.form = form;
    }

    resetarFiltros() {
        this.form = null;
        this.consultarModal = false;
        this.filter = {
            tipoMensagem: TipoMensagem.MENSAGEM,
            usuario: this.perfilUsuario === PerfilEnum.EMPREENDEDOR ? this.usuarioId : null,
            usuarioMensagem: this.perfilUsuario === PerfilEnum.INVESTIDOR ? this.usuarioId : null
        };
        this.initUsuario();
    }

    consultar() {
        this.filter = this.form.value;
    }

    initUsuario() {
        this.usuarioId = null;
        this.perfilUsuario = null;

        this.blockUI.start(Constants.CARREGANDO);
        this.usuarioService.buscarUsuarioLogado().pipe(finalize(() => this.blockUI.stop()))
            .subscribe(res => {
                this.usuarioId = res.id;
                this.perfilUsuario = res.perfilEnum.toString();
                this.filter = {
                    tipoMensagem: TipoMensagem.MENSAGEM,
                    usuario: this.perfilUsuario === 'EMPREENDEDOR' ? this.usuarioId : null,
                    usuarioMensagem: this.perfilUsuario === PerfilEnum.INVESTIDOR ? this.usuarioId : null
                };
            });

    }

}
