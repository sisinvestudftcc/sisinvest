import {Component, Input, OnInit, ViewChild} from "@angular/core";
import {Coluna} from "../../../shared/models/coluna";
import {Observable} from "rxjs";
import {CentralComunicacaoService} from "../../central-comunicacao.service";
import {ProjetoApp} from "../../../shared/models/app.models";
import {ProjetosApresentacaoService} from "../../../projetos/projetos-apresentacao.service";
import {DateTime} from "luxon";
import Idioma = ProjetoApp.Idioma;
import Mensagem = ProjetoApp.Mensagem;
import TipoMensagem = ProjetoApp.TipoMensagem;
import {GridComponent} from "../../../shared/views/grid/grid.component";
import {TranslateService} from "@ngx-translate/core";

@Component({
    selector: 'app-central-comunicacao-lista-grid',
    templateUrl: 'grid.component.html'
})
export class CentralComunicacaoListaGridComponent implements OnInit {

    @ViewChild('gridComponent') gridComponent: GridComponent;

    @Input() usuarioId: number;

    callbackBuscarDados: (evento) => Observable<any>;

    colunasFavoritos: Coluna[] = [
        {
            campo: 'dataAtualizacao',
            label: this.translateService.instant('CENTRAL_COMUNICACAO.GRID.ULTIMA_ATUALIZACAO'),
            getResultado: (item: Mensagem) => {
                return this.projetosApresentacaoService.converterDataAtualizacao(item.dataAtualizacao)
            }
        },
        {
            campo: 'dataCriacao',
            label: this.translateService.instant('CENTRAL_COMUNICACAO.GRID.DATA_ENVIO'),
            getResultado: (item: Mensagem) => {
                return DateTime.fromISO(item.dataCriacao, {zone: 'utc'}).toFormat("dd/LL/yyyy");
            }
        },
        {
            campo: 'protocolo',
            label: this.translateService.instant('CENTRAL_COMUNICACAO.GRID.PROTOCOLO'),
            getResultado: (item: Mensagem) => {
                return item.protocolo;
            }
        },
        {
            campo: 'projeto.projetoInternacionalizacaoList.nome',
            label: this.translateService.instant('CENTRAL_COMUNICACAO.GRID.NOME_PROJETO'),
            getResultado: (item: Mensagem) => {
                return item.projeto.internacionalizacao.filter(traducao => traducao.idioma.sigla === Idioma.SIGLA_PADRAO)
                    .map(traducao => traducao.nome);
            }
        },
        {
            campo: 'status.nome',
            label: this.translateService.instant('CENTRAL_COMUNICACAO.GRID.STATUS'),
            getResultado: (item: Mensagem) => {
                return this.translateService.instant(`STATUS.${item.status.id}`);
            }
        }
    ];

    filtro: any = {
        tipoMensagem: TipoMensagem.MENSAGEM,
        usuario: this.usuarioId
    };

    _filterConsultar: any = {
        tipoMensagem: TipoMensagem.MENSAGEM,
        usuario: this.usuarioId
    };

    @Input() set filterConsultar(filtro: any) {
        this.filtro = filtro;

        if (filtro == null) {
            this.filtro = {
                tipoMensagem: TipoMensagem.MENSAGEM,
                usuario: this.usuarioId
            };
        } else {
            if(filtro.segmentoList && filtro.segmentoList.length) {
                filtro.segmentoList = filtro.segmentoList.map(segmento => segmento.value);
            }

            this.filtro = filtro;
        }

        this._filterConsultar = this.filtro;

        if (this.gridComponent) {
            this.gridComponent.reset()
        }
    };

    get filterConsultar() {
        return this._filterConsultar;
    }

    constructor(
        private centralComunicacaoService: CentralComunicacaoService,
        private projetosApresentacaoService: ProjetosApresentacaoService,
        protected translateService: TranslateService
    ) {

    }

    ngOnInit() {

        this.initCallback();

    }

    initCallback() {
        this.callbackBuscarDados = (evento) => this.centralComunicacaoService.filtrar(this.filterConsultar, evento.paginacao);
    }

}
