import {Component, Input, OnInit} from "@angular/core";
import {ProjetoApp} from "../../../../shared/models/app.models";
import Mensagem = ProjetoApp.Mensagem;

@Component({
    selector: 'app-central-comunicacao-lista-acoes',
    templateUrl: 'acoes.component.html'
})
export class CentralComunicacaoListaAcoesComponent implements OnInit {

    @Input()
    item: Mensagem;

    constructor() {

    }

    ngOnInit() {

    }

}
