import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {ButtonModule, TableModule} from "primeng";
import {CommonModule} from "@angular/common";
import {SharedModule} from "../shared/shared.module";
import {PRIMENG_IMPORTS} from "../shared/primeng-imports";
import {CentralComunicacaoRoute} from "./central-comunicacao.route";
import {ReactiveFormsModule} from "@angular/forms";
import {CentralComunicacaoListaComponent} from "./lista/lista.component";
import {CentralComunicacaoListaGridComponent} from "./lista/grid/grid.component";
import {CentralComunicacaoListaConsultaComponent} from "./lista/consulta/consulta.component";
import {CentralComunicacaoListaAcoesComponent} from "./lista/grid/acoes/acoes.component";
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
    imports: [
        PRIMENG_IMPORTS,
        ButtonModule,
        TableModule,
        CommonModule,
        SharedModule,
        ReactiveFormsModule,
        RouterModule.forChild(CentralComunicacaoRoute),
        TranslateModule
    ],
    exports: [],
    declarations: [
        CentralComunicacaoListaComponent,
        CentralComunicacaoListaGridComponent,
        CentralComunicacaoListaAcoesComponent,
        CentralComunicacaoListaConsultaComponent
    ],
    providers: [
    ]
})
export class CentralComunicacaoModule {
}
