export class Coluna {

    public campo: string;
    public label: string;
    public getResultado: (campo: any) => {};
    public ordenavel?: boolean = true;
    public tipo?: string = 'texto';
    public getDesabilitado?: (campo: any) => {};

}
