export namespace ProjetoApp {

    export class TipoMensagem {
        public static MENSAGEM = 1;

        id: number;
        nome: string;
    }

    export class MensagemArquivo {
        id: number;
        arquivo: Arquivo;
        mensagem: Mensagem;
    }

    export class Mensagem {
        id: number;
        descricao: string;
        protocolo: string;
        usuario: number;
        status: Status;
        projeto: Projeto;
        tipoMensagem: TipoMensagem;
        arquivoList: MensagemArquivo[];
        respostaList: Mensagem[];
        dataCriacao: string;
        dataAtualizacao: string;
    }

    export class Projeto {

        // 50MB = 50 * 1024 * 1024 = 52428800
        static TAMANHO_MAXIMO_IMAGEM = 52428800;
        static FORMATO_IMAGEM = ['jpg', 'jpeg', 'png'];
        static FORMATO_IMAGEM_MIME_TYPE = ['image/jpeg', 'image/png'];

        static TAMANHO_MAXIMO_ANEXO = 52428800;
        static FORMATO_ANEXO = ['pdf', 'jpg', 'jpeg', 'png', 'mp4', 'wmv', 'avi', 'mpeg', 'gif'];
        static FORMATO_ANEXO_MIME_TYPE = [
            'application/pdf',
            'image/jpeg',
            'image/png',
            'video/mp4',
            'video/x-ms-asf',
            'video/x-ms-wmv',
            'application/x-troff-msvideo',
            'video/avi',
            'video/x-msvideo',
            'video/mpeg',
            'image/gif',
            'video/msvideo'
        ];

        id: number;
        protocolo: string;
        usuario: number;
        statusId: number;
        statusNome: string;
        dataCriacao: string;
        dataAtualizacao: string;
        empresas: ProjetoEmpresa[];
        localizacoes: ProjetoLocalizacao[];
        detalhes: ProjetoDetalhe[];
        internacionalizacao: ProjetoInternacionalizacao[];
        equipamentos: ProjetoEquipamento[];
        segmentos: ProjetoSegmento[];

    }

    export class ProjetoLinhaTemporal {

        // 50MB = 50 * 1024 * 1024 = 52428800
        static TAMANHO_MAXIMO_ANEXO = 52428800;

        id: number;
        status: Status;
        acao: Acao;
        dataCriacao: Date;
        comentario: string;
        usuario: number;
        linhaTemporalArquivoList: LinhaTemporalArquivo[];

    }

    export class LinhaTemporalArquivo {
        id: number;
        arquivo: Arquivo;
    }

    export class Arquivo {
        id: number;
        nome: string;
    }

    export class ProjetoEmpresa {

        id: number;
        razaoSocial: string;
        cnpj: string;
        email: string;
        url: string;
        proprietario: string;
        responsavel: string;
        pais: Pais;
        projetoEmpresaTelefoneList: ProjetoEmpresaTelefone[];

    }

    export class ProjetoSegmento {

        id: number;
        segmento: Segmento;

    }

    export class ProjetoLocalizacao {

        id: number;
        latitude: string;
        longitude: string;
        cidade: string;
        estado: string;

    }

    export class ProjetoDetalhe {

        id: number;
        naturezaInvestimento: NaturezaInvestimento;
        dataInicio: Date;
        dataConclusao: Date;
        modeloContrato: ModeloContrato;
        modeloContratoOutro: string;
        valorProjeto: ValorProjeto;
        empregosProjeto: EmpregoProjeto;
        areaConstruida: string;
        areaTotal: string;
        grupoOperador: string;

    }

    export class ProjetoEmpresaTelefone {

        id: number;
        telefone: string;

    }

    export class ProjetoEquipamento {
        id: number;
        grupoEquipamento: GrupoEquipamento;
        equipamento: Equipamento;
        capacidadeAtracao: string;
        capacidadePublico: string;
        capacidadesMesas: string;
        gastoMedioVisitantes: string;
        numeroAnualVisitantes: string;
        numeroPavilhoes: string;
        numeroSalasReuniao: string;
        rendaAnual: string;
        tamanhoProjeto: string;
        unidadesHabitacionais: string;
        unidadesImobiliarias: string;
        outroEquipamento: string;
    }

    export class ProjetoInternacionalizacao {

        id: number;
        idioma: Idioma;
        nome: string;
        descricao: string;
        projetoInternacionalizacaoArquivos: ProjetoInternacionalizacaoArquivos[];
        internacionalizacaoPadrao: boolean;

    }

    export class ProjetoInternacionalizacaoArquivos {

        id: number;
        arquivo: string;
        arquivoId: number;
        tipoArquivo: number;

    }

    export class Status {

        static ATIVO = 1;
        static INATIVO = 2;
        static EXCLUIDO = 3;
        static RASCUNHO = 4;
        static SOB_ANALISE = 5;
        static APROVADO = 6;
        static REPROVADO = 7;
        static ESCLARECIMENTO = 8;
        static NAO_RESPONDIDO = 9;
        static RESPONDIDO = 10;

        static APROVADO_LABEL = 'Aprovado';

        id: number;
        nome: string;

    }

    export class Acao {

        static ESCLARECIMENTO = 1;
        static COMENTARIO = 2;
        static APROVAR = 3;
        static REPROVAR = 4;
        static RESTAURAR = 5;
        static RESPONDER_ESCLARECIMENTO = 6;
        static DESPUBLICAR = 7;
        static REMOVER_DESTAQUE = 8;

        id: number;
        nome: string;

    }

    export class Idioma {

        static SIGLA_PADRAO = 'pt-BR';

        static PORTUGUES = 'pt-BR';
        static INGLES = 'en-US';
        static ESPANHOL = 'es';

        id: number;
        nome: string;
        sigla: string;

    }

    export class Option {

        constructor(
            public value: number | string,
            public label: string
        ) {

        }

    }

    export class Pais {

        static BRASIL_ID = 31;
        static BRASIL_NOME = 'Brasil';

        id: number;
        nome: string;
        sigla: string;
        siglaResumida: string;

    }

    export class NaturezaInvestimento {

        id: number;
        nome: string;

    }

    export class ModeloContrato {

        static OUTRO_MODELO_ID = 4;

        id: number;
        nome: string;

    }

    export class ValorProjeto {

        id: number;
        nome: string;

    }

    export class EmpregoProjeto {

        id: number;
        nome: string;

    }

    export class GrupoEquipamento {

        static MEIO_HOSPEDAGEM = 1;
        static BAR_ENTRETENIMENTO = 2;
        static BAR_SEM_ENTRETENIMENTO = 3;
        static RESTAURANTE_ENTRETENIMENTO = 4;
        static RESTAURANTE_SEM_ENTRETENIMENTO = 5;
        static CENTRO_CONVENCOES = 6;
        static EQUIPAMENTOS_LAZER = 7;
        static COMPLEXO_TURISTICO = 8;
        static MUSEU = 9;
        static TEATRO = 10;
        static OUTROS = 11;

        id: number;
        nome: string;

    }


    export class Equipamento {

        static HOTEL_BOUTIQUE = 1;
        static SPA = 2;
        static RESORT = 3;
        static RESORT_INTEGRADO = 4;
        static HOTEL_LUXO = 5;
        static HOTEL_NEGOCIOS = 6;
        static HOTEL_ECONOMICO = 7;
        static HOTEL_FAZENDA = 8;
        static HOTEL_CAMA = 9;
        static HOSTEL = 10;
        static POUSADA = 11;
        static CAMPING = 12;
        static GLAMPING = 13;
        static EMPREENDIMENTO_TURISTICO = 14;

        static PARQUE_TEMATICO = 15;
        static PARQUE_AQUATICO = 16;
        static CLUBE_GOLF = 17;
        static PLANETARIO = 18;
        static AQUARIO = 19;
        static KARTODROMO = 20;
        static POLOS_GASTRONOMICOS = 21;
        static SHOPPING_CENTER = 22;
        static CASA_SHOW = 23;
        static CONCHA_ACUSTICA = 24;
        static ESTADIO_DESPORTIVO = 25;
        static CASA_NOTURNA = 26;
        static MARINA = 27;
        static INSTALACAO_PORTUARIA = 28;

        id: number;
        nome: string;
        grupoEquipamento: GrupoEquipamento;

    }

    export class Segmento {

        id: number;
        nome: string;

    }

    export class TipoArquivo {

        static ANEXO = 1;
        static IMAGEM = 2;

        id: number;
        nome: string;

    }

}
