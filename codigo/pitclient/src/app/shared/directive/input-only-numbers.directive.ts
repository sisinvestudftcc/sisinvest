import {Directive, ElementRef, HostListener} from '@angular/core';

@Directive({
    selector: 'input[appOnlyNumbers]'
})
export class InputOnlyNumbersDirective {

    // Allow decimal numbers and negative values
    private regex: RegExp = new RegExp(/[^0-9() \-+]$/g);
    // Allow key codes for special events. Reflect :
    // Backspace, tab, end, home
    private specialKeys: Array<string> = ['Backspace', 'Delete', 'Tab', 'End', 'Home', 'ArrowLeft', 'ArrowRight'];

    // Allow Ctrl + ['a', 'c']
    private controlKeys: Array<string> = ['a', 'c', 'r', 'v'];

    constructor(private el: ElementRef) {
    }

    @HostListener('keydown', ['$event'])
    onKeyDown(event: KeyboardEvent) {

        const current: string = this.el.nativeElement.value;
        const next: string = current.concat(event.key);

        if (this.specialKeys.indexOf(event.key) !== -1
            || (event.ctrlKey && this.controlKeys.indexOf(event.key) !== -1)) {
            return;
        }

        if (next && String(next).match(this.regex)) {
            event.preventDefault();
        }
    }

    @HostListener('paste', ['$event'])
    onPaste(event: ClipboardEvent) {
        const pastedData = event.clipboardData.getData('Text');

        if (String(pastedData).match(this.regex)) {
            event.preventDefault();
        }
    }

}
