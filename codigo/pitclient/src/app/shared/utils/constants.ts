export class Constants {
    public static ICONE_GESTOR = 'ui-icon-person';
    public static ICONE_INVESTIDOR = 'ui-icon-monetization-on';
    public static ICONE_EMPREENDEDOR = 'ui-icon-show-chart';
    public static CARREGANDO = 'Carregando...';
    public static SALVANDO = 'Salvando...';
    public static PATTERN_LINKEDIN = '((https?:\\/\\/)?((www|\\w\\w)\\.)?linkedin\\.com\\/)((([\\w]{2,3})?)|([^\\/]+\\/(([\\w|\\d\\-&#?=])+\\/?){1,}))$';
    public static MENSAGEM_SENHA = 'Para criar uma senha é necessário que ela contenha 8 caracteres, seguindo os seguintes' +
        ' critérios: no mínimo Uma letra maiúscula, Uma letra minúscula, Um número e Um carácter especial.' ;
}
