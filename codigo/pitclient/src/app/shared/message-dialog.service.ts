import {Injectable} from '@angular/core';
import Swal from 'sweetalert2';
import {TranslateService} from "@ngx-translate/core";

@Injectable({providedIn: 'root'})
export class MessageDialogService {

    msgSuccess = 'Operação realizada com sucesso';

    constructor(private translate: TranslateService) {
    }

    private static addMsg(severity, title: string) {
        Swal.fire({icon: severity, title: title, timer: 3000, showConfirmButton: false});
    }

    private static addMessage(severity, title: string, html: string, buttonText) {
        Swal.fire({
            icon: severity,
            title: title,
            html: html,
            timer: 3000,
            showConfirmButton: true,
            confirmButtonText: buttonText,
            confirmButtonColor: '#009688',
            showCancelButton: false
        });
    }

    private static addOptionalMsg(severity, title, confirm, sim, nao, html = '') {
        Swal.fire({
            icon: severity, title: title, html: html,
            showConfirmButton: true, confirmButtonText: sim, confirmButtonColor: '#009688',
            showCancelButton: true, cancelButtonText: nao,
            allowOutsideClick: false, preConfirm() {
                confirm();
            }
        });
    }

    addOptionalCancelar(confirm) {
        MessageDialogService.addOptionalMsg('question', this.translate.instant('DESEJA_CANCELAR'), confirm,
            this.translate.instant('SIM'), this.translate.instant('NAO'));
    }

    addOptionalBox(confirm, severidade = 'info', titulo, mensagem = '') {
        MessageDialogService.addOptionalMsg(severidade, this.translate.instant(titulo), confirm,
            this.translate.instant('CONFIRMAR'), this.translate.instant('CANCELAR'), mensagem);
    }

    addModerarBoxJustificativa(icon, titulo, mensagem, btnConfirmar, btnCancelar, confirmAprovar) {
        Swal.fire({
            icon: icon,
            title: titulo,
            html: mensagem,
            input: 'textarea',
            customClass: {
                popup: 'custom-swal'
            },
            allowOutsideClick: false,
            confirmButtonText: btnConfirmar,
            confirmButtonColor: '#009688',
            cancelButtonText: btnCancelar,
            showCloseButton: true,
            showCancelButton: true,
            showConfirmButton: true,
            inputValidator: (value) => {
                return !value && 'Campo obrigatório!'
            },
            preConfirm(event) {
                if (event) {
                    confirmAprovar(event);
                }
            },
        });
    }

    addModerarBoxSemJustificativa(icon, titulo, mensagem, btnConfirmar, btnCancelar, confirmAprovar) {
        Swal.fire({
            icon: icon,
            title: titulo,
            html: mensagem,
            customClass: {
                popup: 'custom-swal'
            },
            allowOutsideClick: false,
            confirmButtonText: btnConfirmar,
            confirmButtonColor: '#009688',
            cancelButtonText: btnCancelar,
            showCloseButton: true,
            showCancelButton: true,
            showConfirmButton: true,
            preConfirm(event) {
                confirmAprovar();
            },
        });
    }

    addSuccessMsg(msg = this.msgSuccess) {
        MessageDialogService.addMsg('success', msg);
    }

    addErrorMessage(title, msg = this.msgSuccess) {
        MessageDialogService.addMessage('error', title, msg, this.translate.instant('CONFIRMAR'));
    }
}
