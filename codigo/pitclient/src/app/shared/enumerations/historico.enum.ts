export class HistoricoEnum {

    static USUARIO = 'USUARIO';

    static ENTIDADE_USUARIO = 'usuario';

    static selecionaEntidade(entidade) {
        return {
            [this.ENTIDADE_USUARIO]: this.USUARIO,
        }[entidade];
    }

    static selecionaCampo(entidade, campo) {
        return {
            [this.USUARIO]: this.camposUsuario()[campo],
        }[entidade];
    }

    static camposUsuario() {
        return {
            nome: 'Nome',
            email: 'E-mail',
            linkedin: 'LinkedIn',
            cpf: 'CPF',
            cnpj: 'CNPJ',
            rne: 'RNE',
            passaporte: 'Número do Passaporte',
            telefones: 'Telefone',
            endereco: 'Endereço',
            informacaoAdicional: 'Informações adicionais',
            areaInteresses: 'Áreas de Interesse',
        };
    }
}
