export class TipoAcaoHistoricoEnum {

    static EDICAO = 'EDICAO';
    static MODERACAO = 'MODERACAO';
    static CADASTRO = 'CADASTRO';
    static ALTERACAO = 'ALTERACAO';
    static ATIVACAO = 'ATIVACAO';
    static INATIVACAO = 'INATIVACAO';

    static items = [
        { label: TipoAcaoHistoricoEnum.ATIVACAO, value: 'Ativação' },
        { label: TipoAcaoHistoricoEnum.ALTERACAO, value: 'Alteração' },
        { label: TipoAcaoHistoricoEnum.CADASTRO, value: 'Cadastro' },
        { label: TipoAcaoHistoricoEnum.INATIVACAO, value: 'Inativação' },
        { label: TipoAcaoHistoricoEnum.MODERACAO, value: 'Moderação'},
        { label: TipoAcaoHistoricoEnum.EDICAO, value: 'Alteração - Meus Dados'}
    ];
}
