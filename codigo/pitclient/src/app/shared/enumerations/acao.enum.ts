export class AcaoEnum {
    static CADASTRAR = 'cadastrar';
    static VISUALIZAR = 'visualizar';
    static ALTERAR = 'alterar';

    static breadcrumbItem(acao) {
        return {
            cadastrar: 'Cadastrar',
            visualizar: 'Visualizar',
            alterar: 'Alterar',
            historico: 'Histórico de'
        }[acao];
    }

    static urlItem(acao) {
        return {
            cadastrar: 'cadastrar',
            visualizar: 'visualizar',
            alterar: 'alterar',
            historico: 'Histórico de'
        }[acao];
    }
}
