import {SelectItem} from 'primeng';

export class PerfilEnum {
    static EMPREENDEDOR = 'EMPREENDEDOR';
    static INVESTIDOR = 'INVESTIDOR';
    static GESTOR = 'GESTOR';

    static selectItem: SelectItem[] = [
        { value: null },
        { label: 'Empreendedor', value: PerfilEnum.EMPREENDEDOR },
        { label: 'Investidor', value: PerfilEnum.INVESTIDOR },
        { label: 'Gestor', value: PerfilEnum.GESTOR }
    ];

    static iconeUsuario: SelectItem[] = [
        { label: 'ui-icon-person', value: PerfilEnum.EMPREENDEDOR },
        { label: 'ui-icon-monetization-on', value: PerfilEnum.INVESTIDOR },
        { label: 'ui-icon-show-chart', value: PerfilEnum.GESTOR }
    ];
    static urlItem(acao) {
        return {
            EMPREENDEDOR: 'empreendedor',
            INVESTIDOR: 'investidor',
            GESTOR: 'gestor',
        }[acao];
    }

    static mensagemInativar(perfil) {
        return {
            EMPREENDEDOR: 'Caso o usuário selecionado seja o ' +
                'único responsável por algum projeto, esse(s) projeto(s) também serão inativado(s).' +
                ' Deseja continuar? Se sim, justifique.',
            INVESTIDOR: 'Se sim, justique.',
            GESTOR: 'Se sim, justique.',
        }[perfil];
    }
}
