import {SelectItem} from 'primeng';

export class StatusEnum {
    static ATIVO = 'ATIVO';
    static INATIVO = 'INATIVO';
    static SOB_ANALISE = 'SOB_ANALISE';
    static REPROVADO = 'REPROVADO';
    static INATIVAR = 'inativar';
    static ATIVAR = 'ativar';

    static selectItem: SelectItem[] = [
        { value: null },
        { label: 'Ativo', value: StatusEnum.ATIVO },
        { label: 'Inativo', value: StatusEnum.INATIVO },
        { label: 'Reprovado', value: StatusEnum.REPROVADO },
        { label: 'Sob Análise', value: StatusEnum.SOB_ANALISE }
    ];
}
