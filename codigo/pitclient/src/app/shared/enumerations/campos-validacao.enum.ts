export enum CamposValidacaoEnum {
    CPF = 'CPF',
    RNE = 'RNE',
    CNPJ = 'CNPJ',
    EMAIL = 'EMAIL',
    LINKEDIN = 'LINKEDIN',
    PASSAPORTE = 'PASSAPORTE'
}
