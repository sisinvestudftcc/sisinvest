import {Component, Input} from '@angular/core';
import {NgForm} from '@angular/forms';
import {TranslateService} from "@ngx-translate/core";

@Component({
    selector: 'app-input-label',
    templateUrl: './input-label.component.html'
})
export class InputLabelComponent {

    @Input() label = '';

    @Input() size = '6';

    @Input() labelObrigatorio = 'CAMPO_OBRIGATORIO';

    @Input() required = false;

    @Input() form: NgForm;

    @Input() field = null;

    constructor(private translateService: TranslateService) {
        this.translateService.addLangs(['en', 'pt', 'es']);
    }

    validaMensagem() {
        return this.form.controls[this.field] && !this.form.controls[this.field].valid
            && (this.form.controls[this.field].touched || this.form.submitted);
    }
}
