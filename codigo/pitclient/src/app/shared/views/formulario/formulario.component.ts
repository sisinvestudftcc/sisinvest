import {Component, Input, OnInit} from '@angular/core';
import {Usuario} from "../../../models/usuario";
import {Constants} from "../../utils/constants";
import {finalize} from "rxjs/operators";
import {UsuarioService} from "../../../gerenciar-usuario/usuario.service";
import {BlockUI, NgBlockUI} from "ng-block-ui";
import {BreadcrumbService, PageNotificationService} from "@nuvem/primeng-components";
import {ActivatedRoute, Router} from "@angular/router";
import {AcaoEnum} from "../../enumerations/acao.enum";
import {Perfil} from "../../../models/perfil";
import {Telefone} from "../../../models/telefone";
import {PerfilService} from "../../services/perfil.service";
import {PerfilEnum} from "../../enumerations/perfil.enum";
import {NgForm} from "@angular/forms";
import {AreaInteresseService} from "../../services/area-interesse.service";
import {CamposValidacaoEnum} from "../../enumerations/campos-validacao.enum";
import {CampoValidacao} from "../../../models/campo-validacao";
import {Endereco} from "../../../models/endereco";
import {TranslateService} from "@ngx-translate/core";
import {MessageDialogService} from "../../message-dialog.service";

@Component({
    selector: 'app-formulario',
    templateUrl: 'formulario.component.html'
})
export class FormularioComponent implements OnInit {
    @BlockUI() blockUI: NgBlockUI;
    acao: string;
    acoes = AcaoEnum;
    areasInteresse = [];
    campo = CamposValidacaoEnum;
    constants = Constants;
    @Input() perfil = new Perfil();
    telefone = new Telefone();
    @Input() usuario = new Usuario();
    cols = [
        {field: 'numero', header: 'Telefones'},
        {type: 'button'}
    ];

    constructor(private usuarioService: UsuarioService,
                private perfilService: PerfilService,
                private translateService: TranslateService,
                private breadcrumbService: BreadcrumbService,
                private areaInteresseService: AreaInteresseService,
                private messageDialogService: MessageDialogService,
                private pageNotificationService: PageNotificationService,
                private router: Router, private route: ActivatedRoute) {
        translateService.addLangs(['en', 'pt', 'es']);
        translateService.setDefaultLang('pt');
    }

    ngOnInit(): void {
        this.usuario.endereco = new Endereco();
        this.usuario.telefones = [];
        this.usuario.endereco = new Endereco();
        this.verificarAcao();
        this.buscarAreasInteresse();
    }

    acaoCancelar(alteracao: boolean) {
        if (alteracao) {
            this.messageDialogService.addOptionalCancelar(() => this.voltar());
            return;
        }
        this.voltar();
    }

    adicionarTelefone() {
        if (!this.usuario.telefone || this.usuario.telefone == '+') {
            this.usuario.telefone = null;
            return;
        }
        this.telefone.numero = this.usuario.telefone;
        this.usuario.telefones = [...this.usuario.telefones, this.telefone];
        this.usuario.telefone = null;
        this.telefone = new Telefone();
    }

    buscarAreasInteresse() {
        this.areasInteresse = this.route.snapshot.data.areasInteresse;
    }

    cadastro() {
        this.usuarioService.cadastrar(this.usuario).pipe(finalize(() => this.blockUI.stop())).subscribe(() => {
            this.route.params.subscribe(params => {
                this.pageNotificationService.addSuccessMessage('Operação realizada com sucesso.');
                if (params['acao'] || params['perfil']) {
                    this.voltar();
                    return;
                }
                this.router.navigate(['/'])
            });
        });
    }

    cadastroExterno() {
        this.usuarioService.cadastroExterno(this.usuario).pipe(finalize(() => this.blockUI.stop()))
            .subscribe(() => {
                this.pageNotificationService.addSuccessMessage(this.translateService.instant('CADASTRO_FINALIZADO'));
                this.router.navigate(['/login']);
            });
    }

    entradaTelefone(event) {
        if (event.key == '+' && this.usuario.telefone.match(/[+]/) && this.usuario.telefone.length > 1) {
            this.usuario.telefone = this.usuario.telefone.substr(0, (this.usuario.telefone.length - 1));
        }
        this.verificarCaractere();
        if (this.usuario.telefone.indexOf("++") != -1) {
            this.usuario.telefone = null;
        }
    }

    exibirCampos() {
        return !this.isMeusDados() && !this.isGestor();
    }

    exibirCamposInfoAdicional() {
        return !this.isCadastroExterno() && !this.isMeusDados() && !this.isGestor();
    }

    isDisabled() {
        return AcaoEnum.VISUALIZAR == this.acao;
    }

    isEmpreendedor() {
        return this.perfil.nome == PerfilEnum.EMPREENDEDOR;
    }

    isGestor() {
        return this.perfil.nome == PerfilEnum.GESTOR;
    }

    isInvestidor() {
        return this.perfil.nome == PerfilEnum.INVESTIDOR;
    }

    isMeusDados() {
        return window.location.hash.startsWith('#/usuario/meus-dados');
    }

    isCadastroExterno() {
        return window.location.hash.startsWith('#/cadastro-usuario');
    }

    removerTelefone(rowData, ngForm: NgForm) {
        ngForm.controls['telefone'].markAsDirty();
        this.usuario.telefones.splice(this.usuario.telefones.indexOf(rowData), 1);
    }

    salvar(form) {
        if (!form.valid) {
            return;
        }
        this.blockUI.start(Constants.SALVANDO);
        if (this.isCadastroExterno()) {
            this.cadastroExterno();
            return;
        }
        this.cadastro();
    }

    validarCampos(ngForm) {
        if (!ngForm.valid) {
            return true;
        }
        if (!this.isCadastroExterno() && !this.usuario.informacaoAdicional && !this.isGestor()) {
            return true;
        }
        if (this.isInvestidor()) {
            if (!this.usuario.passaporte && !this.usuario.cpf) {
                return true;
            }
        }
        if (this.isEmpreendedor()) {
            if (!this.usuario.passaporte && !this.usuario.cpf && !this.usuario.cnpj && !this.usuario.rne) {
                return true;
            }
        }
        return false;
    }

    verificarAcao() {
        if (this.isMeusDados()) {
            return;
        }
        this.route.params.subscribe(params => {
            if (params['acao']) {
                this.acao = params['acao'];
                this.breadcrumbService.setItems([{label: `${AcaoEnum.breadcrumbItem(this.acao)} Usuário`}]);
                return;
            }
            this.breadcrumbService.setItems([{label: `${AcaoEnum.breadcrumbItem('cadastrar')} Usuário`}]);
        });
    }

    verificarCaractere() {
        if (this.usuario.telefone) {
            this.usuario.telefone = this.usuario.telefone.replace(/[^+\d]/, '');
        }
    }

    verificarCampo(valor, tipo) {
        if (valor) {
            this.blockUI.start(Constants.CARREGANDO);
            this.usuarioService.validarCampo(new CampoValidacao(valor, tipo, this.usuario.id))
                .pipe(finalize(() => this.blockUI.stop())).subscribe();
        }
    }

    verificarEmail(valor, tipo, form) {
        if (!form.controls['email'].valid) {
            return this.pageNotificationService.addErrorMessage('Dado Inválido.');
        }
        this.verificarCampo(valor, tipo);
    }

    verificarTelefones() {
        return this.usuario.telefones && this.usuario.telefones.length > 4 || AcaoEnum.VISUALIZAR == this.acao;
    }

    voltar() {
        if (this.isMeusDados()) {
            this.router.navigate(['']);
            return;
        }
        if (this.isCadastroExterno()) {
            this.router.navigate(['login']);
            return;
        }
        this.router.navigate(['usuario']);
    }

    mostrarParaGestor() {
        if (!this.isGestor() && this.isMeusDados() || !this.isMeusDados()) {
            return true;
        }
        return false;
    }
}
