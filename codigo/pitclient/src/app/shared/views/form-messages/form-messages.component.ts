import {Component, Input, OnInit} from '@angular/core';
import {AbstractControl} from "@angular/forms";

@Component({
    selector: 'app-form-messages',
    templateUrl: './form-messages.component.html'
})
export class AppFormMessagesComponent implements OnInit {

    @Input() control: AbstractControl;
    @Input() messageParams: any;

    constructor() {
    }

    ngOnInit() {
    }

    needShowError(){
        return this.control.invalid && this.control.dirty;
    }

    /**
     *
     * @returns {string[]}
     */
    getErrors(): string[] {
        return this.control.errors
            ? Object.keys(this.control.errors)
            : [];
    }

    getErrorName(error) {
        return 'VALIDACOES.' + error.toUpperCase();
    }

    getParams(error) {
        return this.messageParams && this.messageParams[error]
            ? this.messageParams[error]
            : {};
    }

}
