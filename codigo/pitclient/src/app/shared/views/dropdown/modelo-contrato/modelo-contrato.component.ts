import {Component, forwardRef, Injector, Input, ViewEncapsulation} from '@angular/core';
import {NG_VALUE_ACCESSOR} from '@angular/forms';
import {DropdownComponent} from "../dropdown.component";
import {ProjetoApp} from "../../../models/app.models";
import Option = ProjetoApp.Option;
import Pais = ProjetoApp.Pais;
import {TranslateService} from "@ngx-translate/core";

@Component({
    selector: 'app-dropdown-modelo-contrato',
    templateUrl: '../dropdown.component.html',
    encapsulation: ViewEncapsulation.None,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => DropdownModeloContratoComponent),
            multi: true
        }
    ]
})
export class DropdownModeloContratoComponent extends DropdownComponent {

    @Input() public attributeId = 'app-form-select-interlocutor';

    @Input() nullItem: Option = new Option(null, this.translateService.instant('SELECIONE'));

    @Input() firstItem: Option = null;

    @Input() lastItem: Option = null;

    /**
     *
     * @param {Injector} injector
     */
    public constructor(protected injector: Injector, protected translateService: TranslateService) {
        super();
    }

    /**
     * Inicializa os valores disponíveis para o select
     */
    initOptions(): void {
        this.selectService
            .modeloContratoFiltrar({}, {page: 0, size: 9999, sort: 'nome'})
            .subscribe(
                (optionList: any) => {
                    optionList = optionList.content;

                    optionList = optionList.map(option => {
                        return new Option(option.id, this.translateService.instant('MODELO_CONTRATO.' + option.id));
                    });

                    this.loadSuccessAction(optionList);
                },
                (error) => this.loadStatusErrorAction(error)
            );
    }

}
