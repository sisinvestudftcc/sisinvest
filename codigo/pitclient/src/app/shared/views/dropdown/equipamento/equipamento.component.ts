import {Component, forwardRef, Injector, Input, ViewEncapsulation} from '@angular/core';
import {NG_VALUE_ACCESSOR} from '@angular/forms';
import {DropdownComponent} from "../dropdown.component";
import {ProjetoApp} from "../../../models/app.models";
import Option = ProjetoApp.Option;
import {TranslateService} from "@ngx-translate/core";

@Component({
    selector: 'app-dropdown-equipamento',
    templateUrl: '../dropdown.component.html',
    encapsulation: ViewEncapsulation.None,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => DropdownEquipamentoComponent),
            multi: true
        }
    ]
})
export class DropdownEquipamentoComponent extends DropdownComponent {

    _grupo = null;

    @Input() public attributeId = 'app-form-select-interlocutor';

    @Input() nullItem: Option = new Option(null, this.translateService.instant('SELECIONE'));

    @Input() firstItem: Option = null;

    @Input() lastItem: Option = null;

    @Input() set grupoEquipamento(grupo: number) {
        this._grupo = grupo;
        this.initOptions();
    };

    get grupoEquipamento() {
        return this._grupo;
    }

    /**
     *
     * @param {Injector} injector
     */
    public constructor(protected injector: Injector, protected translateService: TranslateService) {
        super();
    }

    /**
     * Inicializa os valores disponíveis para o select
     */
    initOptions(): void {
        this.selectService
            .equipamentoFiltrar({grupoEquipamento: this.grupoEquipamento}, {page: 0, size: 9999})
            .subscribe(
                (optionList: any) => {
                    optionList = optionList.content;

                    optionList = optionList.map(option => {
                        return new Option(option.id, this.translateService.instant('EQUIPAMENTO.' + option.id));
                    });

                    this.loadSuccessAction(optionList);
                },
                (error) => this.loadStatusErrorAction(error)
            );
    }

}
