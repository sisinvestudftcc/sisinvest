import {Component, forwardRef, Injector, Input, ViewEncapsulation} from '@angular/core';
import {NG_VALUE_ACCESSOR} from '@angular/forms';
import {DropdownComponent} from "../dropdown.component";
import {ProjetoApp} from "../../../models/app.models";
import Option = ProjetoApp.Option;
import Pais = ProjetoApp.Pais;
import {TranslateService} from "@ngx-translate/core";

@Component({
    selector: 'app-dropdown-estado',
    templateUrl: '../dropdown.component.html',
    encapsulation: ViewEncapsulation.None,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => DropdownEstadoComponent),
            multi: true
        }
    ]
})
export class DropdownEstadoComponent extends DropdownComponent {

    @Input() public attributeId = 'app-form-select-interlocutor';

    @Input() nullItem: Option = new Option(null, this.translateService.instant('SELECIONE'));

    @Input() firstItem: Option = null;

    @Input() lastItem: Option = null;

    @Input() notStatusList = [];

    /**
     *
     * @param {Injector} injector
     */
    public constructor(protected injector: Injector, protected translateService: TranslateService) {
        super();
    }

    /**
     * Inicializa os valores disponíveis para o select
     */
    initOptions(): void {
        this.selectService
            .estadoFiltrar()
            .subscribe(
                (optionList: any) => {
                    optionList = optionList.content;

                    optionList = optionList.map(option => {
                        return new Option(option.id, option.nome);
                    });

                    this.loadSuccessAction(optionList);
                },
                (error) => this.loadStatusErrorAction(error)
            );
    }

}
