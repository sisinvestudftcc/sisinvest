import {AfterViewInit, EventEmitter, Injector, Input, OnInit, Output, ViewChild} from '@angular/core';

import {DominioService} from "../../../projetos/dominio.service";
import {ControlValueAccessor} from "@angular/forms";
import {ProjetoApp} from "../../models/app.models";
import Option = ProjetoApp.Option;
import {Dropdown} from "primeng";

export abstract class DropdownComponent implements OnInit, AfterViewInit, ControlValueAccessor {

    protected abstract injector: Injector;

    @ViewChild('dropdown') dropdownComponent: Dropdown;

    @Output() public onSearch: EventEmitter<Option[]> = new EventEmitter();

    @Input() public abstract attributeId: string;

    @Input() disabled = false;

    @Input() group = false;

    @Input() title;

    @Input() label;

    @Input() nullItem: Option = new Option(null, 'Selecione');

    @Input() firstItem: Option = new Option(null, '');

    @Input() lastItem: Option = new Option(null, '');

    @Input() autoDisplayFirst: boolean = false;

    protected isFilled = false;

    search: any;

    errorMessage: string;

    optionList: Option[] = [];

    _value: any;

    onChange = (_: any) => {
    };
    onTouched = () => {
    };

    /**
     *
     * @returns {DominioService}
     */
    protected get selectService() {
        return this.injector.get(DominioService);
    };

    get value() {
        if(this.dropdownComponent) {
            this.dropdownComponent.updateFilledState();
        }
        return this._value;
    }

    set value(value) {
        this.writeValue(value);
    }

    ngAfterViewInit() {
        this.initOptions();
    }

    ngOnInit() {
        this.initConfig();
    }

    /**
     * Inicializa o componente com um valor e é utilizado para informar o cliente do componente que o valor foi alterado
     * Utilizado em conjunto ao modulo de formulário escolhido (reativo ou comum)
     *
     * @param value
     */
    writeValue(value: any): void {
        this._value = value;
        this.onChange(value);
    }

    registerOnChange(fn: (value: any) => any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: () => any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }

    /**
     * Inicializa as configurações da combo
     */
    initConfig() {

    }

    /**
     * Inicializa os valores disponíveis para o select
     */
    abstract initOptions();

    /**
     * Ação executada caso os options sejam carregadas com sucesso
     *
     * @param {Option[]} optionList
     */
    loadSuccessAction(optionList: Option[]) {

        if (this.firstItem) {
            optionList = this.removeOption(optionList, this.firstItem);
            optionList.unshift(this.firstItem);
        }

        if (this.lastItem) {
            optionList = this.removeOption(optionList, this.lastItem);
            optionList.push(this.lastItem);
        }

        if (this.nullItem) {
            optionList = this.removeOption(optionList, this.nullItem);
            optionList.unshift(this.nullItem);
        }

        this.optionList = optionList;

        this.errorMessage = '';

        this.onSearch.emit(optionList);
    }

    private removeOption(optionList: Option[], option: Option): Option[] {
        let index = optionList.findIndex(searchOption => searchOption.value === option.value);
        if (index > -1) {
            optionList.splice(index, 1);
        }
        return optionList;
    }

    /**
     * Ação executada caso ocorra algum problema ao carregar as linguagens
     *
     * @param error
     */
    loadStatusErrorAction(error) {
        this.errorMessage = error.message;
    }

}
