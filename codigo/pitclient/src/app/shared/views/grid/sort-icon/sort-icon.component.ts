import {Component} from "@angular/core";
import {SortIcon} from "primeng";

@Component({
    selector: 'app-grid-sort-icon',
    templateUrl: 'sort-icon.component.html'
})
export class GridSortIconComponent extends SortIcon {

}
