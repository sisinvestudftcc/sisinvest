import {
    Component,
    ContentChild,
    ElementRef,
    EventEmitter,
    Input,
    OnInit,
    Output,
    TemplateRef, ViewChild,
    ViewEncapsulation
} from "@angular/core";
import {Coluna} from "../../models/coluna";
import {Observable} from "rxjs";
import {LazyLoadEvent, Table} from "primeng";
import {BlockUI, NgBlockUI} from "ng-block-ui";
import {Constants} from "../../utils/constants";
import {TranslateService} from "@ngx-translate/core";

@Component({
    selector: 'app-grid',
    templateUrl: 'grid.component.html',
    styleUrls: ['./grid.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class GridComponent implements OnInit {

    @ViewChild('dt') tableComponent: Table;

    @BlockUI() blockUI: NgBlockUI;

    @ContentChild('acoesGrid')
    acoesGrid: TemplateRef<ElementRef>;

    @Input()
    mostrarAcoes = true;

    @Input()
    titulo = '';

    @Input()
    mostrarPaginacao = true;

    @Input()
    desabilitarOrdenacao = false;

    @Input()
    quantidadePorPagina = 30;

    @Input()
    campoOrdenacao: string = '';

    @Input()
    modoOrdenacao: number = 1;

    @Input()
    colunas: Coluna[] = [];

    @Input()
    filtros = {};

    @Input()
    selecionarLinha: boolean = false;

    @Input()
    callbackBuscarDados: (evento) => Observable<any>;

    @Input()
    mensagemSemResultados = this.translateService.instant('SEM_REGISTROS');

    @Output()
    onIniciarConsulta: EventEmitter<any> = new EventEmitter();

    @Output()
    onFinalizarConsulta: EventEmitter<any> = new EventEmitter();

    @Output()
    checkboxMarcado: EventEmitter<{ item: any, index: number }> = new EventEmitter();

    @Output()
    itemSelecionadoEvent: EventEmitter<any> = new EventEmitter();

    lista: any[];

    mensagemDeErro: any[] = [];

    estaCarregando: boolean = true;

    quantidadeRegistros: number;

    public itensSelecionados = [];

    constructor(
        protected translateService: TranslateService
    ) {

    }

    ngOnInit() {

    }

    lazyLoad(evento: LazyLoadEvent): void {

        this.blockUI.start(Constants.CARREGANDO);

        const page = (evento.first / evento.rows);

        const sortOrder = evento.sortOrder === 1 ? 'ASC' : 'DESC';

        const sort = evento.sortField
            ? `${evento.sortField},${sortOrder}`
            : "";

        this.quantidadePorPagina = evento.rows;

        this.estaCarregando = true;

        this.mensagemDeErro = [];

        this.onIniciarConsulta.emit(true);

        if (this.callbackBuscarDados) {
            this.callbackBuscarDados({
                filtros: this.filtros,
                paginacao: {
                    page: page,
                    size: this.quantidadePorPagina,
                    sort: sort
                }
            }).subscribe(
                resposta => this.tratarDados(resposta, evento),
                erro => this.tratarErro(erro)
            );
        }

    }

    tratarDados(resposta: any, evento: LazyLoadEvent): void {

        this.estaCarregando = false;
        this.lista = resposta.content;
        this.quantidadeRegistros = resposta.totalElements;
        this.onFinalizarConsulta.emit(resposta);
        this.blockUI.stop()

    }

    tratarErro(erro: string | any): void {

        this.estaCarregando = false;
        this.lista = [];
        this.quantidadeRegistros = 0;

        const mensagemErro = Array.isArray(erro)
            ? erro.map(error => error.message).join(' || ')
            : erro.message;

        this.mensagemDeErro = [
            {
                gravidade: 'error',
                resumo: 'Erro ao carregar: ',
                detalhamento: mensagemErro
            }
        ]

        this.onFinalizarConsulta.emit(this.mensagemDeErro);
        this.blockUI.stop()

    }

    checkboxAlterado(item: any, index: number) {
        this.checkboxMarcado.emit({item, index});
    }

    colunaComum(coluna) {
        return !coluna.tipo || coluna.tipo === 'texto';
    }

    colunaHtml(coluna) {
        return coluna.tipo && coluna.tipo === 'html';
    }

    colunaCheckbox(coluna) {
        return coluna.tipo && coluna.tipo === 'checkbox';
    }

    colunaIcone(coluna) {
        return coluna.tipo && coluna.tipo === 'icone';
    }

    public reset() {
        this.tableComponent.reset();
    }

    itemSelecionado() {
        this.itemSelecionadoEvent.emit(this.itensSelecionados);
    }
}
