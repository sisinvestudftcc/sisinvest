import {Component, Input} from '@angular/core';
import {NgForm} from '@angular/forms';

@Component({
    selector: 'app-dropdown-label',
    templateUrl: './dropdown-label.component.html',
    styleUrls: ['./dropdown-label.component.scss']
})
export class DropdownLabelComponent {

    @Input() field = null;

    @Input() form: NgForm;

    @Input() label = '';

    @Input() labelObrigatorio = 'Campo obrigatório';

    @Input() validate: any;

    @Input() options: any[];

    @Input() selected: any = null;

    @Input() required = false;

    validaMensagem() {
        return this.form.controls[this.field] && !this.form.controls[this.field].value && this.form.submitted;
    }
}
