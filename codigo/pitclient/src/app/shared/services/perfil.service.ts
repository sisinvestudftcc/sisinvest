import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {Perfil} from "../../models/perfil";

@Injectable({providedIn: "root"})
export class PerfilService {

    baseUrl = `${environment.pitsegurancaUrl}/public/perfil`;

    constructor(private http: HttpClient) {
    }

    buscarPerfil(filtro): Observable<any> {
        return this.http.post<Perfil[]>(this.baseUrl, filtro);
    }
}
