import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {environment} from "../../../environments/environment";

@Injectable({providedIn: "root"})
export class RecuperarSenhaService {

    baseUrl = `${environment.pitsegurancaUrl}/recuperar-senha`;

    constructor(private http: HttpClient) {
    }

    gerarSenha(email): Observable<any> {
        return this.http.post(this.baseUrl, null, {params: {email: email}});
    }
}
