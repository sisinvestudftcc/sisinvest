import {Injectable} from "@angular/core";
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {AreaInteresse} from "../../models/area-interesse";

@Injectable({providedIn: "root"})
export class AreaInteresseService {

    baseUrl = `${environment.pitsegurancaUrl}/public/area-interesse`;

    constructor(private http: HttpClient) {
    }

    buscarTodos() {
        return this.http.get<AreaInteresse[]>(this.baseUrl);
    }
}
