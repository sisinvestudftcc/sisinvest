import {Injectable} from '@angular/core';
import {FormArray, FormGroup} from "@angular/forms";

@Injectable({
    providedIn: 'root'
})
export class FormUtilsService {

    constructor() {
    }

    validate(formGroup: FormGroup | FormArray): boolean {

        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            control.markAsDirty();
            if (control instanceof FormGroup || control instanceof FormArray) {
                this.validate(control);
            }
        });

        return formGroup.valid;

    }

    /**
     * Verifica se ocorreu alguma alteração no formulário
     *
     * @param {FormGroup} form
     * @param {any} formPopulatedValues
     */
    hasChanges(form: FormGroup, formPopulatedValues: any): boolean {

        if (form) {

            const formDataDefault = Object.assign({}, formPopulatedValues);
            const formData = Object.assign({}, form.value);

            return JSON.stringify(formDataDefault, this.replacerEmptyNull) !== JSON.stringify(formData, this.replacerEmptyNull);

        } else {

            return false;

        }

    }

    replacerEmptyNull(key: any, value: any): any {

        value = value === '' || value === undefined ? null : value;
        return value;

    }

    convertToFormData(data: any, form = null, namespace = ''): FormData {

        let fd = form || new FormData();
        let formKey;

        if (data) {
            Object.keys(data).forEach((key) => {

                if (data.hasOwnProperty(key)) {

                    if (namespace) {
                        formKey = namespace + '[' + key + ']';
                    } else {
                        formKey = key;
                    }

                    if (typeof data[key] === 'object' && !(data[key] instanceof File) && !(data[key] instanceof Date)) {

                        const space = namespace ? `${namespace}[${key}]` : key;

                        this.convertToFormData(data[key], fd, space);

                    } else if (!(data[key] instanceof File)) {
                        fd.append(formKey, data[key]);
                    } else if((data[key] instanceof File)) {
                        fd.append(formKey, data[key].name);
                        fd.append('arquivosUpload', data[key]);
                    }

                }

            });
        }

        return fd;
    }


}
