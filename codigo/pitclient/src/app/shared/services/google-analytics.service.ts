import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {AreaInteresse} from "../../models/area-interesse";
import {environment} from "../../../environments/environment.prod";
declare let gtag: Function;

@Injectable({providedIn: "root"})
export class GoogleAnalyticsService {

    baseUrl = `${environment.pitsegurancaUrl}/public/area-interesse`;

    analyticsKey = "";

    constructor(private http: HttpClient) {

        let analyticsKey = "";

        switch(window.location.hostname) {
            case environment.dnsSistema.hlog:
                analyticsKey = environment.googleAnalytics.hlogKey
                break;
            case environment.dnsSistema.prod:
                analyticsKey = environment.googleAnalytics.prodKey
                break;
            default:
                analyticsKey = environment.googleAnalytics.key
                break;
        }

        this.analyticsKey = analyticsKey;

    }

    public dispararAcesso(url: string) {
        gtag('config', this.analyticsKey, {'page_path':url});
    }
}
