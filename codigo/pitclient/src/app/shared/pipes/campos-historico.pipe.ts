import {Pipe, PipeTransform} from "@angular/core";
import {HistoricoEnum} from "../enumerations/historico.enum";

@Pipe({name: 'camposHistoricoPipe' })
export class CamposHistoricoPipe implements PipeTransform {
    transform(value: any, entidade: string) {
        if (!value) {
            return null;
        }
        return HistoricoEnum.selecionaCampo(entidade, value);
    }
}
