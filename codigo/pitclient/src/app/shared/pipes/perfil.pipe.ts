import {Pipe, PipeTransform} from '@angular/core';
import {StatusEnum} from '../enumerations/status.enum';
import {PerfilEnum} from "../enumerations/perfil.enum";

@Pipe({name: 'perfilPipe' })
export class PerfilPipe implements PipeTransform {
    transform(value: any) {
        if (value == null || value == '') {
            return null;
        }
        return PerfilEnum.selectItem.find( item => item.value == value).label.toUpperCase();
    }
}
