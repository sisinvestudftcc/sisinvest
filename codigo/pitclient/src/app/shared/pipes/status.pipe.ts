import {Pipe, PipeTransform} from '@angular/core';
import {StatusEnum} from '../enumerations/status.enum';

@Pipe({name: 'statusPipe' })
export class StatusPipe implements PipeTransform {
    transform(value: any) {
        if (value == null || value == '') {
            return null;
        }
        return StatusEnum.selectItem.find( item => item.value == value).label;
    }
}
