import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'valorHistoricoPipe'})
export class ValorHistoricoPipe implements PipeTransform {
    transform(value): any {
        if (Array.isArray(value) && value.length > 0) {
            let res = null;
            if (value[0].nome) {
                value.map(it => {
                    if (!res) {
                        res = it.nome;
                    } else {
                        res = `${res} - ${it.nome}`;
                    }
                });
            }
            if (value[0].numero) {
                res = null
                value.map(it => {
                    if (!res) {
                        res = it.numero;
                    } else {
                        res = `${res} - ${it.numero}`;
                    }
                });
            }
            return res;
        }
        return value;
    }
}
