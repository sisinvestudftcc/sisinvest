import {Pipe, PipeTransform} from '@angular/core';

@Pipe({ name: 'historicoPipe' })
export class HistoricoPipe implements PipeTransform {
    transform(fields: any[], campo, campoPesquisa, valor): any {
        if (!valor) {
            return '';
        }
        return fields.find(it => it[campo] == campoPesquisa)[valor];
    }
}
