import {NgModule} from '@angular/core';
import {PRIMENG_IMPORTS} from './primeng-imports';
import {DropdownLabelComponent} from "./views/dropdown-label/dropdown-label.component";
import {InputLabelComponent} from "./views/input-label/input-label.component";
import {StatusPipe} from "./pipes/status.pipe";
import {PerfilPipe} from "./pipes/perfil.pipe";
import {FormularioComponent} from "./views/formulario/formulario.component";
import {ConfirmationService, KeyFilterModule} from "primeng";
import {AreaInteresseResolver} from "./resolvers/area-interesse.resolver";
import {TranslateModule} from "@ngx-translate/core";
import {HistoricoPipe} from "./pipes/historico.pipe";
import {CamposHistoricoPipe} from "./pipes/campos-historico.pipe";
import {ValorHistoricoPipe} from "./pipes/valor-historico.pipe";
import {GridComponent} from "./views/grid/grid.component";
import {GridSortIconComponent} from "./views/grid/sort-icon/sort-icon.component";
import {DropdownPaisComponent} from "./views/dropdown/pais/pais.component";
import {DropdownNaturezaInvestimentoComponent} from "./views/dropdown/natureza-investimento/natureza-investimento.component";
import {DropdownModeloContratoComponent} from "./views/dropdown/modelo-contrato/modelo-contrato.component";
import {DropdownValorProjetoComponent} from "./views/dropdown/valor-projeto/valor-projeto.component";
import {DropdownEmpregosProjetoComponent} from "./views/dropdown/empregos-projeto/empregos-projeto.component";
import {PChipsOnlyNumbersDirective} from "./directive/p-chips-only-numbers.directive";
import {InputFloatNumbersDirective} from "./directive/float-numbers.directive";
import {DropdownGrupoEquipamentoComponent} from "./views/dropdown/grupo-equipamento/grupo-equipamento.component";
import {DropdownEquipamentoComponent} from "./views/dropdown/equipamento/equipamento.component";
import {cnpj} from "./validators/cnpj";
import {email} from "./validators/email";
import {url} from "./validators/url";
import {AppFormMessagesComponent} from "./views/form-messages/form-messages.component";
import {SafeHtmlPipe} from "./pipes/safe-html.pipe";
import {DropdownStatusComponent} from "./views/dropdown/status/status.component";
import {DropdownEstadoComponent} from "./views/dropdown/estado/estado.component";
import {DropdownCidadeComponent} from "./views/dropdown/cidade/cidade.component";
import {InputOnlyNumbersDirective} from "./directive/input-only-numbers.directive";
import {NgxCurrencyModule} from "ngx-currency";
import {CurrencyPipe} from "@angular/common";

export const AppValidators: any = {
    url,
    cnpj,
    email
};


@NgModule({
    declarations: [
        AppFormMessagesComponent,
        PChipsOnlyNumbersDirective,
        InputOnlyNumbersDirective,
        InputFloatNumbersDirective,
        DropdownEquipamentoComponent,
        DropdownGrupoEquipamentoComponent,
        DropdownNaturezaInvestimentoComponent,
        DropdownModeloContratoComponent,
        DropdownValorProjetoComponent,
        DropdownEmpregosProjetoComponent,
        DropdownStatusComponent,
        DropdownEstadoComponent,
        DropdownCidadeComponent,
        DropdownPaisComponent,
        DropdownLabelComponent,
        InputLabelComponent,
        FormularioComponent,
        StatusPipe,
        PerfilPipe,
        HistoricoPipe,
        SafeHtmlPipe,
        CamposHistoricoPipe,
        ValorHistoricoPipe,
        GridComponent,
        GridSortIconComponent
    ],
    imports: [
        NgxCurrencyModule,
        PRIMENG_IMPORTS,
        KeyFilterModule,
        TranslateModule,
    ],
    providers: [
        CurrencyPipe,
        AreaInteresseResolver,
        ConfirmationService
    ],
    exports: [
        NgxCurrencyModule,
        PRIMENG_IMPORTS,
        DropdownLabelComponent,
        InputLabelComponent,
        FormularioComponent,
        GridComponent,
        StatusPipe,
        PerfilPipe,
        HistoricoPipe,
        SafeHtmlPipe,
        DropdownPaisComponent,
        DropdownNaturezaInvestimentoComponent,
        DropdownModeloContratoComponent,
        DropdownValorProjetoComponent,
        DropdownEmpregosProjetoComponent,
        InputOnlyNumbersDirective,
        PChipsOnlyNumbersDirective,
        InputFloatNumbersDirective,
        DropdownGrupoEquipamentoComponent,
        DropdownEquipamentoComponent,
        AppFormMessagesComponent,
        DropdownStatusComponent,
        DropdownEstadoComponent,
        DropdownCidadeComponent
    ]
})
export class SharedModule {
}
