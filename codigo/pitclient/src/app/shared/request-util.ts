import {HttpParams} from '@angular/common/http';
import {Table} from "primeng";

export class RequestUtil {
    static getRequestParams = (datatable: Table): HttpParams => {
        let params: HttpParams = new HttpParams();
        if (datatable == null) {
            return params;
        }
        const rows = datatable.rows ? datatable.rows : 10;
        params = params.append('page', Math.round(datatable.first / rows).toString());
        params = params.append('size', rows.toString());
        const direction = datatable.sortOrder === 1 ? 'ASC' : 'DESC';
        params = params.append('sort', datatable.sortField == null ? '' : datatable.sortField + ',' + direction);
        return params;
    }
}
