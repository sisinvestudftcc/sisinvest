import {Injectable} from "@angular/core";
import {Resolve} from "@angular/router";
import {BlockUI, NgBlockUI} from "ng-block-ui";
import {AreaInteresseService} from "../services/area-interesse.service";
import {Constants} from "../utils/constants";
import {finalize} from "rxjs/operators";

@Injectable()
export class AreaInteresseResolver implements Resolve<Promise<any>> {
    @BlockUI() blockUI: NgBlockUI;

    constructor(public service: AreaInteresseService) {
    }

    resolve() {
        this.blockUI.start(Constants.CARREGANDO);
        return new Promise(res => {
            this.service.buscarTodos().pipe(finalize(() => this.blockUI.stop()))
                .subscribe(data => res(data));
        });
    }
}
