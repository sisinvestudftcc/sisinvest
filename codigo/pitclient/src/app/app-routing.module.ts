import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DiarioErrosComponent} from './components/diario-erros/diario-erros.component';
import {AuthGuard, LoginSuccessComponent} from '@nuvem/angular-base';
import {LoginInternoComponent} from "./login/interno/login-interno.component";
import {MainComponent} from "./components/main/main.component";
import {UsuarioExternoFormComponent} from "./usuario-externo-form/usuario-externo-form.component";
import {AreaInteresseResolver} from "./shared/resolvers/area-interesse.resolver";

const routes: Routes = [
    {
        path: 'login',
        component: LoginInternoComponent,
        children: [
            {path: '', loadChildren: './login/login.module#LoginModule'}
        ]
    },
    {
        path: 'cadastro-usuario/:perfil',
        component: UsuarioExternoFormComponent,
        resolve: {
            areasInteresse: AreaInteresseResolver
        },
        children: [
            {path: '', loadChildren: './usuario-externo-form/usuario-externo.module#UsuarioExternoModule'}
        ]
    },
    {
        path: '',
        component: MainComponent,
        children: [
            {path: 'login-success', component: LoginSuccessComponent},
            {path: 'diario-erros', component: DiarioErrosComponent, data: {breadcrumb: 'Diário de Erros'}},
            {path: 'usuario', loadChildren: './gerenciar-usuario/usuario.module#UsuarioModule', canActivate: [AuthGuard]},
            {path: 'projetos', loadChildren: './projetos/projetos.module#ProjetosModule', canActivate: [AuthGuard]},
            {path: 'central-comunicacao', loadChildren: './central-comunicacao/central-comunicacao.module#CentralComunicacaoModule', canActivate: [AuthGuard]},
            {path: 'minha-senha', loadChildren: './minha-senha/minha-senha.module#MinhaSenhaModule', canActivate: [AuthGuard]},
        ]
    },
    { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
