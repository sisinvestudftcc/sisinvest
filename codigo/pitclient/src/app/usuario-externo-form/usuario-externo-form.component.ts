import {Component, OnInit} from '@angular/core';
import {Perfil} from "../models/perfil";
import {Usuario} from "../models/usuario";
import {ActivatedRoute, Router} from "@angular/router";
import {PerfilEnum} from "../shared/enumerations/perfil.enum";
import {Endereco} from "../models/endereco";

@Component({
    selector: 'app-usuario-externo-form',
    templateUrl: 'usuario-externo-form.component.html'
})

export class UsuarioExternoFormComponent implements OnInit {
    perfil: Perfil;
    perfilEnum = PerfilEnum;
    usuario: Usuario;

    constructor(private router: Router,
                private route: ActivatedRoute,) {
    }

    ngOnInit() {
        this.perfil = new Perfil();
        this.usuario = new Usuario();
        this.usuario.endereco = new Endereco();
        this.usuario.telefones = [];
        this.route.params.subscribe(params => {
            this.verificarPerfil(params['perfil']);
            this.perfil.nome = params['perfil'].toUpperCase();
            this.usuario.perfilEnum = this.perfil.nome;
        });
    }

    cadastroInvestidor() {
        return this.perfilEnum.INVESTIDOR == this.usuario.perfilEnum;
    }

    verificarPerfil(perfil) {
        const listaPerfil = [PerfilEnum.GESTOR, PerfilEnum.EMPREENDEDOR, PerfilEnum.INVESTIDOR];
        if (!listaPerfil.includes(perfil.toUpperCase())) {
            this.router.navigate(['']);
        }
    }
}
