import {Routes} from "@angular/router";
import {UsuarioExternoFormComponent} from "./usuario-externo-form.component";
import {AreaInteresseResolver} from "../shared/resolvers/area-interesse.resolver";

export const UsuarioExternoRoute: Routes = [
    {
        path: ':perfil',
        component: UsuarioExternoFormComponent,
        resolve: {
            areasInteresse: AreaInteresseResolver
        }
    }
]
