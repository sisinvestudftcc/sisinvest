import {NgModule} from '@angular/core';
import {UsuarioExternoFormComponent} from './usuario-externo-form.component';
import {RouterModule} from "@angular/router";
import {UsuarioExternoRoute} from "./usuario-externo.route";
import {SharedModule} from "../shared/shared.module";
import {PRIMENG_IMPORTS} from "../shared/primeng-imports";
import {ConfirmationService, KeyFilterModule} from "primeng";
import {AreaInteresseResolver} from "../shared/resolvers/area-interesse.resolver";
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
    imports: [
        RouterModule.forChild(UsuarioExternoRoute),
        PRIMENG_IMPORTS,
        SharedModule,
        KeyFilterModule,
        TranslateModule
    ],
    exports: [],
    declarations: [
        UsuarioExternoFormComponent
    ],
    providers: [
        AreaInteresseResolver,
        ConfirmationService
    ],
})
export class UsuarioExternoModule {
}
