// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.

export const environment = {
    version: '1.0.0',
    production: false,
    apiUrl: '/api',
    pitprojetoUrl: '/pitprojeto/api',
    pitsegurancaUrl: '/pitseguranca/api',
    googleMapsUrl: 'https://maps.googleapis.com/maps/api/geocode/json',
    googleMapsKey: 'AIzaSyADJbTqudNqz8UKXQqiYI28GsDl1MDFfUc',
    dnsSistema: {
        hlog: 'pit.hlog.turismo.gov.br',
        prod: 'pit.turismo.gov.br',
    },
    googleAnalytics: {
        key: 'UA-121850035-2',
        hlogKey: 'UA-194442384-1',
        prodKey: 'UA-194442384-2',
    },
    auth: {
        baseUrl: '',
        authUrl: '/#/login',
        loginUrl: '/#/login',
        logoutUrl: '/pitseguranca/api/logout',
        detailsUrl: '/pitseguranca/api/user/details',
        tokenValidationUrl: '/api/token/validate',
        storage: localStorage,
        tokenStorageIndex: 'token',
        userStorageIndex: 'user',
        loginSuccessRoute: ''
    }
};
