export const environment = {
    version: '1.0.0',
    production: true,
    apiUrl: '/api',
    pitprojetoUrl: '/pitprojeto/api',
    pitsegurancaUrl: '/pitseguranca/api',
    googleMapsUrl: 'https://maps.googleapis.com/maps/api/geocode/json',
    googleMapsKey: 'AIzaSyADJbTqudNqz8UKXQqiYI28GsDl1MDFfUc',
    dnsSistema: {
        hlog: 'homologacao.sisinvest.com.br',
        prod: 'sisinvest.com.br',
    },
    googleAnalytics: {
        key: 'UA-121850035-2',
        hlogKey: 'UA-194442384-1',
        prodKey: 'UA-194442384-2',
    },
    auth: {
        baseUrl: '',
        authUrl: '/#/login',
        loginUrl: '/#/login',
        logoutUrl: '/pitseguranca/api/logout',
        detailsUrl: '/pitseguranca/api/user/details',
        tokenValidationUrl: '/api/token/validate',
        storage: localStorage,
        tokenStorageIndex: 'token',
        userStorageIndex: 'user',
        loginSuccessRoute: ''
    }
};
