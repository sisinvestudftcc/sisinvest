import {by, element} from "protractor";

export class PrimengTable {

    static clickByCellNumber(cellNumber: number) {
        return element.all(by.tagName('td')).get(cellNumber).click();
    }

    static clickByCellText(text: string) {
        return element(by.cssContainingText('td', text)).click();
    }
}
