import {by, element} from "protractor";

export class PrimeDropdown {
    static selectValueText(id: string, value: string) {
        element(by.id(id)).click();
        element(by.cssContainingText('span', value)).click();
    }
}
