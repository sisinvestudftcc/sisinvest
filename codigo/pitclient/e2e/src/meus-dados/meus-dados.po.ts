import {browser, by, element} from 'protractor';
import {PrimengButton, PrimengInputtext} from '@nuvem/protractor-primeng';

export class DadosPage {

    static paginaLogin() {
        return browser.get('/#/login');
    }

    static paginaDados() {
        return browser.get('/#/usuario/meus-dados');
    }

    static paginaAlterarSenha() {
        return browser.get('/#/minha-senha');
    }

    static validarPaginaLoginSucesso() {
        return `${browser.baseUrl}/#/login-success`;
    }

    static validarPaginaDados() {
        return `${browser.baseUrl}/#/usuario/meus-dados`;
    }

    static validarPaginaAlterarSenha() {
        return `${browser.baseUrl}/#/minha-senha`;
    }

    static setarCampoId(id: string, value: string) {
        PrimengInputtext.setInputWithTextByid(id, value);
    }

    static clicarBotao(id: string) {
        PrimengButton.clickById(id);
    }

    static clicarTexto(seletor: string, value: string) {
        element.all(by.cssContainingText(seletor, value)).first().click();
    }

    static fazerLogin() {
        this.setarCampoId('inputEmail', 'usuariotcc@teste.com.br');
        this.setarCampoId('inputSenha', 'B@sis123');
        this.clicarBotao('btnLogin');
    }
}
