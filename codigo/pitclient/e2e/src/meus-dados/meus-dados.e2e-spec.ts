import {DadosPage} from "./meus-dados.po";
import {browser} from "protractor";

describe('Funcionalidade de Meus Dados', () => {
    let pagina: DadosPage;

    beforeEach(() => {
        pagina = new DadosPage();
    });

    it('01. Meus Dados Empreendedor', async () => {
        DadosPage.paginaLogin();
        DadosPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaLoginSucesso());
        DadosPage.paginaDados();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaDados());
        DadosPage.setarCampoId('inputTelefone', '31999999999');
        DadosPage.clicarBotao('btnAdd');
        DadosPage.clicarBotao('btnSalvar');
    });

    it('01.1. Meus Dados Empreendedor - Adicionar telefone', async () => {
        DadosPage.paginaLogin();
        DadosPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaLoginSucesso());
        DadosPage.paginaDados();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaDados());
        DadosPage.setarCampoId('inputTelefone', '31999999999');
        DadosPage.clicarBotao('btnAdd');
    });

    it('01.2. Meus Dados Empreendedor - Excluir telefone', async () => {
        DadosPage.paginaLogin();
        DadosPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaLoginSucesso());
        DadosPage.paginaDados();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaDados());
        DadosPage.setarCampoId('inputTelefone', '31999999999');
        DadosPage.clicarBotao('btnAdd');
        DadosPage.clicarBotao('btnRemoveTel');
    });

    it('02. Meus Dados Empreendedor - Duplicidade', async () => {
        DadosPage.paginaLogin();
        DadosPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaLoginSucesso());
        DadosPage.paginaDados();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaDados());
        DadosPage.setarCampoId('inputEmail', 'email@email.email');
        DadosPage.setarCampoId('inputTelefone', '');
    });

    it('03. Meus Dados Empreendedor - E-mail inválido', async () => {
        DadosPage.paginaLogin();
        DadosPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaLoginSucesso());
        DadosPage.paginaDados();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaDados());
        DadosPage.setarCampoId('inputEmail', 'email');
        DadosPage.setarCampoId('inputTelefone', '');
    });

    it('03.1 Meus Dados Empreendedor - Linkedin inválido', async () => {
        DadosPage.paginaLogin();
        DadosPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaLoginSucesso());
        DadosPage.paginaDados();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaDados());
        DadosPage.setarCampoId('inputLinkedin','linkedin');
        DadosPage.setarCampoId('inputTelefone', '');
    });

    it('04. Meus Dados Empreendedor - Cancelar - Sem Dados', async () => {
        DadosPage.paginaLogin();
        DadosPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaLoginSucesso());
        DadosPage.paginaDados();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaDados());
        DadosPage.clicarBotao('btnCancelar');
    });

    it('05. Meus Dados Empreendedor - Cancelar - Com Dados - SIM', async () => {
        DadosPage.paginaLogin();
        DadosPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaLoginSucesso());
        DadosPage.paginaDados();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaDados());
        DadosPage.setarCampoId('inputTelefone', '0');
        DadosPage.clicarBotao('btnCancelar');
        DadosPage.clicarTexto('button', 'SIM');
    });

    it('05.1 Meus Dados Empreendedor - Cancelar - Com Dados - NÃO', async () => {
        DadosPage.paginaLogin();
        DadosPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaLoginSucesso());
        DadosPage.paginaDados();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaDados());
        DadosPage.setarCampoId('inputTelefone', '0');
        DadosPage.clicarBotao('btnCancelar');
        DadosPage.clicarTexto('button', 'NÃO');
    });

    it('06. Meus Dados Empreendedor - Obrigatoriedade', async () => {
        DadosPage.paginaLogin();
        DadosPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaLoginSucesso());
        DadosPage.paginaDados();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaDados());
        DadosPage.setarCampoId('inputEmail', '');
    });

    it('01. Meus Dados Gestor', async () => {
        DadosPage.paginaLogin();
        DadosPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaLoginSucesso());
        DadosPage.paginaDados();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaDados());
        DadosPage.setarCampoId('inputTelefone', '31999999999');
        DadosPage.clicarBotao('btnAdd');
        DadosPage.clicarBotao('btnSalvar');
    });

    it('01.1. Meus Dados Gestor - Adicionar telefone', async () => {
        DadosPage.paginaLogin();
        DadosPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaLoginSucesso());
        DadosPage.paginaDados();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaDados());
        DadosPage.setarCampoId('inputTelefone', '31999999999');
        DadosPage.clicarBotao('btnAdd');
    });

    it('01.2. Meus Dados Gestor - Excluir telefone', async () => {
        DadosPage.paginaLogin();
        DadosPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaLoginSucesso());
        DadosPage.paginaDados();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaDados());
        DadosPage.setarCampoId('inputTelefone', '31999999999');
        DadosPage.clicarBotao('btnAdd');
        DadosPage.clicarBotao('btnRemoveTel');
    });

    it('02. Meus Dados Gestor - Duplicidade', async () => {
        DadosPage.paginaLogin();
        DadosPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaLoginSucesso());
        DadosPage.paginaDados();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaDados());
        DadosPage.setarCampoId('inputEmail', 'email@email.email');
        DadosPage.setarCampoId('inputTelefone', '');
    });

    it('03. Meus Dados Gestor - E-mail inválido', async () => {
        DadosPage.paginaLogin();
        DadosPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaLoginSucesso());
        DadosPage.paginaDados();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaDados());
        DadosPage.setarCampoId('inputEmail', 'email');
        DadosPage.setarCampoId('inputTelefone', '');
    });

    it('04. Meus Dados Gestor - Cancelar - Sem Dados', async () => {
        DadosPage.paginaLogin();
        DadosPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaLoginSucesso());
        DadosPage.paginaDados();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaDados());
        DadosPage.clicarBotao('btnCancelar');
    });

    it('05. Meus Dados Gestor - Cancelar - Com Dados - SIM', async () => {
        DadosPage.paginaLogin();
        DadosPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaLoginSucesso());
        DadosPage.paginaDados();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaDados());
        DadosPage.setarCampoId('inputTelefone', '0');
        DadosPage.clicarBotao('btnCancelar');
        DadosPage.clicarTexto('button', 'SIM');
    });

    it('05.1 Meus Dados Gestor - Cancelar - Com Dados - NÃO', async () => {
        DadosPage.paginaLogin();
        DadosPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaLoginSucesso());
        DadosPage.paginaDados();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaDados());
        DadosPage.setarCampoId('inputTelefone', '0');
        DadosPage.clicarBotao('btnCancelar');
        DadosPage.clicarTexto('button', 'NÃO');
    });

    it('06. Meus Dados Gestor - Obrigatoriedade', async () => {
        DadosPage.paginaLogin();
        DadosPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaLoginSucesso());
        DadosPage.paginaDados();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaDados());
        DadosPage.setarCampoId('inputEmail', '');
    });

    it('01. Meus Dados Investidor', async () => {
        DadosPage.paginaLogin();
        DadosPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaLoginSucesso());
        DadosPage.paginaDados();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaDados());
        DadosPage.setarCampoId('inputTelefone', '31999999999');
        DadosPage.clicarBotao('btnAdd');
        DadosPage.clicarBotao('btnSalvar');
    });

    it('01.1. Meus Dados Investidor - Adicionar telefone', async () => {
        DadosPage.paginaLogin();
        DadosPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaLoginSucesso());
        DadosPage.paginaDados();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaDados());
        DadosPage.setarCampoId('inputTelefone', '31999999999');
        DadosPage.clicarBotao('btnAdd');
    });

    it('01.2. Meus Dados Investidor - Excluir telefone', async () => {
        DadosPage.paginaLogin();
        DadosPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaLoginSucesso());
        DadosPage.paginaDados();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaDados());
        DadosPage.setarCampoId('inputTelefone', '31999999999');
        DadosPage.clicarBotao('btnAdd');
        DadosPage.clicarBotao('btnRemoveTel');
    });

    it('02. Meus Dados Investidor - Duplicidade', async () => {
        DadosPage.paginaLogin();
        DadosPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaLoginSucesso());
        DadosPage.paginaDados();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaDados());
        DadosPage.setarCampoId('inputEmail', 'email@email.email');
        DadosPage.setarCampoId('inputTelefone', '');
    });

    it('03. Meus Dados Investidor - E-mail inválido', async () => {
        DadosPage.paginaLogin();
        DadosPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaLoginSucesso());
        DadosPage.paginaDados();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaDados());
        DadosPage.setarCampoId('inputEmail', 'email');
        DadosPage.setarCampoId('inputTelefone', '');
    });

    it('03.1 Meus Dados Investidor - Linkedin inválido', async () => {
        DadosPage.paginaLogin();
        DadosPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaLoginSucesso());
        DadosPage.paginaDados();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaDados());
        DadosPage.setarCampoId('inputLinkedin','linkedin');
        DadosPage.setarCampoId('inputTelefone', '');
    });

    it('04. Meus Dados Investidor - Cancelar - Sem Dados', async () => {
        DadosPage.paginaLogin();
        DadosPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaLoginSucesso());
        DadosPage.paginaDados();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaDados());
        DadosPage.clicarBotao('btnCancelar');
    });

    it('05. Meus Dados Investidor - Cancelar - Com Dados - SIM', async () => {
        DadosPage.paginaLogin();
        DadosPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaLoginSucesso());
        DadosPage.paginaDados();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaDados());
        DadosPage.setarCampoId('inputTelefone', '0');
        DadosPage.clicarBotao('btnCancelar');
        DadosPage.clicarTexto('button', 'SIM');
    });

    it('05.1 Meus Dados Investidor - Cancelar - Com Dados - NÃO', async () => {
        DadosPage.paginaLogin();
        DadosPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaLoginSucesso());
        DadosPage.paginaDados();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaDados());
        DadosPage.setarCampoId('inputTelefone', '0');
        DadosPage.clicarBotao('btnCancelar');
        DadosPage.clicarTexto('button', 'NÃO');
    });

    it('06. Meus Dados Investidor - Obrigatoriedade', async () => {
        DadosPage.paginaLogin();
        DadosPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaLoginSucesso());
        DadosPage.paginaDados();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaDados());
        DadosPage.setarCampoId('inputEmail', '');
    });

    it('01. Alterar senha', async () => {
        DadosPage.paginaLogin();
        DadosPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaLoginSucesso());
        DadosPage.paginaAlterarSenha();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaAlterarSenha());
        DadosPage.setarCampoId('inputSenhaAtual', 'B@sis123');
        DadosPage.setarCampoId('inputNovaSenha', 'B@sis123');
        DadosPage.setarCampoId('inputConfirmarSenha', 'B@sis123');
        DadosPage.clicarBotao('btnSalvar');
    });

    it('02. Alterar senha - Senha Incorreta', async () => {
        DadosPage.paginaLogin();
        DadosPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaLoginSucesso());
        DadosPage.paginaAlterarSenha();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaAlterarSenha());
        DadosPage.setarCampoId('inputSenhaAtual', '1');
        DadosPage.setarCampoId('inputNovaSenha', '');
    });

    it('03. Alterar senha - Senhas não coincidem', async () => {
        DadosPage.paginaLogin();
        DadosPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaLoginSucesso());
        DadosPage.paginaAlterarSenha();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaAlterarSenha());
        DadosPage.setarCampoId('inputNovaSenha', 'B@sis123_');
        DadosPage.setarCampoId('inputConfirmarSenha', 'B@sis123');
        DadosPage.setarCampoId('inputSenhaAtual', '');
    });

    it('04. Alterar senha - Senhas fora do padrão', async () => {
        DadosPage.paginaLogin();
        DadosPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaLoginSucesso());
        DadosPage.paginaAlterarSenha();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaAlterarSenha());
        DadosPage.setarCampoId('inputNovaSenha', '12345678');
        DadosPage.setarCampoId('inputConfirmarSenha', '12345678');
        DadosPage.setarCampoId('inputSenhaAtual', '');
    });

    it('05. Alterar senha - Obrigatoriedade', async () => {
        DadosPage.paginaLogin();
        DadosPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaLoginSucesso());
        DadosPage.paginaAlterarSenha();
        await expect(browser.getCurrentUrl()).toEqual(DadosPage.validarPaginaAlterarSenha());
    });

});
