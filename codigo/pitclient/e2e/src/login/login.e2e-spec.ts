import {LoginPage} from "./login.po";
import {browser} from "protractor";
import {PrimeDropdown} from "../components/prime-dropdown";

describe('Funcionalidade de Login', () => {
    let pagina: LoginPage;

    beforeEach(() => {
        pagina = new LoginPage();
    });

    it('Alterar Idioma', async () => {
        LoginPage.paginaLogin();
        PrimeDropdown.selectValueText(pagina.selectLinguagem, 'English');
    });

    it('Cadastro Usuário sob Análise', async() => {
        LoginPage.paginaLogin();
        LoginPage.clicarBotao('btnCadastrar');
        LoginPage.clicarBotao('EMPREENDEDOR');
        LoginPage.clicarBotao('btnConfirmar');
        await expect(browser.getCurrentUrl()).toEqual(LoginPage.validarPaginaCadastro());
        LoginPage.setarCampoId('inputPassaporte','55555555');
        LoginPage.setarCampoSeletor('input[name=cnpj]','55555555555555');
        LoginPage.setarCampoSeletor('input[name=cpf]','55555555555');
        LoginPage.setarCampoId('inputRne','555555555');
        LoginPage.setarCampoId('inputNome','Nome sob Análise');
        LoginPage.setarCampoId('inputEmail','email@email.email');
        LoginPage.clicarBotao('btnSalvar');
    });

    it('Cadastro Usuário Moderar e Inativar', async() => {
        LoginPage.paginaLogin();
        LoginPage.clicarBotao('btnCadastrar');
        LoginPage.clicarBotao('EMPREENDEDOR');
        LoginPage.clicarBotao('btnConfirmar');
        await expect(browser.getCurrentUrl()).toEqual(LoginPage.validarPaginaCadastro());
        LoginPage.setarCampoId('inputPassaporte','66666666');
        LoginPage.setarCampoSeletor('input[name=cnpj]','66666666666666');
        LoginPage.setarCampoSeletor('input[name=cpf]','66666666666');
        LoginPage.setarCampoId('inputRne','666666666');
        LoginPage.setarCampoId('inputNome','Nome Ativar');
        LoginPage.setarCampoId('inputEmail','email1@email.email');
        LoginPage.clicarBotao('btnSalvar');
        await expect(browser.getCurrentUrl()).toEqual(LoginPage.validarPaginaLogin());
        LoginPage.setarCampoId('inputEmail', 'usuariotcc@teste.com.br');
        LoginPage.setarCampoId('inputSenha', 'B@sis123');
        LoginPage.clicarBotao('btnLogin');
        await expect(browser.getCurrentUrl()).toEqual(LoginPage.validarPaginaLoginSucesso());
        LoginPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(LoginPage.validarPaginaUsuario());
        LoginPage.clicarIdElemento('btnVincular');
        LoginPage.setarCampoArea('inputJustificarModeracao', 'moderar usuário');
        LoginPage.clicarElemento('button[label=Aprovar]');
        LoginPage.clicarElemento('span[class=ui-inputswitch-slider]');
        LoginPage.setarCampoArea('inputJustificativa', 'inativar usuário');
        LoginPage.clicarElemento('button[label=Salvar]');
        LoginPage.clicarTexto('i', 'menu');
        LoginPage.clicarTexto('i', 'settings');
        LoginPage.clicarElemento('a[logout]');
        await expect(browser.getCurrentUrl()).toEqual(LoginPage.validarPaginaLogin());
    });

    it('01. Login', async () => {
        LoginPage.paginaLogin();
        LoginPage.setarCampoId('inputEmail', 'usuariotcc@teste.com.br');
        LoginPage.setarCampoId('inputSenha', 'B@sis123');
        LoginPage.clicarBotao('btnLogin');
    });

    it('02. Login - Usuário não cadastrado', async () => {
        LoginPage.paginaLogin();
        LoginPage.setarCampoId('inputEmail', 'email2@email.email');
        LoginPage.setarCampoId('inputSenha', 'senha');
        LoginPage.clicarBotao('btnLogin');
    });

    it('02.1 Login - Usuário Sob Análise', async () => {
        LoginPage.paginaLogin();
        LoginPage.setarCampoId('inputEmail', 'email1@email.email');
        LoginPage.setarCampoId('inputSenha', 'senha');
        LoginPage.clicarBotao('btnLogin');
    });

    it('02.2 Login - Usuário Inativo', async () => {
        LoginPage.paginaLogin();
        LoginPage.setarCampoId('inputEmail', 'email@email.email');
        LoginPage.setarCampoId('inputSenha', 'senha');
        LoginPage.clicarBotao('btnLogin');
    });

    it('03. Login - Senha incorreta', async () => {
        LoginPage.paginaLogin();
        LoginPage.setarCampoId('inputEmail', 'usuariotcc@teste.com.br');
        LoginPage.setarCampoId('inputSenha', 'senha');
        LoginPage.clicarBotao('btnLogin');
    });

    it('04. Login - Obrigatoriedade', async () => {
        LoginPage.paginaLogin();
    });

    it('05. Login - Recuperar Senha', async () => {
        LoginPage.paginaLogin();
        LoginPage.clicarBotao('btnRecuperarSenha');
    });

    it('01. Recuperar Senha', async () => {
        LoginPage.paginaLogin();
        LoginPage.clicarBotao('btnRecuperarSenha');
        LoginPage.setarCampoId('inputRecuperar', 'usuariotcc@teste.com.br');
        LoginPage.clicarBotao('btnRecuperar');
    });

    it('02. Recuperar Senha - Usuário não cadastrado', async () => {
        LoginPage.paginaLogin();
        LoginPage.clicarBotao('btnRecuperarSenha');
        LoginPage.setarCampoId('inputRecuperar', 'email@email.email');
        LoginPage.clicarBotao('btnRecuperar');
    });

    it('02.2 Recuperar Senha - Usuário Inativo', async () => {
        LoginPage.paginaLogin();
        LoginPage.clicarBotao('btnRecuperarSenha');
        LoginPage.setarCampoId('inputRecuperar', 'email1@email.email');
        LoginPage.clicarBotao('btnRecuperar');
    });

    it('03. Recuperar Senha - Obrigatoriedade', async () => {
        LoginPage.paginaLogin();
        LoginPage.clicarBotao('btnRecuperarSenha');
    });

    it('01. Logout', async() => {
        LoginPage.paginaLogin();
        LoginPage.setarCampoId('inputEmail', 'usuariotcc@teste.com.br');
        LoginPage.setarCampoId('inputSenha', 'B@sis123');
        LoginPage.clicarBotao('btnLogin');
        await expect(browser.getCurrentUrl()).toEqual(LoginPage.validarPaginaLoginSucesso());
        LoginPage.clicarTexto('i', 'menu');
        LoginPage.clicarTexto('i', 'settings');
        LoginPage.clicarElemento('a[logout]');
        await expect(browser.getCurrentUrl()).toEqual(LoginPage.validarPaginaLogin());
    });

});
