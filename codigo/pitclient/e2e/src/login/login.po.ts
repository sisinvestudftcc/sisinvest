import {browser, by, element} from "protractor";
import {PrimengButton, PrimengInputtext, PrimengTextarea} from "@nuvem/protractor-primeng";

export class LoginPage {

    selectLinguagem = 'selectLinguagem';

    static paginaLogin() {
        return browser.get('/#/login');
    }

    static paginaUsuario() {
        return browser.get('/#/usuario');
    }

    static validarPaginaCadastro() {
        return `${browser.baseUrl}/#/cadastro-usuario/empreendedor`;
    }

    static validarPaginaLoginSucesso() {
        return `${browser.baseUrl}/#/login-success`;
    }

    static validarPaginaLogin() {
        return `${browser.baseUrl}/#/login`;
    }

    static validarPaginaUsuario() {
        return `${browser.baseUrl}/#/usuario`;
    }

    static setarCampoId(id: string, value: string) {
        PrimengInputtext.setInputWithTextByid(id, value);
    }

    static setarCampoSeletor(seletor: string, value: string) {
        PrimengInputtext.setInputWithTextByCss(seletor, value);
    }

    static setarCampoArea(id: string, value: string) {
        PrimengTextarea.setValueById(id, value);
    }

    static clicarBotao(id: string) {
        PrimengButton.clickById(id);
    }

    static clicarIdElemento(id: string) {
        element.all(by.id(id)).last().click();
    }

    static clicarElemento(id: string) {
        element.all(by.css(id)).last().click();
    }

    static clicarTexto(seletor: string, value: string) {
        element.all(by.cssContainingText(seletor, value)).first().click();
    }
}
