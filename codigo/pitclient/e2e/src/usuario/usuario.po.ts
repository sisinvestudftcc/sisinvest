import {browser, by, element} from "protractor";
import {PrimengButton, PrimengInputtext, PrimengTextarea} from "@nuvem/protractor-primeng";

export class UsuarioPage {

    selectAtatus = 'selectAtatus';

    selectPerfil = 'selectPerfil';

    static paginaLogin() {
        return browser.get('/#/login');
    }

    static paginaUsuario() {
        return browser.get('/#/usuario');
    }

    static validarPaginaLoginSucesso() {
        return `${browser.baseUrl}/#/login-success`;
    }

    static validarPaginaUsuario() {
        return `${browser.baseUrl}/#/usuario`;
    }

    static validarPaginaAlterar() {
        return `${browser.baseUrl}/#/usuario/alterar`;
    }

    static setarCampoId(id: string, value: string) {
        PrimengInputtext.setInputWithTextByid(id, value);
    }

    static setarCampoSeletor(seletor: string, value: string) {
        PrimengInputtext.setInputWithTextByCss(seletor, value);
    }

    static setarCampoArea(id: string, value: string) {
        PrimengTextarea.setValueById(id, value);
    }

    static clicarBotao(id: string) {
        PrimengButton.clickById(id);
    }

    static clicarTexto(seletor: string, value: string) {
        element.all(by.cssContainingText(seletor, value)).first().click();
    }

    static clicarEspecifico(id: string, valor: number) {
        element.all(by.id(id)).get(valor).click();
    }

    static clicarElemento(id: string) {
        element.all(by.css(id)).last().click();
    }

    static fazerLogin() {
        this.setarCampoId('inputEmail', 'usuariotcc@teste.com.br');
        this.setarCampoId('inputSenha', 'B@sis123');
        this.clicarBotao('btnLogin');
    }
}
