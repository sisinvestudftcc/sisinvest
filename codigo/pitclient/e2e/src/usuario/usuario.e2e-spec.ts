import {UsuarioPage} from './usuario.po';
import {browser} from 'protractor';
import {PrimeDropdown} from '../components/prime-dropdown';

describe('Funcionalidade do Usuário', () => {
    let pagina: UsuarioPage;

    beforeEach(() => {
        pagina = new UsuarioPage();
    });

    it('01. Cadastrar Empreendedor', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnCadastrar');
        UsuarioPage.clicarBotao('EMPREENDEDOR');
        UsuarioPage.clicarBotao('btnConfirmar');
        UsuarioPage.setarCampoId('inputPassaporte', '60000000');
        UsuarioPage.setarCampoSeletor('input[name=cnpj]', '60000000000000');
        UsuarioPage.setarCampoSeletor('input[name=cpf]', '60000000000');
        UsuarioPage.setarCampoId('inputRne', '600000000');
        UsuarioPage.setarCampoId('inputNome', 'Nome Interno Empreendedor');
        UsuarioPage.setarCampoId('inputEmail', 'interno_empreendedor@email.email');
        UsuarioPage.setarCampoArea('text-area', 'cadastro interno');
        UsuarioPage.clicarBotao('btnSalvar');
    });

    it('01.1. Cadastrar Empreendedor - Adicionar telefone', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnCadastrar');
        UsuarioPage.clicarBotao('EMPREENDEDOR');
        UsuarioPage.clicarBotao('btnConfirmar');
        UsuarioPage.setarCampoId('inputTelefone', '31999999999');
        UsuarioPage.clicarBotao('btnAdd');
    });

    it('01.2. Cadastrar Empreendedor - Excluir telefone', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnCadastrar');
        UsuarioPage.clicarBotao('EMPREENDEDOR');
        UsuarioPage.clicarBotao('btnConfirmar');
        UsuarioPage.setarCampoId('inputTelefone', '31999999999');
        UsuarioPage.clicarBotao('btnAdd');
        UsuarioPage.clicarBotao('btnRemoveTel');
    });

    it('02. Cadastrar Empreendedor - Duplicidade', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnCadastrar');
        UsuarioPage.clicarBotao('EMPREENDEDOR');
        UsuarioPage.clicarBotao('btnConfirmar');
        UsuarioPage.setarCampoId('inputPassaporte', '60000000');
        UsuarioPage.setarCampoSeletor('input[name=cnpj]', '60000000000000');
        UsuarioPage.setarCampoSeletor('input[name=cpf]', '60000000000');
        UsuarioPage.setarCampoId('inputRne', '600000000');
        UsuarioPage.setarCampoId('inputNome', 'Nome Interno Empreendedor');
        UsuarioPage.setarCampoId('inputEmail', 'interno_empreendedor@email.email');
        UsuarioPage.setarCampoArea('text-area', 'cadastro interno');
        UsuarioPage.clicarBotao('btnSalvar');
    });

    it('03. Cadastrar Empreendedor - E-mail inválido', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnCadastrar');
        UsuarioPage.clicarBotao('EMPREENDEDOR');
        UsuarioPage.clicarBotao('btnConfirmar');
        UsuarioPage.setarCampoId('inputEmail', 'email');
        UsuarioPage.setarCampoId('inputLinkedin', '');
    });

    it('03.1. Cadastrar Empreendedor - Linkedin inválido', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnCadastrar');
        UsuarioPage.clicarBotao('EMPREENDEDOR');
        UsuarioPage.clicarBotao('btnConfirmar');
        UsuarioPage.setarCampoId('inputLinkedin', 'linkedin');
        UsuarioPage.setarCampoId('inputTelefone', '');
    });

    it('03.2. Cadastrar Empreendedor - CPF inválido', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnCadastrar');
        UsuarioPage.clicarBotao('EMPREENDEDOR');
        UsuarioPage.clicarBotao('btnConfirmar');
        UsuarioPage.setarCampoSeletor('input[name=cpf]', '0');
        UsuarioPage.setarCampoId('inputRne', '');
    });

    it('03.3. Cadastrar Empreendedor - CNPJ inválido', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnCadastrar');
        UsuarioPage.clicarBotao('EMPREENDEDOR');
        UsuarioPage.clicarBotao('btnConfirmar');
        UsuarioPage.setarCampoSeletor('input[name=cnpj]', '0');
        UsuarioPage.setarCampoSeletor('input[name=cpf]', '');
    });

    it('04. Cadastrar Empreendedor - Cancelar - Sem Dados', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnCadastrar');
        UsuarioPage.clicarBotao('EMPREENDEDOR');
        UsuarioPage.clicarBotao('btnConfirmar');
        UsuarioPage.clicarBotao('btnCancelar');
    });

    it('05. Cadastrar Empreendedor - Cancelar - Com Dados - SIM', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnCadastrar');
        UsuarioPage.clicarBotao('EMPREENDEDOR');
        UsuarioPage.clicarBotao('btnConfirmar');
        UsuarioPage.setarCampoId('inputPassaporte', '0');
        UsuarioPage.clicarBotao('btnCancelar');
        UsuarioPage.clicarTexto('button', 'SIM');
    });

    it('05.1. Cadastrar Empreendedor - Cancelar - Com Dados - NÃO', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnCadastrar');
        UsuarioPage.clicarBotao('EMPREENDEDOR');
        UsuarioPage.clicarBotao('btnConfirmar');
        UsuarioPage.setarCampoId('inputPassaporte', '0');
        UsuarioPage.clicarBotao('btnCancelar');
        UsuarioPage.clicarTexto('button', 'NÃO');
    });

    it('06. Cadastrar Empreendedor - Obrigatoriedade', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnCadastrar');
        UsuarioPage.clicarBotao('EMPREENDEDOR');
        UsuarioPage.clicarBotao('btnConfirmar');
    });

    it('01. Cadastrar Gestor', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnCadastrar');
        UsuarioPage.clicarBotao('GESTOR');
        UsuarioPage.clicarBotao('btnConfirmar');
        UsuarioPage.setarCampoId('inputNome', 'Nome Interno Gestor');
        UsuarioPage.setarCampoId('inputEmail', 'interno_gestor@email.email');
        UsuarioPage.clicarBotao('btnSalvar');
    });

    it('01.1. Cadastrar Gestor - Adicionar telefone', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnCadastrar');
        UsuarioPage.clicarBotao('GESTOR');
        UsuarioPage.clicarBotao('btnConfirmar');
        UsuarioPage.setarCampoId('inputTelefone', '31999999999');
        UsuarioPage.clicarBotao('btnAdd');
    });

    it('01.2. Cadastrar Gestor - Excluir telefone', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnCadastrar');
        UsuarioPage.clicarBotao('GESTOR');
        UsuarioPage.clicarBotao('btnConfirmar');
        UsuarioPage.setarCampoId('inputTelefone', '31999999999');
        UsuarioPage.clicarBotao('btnAdd');
        UsuarioPage.clicarBotao('btnRemoveTel');
    });

    it('02. Cadastrar Gestor - Duplicidade', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnCadastrar');
        UsuarioPage.clicarBotao('GESTOR');
        UsuarioPage.clicarBotao('btnConfirmar');
        UsuarioPage.setarCampoId('inputNome', 'Nome Interno Gestor');
        UsuarioPage.setarCampoId('inputEmail', 'interno_gestor@email.email');
        UsuarioPage.clicarBotao('btnSalvar');
    });

    it('03. Cadastrar Gestor - E-mail inválido', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnCadastrar');
        UsuarioPage.clicarBotao('GESTOR');
        UsuarioPage.clicarBotao('btnConfirmar');
        UsuarioPage.setarCampoId('inputEmail', 'email');
        UsuarioPage.setarCampoId('inputTelefone', '');
    });

    it('04. Cadastrar Gestor - Cancelar - Sem Dados', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnCadastrar');
        UsuarioPage.clicarBotao('GESTOR');
        UsuarioPage.clicarBotao('btnConfirmar');
        UsuarioPage.clicarBotao('btnCancelar');
    });

    it('05. Cadastrar Gestor - Cancelar - Com Dados - SIM', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnCadastrar');
        UsuarioPage.clicarBotao('GESTOR');
        UsuarioPage.clicarBotao('btnConfirmar');
        UsuarioPage.setarCampoId('inputNome', 'Nome');
        UsuarioPage.clicarBotao('btnCancelar');
        UsuarioPage.clicarTexto('button', 'SIM');
    });

    it('05.1. Cadastrar Gestor - Cancelar - Com Dados - NÃO', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnCadastrar');
        UsuarioPage.clicarBotao('GESTOR');
        UsuarioPage.clicarBotao('btnConfirmar');
        UsuarioPage.setarCampoId('inputNome', 'Nome');
        UsuarioPage.clicarBotao('btnCancelar');
        UsuarioPage.clicarTexto('button', 'NÃO');
    });

    it('06. Cadastrar Gestor - Obrigatoriedade', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnCadastrar');
        UsuarioPage.clicarBotao('GESTOR');
        UsuarioPage.clicarBotao('btnConfirmar');
    });

    it('01. Cadastrar Investidor', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnCadastrar');
        UsuarioPage.clicarBotao('INVESTIDOR');
        UsuarioPage.clicarBotao('btnConfirmar');
        UsuarioPage.setarCampoId('inputPassaporte', '70000000');
        UsuarioPage.setarCampoSeletor('input[name=cpf]', '70000000000');
        UsuarioPage.setarCampoId('inputNome', 'Nome Interno Investidor');
        UsuarioPage.setarCampoId('inputEmail', 'interno_investidor@email.email');
        UsuarioPage.setarCampoArea('text-area', 'cadastro interno');
        UsuarioPage.clicarBotao('btnSalvar');
    });

    it('01.1. Cadastrar Investidor - Adicionar telefone', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnCadastrar');
        UsuarioPage.clicarBotao('INVESTIDOR');
        UsuarioPage.clicarBotao('btnConfirmar');
        UsuarioPage.setarCampoId('inputTelefone', '31999999999');
        UsuarioPage.clicarBotao('btnAdd');
    });

    it('01.2. Cadastrar Investidor - Excluir telefone', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnCadastrar');
        UsuarioPage.clicarBotao('INVESTIDOR');
        UsuarioPage.clicarBotao('btnConfirmar');
        UsuarioPage.setarCampoId('inputTelefone', '31999999999');
        UsuarioPage.clicarBotao('btnAdd');
        UsuarioPage.clicarBotao('btnRemoveTel');
    });

    it('02. Cadastrar Investidor - Duplicidade', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnCadastrar');
        UsuarioPage.clicarBotao('INVESTIDOR');
        UsuarioPage.clicarBotao('btnConfirmar');
        UsuarioPage.setarCampoId('inputPassaporte', '70000000');
        UsuarioPage.setarCampoSeletor('input[name=cpf]', '70000000000');
        UsuarioPage.setarCampoId('inputNome', 'Nome Interno Investidor');
        UsuarioPage.setarCampoId('inputEmail', 'interno_investidor@email.email');
        UsuarioPage.setarCampoArea('text-area', 'cadastro interno');
        UsuarioPage.clicarBotao('btnSalvar');
    });

    it('03. Cadastrar Investidor - E-mail inválido', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnCadastrar');
        UsuarioPage.clicarBotao('INVESTIDOR');
        UsuarioPage.clicarBotao('btnConfirmar');
        UsuarioPage.setarCampoId('inputEmail', 'email');
        UsuarioPage.setarCampoId('inputLinkedin', '');
    });

    it('03.1. Cadastrar Investidor - Linkedin inválido', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnCadastrar');
        UsuarioPage.clicarBotao('INVESTIDOR');
        UsuarioPage.clicarBotao('btnConfirmar');
        UsuarioPage.setarCampoId('inputLinkedin', 'linkedin');
        UsuarioPage.setarCampoId('inputTelefone', '');
    });

    it('03.2. Cadastrar Investidor - CPF inválido', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnCadastrar');
        UsuarioPage.clicarBotao('INVESTIDOR');
        UsuarioPage.clicarBotao('btnConfirmar');
        UsuarioPage.setarCampoSeletor('input[name=cpf]', '0');
        UsuarioPage.setarCampoId('inputNome', '');
    });

    it('04. Cadastrar Investidor - Cancelar - Sem Dados', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnCadastrar');
        UsuarioPage.clicarBotao('INVESTIDOR');
        UsuarioPage.clicarBotao('btnConfirmar');
        UsuarioPage.clicarBotao('btnCancelar');
    });

    it('05. Cadastrar Gestor - Cancelar - Com Dados - SIM', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnCadastrar');
        UsuarioPage.clicarBotao('INVESTIDOR');
        UsuarioPage.clicarBotao('btnConfirmar');
        UsuarioPage.setarCampoId('inputPassaporte', '0');
        UsuarioPage.clicarBotao('btnCancelar');
        UsuarioPage.clicarTexto('button', 'SIM');
    });

    it('05.1. Cadastrar Investidor - Cancelar - Com Dados - NÃO', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnCadastrar');
        UsuarioPage.clicarBotao('INVESTIDOR');
        UsuarioPage.clicarBotao('btnConfirmar');
        UsuarioPage.setarCampoId('inputPassaporte', '0');
        UsuarioPage.clicarBotao('btnCancelar');
        UsuarioPage.clicarTexto('button', 'NÃO');
    });

    it('06. Cadastrar Investidor - Obrigatoriedade', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnCadastrar');
        UsuarioPage.clicarBotao('INVESTIDOR');
        UsuarioPage.clicarBotao('btnConfirmar');
    });

    it('01. Listar Usuários - Cadastrar Usuários', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnCadastrar');
    });

    it('02. Listar Usuários - Consultar Usuários', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnConsultar');
    });

    it('01. Consultar Usuários', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnConsultar');
        UsuarioPage.setarCampoId('inputEmail', 'interno_investidor@email.email');
        UsuarioPage.clicarBotao('btnConsulta');
    });

    it('02. Consultar Usuários - Consulta sem retorno', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnConsultar');
        UsuarioPage.setarCampoId('inputEmail', 'inexistente');
        UsuarioPage.clicarBotao('btnConsulta');
    });

    it('03. Consultar Usuários - Cancelar após consulta', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnConsultar');
        UsuarioPage.setarCampoId('inputEmail', 'interno_investidor@email.email');
        UsuarioPage.clicarBotao('btnConsulta');
        UsuarioPage.clicarBotao('btnCancelar');
    });

    it('04. Consultar Usuários - Cancelar', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnConsultar');
        UsuarioPage.clicarBotao('btnCancelar');
    });

    it('01. Alterar Empreendedor', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarEspecifico('btnEditar', 1);
        await expect(browser.getCurrentUrl()).toContain(UsuarioPage.validarPaginaAlterar());
        UsuarioPage.setarCampoArea('text-area', 'alterar interno');
        UsuarioPage.clicarBotao('btnSalvar');
    });

    it('01.1. Alterar Empreendedor - Adicionar telefone', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarEspecifico('btnEditar', 1);
        await expect(browser.getCurrentUrl()).toContain(UsuarioPage.validarPaginaAlterar());
        UsuarioPage.setarCampoId('inputTelefone', '31999999999');
        UsuarioPage.clicarBotao('btnAdd');
    });

    it('01.2. Alterar Empreendedor - Excluir telefone', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarEspecifico('btnEditar', 1);
        await expect(browser.getCurrentUrl()).toContain(UsuarioPage.validarPaginaAlterar());
        UsuarioPage.setarCampoId('inputTelefone', '31999999999');
        UsuarioPage.clicarBotao('btnAdd');
        UsuarioPage.clicarBotao('btnRemoveTel');
    });

    it('02. Alterar Empreendedor - Duplicidade', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarEspecifico('btnEditar', 1);
        await expect(browser.getCurrentUrl()).toContain(UsuarioPage.validarPaginaAlterar());
        UsuarioPage.setarCampoArea('inputEmail', 'usuariotcc@teste.com.br');
        UsuarioPage.clicarBotao('btnSalvar');
    });

    it('03. Alterar Empreendedor - E-mail inválido', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarEspecifico('btnEditar', 1);
        await expect(browser.getCurrentUrl()).toContain(UsuarioPage.validarPaginaAlterar());
        UsuarioPage.setarCampoArea('inputEmail', 'email');
        UsuarioPage.setarCampoId('inputLinkedin', '');
    });

    it('03.1. Alterar Empreendedor - Linkedin inválido', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarEspecifico('btnEditar', 1);
        await expect(browser.getCurrentUrl()).toContain(UsuarioPage.validarPaginaAlterar());
        UsuarioPage.setarCampoArea('inputLinkedin', 'linkedin');
        UsuarioPage.setarCampoId('inputTelefone', '');
    });

    it('03.2. Alterar Empreendedor - CPF inválido', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarEspecifico('btnEditar', 1);
        await expect(browser.getCurrentUrl()).toContain(UsuarioPage.validarPaginaAlterar());
        UsuarioPage.setarCampoSeletor('input[name=cpf]', '0');
        UsuarioPage.setarCampoId('inputRne', '');
    });

    it('03.3. Alterar Empreendedor - CNPJ inválido', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarEspecifico('btnEditar', 1);
        await expect(browser.getCurrentUrl()).toContain(UsuarioPage.validarPaginaAlterar());
        UsuarioPage.setarCampoSeletor('input[name=cnpj]', '0');
        UsuarioPage.setarCampoSeletor('input[name=cpf]', '');
    });

    it('04. Alterar Empreendedor - Cancelar - Sem Dados', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarEspecifico('btnEditar', 1);
        await expect(browser.getCurrentUrl()).toContain(UsuarioPage.validarPaginaAlterar());
        UsuarioPage.clicarBotao('btnCancelar');
    });

    it('05. Alterar Empreendedor - Cancelar - Com Dados - SIM', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarEspecifico('btnEditar', 1);
        await expect(browser.getCurrentUrl()).toContain(UsuarioPage.validarPaginaAlterar());
        UsuarioPage.setarCampoId('inputPassaporte', '0');
        UsuarioPage.clicarBotao('btnCancelar');
        UsuarioPage.clicarTexto('button', 'SIM');
    });

    it('05.1. Alterar Empreendedor - Cancelar - Com Dados - NÃO', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarEspecifico('btnEditar', 1);
        await expect(browser.getCurrentUrl()).toContain(UsuarioPage.validarPaginaAlterar());
        UsuarioPage.setarCampoId('inputPassaporte', '0');
        UsuarioPage.clicarBotao('btnCancelar');
        UsuarioPage.clicarTexto('button', 'NÃO');
    });

    it('06. Alterar Empreendedor - Obrigatoriedade', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarEspecifico('btnEditar', 1);
        await expect(browser.getCurrentUrl()).toContain(UsuarioPage.validarPaginaAlterar());
        UsuarioPage.setarCampoId('inputPassaporte', '');
        UsuarioPage.setarCampoSeletor('input[name=cpf]', '');
        UsuarioPage.setarCampoSeletor('input[name=cnpj]', '');
        UsuarioPage.setarCampoId('inputRne', '');
        UsuarioPage.clicarBotao('btnSalvar');
    });

    it('01. Alterar Gestor', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarEspecifico('btnEditar', 2);
        await expect(browser.getCurrentUrl()).toContain(UsuarioPage.validarPaginaAlterar());
        UsuarioPage.setarCampoId('inputEmail', 'interno_gestor2@email.email');
        UsuarioPage.clicarBotao('btnSalvar');
    });

    it('01.1. Alterar Gestor - Adicionar telefone', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarEspecifico('btnEditar', 2);
        await expect(browser.getCurrentUrl()).toContain(UsuarioPage.validarPaginaAlterar());
        UsuarioPage.setarCampoId('inputTelefone', '31999999999');
        UsuarioPage.clicarBotao('btnAdd');
    });

    it('01.2. Alterar Gestor - Excluir telefone', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarEspecifico('btnEditar', 2);
        await expect(browser.getCurrentUrl()).toContain(UsuarioPage.validarPaginaAlterar());
        UsuarioPage.setarCampoId('inputTelefone', '31999999999');
        UsuarioPage.clicarBotao('btnAdd');
        UsuarioPage.clicarBotao('btnRemoveTel');
    });

    it('02. Alterar Gestor - Duplicidade', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarEspecifico('btnEditar', 2);
        await expect(browser.getCurrentUrl()).toContain(UsuarioPage.validarPaginaAlterar());
        UsuarioPage.setarCampoArea('inputEmail', 'usuariotcc@teste.com.br');
        UsuarioPage.clicarBotao('btnSalvar');
    });

    it('03. Alterar Gestor - E-mail inválido', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarEspecifico('btnEditar', 2);
        await expect(browser.getCurrentUrl()).toContain(UsuarioPage.validarPaginaAlterar());
        UsuarioPage.setarCampoArea('inputEmail', 'email');
        UsuarioPage.setarCampoArea('inputTelefone', '');
    });

    it('04. Alterar Gestor - Cancelar - Sem Dados', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarEspecifico('btnEditar', 2);
        await expect(browser.getCurrentUrl()).toContain(UsuarioPage.validarPaginaAlterar());
        UsuarioPage.clicarBotao('btnCancelar');
    });

    it('05. Alterar Gestor - Cancelar - Com Dados - SIM', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarEspecifico('btnEditar', 2);
        await expect(browser.getCurrentUrl()).toContain(UsuarioPage.validarPaginaAlterar());
        UsuarioPage.setarCampoId('inputEmail', 'email_novo@email.email');
        UsuarioPage.clicarBotao('btnCancelar');
        UsuarioPage.clicarTexto('button', 'SIM');
    });

    it('05.1. Alterar Gestor - Cancelar - Com Dados - NÃO', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarEspecifico('btnEditar', 2);
        await expect(browser.getCurrentUrl()).toContain(UsuarioPage.validarPaginaAlterar());
        UsuarioPage.setarCampoId('inputEmail', 'email_novo@email.email');
        UsuarioPage.clicarBotao('btnCancelar');
        UsuarioPage.clicarTexto('button', 'NÃO');
    });

    it('06. Alterar Gestor - Obrigatoriedade', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarEspecifico('btnEditar', 2);
        await expect(browser.getCurrentUrl()).toContain(UsuarioPage.validarPaginaAlterar());
        UsuarioPage.setarCampoId('inputNome', '');
    });

    it('01. Alterar Investidor', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarEspecifico('btnEditar', 3);
        await expect(browser.getCurrentUrl()).toContain(UsuarioPage.validarPaginaAlterar());
        UsuarioPage.setarCampoArea('text-area', 'alterar interno');
        UsuarioPage.clicarBotao('btnSalvar');
    });

    it('01.1. Alterar Investidor - Adicionar telefone', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarEspecifico('btnEditar', 3);
        await expect(browser.getCurrentUrl()).toContain(UsuarioPage.validarPaginaAlterar());
        UsuarioPage.setarCampoId('inputTelefone', '31999999999');
        UsuarioPage.clicarBotao('btnAdd');
    });

    it('01.2. Alterar Investidor - Excluir telefone', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarEspecifico('btnEditar', 3);
        await expect(browser.getCurrentUrl()).toContain(UsuarioPage.validarPaginaAlterar());
        UsuarioPage.setarCampoId('inputTelefone', '31999999999');
        UsuarioPage.clicarBotao('btnAdd');
        UsuarioPage.clicarBotao('btnRemoveTel');
    });

    it('02. Alterar Investidor - Duplicidade', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarEspecifico('btnEditar', 3);
        await expect(browser.getCurrentUrl()).toContain(UsuarioPage.validarPaginaAlterar());
        UsuarioPage.setarCampoArea('inputEmail', 'usuariotcc@teste.com.br');
        UsuarioPage.clicarBotao('btnSalvar');
    });

    it('03. Alterar Investidor - E-mail inválido', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarEspecifico('btnEditar', 3);
        await expect(browser.getCurrentUrl()).toContain(UsuarioPage.validarPaginaAlterar());
        UsuarioPage.setarCampoArea('inputEmail', 'email');
        UsuarioPage.setarCampoId('inputLinkedin', '');
    });

    it('03.1. Alterar Investidor - Linkedin inválido', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarEspecifico('btnEditar', 3);
        await expect(browser.getCurrentUrl()).toContain(UsuarioPage.validarPaginaAlterar());
        UsuarioPage.setarCampoArea('inputLinkedin', 'linkedin');
        UsuarioPage.setarCampoId('inputTelefone', '');
    });

    it('03.2. Alterar Investidor - CPF inválido', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarEspecifico('btnEditar', 3);
        await expect(browser.getCurrentUrl()).toContain(UsuarioPage.validarPaginaAlterar());
        UsuarioPage.setarCampoSeletor('input[name=cpf]', '0');
        UsuarioPage.setarCampoId('inputNome', '');
    });

    it('04. Alterar Investidor - Cancelar - Sem Dados', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarEspecifico('btnEditar', 3);
        await expect(browser.getCurrentUrl()).toContain(UsuarioPage.validarPaginaAlterar());
        UsuarioPage.clicarBotao('btnCancelar');
    });

    it('05. Alterar Investidor - Cancelar - Com Dados - SIM', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarEspecifico('btnEditar', 3);
        await expect(browser.getCurrentUrl()).toContain(UsuarioPage.validarPaginaAlterar());
        UsuarioPage.setarCampoId('inputPassaporte', '0');
        UsuarioPage.clicarBotao('btnCancelar');
        UsuarioPage.clicarTexto('button', 'SIM');
    });

    it('05.1. Alterar Investidor - Cancelar - Com Dados - NÃO', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarEspecifico('btnEditar', 3);
        await expect(browser.getCurrentUrl()).toContain(UsuarioPage.validarPaginaAlterar());
        UsuarioPage.setarCampoId('inputPassaporte', '0');
        UsuarioPage.clicarBotao('btnCancelar');
        UsuarioPage.clicarTexto('button', 'NÃO');
    });

    it('06. Alterar Investidor - Obrigatoriedade', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarEspecifico('btnEditar', 3);
        await expect(browser.getCurrentUrl()).toContain(UsuarioPage.validarPaginaAlterar());
        UsuarioPage.setarCampoId('inputPassaporte', '');
        UsuarioPage.clicarBotao('btnSalvar');
    });

    it('01. Visualizar Empreendedor', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarEspecifico('btnVisualizar', 4);
        UsuarioPage.clicarBotao('btnVoltar');
    });

    it('02. Visualizar Empreendedor - Voltar', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarEspecifico('btnVisualizar', 4);
    });

    it('01. Visualizar Gestor', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarEspecifico('btnVisualizar', 5);
    });

    it('02. Visualizar Gestor - Voltar', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarEspecifico('btnVisualizar', 5);
        UsuarioPage.clicarBotao('btnVoltar');
    });

    it('01. Visualizar Investidor', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarEspecifico('btnVisualizar', 6);
    });

    it('02. Visualizar Investidor - Voltar', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarEspecifico('btnVisualizar', 6);
        UsuarioPage.clicarBotao('btnVoltar');
    });

    it('01. Exibição histórico', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarEspecifico('btnHistorico', 0);
    });

    it('02. Exibição histórico - Voltar', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarEspecifico('btnHistorico', 0);
        UsuarioPage.clicarBotao('btnVoltar');
    });

    it('01. Ativar usuário - Salvar', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnConsultar');
        PrimeDropdown.selectValueText(pagina.selectAtatus, 'Inativo');
        PrimeDropdown.selectValueText(pagina.selectPerfil, 'Empreendedor');
        UsuarioPage.clicarBotao('btnConsulta');
        UsuarioPage.clicarEspecifico('btnAlterStatus', 0);
        UsuarioPage.setarCampoArea('inputJustificativa', 'ativar usuário');
        UsuarioPage.clicarElemento('button[label=Salvar]');
    });

    it('02. Ativar usuário - Cancelar com dados - SIM', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnConsultar');
        PrimeDropdown.selectValueText(pagina.selectAtatus, 'Inativo');
        PrimeDropdown.selectValueText(pagina.selectPerfil, 'Empreendedor');
        UsuarioPage.clicarBotao('btnConsulta');
        UsuarioPage.clicarEspecifico('btnAlterStatus', 0);
        UsuarioPage.setarCampoArea('inputJustificativa', 'ativar usuário');
        UsuarioPage.clicarElemento('button[label=Cancelar]');
        UsuarioPage.clicarTexto('button', 'SIM');
    });

    it('03. Ativar usuário - Cancelar com dados - NÃO', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnConsultar');
        PrimeDropdown.selectValueText(pagina.selectAtatus, 'Inativo');
        PrimeDropdown.selectValueText(pagina.selectPerfil, 'Empreendedor');
        UsuarioPage.clicarBotao('btnConsulta');
        UsuarioPage.clicarEspecifico('btnAlterStatus', 0);
        UsuarioPage.setarCampoArea('inputJustificativa', 'ativar usuário');
        UsuarioPage.clicarElemento('button[label=Cancelar]');
        UsuarioPage.clicarTexto('button', 'NÃO');
    });

    it('04. Ativar usuário - Cancelar sem dados - SIM', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnConsultar');
        PrimeDropdown.selectValueText(pagina.selectAtatus, 'Inativo');
        PrimeDropdown.selectValueText(pagina.selectPerfil, 'Empreendedor');
        UsuarioPage.clicarBotao('btnConsulta');
        UsuarioPage.clicarEspecifico('btnAlterStatus', 0);
        UsuarioPage.clicarElemento('button[label=Cancelar]');
        UsuarioPage.clicarTexto('button', 'SIM');
    });

    it('05. Ativar usuário - Cancelar sem dados - NÃO', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnConsultar');
        PrimeDropdown.selectValueText(pagina.selectAtatus, 'Inativo');
        PrimeDropdown.selectValueText(pagina.selectPerfil, 'Empreendedor');
        UsuarioPage.clicarBotao('btnConsulta');
        UsuarioPage.clicarEspecifico('btnAlterStatus', 0);
        UsuarioPage.clicarElemento('button[label=Cancelar]');
        UsuarioPage.clicarTexto('button', 'NÃO');
    });

    it('06. Ativar usuário - Obrigatoriedade', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnConsultar');
        PrimeDropdown.selectValueText(pagina.selectAtatus, 'Inativo');
        PrimeDropdown.selectValueText(pagina.selectPerfil, 'Empreendedor');
        UsuarioPage.clicarBotao('btnConsulta');
        UsuarioPage.clicarEspecifico('btnAlterStatus', 0);
    });

    it('01. Ativar usuário - Salvar', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnConsultar');
        PrimeDropdown.selectValueText(pagina.selectAtatus, 'Inativo');
        PrimeDropdown.selectValueText(pagina.selectPerfil, 'Gestor');
        UsuarioPage.clicarBotao('btnConsulta');
        UsuarioPage.clicarEspecifico('btnAlterStatus', 0);
        UsuarioPage.clicarElemento('button[label=Salvar]');
    });

    it('02. Ativar usuário - Cancelar', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnConsultar');
        PrimeDropdown.selectValueText(pagina.selectAtatus, 'Inativo');
        PrimeDropdown.selectValueText(pagina.selectPerfil, 'Investidor');
        UsuarioPage.clicarBotao('btnConsulta');
        UsuarioPage.clicarEspecifico('btnAlterStatus', 0);
        UsuarioPage.clicarElemento('button[label=Cancelar]');
    });

    it('01. Inativar usuário - Salvar', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnConsultar');
        PrimeDropdown.selectValueText(pagina.selectAtatus, 'Ativo');
        PrimeDropdown.selectValueText(pagina.selectPerfil, 'Empreendedor');
        UsuarioPage.clicarBotao('btnConsulta');
        UsuarioPage.clicarEspecifico('btnAlterStatus', 0);
        UsuarioPage.setarCampoArea('inputJustificativa', 'inativar usuário');
        UsuarioPage.clicarElemento('button[label=Salvar]');
    });

    it('02. Inativar usuário - Cancelar com dados - SIM', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnConsultar');
        PrimeDropdown.selectValueText(pagina.selectAtatus, 'Ativo');
        PrimeDropdown.selectValueText(pagina.selectPerfil, 'Empreendedor');
        UsuarioPage.clicarBotao('btnConsulta');
        UsuarioPage.clicarEspecifico('btnAlterStatus', 0);
        UsuarioPage.setarCampoArea('inputJustificativa', 'inativar usuário');
        UsuarioPage.clicarElemento('button[label=Cancelar]');
        UsuarioPage.clicarTexto('button', 'SIM');
    });

    it('03. Inativar usuário - Cancelar com dados - NÃO', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnConsultar');
        PrimeDropdown.selectValueText(pagina.selectAtatus, 'Ativo');
        PrimeDropdown.selectValueText(pagina.selectPerfil, 'Empreendedor');
        UsuarioPage.clicarBotao('btnConsulta');
        UsuarioPage.clicarEspecifico('btnAlterStatus', 0);
        UsuarioPage.setarCampoArea('inputJustificativa', 'inativar usuário');
        UsuarioPage.clicarElemento('button[label=Cancelar]');
        UsuarioPage.clicarTexto('button', 'NÃO');
    });

    it('04. Inativar usuário - Cancelar sem dados - SIM', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnConsultar');
        PrimeDropdown.selectValueText(pagina.selectAtatus, 'Ativo');
        PrimeDropdown.selectValueText(pagina.selectPerfil, 'Empreendedor');
        UsuarioPage.clicarBotao('btnConsulta');
        UsuarioPage.clicarEspecifico('btnAlterStatus', 0);
        UsuarioPage.clicarElemento('button[label=Cancelar]');
        UsuarioPage.clicarTexto('button', 'SIM');
    });

    it('05. Inativar usuário - Cancelar sem dados - NÃO', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnConsultar');
        PrimeDropdown.selectValueText(pagina.selectAtatus, 'Ativo');
        PrimeDropdown.selectValueText(pagina.selectPerfil, 'Empreendedor');
        UsuarioPage.clicarBotao('btnConsulta');
        UsuarioPage.clicarEspecifico('btnAlterStatus', 0);
        UsuarioPage.clicarElemento('button[label=Cancelar]');
        UsuarioPage.clicarTexto('button', 'NÃO');
    });

    it('06. Inativar usuário - Obrigatoriedade', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnConsultar');
        PrimeDropdown.selectValueText(pagina.selectAtatus, 'Ativo');
        PrimeDropdown.selectValueText(pagina.selectPerfil, 'Empreendedor');
        UsuarioPage.clicarBotao('btnConsulta');
        UsuarioPage.clicarEspecifico('btnAlterStatus', 0);
    });

    it('01. Inativar usuário - Salvar', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnConsultar');
        PrimeDropdown.selectValueText(pagina.selectAtatus, 'Ativo');
        PrimeDropdown.selectValueText(pagina.selectPerfil, 'Gestor');
        UsuarioPage.clicarBotao('btnConsulta');
        UsuarioPage.clicarEspecifico('btnAlterStatus', 1);
        UsuarioPage.setarCampoArea('inputJustificativa', 'inativar usuário');
        UsuarioPage.clicarElemento('button[label=Salvar]');
    });

    it('02. Inativar usuário - Cancelar com dados - SIM', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnConsultar');
        PrimeDropdown.selectValueText(pagina.selectAtatus, 'Ativo');
        PrimeDropdown.selectValueText(pagina.selectPerfil, 'Gestor');
        UsuarioPage.clicarBotao('btnConsulta');
        UsuarioPage.clicarEspecifico('btnAlterStatus', 1);
        UsuarioPage.setarCampoArea('inputJustificativa', 'inativar usuário');
        UsuarioPage.clicarElemento('button[label=Cancelar]');
        UsuarioPage.clicarTexto('button', 'SIM');
    });

    it('03. Inativar usuário - Cancelar com dados - NÃO', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnConsultar');
        PrimeDropdown.selectValueText(pagina.selectAtatus, 'Ativo');
        PrimeDropdown.selectValueText(pagina.selectPerfil, 'Gestor');
        UsuarioPage.clicarBotao('btnConsulta');
        UsuarioPage.clicarEspecifico('btnAlterStatus', 1);
        UsuarioPage.setarCampoArea('inputJustificativa', 'inativar usuário');
        UsuarioPage.clicarElemento('button[label=Cancelar]');
        UsuarioPage.clicarTexto('button', 'NÃO');
    });

    it('04. Inativar usuário - Cancelar sem dados - SIM', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnConsultar');
        PrimeDropdown.selectValueText(pagina.selectAtatus, 'Ativo');
        PrimeDropdown.selectValueText(pagina.selectPerfil, 'Gestor');
        UsuarioPage.clicarBotao('btnConsulta');
        UsuarioPage.clicarEspecifico('btnAlterStatus', 1);
        UsuarioPage.clicarElemento('button[label=Cancelar]');
        UsuarioPage.clicarTexto('button', 'SIM');
    });

    it('05. Inativar usuário - Cancelar sem dados - NÃO', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnConsultar');
        PrimeDropdown.selectValueText(pagina.selectAtatus, 'Ativo');
        PrimeDropdown.selectValueText(pagina.selectPerfil, 'Gestor');
        UsuarioPage.clicarBotao('btnConsulta');
        UsuarioPage.clicarEspecifico('btnAlterStatus', 1);
        UsuarioPage.clicarElemento('button[label=Cancelar]');
        UsuarioPage.clicarTexto('button', 'NÃO');
    });

    it('06. Inativar usuário - Obrigatoriedade', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnConsultar');
        PrimeDropdown.selectValueText(pagina.selectAtatus, 'Ativo');
        PrimeDropdown.selectValueText(pagina.selectPerfil, 'Gestor');
        UsuarioPage.clicarBotao('btnConsulta');
        UsuarioPage.clicarEspecifico('btnAlterStatus', 1);
    });

    it('01. Moderar usuário - Aprovar', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnConsultar');
        PrimeDropdown.selectValueText(pagina.selectAtatus, 'Sob Análise');
        UsuarioPage.clicarBotao('btnConsulta');
        UsuarioPage.clicarEspecifico('btnVincular', 0);
        UsuarioPage.setarCampoArea('inputJustificarModeracao', 'usuário aprovado');
        UsuarioPage.clicarElemento('button[label=Aprovar]');
    });

    it('02. Moderar usuário - Reprovar', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnConsultar');
        PrimeDropdown.selectValueText(pagina.selectAtatus, 'Sob Análise');
        UsuarioPage.clicarBotao('btnConsulta');
        UsuarioPage.clicarEspecifico('btnVincular', 0);
        UsuarioPage.setarCampoArea('inputJustificarModeracao', 'usuário reprovado');
        UsuarioPage.clicarElemento('button[label=Reprovar]');
    });

    it('03. Moderar usuário - Fechar sem dados', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnConsultar');
        PrimeDropdown.selectValueText(pagina.selectAtatus, 'Sob Análise');
        UsuarioPage.clicarBotao('btnConsulta');
        UsuarioPage.clicarEspecifico('btnVincular', 0);
        UsuarioPage.clicarBotao('btnFechar');
    });

    it('04. Moderar usuário - Fechar com dados - SIM', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnConsultar');
        PrimeDropdown.selectValueText(pagina.selectAtatus, 'Sob Análise');
        UsuarioPage.clicarBotao('btnConsulta');
        UsuarioPage.clicarEspecifico('btnVincular', 0);
        UsuarioPage.setarCampoArea('inputJustificarModeracao', 'justificativa');
        UsuarioPage.clicarBotao('btnFechar');
        UsuarioPage.clicarTexto('button', 'SIM');
    });

    it('05. Moderar usuário - Fechar com dados - NÃO', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnConsultar');
        PrimeDropdown.selectValueText(pagina.selectAtatus, 'Sob Análise');
        UsuarioPage.clicarBotao('btnConsulta');
        UsuarioPage.clicarEspecifico('btnVincular', 0);
        UsuarioPage.setarCampoArea('inputJustificarModeracao', 'justificativa');
        UsuarioPage.clicarBotao('btnFechar');
        UsuarioPage.clicarTexto('button', 'NÃO');
    });

    it('06. Moderar usuário - Obrigatoriedade', async () => {
        UsuarioPage.paginaLogin();
        UsuarioPage.fazerLogin();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaLoginSucesso());
        UsuarioPage.paginaUsuario();
        await expect(browser.getCurrentUrl()).toEqual(UsuarioPage.validarPaginaUsuario());
        UsuarioPage.clicarBotao('btnConsultar');
        PrimeDropdown.selectValueText(pagina.selectAtatus, 'Sob Análise');
        UsuarioPage.clicarBotao('btnConsulta');
        UsuarioPage.clicarEspecifico('btnVincular', 0);
    });

});
