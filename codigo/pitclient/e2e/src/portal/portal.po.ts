import {browser, by, element} from "protractor";
import {PrimengButton, PrimengInputtext} from "@nuvem/protractor-primeng";

export class PortalPage {

    static paginaPortalEmpreendedor() {
        return browser.get('/#/cadastro-usuario/empreendedor');
    }

    static paginaPortalInvestidor() {
        return browser.get('/#/cadastro-usuario/investidor');
    }

    static setarCampoId(id: string, value: string) {
        PrimengInputtext.setInputWithTextByid(id, value);
    }

    static setarCampoSeletor(seletor: string, value: string) {
        PrimengInputtext.setInputWithTextByCss(seletor, value);
    }

    static clicarBotao(id: string) {
        PrimengButton.clickById(id);
    }

    static clicarTexto(seletor: string, value: string) {
        element.all(by.cssContainingText(seletor, value)).first().click();
    }
}
