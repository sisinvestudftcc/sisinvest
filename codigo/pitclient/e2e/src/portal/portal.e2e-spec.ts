import {PortalPage} from "./portal.po";

describe('Funcionalidade do Portal', () => {
    let pagina: PortalPage;

    beforeEach(() => {
        pagina = new PortalPage();
    });

    it('01. Cadastrar Empreendedor', async () => {
        PortalPage.paginaPortalEmpreendedor();
        PortalPage.setarCampoId('inputPassaporte','88888888');
        PortalPage.setarCampoSeletor('input[name=cnpj]','88888888888888');
        PortalPage.setarCampoSeletor('input[name=cpf]','88888888888');
        PortalPage.setarCampoId('inputRne','888888888');
        PortalPage.setarCampoId('inputNome','Nome Empreendedor');
        PortalPage.setarCampoId('inputEmail','empreendedor@email.email');
        PortalPage.clicarBotao('btnSalvar');
    });

    it('01.1. Cadastrar Empreendedor - Adicionar telefone', async () => {
        PortalPage.paginaPortalEmpreendedor();
        PortalPage.setarCampoId('inputTelefone','31999999999');
        PortalPage.clicarBotao('btnAdd');
    });

    it('01.2. Cadastrar Empreendedor - Excluir telefone', async () => {
        PortalPage.paginaPortalEmpreendedor();
        PortalPage.setarCampoId('inputTelefone','31999999999');
        PortalPage.clicarBotao('btnAdd');
        PortalPage.clicarBotao('btnRemoveTel');
    });

    it('02. Cadastrar Empreendedor - Duplicidade', async () => {
        PortalPage.paginaPortalEmpreendedor();
        PortalPage.setarCampoId('inputPassaporte','88888888');
        PortalPage.setarCampoSeletor('input[name=cnpj]','88888888888888');
        PortalPage.setarCampoSeletor('input[name=cpf]','88888888888');
        PortalPage.setarCampoId('inputRne','888888888');
        PortalPage.setarCampoId('inputNome','Nome Empreendedor');
        PortalPage.setarCampoId('inputEmail','empreendedor@email.email');
        PortalPage.clicarBotao('btnSalvar');
    });

    it('03. Cadastrar Empreendedor - E-mail inválido', async () => {
        PortalPage.paginaPortalEmpreendedor();
        PortalPage.setarCampoId('inputEmail','email');
        PortalPage.setarCampoId('inputLinkedin','');
    });

    it('03.1. Cadastrar Empreendedor - Linkedin inválido', async () => {
        PortalPage.paginaPortalEmpreendedor();
        PortalPage.setarCampoId('inputLinkedin','linkedin');
        PortalPage.setarCampoId('inputTelefone','');
    });

    it('03.2. Cadastrar Empreendedor - CPF inválido', async () => {
        PortalPage.paginaPortalEmpreendedor();
        PortalPage.setarCampoSeletor('input[name=cpf]','0');
        PortalPage.setarCampoId('inputRne','');
    });

    it('03.3. Cadastrar Empreendedor - CNPJ inválido', async () => {
        PortalPage.paginaPortalEmpreendedor();
        PortalPage.setarCampoSeletor('input[name=cnpj]','0');
        PortalPage.setarCampoSeletor('input[name=cpf]','');
    });

    it('04. Cadastrar Empreendedor - Cancelar - Sem Dados', async () => {
        PortalPage.paginaPortalEmpreendedor();
        PortalPage.clicarBotao('btnCancelar');
    });

    it('05. Cadastrar Empreendedor - Cancelar - Com Dados - SIM', async () => {
        PortalPage.paginaPortalEmpreendedor();
        PortalPage.setarCampoId('inputPassaporte','0');
        PortalPage.clicarBotao('btnCancelar');
        PortalPage.clicarTexto('button', 'SIM');
    });

    it('05.1. Cadastrar Empreendedor - Cancelar - Com Dados - NÃO', async () => {
        PortalPage.paginaPortalEmpreendedor();
        PortalPage.setarCampoId('inputPassaporte','0');
        PortalPage.clicarBotao('btnCancelar');
        PortalPage.clicarTexto('button', 'NÃO');
    });

    it('06. Cadastrar Empreendedor - Obrigatoriedade', async () => {
        PortalPage.paginaPortalEmpreendedor();
    });

    it('01. Cadastrar Investidor', async () => {
        PortalPage.paginaPortalInvestidor();
        PortalPage.setarCampoId('inputPassaporte','99999999');
        PortalPage.setarCampoSeletor('input[name=cpf]','99999999999');
        PortalPage.setarCampoId('inputNome','Nome Investidor');
        PortalPage.setarCampoId('inputEmail','investidor@email.email');
        PortalPage.clicarBotao('btnSalvar');
    });

    it('01.1. Cadastrar Investidor - Adicionar telefone', async () => {
        PortalPage.paginaPortalInvestidor();
        PortalPage.setarCampoId('inputTelefone','31999999999');
        PortalPage.clicarBotao('btnAdd');
    });

    it('01.2. Cadastrar Investidor - Excluir telefone', async () => {
        PortalPage.paginaPortalInvestidor();
        PortalPage.setarCampoId('inputTelefone','31999999999');
        PortalPage.clicarBotao('btnAdd');
        PortalPage.clicarBotao('btnRemoveTel');
    });

    it('02. Cadastrar Investidor - Duplicidade', async () => {
        PortalPage.paginaPortalInvestidor();
        PortalPage.setarCampoId('inputPassaporte','99999999');
        PortalPage.setarCampoSeletor('input[name=cpf]','99999999999');
        PortalPage.setarCampoId('inputNome','Nome Investidor');
        PortalPage.setarCampoId('inputEmail','investidor@email.email');
        PortalPage.clicarBotao('btnSalvar');
    });

    it('03. Cadastrar Investidor - E-mail inválido', async () => {
        PortalPage.paginaPortalInvestidor();
        PortalPage.setarCampoId('inputEmail','email');
        PortalPage.setarCampoId('inputLinkedin','');
    });

    it('03.1. Cadastrar Investidor - Linkedin inválido', async () => {
        PortalPage.paginaPortalInvestidor();
        PortalPage.setarCampoId('inputLinkedin','linkedin');
        PortalPage.setarCampoId('inputTelefone','');
    });

    it('03.2. Cadastrar Investidor - CPF inválido', async () => {
        PortalPage.paginaPortalInvestidor();
        PortalPage.setarCampoSeletor('input[name=cpf]','0');
        PortalPage.setarCampoId('inputNome', '');
    });

    it('04. Cadastrar Investidor - Cancelar - Sem Dados', async () => {
        PortalPage.paginaPortalInvestidor();
        PortalPage.clicarBotao('btnCancelar');
    });

    it('05. Cadastrar Investidor - Cancelar - Com Dados - SIM', async () => {
        PortalPage.paginaPortalInvestidor();
        PortalPage.setarCampoId('inputPassaporte','0');
        PortalPage.clicarBotao('btnCancelar');
        PortalPage.clicarTexto('button', 'SIM');
    });

    it('05.1. Cadastrar Investidor - Cancelar - Com Dados - NÃO', async () => {
        PortalPage.paginaPortalInvestidor();
        PortalPage.setarCampoId('inputPassaporte','0');
        PortalPage.clicarBotao('btnCancelar');
        PortalPage.clicarTexto('button', 'NÃO');
    });

    it('06. Cadastrar Investidor - Obrigatoriedade', async () => {
        PortalPage.paginaPortalInvestidor();
    });

});
