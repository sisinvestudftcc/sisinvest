require('ts-node').register({
    project: 'e2e/tsconfig.e2e.json'
});
var HtmlReporter = require('protractor-beautiful-reporter');

exports.config = {
    allScriptsTimeout: 42000,
    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: [
        './e2e/src/login/login.e2e-spec.ts',
        './e2e/src/portal/portal.e2e-spec.ts',
        './e2e/src/usuario/usuario.e2e-spec.ts',
        './e2e/src/meus-dados/meus-dados.e2e-spec.ts',
    ],
    capabilities: {
        'browserName': 'chrome',
        'chromeOptions': {
            args: ['--no-sandbox', '--test-type=browser'],
            prefs: {
                'download': {
                    'prompt_for_download': false,
                    'default_directory': __dirname + '/e2e/downloads'
                }
            }
        }
    },
    directConnect: true,
    baseUrl: 'http://localhost:4200',
    framework: 'jasmine2',
    onPrepare: function() {
        jasmine.getEnv().addReporter(new HtmlReporter({
            baseDirectory: 'tmp/screenshots'
        }).getJasmine2Reporter());
        var reporter = new HtmlReporter({
            baseDirectory: 'tmp/screenshots'
        });
    },
    jasmineNodeOpts: {
        showColors: true,
        defaultTimeoutInterval: 300000,
        print: function () {
        }
    }
};
