package br.com.sisinvest.pitservice.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableJpaRepositories("br.com.sisinvest.pitservice.repository")
@EnableTransactionManagement
public class DatabaseConfiguration {
}
