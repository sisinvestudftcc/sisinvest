package br.com.sisinvest.pitseguranca.utils;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public interface ComumUtils {

    default String gerarSenha() {
        String letras = ConstantsUtil.LETRAS;
        String numbers = RandomStringUtils.randomNumeric(2);
        List<Character> senha = RandomStringUtils.random(2, letras)
            .concat(RandomStringUtils.random(2, ConstantsUtil.CARACTERES_ESPECIAIS))
            .concat(RandomStringUtils.random(2, letras.toLowerCase()))
            .concat(numbers).chars().mapToObj(c -> (char) c).collect(Collectors.toList());
        Collections.shuffle(senha);
        return senha.stream().collect(StringBuilder::new, StringBuilder::append, StringBuilder::append).toString();
    }
}
