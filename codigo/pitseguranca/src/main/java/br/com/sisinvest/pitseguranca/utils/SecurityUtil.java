package br.com.sisinvest.pitseguranca.utils;

import br.com.sisinvest.pitseguranca.service.dto.UsuarioLogadoDTO;
import br.gov.nuvem.security.jwt.service.dto.UserDetailsDTO;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;

public interface SecurityUtil {

    default UsuarioLogadoDTO obterUsuarioLogado() {
        if (SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken) {
            return null;
        }
        UserDetailsDTO user = (UserDetailsDTO) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return UsuarioLogadoDTO.builder()
            .perfil(String.valueOf(user.getAttributes().get("perfil")))
            .idUsuario((Long) user.getAttributes().get("idUsuario"))
            .build();
    }
}
