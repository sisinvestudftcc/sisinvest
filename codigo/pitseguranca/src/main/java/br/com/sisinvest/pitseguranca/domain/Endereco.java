package br.com.sisinvest.pitseguranca.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "TB_ENDERECO")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Endereco implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final String SEQ_ENDERECO = "SEQ_ENDERECO";
    public static final String ENDERECO_ID = "ID";

    @Id
    @Column(name = ENDERECO_ID)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQ_ENDERECO)
    @SequenceGenerator(name = SEQ_ENDERECO, sequenceName = SEQ_ENDERECO, allocationSize = 1)
    private Long id;

    @Column(name = "PAIS")
    private String pais;

    @Column(name = "ESTADO")
    private String estado;

    @Column(name = "CIDADE")
    private String cidade;

    @Column(name = "COD_POSTAL")
    private String codigoPostal;
}
