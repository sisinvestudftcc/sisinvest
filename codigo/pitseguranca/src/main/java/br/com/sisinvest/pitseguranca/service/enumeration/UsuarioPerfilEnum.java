package br.com.sisinvest.pitseguranca.service.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum UsuarioPerfilEnum {
    GESTOR("Gestor"),
    INVESTIDOR("Investidor"),
    EMPREENDEDOR("Empreendedor");

    private String descricao;
}
