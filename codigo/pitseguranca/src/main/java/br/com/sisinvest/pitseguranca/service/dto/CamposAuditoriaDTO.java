package br.com.sisinvest.pitseguranca.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CamposAuditoriaDTO {

    private String campo;
    private Object valorAnterior;
    private Object valorAtual;
}
