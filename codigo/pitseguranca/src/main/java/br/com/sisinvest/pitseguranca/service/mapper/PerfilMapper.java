package br.com.sisinvest.pitseguranca.service.mapper;

import br.com.sisinvest.pitseguranca.domain.Perfil;
import br.com.sisinvest.pitseguranca.service.dto.PerfilDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PerfilMapper {

    Perfil toEntity(PerfilDTO dto);

    List<PerfilDTO> dtoList(List<Perfil> listEntity);
}
