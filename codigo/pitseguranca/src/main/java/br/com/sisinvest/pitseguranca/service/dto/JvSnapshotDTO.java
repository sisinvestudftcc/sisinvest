package br.com.sisinvest.pitseguranca.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Map;

@Getter
@Setter
public class JvSnapshotDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long snapshotPk;
    private String type;
    private Long version;
    private Long commitPk;
    private String author;
    private LocalDateTime commitDate;
    private LocalDateTime commitDateInstant;
    private BigDecimal commitId;
    private Map<String, String> jvCommitProperties;
    private Long globalIdPk;
    private String localId;
    private String fragment;
    private String typeName;
    private String ownerIdFk;

}
