package br.com.sisinvest.pitseguranca.service;

import br.com.sisinvest.pitseguranca.domain.Usuario;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;

@Slf4j
@Service
@RequiredArgsConstructor
public class EmailService {

    public final JavaMailSender emailSender;

    @Value("${application.mail.endereco-remetente}")
    private String remetente;
    @Value("${application.mail.nome-remetente}")
    private String nomeRemetente;

    private static final String ASSUNTO_EMAIL = "[SISINVEST] Cadastro de usuário";
    private static final String RECUPERACAO_SENHA = "[SISINVEST] Recuperação de Senha";
    private static final String CADASTRO_ANALISE = "[SISINVEST] Solicitação de cadastro";
    private static final String CADASTRO_APROVADO = "[SISINVEST] Cadastro Aprovado";
    private static final String CADASTRO_REPROVADO = "[SISINVEST] Cadastro Reprovado";
    private static final String SAUDACAO = "<p><span> Prezado(a),</span></p> <br/><span>";
    private static final String ASSINATURA_EMAIL = "</br><p><span>SISINVEST</span></p>" +
        "<br/><p>ATENÇÃO! Esta mensagem foi gerada e enviada automaticamente pelo sistema. Favor não responder.</p>";

    private static final String PATH_LOGIN = "/#/login";
    private static final String PATH_USUARIO = "/#/usuario";

    @Async
    public void enviarEmailCadastro(String email, String baseUrl, String senha) {
        String sb = SAUDACAO + "<span> Seu cadastro foi realizado com sucesso!" +
            "<br/> Caso tenha se inscrito como empreendedor, pedimos a gentileza que acesse o SISINVEST " +
            "e cadastre o seu projeto para que seja disponibilizado na nossa plataforma, objetivando" +
            " a atração e captação de investimentos nacionais e internacionais para o seu empreendimento." +
            "<br/> Importante realçar, que caso isso não aconteça, o seu perfil será desativado automaticamente no prazo de <b>45 dias</b>." +
            "<br/>Qualquer dúvida, estamos à disposição." +
            "<br/>Para acessar o sistema basta <span><a href=" + baseUrl + PATH_LOGIN + ">clicar aqui</a>.<br/><p>Sua senha é: <b>" +
            senha + "</b></p><br/>" + ASSINATURA_EMAIL;
        sendBasicEmail(email, ASSUNTO_EMAIL, sb);
    }

    @Async
    public void enviarEmailRecuperacao(Usuario usuario, String baseUrl, String senha) {
        String sb = SAUDACAO + usuario.getNome() +
            ", recebemos uma solicitação para redefinir a sua senha.</span><br/><p>Sua nova senha é: <b>" +
            senha + "</b><br/>Para acessar o sistema basta <span><a href=" + baseUrl + PATH_LOGIN +
            ">clicar aqui</a>.</b></p><br/>" + ASSINATURA_EMAIL;
        sendBasicEmail(usuario.getEmail(), RECUPERACAO_SENHA, sb);
    }

    @Async
    public void enviarEmailAprovado(Usuario usuario, String baseUrl, String senha) {
        String sb = SAUDACAO + "Seu cadastro foi aprovado no SISINVEST.</span><br/>" +
            "<br/>Caso tenha se inscrito como empreendedor, pedimos a gentileza que acesse o SISINVEST " +
            "e cadastre o seu projeto para que seja disponibilizado na nossa plataforma, " +
            "objetivando a atração e captação de investimentos nacionais e internacionais para o seu empreendimento." +
            "<br/>Importante realçar, que caso isso não aconteça, o seu perfil será desativado automaticamente " +
            "no prazo de <b>45 dias</b>.<br/>Qualquer dúvida, estamos à disposição." +
            "<br/>Para acessar o sistema basta <span><a href=" + baseUrl + PATH_LOGIN +
            ">clicar aqui</a>.</b></p><p>Sua senha é: <b>" + senha + "</b><br/>";
        sendBasicEmail(usuario.getEmail(), CADASTRO_APROVADO, sb);
    }

    @Async
    public void enviarEmailReprovado(Usuario usuario, String baseUrl) {
        String sb = SAUDACAO + "Seu cadastro não foi aprovado no SISINVEST.</span><br/>" +
            "<br/>Para maiores informações acesse: <span><a href=" + baseUrl + PATH_LOGIN + ">Link</a>.</b>" +
            "<br/>" + ASSINATURA_EMAIL;
        sendBasicEmail(usuario.getEmail(), CADASTRO_REPROVADO, sb);
    }

    @Async
    public void enviarEmailSolicitacaoCadastro(String email) {
        String sb = SAUDACAO + " Seu cadastro esta sendo avaliado para obter acesso ao Portal de Investimentos " +
            "do Turismo.</span><br/><br/>" + ASSINATURA_EMAIL;
        sendBasicEmail(email, CADASTRO_ANALISE, sb);
    }

    @Async
    public void enviarEmailAprovacaoGestor(String email, String baseUrl) {
        String sb = SAUDACAO + " Um usuário solicitou acesso ao SISINVEST e aguarda análise " +
            "para obter acesso.</span><br/><br/>Para avaliar o cadastro clique aqui: <span>" +
            "<a href=" + baseUrl + PATH_USUARIO + ">Link</a>.</b>" +
            "<br/>" + ASSINATURA_EMAIL;
        sendBasicEmail(email, CADASTRO_ANALISE, sb);
    }

    public void sendBasicEmail(String to, String subject, String text) {
        try {
            MimeMessage mail = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mail);
            helper.setTo(to);
            helper.setFrom(remetente, nomeRemetente);
            helper.setSubject(subject);
            helper.setText(text, true);
            emailSender.send(mail);
        } catch (MessagingException | UnsupportedEncodingException e) {
            log.debug(String.format("Falha ao enviar email para [%s] com assunto: [%s]", to, subject), e);
        }
    }
}
