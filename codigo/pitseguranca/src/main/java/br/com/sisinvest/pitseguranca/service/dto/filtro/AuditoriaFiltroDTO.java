package br.com.sisinvest.pitseguranca.service.dto.filtro;

import br.com.sisinvest.pitseguranca.service.enumeration.AuditoriaEnum;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@Setter
public class AuditoriaFiltroDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    private String id;
    @NotNull
    private AuditoriaEnum auditoria;
}
