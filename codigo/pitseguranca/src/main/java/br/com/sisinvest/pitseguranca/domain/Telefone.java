package br.com.sisinvest.pitseguranca.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "TB_TELEFONE")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Telefone implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final String SEQ_TELEFONE = "SEQ_TELEFONE";
    public static final String TELEFONE_ID = "ID";

    @Id
    @Column(name = TELEFONE_ID)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQ_TELEFONE)
    @SequenceGenerator(name = SEQ_TELEFONE, sequenceName = SEQ_TELEFONE, allocationSize = 1)
    private Long id;

    @Column(name = "NUMERO")
    private String numero;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(referencedColumnName = Usuario.USUARIO_ID, name = "USUARIO_ID")
    private Usuario usuario;
}
