package br.com.sisinvest.pitseguranca.service.feign;

import br.com.sisinvest.pitseguranca.service.dto.CustomPageImpl;
import br.com.sisinvest.pitseguranca.service.dto.JvSnapshotDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@FeignClient(url = "${application.feign.auditoriaUrl}", name = "pitauditoriapesquisa")
public interface PesquisaAuditoriaFeignClient {

    /**
     * GET /api/cdo/filtragem : Busca o historico de acordo com a entidade e o ID.
     *
     * @param pLocalID  ID do objeto
     * @param pTypeName Entidade a ser filtrada.
     * @param pageNum   Numero da pagina.
     * @param pageSize  Tamanho da pagina.
     * @param sortField Item de ordenação.
     * @return Lista de historicos
     */
    @GetMapping(value = "/api/cdo/filtragem", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    CustomPageImpl<JvSnapshotDTO> listarPorEntidade(
        @RequestParam(name = "localId") final String pLocalID,
        @RequestParam(name = "typeName") final String pTypeName, @RequestParam("size") Integer pageSize,
        @RequestParam("sort") String sortField);

    /**
     * Busca as diferenças de determinada entidade
     *
     * @param pTypeName   Entidade a ser consultada
     * @param pLocalID    ID do item da entidade a ser consultada.
     * @param pCommitDate Data da ocorrencia
     * @return Lista com as diferenças
     */
    @GetMapping(value = "/api/cdo/diff", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    Map<String, Object> carregarDiferenca(
        @RequestParam(name = "localId") final String pLocalID,
        @RequestParam(name = "typeName") final String pTypeName,
        @RequestParam(name = "commitDate") final String pCommitDate);

}
