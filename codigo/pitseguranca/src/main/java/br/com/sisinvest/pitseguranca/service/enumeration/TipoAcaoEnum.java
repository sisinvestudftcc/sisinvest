package br.com.sisinvest.pitseguranca.service.enumeration;

import lombok.Getter;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

@Getter
public enum TipoAcaoEnum {
    EDICAO,
    ATIVACAO,
    INATIVACAO,
    ALTERACAO,
    MODERACAO,
    CADASTRO;

    private static final String INITIAL = "INITIAL";

    public static TipoAcaoEnum verificaStatus(String campo, String valor) {
        if (!StringUtils.equals("status", campo)) {
            return null;
        }
        if (StatusEnum.ATIVO.equals(StatusEnum.valueOf(valor))) {
            return ATIVACAO;
        }
        if (StatusEnum.REPROVADO.equals(StatusEnum.valueOf(valor))) {
            return MODERACAO;
        }
        return INATIVACAO;
    }

    public static TipoAcaoEnum getTipoAcao(String tipo, TipoAcaoEnum tipoAcao, Object nome, String usuarioAcao) {
        if (INITIAL.equals(tipo)) {
            return CADASTRO;
        }
        if (ObjectUtils.isNotEmpty(nome) && nome.equals(usuarioAcao)) {
            return EDICAO;
        }
        if (tipoAcao == null) {
            return ALTERACAO;
        }
        return tipoAcao;
    }
}
