package br.com.sisinvest.pitseguranca.repository;

import br.com.sisinvest.pitseguranca.domain.AreaInteresse;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface AreaInteresseRepository extends JpaRepository<AreaInteresse, Long> {

    Set<AreaInteresse> findByIdIn(Set<Long> ids);
}
