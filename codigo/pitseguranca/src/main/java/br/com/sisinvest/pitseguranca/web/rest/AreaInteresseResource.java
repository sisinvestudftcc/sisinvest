package br.com.sisinvest.pitseguranca.web.rest;

import br.com.sisinvest.pitseguranca.service.AreaInteresseService;
import br.com.sisinvest.pitseguranca.service.dto.AreaInteresseDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/public/area-interesse")
public class AreaInteresseResource {

    private final AreaInteresseService service;

    @GetMapping
    public ResponseEntity<List<AreaInteresseDTO>> buscarTodos() {
        log.info("REST requisição para buscar todas as categorias de Áreas de Interesse");
        return ResponseEntity.ok(service.buscarTodos());
    }
}
