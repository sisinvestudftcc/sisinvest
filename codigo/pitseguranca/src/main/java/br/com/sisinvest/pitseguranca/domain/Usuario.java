package br.com.sisinvest.pitseguranca.domain;

import br.com.sisinvest.pitseguranca.service.enumeration.StatusEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Set;
import java.sql.Timestamp;

@Getter
@Setter
@Entity
@Table(name = "TB_USUARIO")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Usuario implements Serializable {

    public static final String USUARIO_ID = "ID";
    private static final long serialVersionUID = 1L;
    private static final String SEQ_USUARIO = "SEQ_USUARIO";

    @Id
    @Column(name = USUARIO_ID)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQ_USUARIO)
    @SequenceGenerator(name = SEQ_USUARIO, sequenceName = SEQ_USUARIO, allocationSize = 1)
    private Long id;

    @Column(name = "NOME", nullable = false)
    private String nome;

    @Column(name = "SENHA")
    private String senha;

    @Column(name = "EMAIL", nullable = false)
    private String email;

    @Column(name = "STATUS", nullable = false)
    @Enumerated(EnumType.STRING)
    private StatusEnum status;

    @Column(name = "JUSTIFICATIVA")
    private String justificativa;

    @Column(name = "PASSAPORTE")
    private String passaporte;

    @Column(name = "CPF")
    private String cpf;

    @Column(name = "CNPJ")
    private String cnpj;

    @Column(name = "NUMERO_RNE")
    private String rne;

    @Column(name = "URL_LINKEDIN")
    private String linkedin;

    @Column(name = "INFO_ADICIONAL")
    private String informacaoAdicional;

    @Column(name = "DATA_CRIACAO")
    private Timestamp dataCriacao;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(referencedColumnName = Perfil.PERFIL_ID, name = "PERFIL_ID")
    private Perfil perfil;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinColumn(referencedColumnName = Endereco.ENDERECO_ID, name = "ENDERECO_ID")
    private Endereco endereco;

    @OneToMany(mappedBy = "usuario", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Telefone> telefones;

    @ManyToMany
    @JoinTable(name = "TB_USUARIO_INTERESSE",
        joinColumns = @JoinColumn(name = "USUARIO_ID"),
        inverseJoinColumns = @JoinColumn(name = "AREA_INTERESSE_ID"))
    private Set<AreaInteresse> areaInteresses;
}
