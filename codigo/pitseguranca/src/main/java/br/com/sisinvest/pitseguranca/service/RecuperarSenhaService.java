package br.com.sisinvest.pitseguranca.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.UriComponentsBuilder;

@Service
@Transactional
@RequiredArgsConstructor
public class RecuperarSenhaService {

    private final UsuarioService usuarioService;

    public void recuperarSenha(String email, UriComponentsBuilder builder) {
        usuarioService.novaSenha(email, builder);
    }
}
