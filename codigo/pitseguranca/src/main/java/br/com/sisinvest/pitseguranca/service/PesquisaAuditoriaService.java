package br.com.sisinvest.pitseguranca.service;

import br.com.sisinvest.pitseguranca.service.feign.PesquisaAuditoriaFeignClient;
import br.com.sisinvest.pitseguranca.service.dto.AuditoriaDTO;
import br.com.sisinvest.pitseguranca.service.dto.CamposAuditoriaDTO;
import br.com.sisinvest.pitseguranca.service.dto.CustomPageImpl;
import br.com.sisinvest.pitseguranca.service.dto.JvSnapshotDTO;
import br.com.sisinvest.pitseguranca.service.dto.filtro.AuditoriaFiltroDTO;
import br.com.sisinvest.pitseguranca.service.enumeration.StatusEnum;
import br.com.sisinvest.pitseguranca.service.enumeration.TipoAcaoEnum;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class PesquisaAuditoriaService {

    private final PesquisaAuditoriaFeignClient auditoriaFeignClient;

    public List<AuditoriaDTO> listarAuditoria(AuditoriaFiltroDTO filtro) {
        final CustomPageImpl<JvSnapshotDTO> auditorias = auditoriaFeignClient.listarPorEntidade(filtro.getId(),
            filtro.getAuditoria().getTypeName(), Integer.MAX_VALUE, "commitDate");
        List<AuditoriaDTO> resultados = new ArrayList<>();
        if (auditorias != null && !CollectionUtils.isEmpty(auditorias.getContent())) {
            Map<String, Object> campos = new HashMap<>();
            for (JvSnapshotDTO audit : auditorias.getContent()) {
                AuditoriaDTO auditoria = new AuditoriaDTO();
                auditoria.setUsuarioAcao(audit.getAuthor());
                auditoria.setDataHoraAcao(audit.getCommitDate());
                // TODO refatorar essa busca dentro de for
                populaCampos(auditoria, filtro, audit, campos);
                auditoria.getCampos().forEach(it -> campos.put(it.getCampo(), it.getValorAtual()));
                if (!CollectionUtils.isEmpty(auditoria.getCampos())) {
                    resultados.add(auditoria);
                }
            }
        }
        Collections.reverse(resultados);
        return resultados;
    }

    private Object adicionaUltimaReferencia(Map<String, Object> ultimosCampos, String referencia) {
        if (CollectionUtils.isEmpty(ultimosCampos)) {
            return null;
        }
        return ultimosCampos.getOrDefault(referencia, null);
    }

    private void populaCampos(AuditoriaDTO auditoria, AuditoriaFiltroDTO filtro, JvSnapshotDTO audit, Map<String, Object> camposAnteriores) {
        final Map<String, Object> campos = auditoriaFeignClient.carregarDiferenca(filtro.getId(),
            filtro.getAuditoria().getTypeName(), audit.getCommitDate().toString());
        List<CamposAuditoriaDTO> listaCampos = new ArrayList<>();
        TipoAcaoEnum tipoAcaoEnum = montarCamposAuditados(campos, camposAnteriores, listaCampos);
        auditoria.setCampos(listaCampos);
        auditoria.setTipoAcao(TipoAcaoEnum.getTipoAcao(audit.getType(), tipoAcaoEnum, camposAnteriores.get("nome"), auditoria.getUsuarioAcao()));
    }

    private TipoAcaoEnum montarCamposAuditados(Map<String, Object> campos, Map<String, Object> camposAnteriores, List<CamposAuditoriaDTO> listaCampos) {
        TipoAcaoEnum tipoAcaoEnum = null;
        for (Map.Entry<String, Object> entry : campos.entrySet()) {
            String chave = entry.getKey();
            Object valor = entry.getValue();
            if (!StringUtils.equals(chave, "senha")) {
                tipoAcaoEnum = TipoAcaoEnum.verificaStatus(chave, valor.toString());
                CamposAuditoriaDTO campoAuditoria = CamposAuditoriaDTO.builder()
                    .campo(chave).valorAnterior(adicionaUltimaReferencia(camposAnteriores, chave)).valorAtual(valor).build();
                validarCampoAuditoria(listaCampos, campoAuditoria);
            }
            List<String> statusModeracao = Arrays.asList(StatusEnum.SOB_ANALISE.toString(), StatusEnum.REPROVADO.toString());
            if (camposAnteriores.size() > 0 && statusModeracao.contains(camposAnteriores.get("status").toString())) {
                tipoAcaoEnum = TipoAcaoEnum.MODERACAO;
            }
        }
        return tipoAcaoEnum;
    }

    private void validarCampoAuditoria(List<CamposAuditoriaDTO> listaCampos, CamposAuditoriaDTO campoAuditoria) {
        if (!ObjectUtils.isEmpty(campoAuditoria.getValorAnterior()) || !ObjectUtils.isEmpty(campoAuditoria.getValorAtual())) {
            listaCampos.add(campoAuditoria);
        }
    }
}
