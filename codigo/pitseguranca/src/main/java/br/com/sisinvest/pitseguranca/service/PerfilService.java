package br.com.sisinvest.pitseguranca.service;

import br.com.sisinvest.pitseguranca.repository.PerfilRepository;
import br.com.sisinvest.pitseguranca.domain.Perfil;
import br.com.sisinvest.pitseguranca.service.dto.PerfilDTO;
import br.com.sisinvest.pitseguranca.service.enumeration.UsuarioPerfilEnum;
import br.com.sisinvest.pitseguranca.service.mapper.PerfilMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.exact;

@Service
@Transactional
@RequiredArgsConstructor
public class PerfilService {

    private final PerfilMapper mapper;
    private final PerfilRepository repository;

    public List<PerfilDTO> filtro(PerfilDTO filtro) {
        return mapper.dtoList(buscarPerfis(filtro));
    }

    public List<Perfil> buscarPerfis(PerfilDTO filtro) {
        return repository.findAll(Example.of(mapper.toEntity(filtro), filtroPerfil()));
    }

    public Perfil buscarPerfilPorNome(UsuarioPerfilEnum nome) {
        return repository.findByNome(nome);
    }

    private ExampleMatcher filtroPerfil() {
        return ExampleMatcher.matching()
            .withMatcher("id", exact())
            .withMatcher("nome", exact());
    }
}
