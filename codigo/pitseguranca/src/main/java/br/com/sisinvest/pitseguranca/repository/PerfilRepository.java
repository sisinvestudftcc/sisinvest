package br.com.sisinvest.pitseguranca.repository;

import br.com.sisinvest.pitseguranca.domain.Perfil;
import br.com.sisinvest.pitseguranca.service.enumeration.UsuarioPerfilEnum;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PerfilRepository extends JpaRepository<Perfil, Long> {
    Perfil findByNome(UsuarioPerfilEnum nomePerfil);
}
