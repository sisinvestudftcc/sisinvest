package br.com.sisinvest.pitseguranca.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class EnderecoDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String pais;
    private String estado;
    private String cidade;
    private String codigoPostal;
}
