package br.com.sisinvest.pitseguranca.security;

public final class AuthoritiesConstants {

    public static final String GESTOR = "ROLE_GESTOR";
    public static final String EMPREENDEDOR = "ROLE_EMPREENDEDOR";
    public static final String INVESTIDOR = "ROLE_INVESTIDOR";

    private AuthoritiesConstants() {
    }
}
