package br.com.sisinvest.pitseguranca.service.dto;

import br.com.sisinvest.pitseguranca.service.enumeration.CamposValidacaoEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CamposValidacaoDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String valor;
    private CamposValidacaoEnum tipo;
    private Long id;
}
