package br.com.sisinvest.pitseguranca.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class UsuarioCpfDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String erro;
}
