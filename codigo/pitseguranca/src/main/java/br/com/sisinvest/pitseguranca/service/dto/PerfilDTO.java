package br.com.sisinvest.pitseguranca.service.dto;

import br.com.sisinvest.pitseguranca.service.enumeration.UsuarioPerfilEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PerfilDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private UsuarioPerfilEnum nome;
    private String descricao;
}
