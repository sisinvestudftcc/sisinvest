package br.com.sisinvest.pitseguranca.service;

import br.com.sisinvest.pitseguranca.repository.UsuarioRepository;
import br.com.sisinvest.pitseguranca.service.dto.filtro.UsuarioFiltroDTO;
import br.com.sisinvest.pitseguranca.service.mapper.UsuarioMapper;
import br.com.sisinvest.pitseguranca.utils.ComumUtils;
import br.com.sisinvest.pitseguranca.utils.ConstantsUtil;
import br.com.sisinvest.pitseguranca.utils.SecurityUtil;
import br.com.sisinvest.pitseguranca.domain.AreaInteresse;
import br.com.sisinvest.pitseguranca.domain.Endereco;
import br.com.sisinvest.pitseguranca.domain.Endereco_;
import br.com.sisinvest.pitseguranca.domain.Perfil;
import br.com.sisinvest.pitseguranca.domain.Perfil_;
import br.com.sisinvest.pitseguranca.domain.Telefone;
import br.com.sisinvest.pitseguranca.domain.Telefone_;
import br.com.sisinvest.pitseguranca.domain.Usuario;
import br.com.sisinvest.pitseguranca.domain.Usuario_;
import br.com.sisinvest.pitseguranca.service.dto.CamposValidacaoDTO;
import br.com.sisinvest.pitseguranca.service.dto.EnderecoDTO;
import br.com.sisinvest.pitseguranca.service.dto.PerfilDTO;
import br.com.sisinvest.pitseguranca.service.dto.SenhaUsuario;
import br.com.sisinvest.pitseguranca.service.dto.UsuarioDTO;
import br.com.sisinvest.pitseguranca.service.dto.UsuarioLogadoDTO;
import br.com.sisinvest.pitseguranca.service.enumeration.CamposValidacaoEnum;
import br.com.sisinvest.pitseguranca.service.enumeration.StatusEnum;
import br.com.sisinvest.pitseguranca.service.enumeration.UsuarioPerfilEnum;
import br.gov.nuvem.comum.microsservico.web.rest.errors.ParametrizedMessageException;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.util.UriComponentsBuilder;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class UsuarioService implements ComumUtils, SecurityUtil {

    private final PerfilService perfilService;
    private final EmailService emailService;
    private final AreaInteresseService areaInteresseService;
    private final UsuarioMapper mapper;
    private final UsuarioRepository repository;
    private final PasswordEncoder encoder;
    private final Argon2PasswordEncoder argonEncoder;

    public void alterarSenha(SenhaUsuario senhaUsuario) {
        UsuarioLogadoDTO logado = obterUsuarioLogado();
        Usuario usuario = findById(logado.getIdUsuario());
        if (!argonEncoder.matches(senhaUsuario.getSenhaAtual(), usuario.getSenha())) {
            throw new ParametrizedMessageException("senha.incorreta", ConstantsUtil.ERROR_TITLE);
        }
        if (!StringUtils.equals(senhaUsuario.getNovaSenha(), senhaUsuario.getConfirmarSenha())) {
            throw new ParametrizedMessageException("senha.diferente.confirmacao", ConstantsUtil.ERROR_TITLE);
        }
        verificarNovaSenha(senhaUsuario.getNovaSenha());
        usuario.setSenha(encoder.encode(senhaUsuario.getNovaSenha()));
        salvarUsuario(usuario);
    }

    public void alterarStatus(Long id, String justificativa) {
        Usuario usuario = findById(id);
        usuarioEmAnalise(usuario.getStatus());
        if (StatusEnum.ATIVO.equals(usuario.getStatus())) {
            incluirJustificativa(justificativa, usuario);
            usuario.setStatus(StatusEnum.INATIVO);
            salvarUsuario(usuario);
            return;
        }
        if (UsuarioPerfilEnum.EMPREENDEDOR.equals(usuario.getPerfil().getNome())
            && StatusEnum.INATIVO.equals(usuario.getStatus())) {
            incluirJustificativa(justificativa, usuario);
        }
        usuario.setStatus(StatusEnum.ATIVO);
        salvarUsuario(usuario);
    }

    private void incluirJustificativa(String justificativa, Usuario usuario) {
        if (StringUtils.isBlank(justificativa)) {
            erroCampoObrigatorioException();
        }
        usuario.setJustificativa(justificativa);
    }

    @Transactional(readOnly = true)
    public UsuarioDTO buscarLogado() {
        return buscarPorId(obterUsuarioLogado().getIdUsuario());
    }

    @Transactional(readOnly = true)
    public UsuarioDTO buscarPorId(Long id) {
        Usuario usuario = findById(id);
        UsuarioDTO usuarioDto = mapper.toDto(usuario);
        if (!CollectionUtils.isEmpty(usuario.getAreaInteresses())) {
            usuarioDto.setAreaInteresseIds(usuario.getAreaInteresses().stream().map(AreaInteresse::getId).collect(Collectors.toSet()));
        }
        return usuarioDto;
    }

    public void cadastroExterno(UsuarioDTO usuarioDto, UriComponentsBuilder builder) {
        usuarioDto.setId(null);
        verificarCampo(new CamposValidacaoDTO(usuarioDto.getEmail(), CamposValidacaoEnum.EMAIL, usuarioDto.getId()));
        usuarioDto.setStatus(StatusEnum.SOB_ANALISE);
        verificacaoRestricaoCampos(usuarioDto);
        if (UsuarioPerfilEnum.EMPREENDEDOR.equals(usuarioDto.getPerfilEnum()) && StringUtils.isAllEmpty(usuarioDto.getPassaporte(),
            usuarioDto.getCnpj(), usuarioDto.getCpf(), usuarioDto.getRne()) || UsuarioPerfilEnum.INVESTIDOR.equals(usuarioDto.getPerfilEnum()) &&
            StringUtils.isAllEmpty(usuarioDto.getPassaporte(), usuarioDto.getCpf())) {
            erroCampoObrigatorioException();
        }
        Usuario usuario = mapper.toEntity(usuarioDto);
        setarDados(usuario, usuarioDto);
        salvarUsuario(usuario);
        emailService.enviarEmailSolicitacaoCadastro(usuario.getEmail());
        enviarEmailGestores(builder);
    }

    private void enviarEmailGestores(UriComponentsBuilder builder) {
        Perfil perfil = perfilService.buscarPerfilPorNome(UsuarioPerfilEnum.GESTOR);
        List<String> emailGestores = repository.getEmailGestores(perfil.getId());
        emailGestores.forEach(emailGestor -> emailService.enviarEmailAprovacaoGestor(emailGestor, builder.build().toString()));
    }

    @Transactional(readOnly = true)
    public Page<UsuarioDTO> filtrar(UsuarioFiltroDTO filtro, Pageable pageable) {
        filtro.setEndereco(Optional.ofNullable(filtro.getEndereco()).orElse(new EnderecoDTO()));
        return repository.findAll(specFiltro(filtro), pageable).map(mapper::toDto);
    }

    public void moderarUsuario(Long id, StatusEnum status, String justificativa, UriComponentsBuilder builder) {
        Usuario usuario = findById(id);
        if (StatusEnum.SOB_ANALISE.equals(usuario.getStatus()) || StatusEnum.REPROVADO.equals(usuario.getStatus())) {
            usuario.setStatus(status);
            incluirJustificativa(justificativa, usuario);
            String senha = gerarSenha();
            usuario.setSenha(encoder.encode(senha));
            salvarUsuario(usuario);
            if (StatusEnum.REPROVADO.equals(status)) {
                emailService.enviarEmailReprovado(usuario, builder.build().toString());
                return;
            }
            emailService.enviarEmailAprovado(usuario, builder.build().toString(), senha);
        }
    }

    public void novaSenha(String email, UriComponentsBuilder builder) {
        Usuario usuario = usuarioPorEmail(email);
        if (usuario == null) {
            throw usuarioInexistenteException();
        }
        restricaoUsuario(usuario.getStatus());
        String senha = gerarSenha();
        usuario.setSenha(encoder.encode(senha));
        salvarUsuario(usuario);
        emailService.enviarEmailRecuperacao(usuario, builder.build().toString(), senha);
    }

    public void save(UsuarioDTO usuarioDto, UriComponentsBuilder builder) {
        removerEspaco(usuarioDto);
        verificarCampo(new CamposValidacaoDTO(usuarioDto.getEmail(), CamposValidacaoEnum.EMAIL, usuarioDto.getId()));
        if (usuarioDto.getId() == null) {
            usuarioDto.setStatus(StatusEnum.ATIVO);
        }
        restricaoUsuario(usuarioDto.getStatus());
        if (!UsuarioPerfilEnum.GESTOR.equals(usuarioDto.getPerfilEnum())) {
            Optional.of(usuarioDto.getInformacaoAdicional().trim()).ifPresent(info -> usuarioDto.setInformacaoAdicional(info.trim()));
            validarUsuarios(usuarioDto);
        }
        senhaUsuario(usuarioDto, builder);
    }

    @Transactional(readOnly = true)
    public Usuario buscarPorEmail(String email) {
        Usuario usuario = repository.findByEmail(email);
        if (usuario == null) {
            throw usuarioInexistenteException();
        }
        restricaoUsuario(usuario.getStatus());
        return usuario;
    }

    @Transactional(readOnly = true)
    public void verificarCampo(CamposValidacaoDTO camposValidacaoDto) {
        validacaoCampos(camposValidacaoDto.getValor(), camposValidacaoDto.getTipo());
        if (!StringUtils.isEmpty(camposValidacaoDto.getValor()) && repository.count(specUsuario(camposValidacaoDto)) > 0) {
            dadoCadastradoException();
        }
    }

    @Transactional(readOnly = true)
    public void verificarSenha(String senha) {
        Usuario usuario = findById(obterUsuarioLogado().getIdUsuario());
        if (!argonEncoder.matches(senha, usuario.getSenha())) {
            throw new ParametrizedMessageException("senha.incorreta", ConstantsUtil.ERROR_TITLE);
        }
    }

    @Transactional(readOnly = true)
    public void verificarNovaSenha(String novaSenha) {
        if (!novaSenha.matches(ConstantsUtil.PATTERN_SENHA)) {
            throw new ParametrizedMessageException("senha.formato.incorreto", ConstantsUtil.ERROR_TITLE);
        }
    }

    private void dadoCadastradoException() {
        throw new ParametrizedMessageException("dado.ja.cadastrado", ConstantsUtil.ERROR_TITLE);
    }

    private void erroCampoObrigatorioException() {
        throw new ParametrizedMessageException("erro.preenchimento", ConstantsUtil.ERROR_TITLE);
    }

    private Usuario findById(Long id) {
        return repository.findById(id).orElseThrow(this::usuarioInexistenteException);
    }

    private void restricaoUsuario(StatusEnum status) {
        usuarioEmAnalise(status);
        if (StatusEnum.INATIVO.equals(status) || StatusEnum.REPROVADO.equals(status)) {
            throw new ParametrizedMessageException("usuario.inativo", ConstantsUtil.ERROR_TITLE);
        }
    }

    private void salvarUsuario(Usuario usuario) {
        usuario.setCpf(verificaCampoVazio(usuario.getCpf()));
        usuario.setRne(verificaCampoVazio(usuario.getRne()));
        usuario.setCnpj(verificaCampoVazio(usuario.getCnpj()));
        usuario.setLinkedin(verificaCampoVazio(usuario.getLinkedin()));
        usuario.setPassaporte(verificaCampoVazio(usuario.getPassaporte()));

        if (usuario.getId() == null) {
            usuario.setDataCriacao(new Timestamp(System.currentTimeMillis()));
        }
        repository.save(usuario);
    }

    private String verificaCampoVazio(String valor) {
        return Optional.ofNullable(valor).filter(StringUtils::isNotEmpty).orElse(null);
    }

    private void senhaUsuario(UsuarioDTO usuarioDto, UriComponentsBuilder builder) {
        Usuario usuario = mapper.toEntity(usuarioDto);
        setarDados(usuario, usuarioDto);
        if (usuarioDto.getId() != null) {
            usuario.setJustificativa(findById(usuario.getId()).getJustificativa());
            usuario.setSenha(findById(usuarioDto.getId()).getSenha());
            salvarUsuario(usuario);
            return;
        }
        String senha = gerarSenha();
        usuario.setSenha(encoder.encode(senha));
        salvarUsuario(usuario);
        emailService.enviarEmailCadastro(usuario.getEmail(), builder.build().toString(), senha);
    }

    private void setarDados(Usuario usuario, UsuarioDTO usuarioDto) {
        usuario.setPerfil(perfilService.buscarPerfis(PerfilDTO.builder().nome(usuarioDto.getPerfilEnum()).build()).get(0));
        if (!CollectionUtils.isEmpty(usuario.getTelefones())) {
            usuario.getTelefones().forEach(telefone -> telefone.setUsuario(usuario));
        }
        if (!CollectionUtils.isEmpty(usuarioDto.getAreaInteresseIds())) {
            Set<AreaInteresse> interesseUsuario = areaInteresseService.findByIdIn(usuarioDto.getAreaInteresseIds());
            usuario.setAreaInteresses(interesseUsuario);
        }
    }

    private ParametrizedMessageException usuarioInexistenteException() {
        return new ParametrizedMessageException("usuario.nao.encontrado", ConstantsUtil.ERROR_TITLE);
    }

    private void removerEspaco(UsuarioDTO usuarioDto) {
        if (StringUtils.isBlank(usuarioDto.getNome())) {
            erroCampoObrigatorioException();
        }
    }

    private void usuarioEmAnalise(StatusEnum status) {
        if (StatusEnum.SOB_ANALISE.equals(status)) {
            throw new ParametrizedMessageException("usuario.sob.analise", ConstantsUtil.ERROR_TITLE);
        }
    }

    private Usuario usuarioPorEmail(String email) {
        return repository.findByEmail(email);
    }

    private void validacaoCampos(String valor, CamposValidacaoEnum tipo) {
        if (CamposValidacaoEnum.LINKEDIN.equals(tipo) && !StringUtils.isEmpty(valor) && !valor.matches(ConstantsUtil.PATTERN_LINKEDIN)) {
            throw new ParametrizedMessageException("dado.invalido", ConstantsUtil.ERROR_TITLE);
        }
        if (CamposValidacaoEnum.CPF.equals(tipo) && !StringUtils.isEmpty(valor)) {
            if (!validarCpf(valor)) {
                throw new ParametrizedMessageException("dado.invalido", ConstantsUtil.ERROR_TITLE);
            }
        }
        if (CamposValidacaoEnum.CNPJ.equals(tipo) && !StringUtils.isEmpty(valor)) {
            if (!validarCnpj(valor)) {
                throw new ParametrizedMessageException("dado.invalido", ConstantsUtil.ERROR_TITLE);
            }
        }
    }

    private boolean validarCpf(String cpf) {
        if (cpf.equals("00000000000") ||
            cpf.equals("11111111111") ||
            cpf.equals("22222222222") || cpf.equals("33333333333") ||
            cpf.equals("44444444444") || cpf.equals("55555555555") ||
            cpf.equals("66666666666") || cpf.equals("77777777777") ||
            cpf.equals("88888888888") || cpf.equals("99999999999") ||
            (cpf.length() != 11))
            return (false);

        char dig10, dig11;
        int sm, i, r, num, peso;

        // "try" - protege o codigo para eventuais erros de conversao de tipo (int)
        try {
            // Calculo do 1o. Digito Verificador
            sm = 0;
            peso = 10;
            for (i = 0; i < 9; i++) {
                // converte o i-esimo caractere do CPF em um numero:
                // por exemplo, transforma o caractere '0' no inteiro 0
                // (48 eh a posicao de '0' na tabela ASCII)
                num = (int) (cpf.charAt(i) - 48);
                sm = sm + (num * peso);
                peso = peso - 1;
            }

            r = 11 - (sm % 11);
            if ((r == 10) || (r == 11))
                dig10 = '0';
            else dig10 = (char) (r + 48); // converte no respectivo caractere numerico

            // Calculo do 2o. Digito Verificador
            sm = 0;
            peso = 11;
            for (i = 0; i < 10; i++) {
                num = (int) (cpf.charAt(i) - 48);
                sm = sm + (num * peso);
                peso = peso - 1;
            }

            r = 11 - (sm % 11);
            if ((r == 10) || (r == 11))
                dig11 = '0';
            else dig11 = (char) (r + 48);

            // Verifica se os digitos calculados conferem com os digitos informados.
            if ((dig10 == cpf.charAt(9)) && (dig11 == cpf.charAt(10)))
                return (true);
            else return (false);
        } catch (InputMismatchException erro) {
            return (false);
        }
    }

    private boolean validarCnpj(String cnpj) {
        if (cnpj.equals("00000000000000") || cnpj.equals("11111111111111") ||
            cnpj.equals("22222222222222") || cnpj.equals("33333333333333") ||
            cnpj.equals("44444444444444") || cnpj.equals("55555555555555") ||
            cnpj.equals("66666666666666") || cnpj.equals("77777777777777") ||
            cnpj.equals("88888888888888") || cnpj.equals("99999999999999") ||
            (cnpj.length() != 14))
            return (false);

        char dig13, dig14;
        int sm, i, r, num, peso;

        // "try" - protege o código para eventuais erros de conversao de tipo (int)
        try {
            // Calculo do 1o. Digito Verificador
            sm = 0;
            peso = 2;
            for (i = 11; i >= 0; i--) {
                // converte o i-ésimo caractere do cnpj em um número:
                // por exemplo, transforma o caractere '0' no inteiro 0
                // (48 eh a posição de '0' na tabela ASCII)
                num = (int) (cnpj.charAt(i) - 48);
                sm = sm + (num * peso);
                peso = peso + 1;
                if (peso == 10)
                    peso = 2;
            }

            r = sm % 11;
            if ((r == 0) || (r == 1))
                dig13 = '0';
            else dig13 = (char) ((11 - r) + 48);

            // Calculo do 2o. Digito Verificador
            sm = 0;
            peso = 2;
            for (i = 12; i >= 0; i--) {
                num = (int) (cnpj.charAt(i) - 48);
                sm = sm + (num * peso);
                peso = peso + 1;
                if (peso == 10)
                    peso = 2;
            }

            r = sm % 11;
            if ((r == 0) || (r == 1))
                dig14 = '0';
            else dig14 = (char) ((11 - r) + 48);

            // Verifica se os dígitos calculados conferem com os dígitos informados.
            if ((dig13 == cnpj.charAt(12)) && (dig14 == cnpj.charAt(13)))
                return (true);
            else return (false);
        } catch (InputMismatchException erro) {
            return (false);
        }
    }

    private void validarUsuarios(UsuarioDTO usuarioDto) {
        if (UsuarioPerfilEnum.EMPREENDEDOR.equals(usuarioDto.getPerfilEnum()) && StringUtils.isAllEmpty(usuarioDto.getPassaporte(),
            usuarioDto.getCnpj(), usuarioDto.getCpf(), usuarioDto.getRne()) || UsuarioPerfilEnum.INVESTIDOR.equals(usuarioDto.getPerfilEnum()) &&
            StringUtils.isAllEmpty(usuarioDto.getPassaporte(), usuarioDto.getCpf()) || StringUtils.isEmpty(usuarioDto.getInformacaoAdicional())) {
            erroCampoObrigatorioException();
        }
        verificacaoRestricaoCampos(usuarioDto);
    }

    private void verificacaoRestricaoCampos(UsuarioDTO usuarioDto) {
        verificarCampo(new CamposValidacaoDTO(usuarioDto.getCpf(), CamposValidacaoEnum.CPF, usuarioDto.getId()));
        verificarCampo(new CamposValidacaoDTO(usuarioDto.getCnpj(), CamposValidacaoEnum.CNPJ, usuarioDto.getId()));
        verificarCampo(new CamposValidacaoDTO(usuarioDto.getPassaporte(), CamposValidacaoEnum.PASSAPORTE, usuarioDto.getId()));
        verificarCampo(new CamposValidacaoDTO(usuarioDto.getRne(), CamposValidacaoEnum.RNE, usuarioDto.getId()));
        verificarCampo(new CamposValidacaoDTO(usuarioDto.getLinkedin(), CamposValidacaoEnum.LINKEDIN, usuarioDto.getId()));
    }

    private Specification<Usuario> specUsuario(CamposValidacaoDTO camposValidacaoDto) {
        return (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(cb.equal(root.get(camposValidacaoDto.getTipo().getDescricao()), camposValidacaoDto.getValor()));
            if (camposValidacaoDto.getId() != null) {
                predicates.add(cb.notEqual(root.get(Usuario_.id), camposValidacaoDto.getId()));
            }
            return cb.and(predicates.toArray(new Predicate[0]));
        };
    }

    private Specification<Usuario> specFiltro(UsuarioFiltroDTO filtro) {
        return (root, query, cb) -> {
            Join<Usuario, Perfil> perfilJoin = root.join(Usuario_.perfil, JoinType.INNER);
            Join<Usuario, Endereco> enderecoJoin = root.join(Usuario_.endereco, JoinType.LEFT);
            List<Predicate> predicates = new ArrayList<>();
            specs(filtro, root, cb, predicates, enderecoJoin);
            if (filtro.getPerfilEnum() != null) {
                predicates.add(cb.equal(perfilJoin.get(Perfil_.nome), filtro.getPerfilEnum()));
            }
            if (!StringUtils.isEmpty(filtro.getTelefone())) {
                Join<Usuario, Telefone> telefoneJoin = root.join(Usuario_.telefones, JoinType.INNER);
                predicates.add(cb.like(telefoneJoin.get(Telefone_.numero), String.join("", "%", filtro.getTelefone(), "%")));
                query.distinct(true);
            }
            return cb.and(predicates.toArray(new Predicate[0]));
        };
    }

    private void specs(UsuarioFiltroDTO filtro, Root<Usuario> root, CriteriaBuilder cb, List<Predicate> predicates, Join<Usuario, Endereco> enderecoJoin) {
        specEquals(Usuario_.cpf, filtro.getCpf(), root, cb, predicates);
        specEquals(Usuario_.cnpj, filtro.getCnpj(), root, cb, predicates);
        specLike(Usuario_.rne, filtro.getRne(), root, cb, predicates);
        specLike(Usuario_.passaporte, filtro.getPassaporte(), root, cb, predicates);
        specLike(Usuario_.email, filtro.getEmail(), root, cb, predicates);
        specLike(Usuario_.nome, filtro.getNome(), root, cb, predicates);
        enderecoSpec(filtro.getEndereco().getPais(), enderecoJoin, cb, predicates, Endereco_.pais);
        enderecoSpec(filtro.getEndereco().getEstado(), enderecoJoin, cb, predicates, Endereco_.estado);
        enderecoSpec(filtro.getEndereco().getCidade(), enderecoJoin, cb, predicates, Endereco_.cidade);
        if (!CollectionUtils.isEmpty(filtro.getListStatus())) {
            predicates.add(root.get(Usuario_.status).in(filtro.getListStatus()));
        }
    }

    private void specEquals(SingularAttribute<Usuario, String> attribute, String campo, Root<Usuario> root, CriteriaBuilder cb, List<Predicate> predicates) {
        if (!StringUtils.isEmpty(campo)) {
            predicates.add(cb.equal(root.get(attribute), campo));
        }
    }

    private void specLike(SingularAttribute<Usuario, String> attribute, String campo, Root<Usuario> root, CriteriaBuilder cb, List<Predicate> predicates) {
        if (!StringUtils.isEmpty(campo)) {
            predicates.add(cb.like(cb.lower(root.get(attribute)), String.join("", "%", campo.toLowerCase().trim(), "%")));
        }
    }

    private void enderecoSpec(String campo, Join<Usuario, Endereco> root, CriteriaBuilder cb, List<Predicate> predicates, SingularAttribute<Endereco, String> attribute) {
        if (!StringUtils.isEmpty(campo)) {
            predicates.add(cb.like(cb.lower(root.get(attribute)), String.join("", "%", campo.toLowerCase().trim(), "%")));
        }
    }
}
