package br.com.sisinvest.pitseguranca.service.mapper;

import br.com.sisinvest.pitseguranca.domain.AreaInteresse;
import br.com.sisinvest.pitseguranca.service.dto.AreaInteresseDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AreaInteresseMapper {

    List<AreaInteresseDTO> listToDto(List<AreaInteresse> listEntity);
}
