package br.com.sisinvest.pitseguranca.repository;

import br.com.sisinvest.pitseguranca.domain.Usuario;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

@JaversSpringDataAuditable
public interface UsuarioRepository extends JpaRepository<Usuario, Long>, JpaSpecificationExecutor<Usuario> {

    Usuario findByEmail(String email);

    @Query("SELECT u.email from Usuario u WHERE u.perfil.id = :perfilId")
    List<String> getEmailGestores(@Param("perfilId") Long perfilId);
}
