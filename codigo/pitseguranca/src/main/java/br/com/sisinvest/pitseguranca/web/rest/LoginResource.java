package br.com.sisinvest.pitseguranca.web.rest;

import br.com.sisinvest.pitseguranca.service.LoginService;
import br.com.sisinvest.pitseguranca.service.dto.UsuarioLoginDTO;
import com.google.common.net.HttpHeaders;
import io.micrometer.core.annotation.Timed;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class LoginResource {

    private final LoginService service;

    /**
     * POST : autenticar Usuário
     *
     * @return the ResponseEntity with status 200 (OK) with no Body
     */
    @PostMapping("/login")
    public ResponseEntity<Void> login(@RequestBody UsuarioLoginDTO login,
                                      HttpServletResponse httpServletResponse,
                                      HttpServletRequest httpServletRequest) {
        log.debug("REST requisição para Login de Usuário");
        service.autenticacaoUsuario(login, httpServletRequest, httpServletResponse);
        return ResponseEntity.ok().build();
    }


    /**
     * GET : logout
     *
     * @return the ResponseEntity with status 307 (TEMPORARY_REDIRECT) and Location = "/#/login"
     */
    @GetMapping("/logout")
    @Timed
    public ResponseEntity<Void> logout(@ApiIgnore HttpServletResponse pHttpServletResponse,
                                       @ApiIgnore HttpServletRequest pHttpServletRequest) {
        log.debug("Request to logout");
        service.logout(pHttpServletResponse, pHttpServletRequest);
        return ResponseEntity.status(HttpStatus.TEMPORARY_REDIRECT).header(HttpHeaders.LOCATION, "/#/login").build();
    }

}
