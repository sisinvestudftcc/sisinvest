package br.com.sisinvest.pitseguranca.web.rest;

import br.com.sisinvest.pitseguranca.security.AuthoritiesConstants;
import br.com.sisinvest.pitseguranca.service.UsuarioService;
import br.com.sisinvest.pitseguranca.service.dto.CamposValidacaoDTO;
import br.com.sisinvest.pitseguranca.service.dto.SenhaUsuario;
import br.com.sisinvest.pitseguranca.service.dto.UsuarioDTO;
import br.com.sisinvest.pitseguranca.service.dto.filtro.UsuarioFiltroDTO;
import br.com.sisinvest.pitseguranca.service.enumeration.StatusEnum;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class UsuarioResource {

    private final UsuarioService service;

    @PostMapping("/usuario/filtrar")
    @Secured({AuthoritiesConstants.GESTOR, AuthoritiesConstants.EMPREENDEDOR, AuthoritiesConstants.INVESTIDOR})
    public ResponseEntity<Page<UsuarioDTO>> filtrar(@RequestBody UsuarioFiltroDTO filtro, Pageable pageable) {
        log.info("REST requisição para buscar Usuários com filtro: {}", filtro);
        return ResponseEntity.ok(service.filtrar(filtro, pageable));
    }

    @GetMapping("/usuario/{id}")
    @Secured({AuthoritiesConstants.GESTOR, AuthoritiesConstants.EMPREENDEDOR, AuthoritiesConstants.INVESTIDOR})
    public ResponseEntity<UsuarioDTO> buscarPorId(@PathVariable Long id) {
        log.debug("REST requisição para buscar Usuário por id: {}", id);
        return new ResponseEntity<>(service.buscarPorId(id), HttpStatus.OK);
    }

    @PostMapping("/usuario")
    public ResponseEntity<Void> save(@RequestBody UsuarioDTO usuario, UriComponentsBuilder builder) {
        log.info("REST requisição para salvar dados de Usuário");
        service.save(usuario, builder);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/usuario/alterar-status")
    @Secured(AuthoritiesConstants.GESTOR)
    public ResponseEntity<Void> alterarStatus(@RequestParam Long id, @RequestParam(required = false) String justificativa) {
        log.debug("REST requisição para alterar status de Usuário: {}", id);
        service.alterarStatus(id, justificativa);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/usuario/alterar-senha")
    public ResponseEntity<Void> alterarSenha(@RequestBody SenhaUsuario senhaUsuario) {
        log.debug("REST requisição para alterar senha de Usuário");
        service.alterarSenha(senhaUsuario);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/usuario/gerar-senha")
    public ResponseEntity<Void> novaSenha(@RequestParam String email, UriComponentsBuilder builder) {
        log.debug("REST requisição para gerar nova senha para Usuário: {}", email);
        service.novaSenha(email, builder);
        return ResponseEntity.ok().build();
    }

    @PostMapping("public/usuario/validar-campo")
    public ResponseEntity<Void> verificarCampo(@RequestBody CamposValidacaoDTO camposValidacaoDto) {
        log.info("REST requisição para validar Campo");
        service.verificarCampo(camposValidacaoDto);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/usuario/buscar-logado")
    public ResponseEntity<UsuarioDTO> buscarLogado() {
        log.debug("REST requisição para buscar Usuário logado");
        return new ResponseEntity<>(service.buscarLogado(), HttpStatus.OK);
    }

    @GetMapping("/usuario/verificar-senha")
    public ResponseEntity<Void> verificarSenha(@RequestParam String senha) {
        log.debug("REST requisição para validar senha atual de Usuário");
        service.verificarSenha(senha);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/usuario/verificar-nova-senha")
    public ResponseEntity<Void> verificarNovaSenha(@RequestParam String novaSenha) {
        log.debug("REST requisição para validar nova senha de Usuário");
        service.verificarNovaSenha(novaSenha);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/usuario/{id}/moderar")
    @Secured(AuthoritiesConstants.GESTOR)
    public ResponseEntity<Void> moderarUsuario(@PathVariable Long id, @RequestParam StatusEnum status,
                                               @RequestParam String justificativa, UriComponentsBuilder builder) {
        log.debug("REST requisição para Moderação de Usuário");
        service.moderarUsuario(id, status, justificativa, builder);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/public/usuario/cadastro-externo")
    public ResponseEntity<Void> cadastroExterno(@RequestBody UsuarioDTO usuarioDto, UriComponentsBuilder builder) {
        log.info("REST Requisição para salvar Usuário Ambiente Externo");
        service.cadastroExterno(usuarioDto, builder);
        return ResponseEntity.ok().build();
    }
}
