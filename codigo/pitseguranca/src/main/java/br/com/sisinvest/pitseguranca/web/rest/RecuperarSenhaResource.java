package br.com.sisinvest.pitseguranca.web.rest;

import br.com.sisinvest.pitseguranca.service.RecuperarSenhaService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/recuperar-senha")
public class RecuperarSenhaResource {

    private final RecuperarSenhaService service;

    @PostMapping
    public ResponseEntity<Void> recuperarSenha(@RequestParam String email, UriComponentsBuilder builder) {
        log.debug("REST requisição para gerar nova senha para Usuário: {}", email);
        service.recuperarSenha(email, builder);
        return ResponseEntity.ok().build();
    }
}
