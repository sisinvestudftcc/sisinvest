package br.com.sisinvest.pitseguranca.service;

import br.com.sisinvest.pitseguranca.repository.AreaInteresseRepository;
import br.com.sisinvest.pitseguranca.domain.AreaInteresse;
import br.com.sisinvest.pitseguranca.service.dto.AreaInteresseDTO;
import br.com.sisinvest.pitseguranca.service.mapper.AreaInteresseMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

@Service
@Transactional
@RequiredArgsConstructor
public class AreaInteresseService {

    private final AreaInteresseMapper mapper;
    private final AreaInteresseRepository repository;

    public List<AreaInteresseDTO> buscarTodos() {
        return mapper.listToDto(repository.findAll());
    }

    public Set<AreaInteresse> findByIdIn(Set<Long> ids) {
        return repository.findByIdIn(ids);
    }
}
