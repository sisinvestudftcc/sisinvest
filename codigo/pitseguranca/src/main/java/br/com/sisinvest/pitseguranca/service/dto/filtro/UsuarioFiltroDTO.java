package br.com.sisinvest.pitseguranca.service.dto.filtro;

import br.com.sisinvest.pitseguranca.service.dto.UsuarioDTO;
import br.com.sisinvest.pitseguranca.service.enumeration.StatusEnum;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class UsuarioFiltroDTO extends UsuarioDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private List<StatusEnum> listStatus;
    private String telefone;
}
