package br.com.sisinvest.pitseguranca.service.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Size;
import java.io.Serializable;

@Getter
@Setter
public class TelefoneDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    @Size(max = 30)
    private String numero;
    private Long usuarioId;
}
