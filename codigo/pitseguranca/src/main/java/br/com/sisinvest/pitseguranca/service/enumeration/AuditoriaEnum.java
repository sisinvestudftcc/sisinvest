package br.com.sisinvest.pitseguranca.service.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AuditoriaEnum {
    USUARIO("br.com.sisinvest.pitseguranca.domain.Usuario");

    private String typeName;
}
