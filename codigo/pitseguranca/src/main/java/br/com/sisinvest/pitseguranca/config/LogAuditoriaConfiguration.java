package br.com.sisinvest.pitseguranca.config;

import br.com.sisinvest.pitseguranca.service.UsuarioService;
import br.com.sisinvest.pitseguranca.service.dto.UsuarioDTO;
import br.com.sisinvest.pitseguranca.utils.SecurityUtil;
import br.com.sisinvest.pitseguranca.service.dto.UsuarioLogadoDTO;
import br.gov.nuvem.auditoria.producer.config.LogAuditoriaAutoConfiguration;
import lombok.RequiredArgsConstructor;
import org.javers.spring.auditable.AuthorProvider;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.stream.binder.kafka.BinderHeaderMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.support.KafkaHeaderMapper;

@Configuration
@RequiredArgsConstructor
@AutoConfigureBefore(LogAuditoriaAutoConfiguration.class)
@ConditionalOnProperty(value = "auditoria.enabled", matchIfMissing = true)
public class LogAuditoriaConfiguration implements SecurityUtil {

    private final UsuarioService usuarioService;

    @Bean(name = "SpringSecurityAuthorProvider")
    public AuthorProvider springSecurityCustomAuthorProvider() {
        return () -> {
            UsuarioLogadoDTO usuarioLogado = obterUsuarioLogado();
            if (usuarioLogado == null) {
                return "Anônimo";
            }
            UsuarioDTO usuario = usuarioService.buscarPorId(usuarioLogado.getIdUsuario());
            return usuario.getNome();
        };
    }

    @Bean("kafkaBinderHeaderMapper")
    public KafkaHeaderMapper kafkaBinderHeaderMapper() {
        BinderHeaderMapper mapper = new BinderHeaderMapper();
        mapper.setEncodeStrings(true);
        return mapper;
    }
}
