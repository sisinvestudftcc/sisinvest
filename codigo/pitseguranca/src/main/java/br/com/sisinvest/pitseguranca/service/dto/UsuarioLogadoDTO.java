package br.com.sisinvest.pitseguranca.service.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class UsuarioLogadoDTO {

    private Long idUsuario;
    private String perfil;
}
