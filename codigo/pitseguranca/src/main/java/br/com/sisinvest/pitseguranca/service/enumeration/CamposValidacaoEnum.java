package br.com.sisinvest.pitseguranca.service.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CamposValidacaoEnum {
    CPF("cpf"),
    CNPJ("cnpj"),
    PASSAPORTE("passaporte"),
    RNE("rne"),
    LINKEDIN("linkedin"),
    EMAIL("email");

    private String descricao;
}
