package br.com.sisinvest.pitseguranca.web.rest;

import br.com.sisinvest.pitseguranca.service.PerfilService;
import br.com.sisinvest.pitseguranca.service.dto.PerfilDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/public/perfil")
public class PerfilResource {

    private final PerfilService service;

    @PostMapping
    public List<PerfilDTO> filtro(@RequestBody PerfilDTO filtro) {
        log.info("Requisição para buscar Perfis de Usuário com filtro");
        return service.filtro(filtro);
    }
}
