package br.com.sisinvest.pitseguranca.service.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Getter
@Setter
public class UsuarioLoginDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @Email
    @Size(max = 200)
    private String email;
    @Size(min = 8)
    private String senha;
}
