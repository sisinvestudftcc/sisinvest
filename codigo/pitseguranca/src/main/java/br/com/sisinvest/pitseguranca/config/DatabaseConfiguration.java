package br.com.sisinvest.pitseguranca.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableJpaRepositories("br.com.sisinvest.pitseguranca.repository")
@EnableTransactionManagement
public class DatabaseConfiguration {
}
