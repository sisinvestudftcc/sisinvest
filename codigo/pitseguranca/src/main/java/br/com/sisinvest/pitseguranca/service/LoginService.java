package br.com.sisinvest.pitseguranca.service;

import br.com.sisinvest.pitseguranca.utils.ConstantsUtil;
import br.com.sisinvest.pitseguranca.domain.Perfil;
import br.com.sisinvest.pitseguranca.domain.Usuario;
import br.com.sisinvest.pitseguranca.service.dto.UsuarioLoginDTO;
import br.gov.nuvem.comum.microsservico.web.rest.errors.ParametrizedMessageException;
import br.gov.nuvem.security.jwt.repository.AuthorizationRepository;
import br.gov.nuvem.security.jwt.service.dto.UserDetailsDTO;
import br.gov.nuvem.security.jwt.web.TokenProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Service
@Transactional
@RequiredArgsConstructor
public class LoginService {

    private final UsuarioService usuarioService;
    private final TokenProvider tokenProvider;
    private final AuthorizationRepository repository;
    private final Argon2PasswordEncoder encoder;

    public void autenticacaoUsuario(UsuarioLoginDTO login, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        final UsernamePasswordAuthenticationToken authentication = validarDadosLogin(login);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        final String credentials = tokenProvider.createToken(authentication, true);
        Cookie lCookie = new Cookie(ConstantsUtil.TOKEN_COOKIE_NAME, credentials);
        lCookie.setMaxAge(-1);
        lCookie.setPath("/");
        lCookie.setSecure(false);
        lCookie.setHttpOnly(true);
        lCookie.setDomain(httpServletRequest.getServerName());
        httpServletResponse.addCookie(lCookie);
    }

    public void logout(final HttpServletResponse pHttpServletResponse, final HttpServletRequest pHttpServletRequest) {
        Cookie lCookie = new Cookie(ConstantsUtil.TOKEN_COOKIE_NAME, null);
        lCookie.setMaxAge(0);
        lCookie.setPath("/");
        lCookie.setSecure(false);
        lCookie.setHttpOnly(true);
        lCookie.setDomain(pHttpServletRequest.getServerName());
        pHttpServletResponse.addCookie(lCookie);
    }

    private UsernamePasswordAuthenticationToken validarDadosLogin(UsuarioLoginDTO login) {
        final Usuario usuario = usuarioService.buscarPorEmail(login.getEmail());
        if (!encoder.matches(login.getSenha(), usuario.getSenha())) {
            throw new ParametrizedMessageException("login.senha.erro", ConstantsUtil.ERROR_TITLE);
        }
        Map<String, Object> attributes = new HashMap<>();
        attributes.put("perfil", usuario.getPerfil().getNome());
        attributes.put("idUsuario", usuario.getId());
        return new UsernamePasswordAuthenticationToken(new UserDetailsDTO(usuario.getEmail(), "******",
            getAuthorities(usuario.getPerfil()),attributes), "******");
    }

    private Collection<? extends GrantedAuthority> getAuthorities(Perfil perfil) {
        return Collections.singletonList(new SimpleGrantedAuthority(rolePrefix(perfil.getNome().toString())));
    }

    private String rolePrefix(String role) {
        return String.format("ROLE_%s", role);
    }
}
