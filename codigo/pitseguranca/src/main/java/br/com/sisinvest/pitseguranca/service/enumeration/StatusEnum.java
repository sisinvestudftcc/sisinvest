package br.com.sisinvest.pitseguranca.service.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
public enum StatusEnum {
    ATIVO,
    INATIVO,
    SOB_ANALISE,
    REPROVADO
}
