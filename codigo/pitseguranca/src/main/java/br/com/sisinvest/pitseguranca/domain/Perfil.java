package br.com.sisinvest.pitseguranca.domain;

import br.com.sisinvest.pitseguranca.service.enumeration.UsuarioPerfilEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "TB_PERFIL")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Perfil implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final String SEQ_PERFIL = "SEQ_PERFIL";
    public static final String PERFIL_ID = "ID";

    @Id
    @Column(name = PERFIL_ID)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQ_PERFIL)
    @SequenceGenerator(name = SEQ_PERFIL, sequenceName = SEQ_PERFIL, allocationSize = 1)
    private Long id;

    @Column(name = "NOME")
    @Enumerated(EnumType.STRING)
    private UsuarioPerfilEnum nome;

    @Column(name = "DESCRICAO")
    private String descricao;
}
