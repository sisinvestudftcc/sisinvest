package br.com.sisinvest.pitseguranca.utils;

import lombok.experimental.UtilityClass;

@UtilityClass
public final class ConstantsUtil {
    public static final String LETRAS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static final String CARACTERES_ESPECIAIS = "!@#$%&*-=+<>?_.,";
    public static final String ERROR_TITLE = "error.title";
    public static final String PATTERN_SENHA = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%&*-=+<>?_.,])(?=\\S+$).{8,}";
    public static final String PATTERN_LINKEDIN = "((https?:\\/\\/)?((www|\\w\\w)\\.)?linkedin\\.com\\/)((([\\w]{2,3})?)|([^\\/]+\\/(([\\w|\\d\\-&#?=])+\\/?){1,}))$";
    public static final String TOKEN_COOKIE_NAME = "jwt-token";
}
