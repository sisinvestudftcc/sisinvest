package br.com.sisinvest.pitseguranca.web.rest;

import br.com.sisinvest.pitseguranca.service.PesquisaAuditoriaService;
import br.com.sisinvest.pitseguranca.service.dto.AuditoriaDTO;
import br.com.sisinvest.pitseguranca.service.dto.filtro.AuditoriaFiltroDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/auditoria")
public class AuditoriaResource {

    private final PesquisaAuditoriaService pesquisaAuditoriaService;

    @PostMapping("/listar-por-entidade")
    public List<AuditoriaDTO> listarPorEntidade(@RequestBody @Valid AuditoriaFiltroDTO filtro) {
        log.debug("REST request to filter Auditoria por Entidade : {}", filtro);
        return pesquisaAuditoriaService.listarAuditoria(filtro);
    }
}
