package br.com.sisinvest.pitseguranca.service.mapper;

import br.com.sisinvest.pitseguranca.domain.Usuario;
import br.com.sisinvest.pitseguranca.service.dto.UsuarioDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UsuarioMapper {

    @Mapping(target = "perfilEnum", source = "perfil.nome")
    UsuarioDTO toDto(Usuario entity);

    @Mapping(target = "perfil.nome", source = "perfilEnum")
    Usuario toEntity(UsuarioDTO usuarioDto);

    List<UsuarioDTO> dtoList(List<Usuario> listEntity);
}
