package br.com.sisinvest.pitseguranca.service.dto;

import br.com.sisinvest.pitseguranca.service.enumeration.TipoAcaoEnum;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
public class AuditoriaDTO {
    private TipoAcaoEnum tipoAcao;
    private String usuarioAcao;
    private LocalDateTime dataHoraAcao;
    private List<CamposAuditoriaDTO> campos;
}
