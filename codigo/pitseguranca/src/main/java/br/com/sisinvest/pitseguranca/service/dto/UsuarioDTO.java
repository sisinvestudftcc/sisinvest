package br.com.sisinvest.pitseguranca.service.dto;

import br.com.sisinvest.pitseguranca.service.enumeration.StatusEnum;
import br.com.sisinvest.pitseguranca.service.enumeration.UsuarioPerfilEnum;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.br.CNPJ;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class UsuarioDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    @NotNull
    @Size(max = 150)
    private String nome;
    @Email
    @NotNull
    @Size(max = 200)
    private String email;
    private UsuarioPerfilEnum perfilEnum;
    private Set<TelefoneDTO> telefones;
    private StatusEnum status;
    @Size(max = 8)
    private String passaporte;
    @CPF
    private String cpf;
    @CNPJ
    private String cnpj;
    @Size(max = 9)
    private String rne;
    @Size(max = 2500)
    private String informacaoAdicional;
    private Set<Long> areaInteresseIds;
    @Size(max = 300)
    private String linkedin;
    private EnderecoDTO endereco;
    private Timestamp dataCriacao;
}
