package br.com.sisinvest.pitseguranca.web.rest;

import br.com.sisinvest.pitseguranca.PitsegurancaApp;
import br.com.sisinvest.pitseguranca.service.CreateEntityService;
import br.com.sisinvest.pitseguranca.service.dto.filtro.UsuarioFiltroDTO;
import br.com.sisinvest.pitseguranca.service.enumeration.StatusEnum;
import br.com.sisinvest.pitseguranca.util.IntTestComum;
import br.com.sisinvest.pitseguranca.domain.Usuario;
import br.com.sisinvest.pitseguranca.security.RunWithMockCustomUser;
import br.com.sisinvest.pitseguranca.service.dto.SenhaUsuario;
import br.com.sisinvest.pitseguranca.service.dto.UsuarioDTO;
import br.com.sisinvest.pitseguranca.service.enumeration.UsuarioPerfilEnum;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.Collections;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWithMockCustomUser
@Transactional
@SpringBootTest(classes = PitsegurancaApp.class)
class UsuarioResourceIT extends IntTestComum implements CreateEntityService {

    private static final String BASE_URL = "/api/usuario";
    private static final String PUBLIC_URL = "/api/public/usuario";

    @Autowired
    private Argon2PasswordEncoder encoder;

    @Autowired
    private EntityManager em;

    @Test
    void alterarSenha() throws Exception {
        SenhaUsuario senhaUsuario = new SenhaUsuario();
        senhaUsuario.setSenhaAtual("B@sis123");
        senhaUsuario.setNovaSenha("B@sis1234");
        senhaUsuario.setConfirmarSenha("B@sis1234");
        this.getMockMvc().perform(MockMvcRequestBuilders.put(buildUrl(BASE_URL, "alterar-senha"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(senhaUsuario)))
            .andExpect(status().isOk());
    }

    @Test
    void alterarSenhaDivergente() throws Exception {
        SenhaUsuario senhaUsuario = new SenhaUsuario();
        senhaUsuario.setSenhaAtual("12345678");
        this.getMockMvc().perform(MockMvcRequestBuilders.put(buildUrl(BASE_URL, "alterar-senha"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(senhaUsuario)))
            .andExpect(status().isBadRequest());
    }

    @Test
    void alterarSenhaDivergenteNova() throws Exception {
        SenhaUsuario senhaUsuario = new SenhaUsuario();
        senhaUsuario.setSenhaAtual("B@sis123");
        senhaUsuario.setNovaSenha("12345678");
        senhaUsuario.setConfirmarSenha("123456789");
        this.getMockMvc().perform(MockMvcRequestBuilders.put(buildUrl(BASE_URL, "alterar-senha"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(senhaUsuario)))
            .andExpect(status().isBadRequest());
    }

    @Test
    void alterarStatus() throws Exception {
        Usuario usuario = entityEmpreendedor();
        usuario.getPerfil().setNome(UsuarioPerfilEnum.EMPREENDEDOR);
        em.persist(usuario);
        this.getMockMvc().perform(MockMvcRequestBuilders.put(buildUrl(BASE_URL, "alterar-status"))
            .contentType(MediaType.APPLICATION_JSON)
            .param("id", usuario.getId().toString()).param("justificativa", "Inativar Usuário."))
            .andExpect(status().isOk());
        this.getMockMvc().perform(MockMvcRequestBuilders.put(buildUrl(BASE_URL, "alterar-status"))
            .contentType(MediaType.APPLICATION_JSON)
            .param("id", usuario.getId().toString()).param("justificativa", "Ativar Usuário."))
            .andExpect(status().isOk());
    }

    @Test
    void alterarStatusSemJustificativa() throws Exception {
        Usuario usuario = entityInvestidor();
        em.persist(usuario);
        this.getMockMvc().perform(MockMvcRequestBuilders.put(buildUrl(BASE_URL, "alterar-status"))
            .contentType(MediaType.APPLICATION_JSON)
            .param("id", usuario.getId().toString()))
            .andExpect(status().isBadRequest());
    }

    @Test
    void alterarStatusEmAnalise() throws Exception {
        Usuario usuario = entityInvestidor();
        usuario.setStatus(StatusEnum.SOB_ANALISE);
        em.persist(usuario);
        this.getMockMvc().perform(MockMvcRequestBuilders.put(buildUrl(BASE_URL, "alterar-status"))
            .contentType(MediaType.APPLICATION_JSON)
            .param("id", usuario.getId().toString()).param("justificativa", "Inativar Usuário."))
            .andExpect(status().isBadRequest());
    }

    @Test
    void buscarPorId() throws Exception {
        Usuario usuario = entityInvestidor();
        em.persist(usuario);
        this.getMockMvc().perform(MockMvcRequestBuilders.get(buildUrl(BASE_URL, usuario.getId().toString()))
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
    }

    @Test
    void buscarUsuarioLogado() throws Exception {
        this.getMockMvc().perform(MockMvcRequestBuilders.get(buildUrl(BASE_URL, "buscar-logado"))
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
    }

    @Test
    void cadastroExterno() throws Exception {
        UsuarioDTO usuario = dtoInvestidor();
        usuario.setPassaporte("BR43007");
        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(PUBLIC_URL, "cadastro-externo"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(usuario)))
            .andExpect(status().isOk());
    }

    @Test
    void cadastroExternoCampoObrigatorio() throws Exception {
        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(PUBLIC_URL, "cadastro-externo"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dtoInvestidor())))
            .andExpect(status().isBadRequest());
    }

    @Test
    void criarGestor() throws Exception {
        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dtoGestor(null))))
            .andExpect(status().isOk());
    }

    @Test
    void criarGestorCampoObrigatório() throws Exception {
        UsuarioDTO usuarioDto = dtoGestor(null);
        usuarioDto.setNome(" ");
        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(usuarioDto)))
            .andExpect(status().isBadRequest());
    }

    @Test
    void criarGestorDuplicidade() throws Exception {
        Usuario usuario = entityGestor();
        usuario.setEmail("t@t.com");
        em.persist(usuario);
        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dtoGestor(null))))
            .andExpect(status().isBadRequest());
    }

    @Test
    void criarInvestidor() throws Exception {
        UsuarioDTO usuario = dtoInvestidor();
        usuario.setEmail("inv@i.com");
        usuario.setCpf("11605838624");
        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(usuario)))
            .andExpect(status().isOk());
    }

    @Test
    void criarInvestidorErro() throws Exception {
        UsuarioDTO usuario = dtoInvestidor();
        usuario.setEmail("inv@i.com");
        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(usuario)))
            .andExpect(status().isBadRequest());
    }

    @Test
    void criarEmpreendedor() throws Exception {
        UsuarioDTO usuario = dtoEmpreendedor();
        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(usuario)))
            .andExpect(status().isOk());
    }

    @Test
    void editarUsuario() throws Exception {
        UsuarioDTO usuario = new UsuarioDTO();
        usuario.setNome("Admin");
        usuario.setId(1L);
        usuario.setStatus(StatusEnum.ATIVO);
        usuario.setPerfilEnum(UsuarioPerfilEnum.GESTOR);
        usuario.setEmail("usuariotcc@teste.com.br");
        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(usuario)))
            .andExpect(status().isOk());
    }

    @Test
    void erroPreenchimentoInvestidor() throws Exception {
        UsuarioDTO usuario = dtoInvestidor();
        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(usuario)))
            .andExpect(status().isBadRequest());
    }

    @Test
    void filtro() throws Exception {
        UsuarioFiltroDTO filtro = new UsuarioFiltroDTO();
        filtro.setPerfilEnum(UsuarioPerfilEnum.INVESTIDOR);
        filtro.setNome("Teste");
        filtro.setEndereco(dtoEndereco());
        filtro.setTelefone("3399275555");
        filtro.setListStatus(Collections.singletonList(StatusEnum.ATIVO));
        filtro.setCpf("01234567890");
        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL, "filtrar"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(filtro)))
            .andExpect(status().isOk());
    }

    @Test
    void linkedinInvalido() throws Exception {
        UsuarioDTO usuario = dtoEmpreendedor();
        usuario.setLinkedin("sss.linkedin.com/in/someDodgeAddress");
        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(usuario)))
            .andExpect(status().isBadRequest());
    }

    @Test
    void moderarUsuarioAprovado() throws Exception {
        Usuario usuario = entityGestor();
        usuario.setStatus(StatusEnum.SOB_ANALISE);
        em.persist(usuario);
        this.getMockMvc().perform(MockMvcRequestBuilders
            .put(buildUrl(BASE_URL, usuario.getId().toString(), "moderar"))
            .contentType(MediaType.APPLICATION_JSON)
            .param("status", StatusEnum.ATIVO.toString())
            .param("justificativa", "Ativar"))
            .andExpect(status().isOk());
    }

    @Test
    void moderarUsuarioReprovado() throws Exception {
        Usuario usuario = entityGestor();
        usuario.setStatus(StatusEnum.SOB_ANALISE);
        em.persist(usuario);
        this.getMockMvc().perform(MockMvcRequestBuilders
            .put(buildUrl(BASE_URL, usuario.getId().toString(), "moderar"))
            .contentType(MediaType.APPLICATION_JSON)
            .param("status", StatusEnum.REPROVADO.toString())
            .param("justificativa", "Reprovado"))
            .andExpect(status().isOk());
    }

    @Test
    void novaSenha() throws Exception {
        this.getMockMvc().perform(MockMvcRequestBuilders.put(buildUrl(BASE_URL, "gerar-senha"))
            .contentType(MediaType.APPLICATION_JSON).param("email", "usuariotcc@teste.com.br"))
            .andExpect(status().isOk());
    }

    @Test
    void novaSenhaInativo() throws Exception {
        Usuario usuario = entityInvestidor();
        usuario.setEmail("inv@i.com");
        usuario.setStatus(StatusEnum.INATIVO);
        em.persist(usuario);
        this.getMockMvc().perform(MockMvcRequestBuilders.put(buildUrl(BASE_URL, "gerar-senha"))
            .contentType(MediaType.APPLICATION_JSON).param("email", "inv@i.com"))
            .andExpect(status().isBadRequest());
    }

    @Test
    void novaSenhaSemUsuario() throws Exception {
        this.getMockMvc().perform(MockMvcRequestBuilders.put(buildUrl(BASE_URL, "gerar-senha"))
            .contentType(MediaType.APPLICATION_JSON).param("email", "sem@t.com"))
            .andExpect(status().isBadRequest());
    }

    @Test
    void validarCampos() throws Exception {
        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(PUBLIC_URL, "validar-campo"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(validacao())))
            .andExpect(status().isOk());
    }

    @Test
    void verificarSenha() throws Exception {
        this.getMockMvc().perform(MockMvcRequestBuilders.get(buildUrl(BASE_URL, "verificar-senha"))
            .contentType(MediaType.APPLICATION_JSON)
            .param("senha", "B@sis123"))
            .andExpect(status().isOk());
    }

    @Test
    void verificarSenhaErro() throws Exception {
        this.getMockMvc().perform(MockMvcRequestBuilders.get(buildUrl(BASE_URL, "verificar-senha"))
            .contentType(MediaType.APPLICATION_JSON)
            .param("senha", "asdf123"))
            .andExpect(status().isBadRequest());
    }

    @Test
    void verificarNovaSenha() throws Exception {
        this.getMockMvc().perform(MockMvcRequestBuilders.get(buildUrl(BASE_URL, "verificar-nova-senha"))
            .contentType(MediaType.APPLICATION_JSON)
            .param("novaSenha", "TT@@tt22"))
            .andExpect(status().isOk());
    }

    @Test
    void verificarNovaSenhaErro() throws Exception {
        this.getMockMvc().perform(MockMvcRequestBuilders.get(buildUrl(BASE_URL, "verificar-nova-senha"))
            .contentType(MediaType.APPLICATION_JSON)
            .param("novaSenha", "asdf123"))
            .andExpect(status().isBadRequest());
    }
}
