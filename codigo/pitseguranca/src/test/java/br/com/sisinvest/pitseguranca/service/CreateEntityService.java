package br.com.sisinvest.pitseguranca.service;

import br.com.sisinvest.pitseguranca.domain.AreaInteresse;
import br.com.sisinvest.pitseguranca.domain.Perfil;
import br.com.sisinvest.pitseguranca.domain.Usuario;
import br.com.sisinvest.pitseguranca.service.dto.CamposValidacaoDTO;
import br.com.sisinvest.pitseguranca.service.dto.EnderecoDTO;
import br.com.sisinvest.pitseguranca.service.dto.TelefoneDTO;
import br.com.sisinvest.pitseguranca.service.dto.UsuarioDTO;
import br.com.sisinvest.pitseguranca.service.enumeration.CamposValidacaoEnum;
import br.com.sisinvest.pitseguranca.service.enumeration.StatusEnum;
import br.com.sisinvest.pitseguranca.service.enumeration.UsuarioPerfilEnum;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public interface CreateEntityService {

    default Usuario entityGestor() {
        Usuario usuarioDto = new Usuario();
        usuarioDto.setEmail("test@t.com");
        usuarioDto.setNome("Teste");
        usuarioDto.setStatus(StatusEnum.ATIVO);
        Perfil perfil = new Perfil();
        perfil.setId(3L);
        perfil.setNome(UsuarioPerfilEnum.GESTOR);
        usuarioDto.setPerfil(perfil);
        return usuarioDto;
    }

    default UsuarioDTO dtoEmpreendedor() {
        UsuarioDTO usuario = new UsuarioDTO();
        usuario.setPassaporte("1234567");
        usuario.setStatus(StatusEnum.ATIVO);
        usuario.setCpf("11605838624");
        usuario.setCnpj("23959597000123");
        usuario.setRne("1234567");
        usuario.setLinkedin("http://www.linkedin.com/in/someDodgeAddress");
        usuario.setEmail("test@t.com");
        usuario.setNome("Teste");
        usuario.setPerfilEnum(UsuarioPerfilEnum.EMPREENDEDOR);
        usuario.setInformacaoAdicional("informação");
        return usuario;
    }

    default EnderecoDTO dtoEndereco() {
        EnderecoDTO enderecoDto = new EnderecoDTO();
        enderecoDto.setPais("Brasil");
        enderecoDto.setEstado("Minas");
        enderecoDto.setCidade("bh");
        return enderecoDto;
    }

    default UsuarioDTO dtoGestor(Long id) {
        UsuarioDTO usuarioDto = new UsuarioDTO();
        if (id != null) {
            usuarioDto.setId(id);
        }
        TelefoneDTO telefone = new TelefoneDTO();
        telefone.setNumero("3133333333");
        usuarioDto.setTelefones(new HashSet<>(Collections.singletonList(telefone)));
        usuarioDto.setStatus(StatusEnum.ATIVO);
        usuarioDto.setEmail("t@t.com");
        usuarioDto.setNome("Teste");
        usuarioDto.setPerfilEnum(UsuarioPerfilEnum.GESTOR);
        return usuarioDto;
    }

    default UsuarioDTO dtoInvestidor() {
        UsuarioDTO usuario = new UsuarioDTO();
        usuario.setAreaInteresseIds(new HashSet<>(Collections.singletonList(1L)));
        usuario.setStatus(StatusEnum.ATIVO);
        usuario.setEmail("teste@t.com");
        usuario.setNome("Teste");
        usuario.setPerfilEnum(UsuarioPerfilEnum.INVESTIDOR);
        usuario.setInformacaoAdicional("informação");
        return usuario;
    }

    default Usuario entityEmpreendedor() {
        Usuario usuario = new Usuario();
        Perfil perfil = new Perfil();
        perfil.setId(1L);
        usuario.setPassaporte("123456");
        usuario.setStatus(StatusEnum.ATIVO);
        usuario.setCpf("11605838624");
        usuario.setCnpj("34939597000156");
        usuario.setRne("123456");
        usuario.setLinkedin("http://uk.linkedin.com/in/someDodgeAddress");
        usuario.setEmail("teste@t.com");
        usuario.setNome("Teste");
        usuario.setPerfil(perfil);
        usuario.setInformacaoAdicional("informação");
        return usuario;
    }

    default Usuario entityInvestidor() {
        Usuario usuario = new Usuario();
        Perfil perfil = new Perfil();
        perfil.setId(2L);
        usuario.setStatus(StatusEnum.ATIVO);
        usuario.setPassaporte("123456");
        usuario.setEmail("teste@t.com");
        usuario.setNome("Teste");
        usuario.setPerfil(perfil);
        usuario.setAreaInteresses(areaInteresseSet());
        usuario.setInformacaoAdicional("informação");
        return usuario;
    }

    default CamposValidacaoDTO validacao() {
        CamposValidacaoDTO validacaoDto = new CamposValidacaoDTO();
        validacaoDto.setValor("http://uk.linkedin.com/in/someDodgeAddress");
        validacaoDto.setTipo(CamposValidacaoEnum.LINKEDIN);
        return validacaoDto;
    }

    default Set<AreaInteresse> areaInteresseSet() {
        AreaInteresse areaInteresse = new AreaInteresse();
        areaInteresse.setId(1L);
        return new HashSet<>(Collections.singletonList(areaInteresse));
    }
}
