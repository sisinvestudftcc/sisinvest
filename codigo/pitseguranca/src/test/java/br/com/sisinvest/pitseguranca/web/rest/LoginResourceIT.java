package br.com.sisinvest.pitseguranca.web.rest;

import br.com.sisinvest.pitseguranca.PitsegurancaApp;
import br.com.sisinvest.pitseguranca.service.CreateEntityService;
import br.com.sisinvest.pitseguranca.util.IntTestComum;
import br.com.sisinvest.pitseguranca.domain.Usuario;
import br.com.sisinvest.pitseguranca.security.RunWithMockCustomUser;
import br.com.sisinvest.pitseguranca.service.dto.UsuarioLoginDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWithMockCustomUser
@Transactional
@SpringBootTest(classes = PitsegurancaApp.class)
class LoginResourceIT extends IntTestComum implements CreateEntityService {

    private static final String BASE_URL = "/api";

    @Autowired
    private Argon2PasswordEncoder encoder;

    @Autowired
    private EntityManager em;

    @Test
    void login() throws Exception {
        Usuario usuario = entityGestor();
        usuario.setSenha(encoder.encode("TTtt@@22"));
        em.persist(usuario);
        UsuarioLoginDTO usuarioLoginDto = new UsuarioLoginDTO();
        usuarioLoginDto.setEmail(usuario.getEmail());
        usuarioLoginDto.setSenha("TTtt@@22");
        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL, "login"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(usuarioLoginDto)))
            .andExpect(status().isOk());
    }

    @Test
    void logout() throws Exception {
        this.getMockMvc().perform(MockMvcRequestBuilders.get(buildUrl(BASE_URL, "logout"))
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isTemporaryRedirect());
    }
}
