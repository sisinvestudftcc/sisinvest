package br.com.sisinvest.pitseguranca.web.rest;

import br.com.sisinvest.pitseguranca.PitsegurancaApp;
import br.com.sisinvest.pitseguranca.util.IntTestComum;
import br.com.sisinvest.pitseguranca.security.RunWithMockCustomUser;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWithMockCustomUser
@Transactional
@SpringBootTest(classes = PitsegurancaApp.class)
class RecuperarSenhaResourceIT extends IntTestComum {

    private static final String BASE_URL = "/api/recuperar-senha";

    @Test
    void recuperarSenha() throws Exception {
        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL))
            .contentType(MediaType.APPLICATION_JSON).param("email", "usuariotcc@teste.com.br"))
            .andExpect(status().isOk());
    }
}
