package br.com.sisinvest.pitseguranca.web.rest;

import br.com.sisinvest.pitseguranca.PitsegurancaApp;
import br.com.sisinvest.pitseguranca.service.dto.filtro.AuditoriaFiltroDTO;
import br.com.sisinvest.pitseguranca.service.feign.PesquisaAuditoriaFeignClient;
import br.com.sisinvest.pitseguranca.util.IntTestComum;
import br.com.sisinvest.pitseguranca.security.RunWithMockCustomUser;
import br.com.sisinvest.pitseguranca.service.dto.CustomPageImpl;
import br.com.sisinvest.pitseguranca.service.dto.JvSnapshotDTO;
import br.com.sisinvest.pitseguranca.service.enumeration.AuditoriaEnum;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWithMockCustomUser
@Transactional
@SpringBootTest(classes = PitsegurancaApp.class)
public class AuditoriaResourceIT extends IntTestComum {

    private static final String BASE_URL = "/api/auditoria";

    @MockBean
    private PesquisaAuditoriaFeignClient auditoriaFeignClient;

    @Test
    void listarPorEntidade() throws Exception {
        List<JvSnapshotDTO> jvSnapshots = new ArrayList<>();
        JvSnapshotDTO jvSnap = new JvSnapshotDTO();
        jvSnap.setAuthor("Anônimo");
        jvSnap.setCommitDate(LocalDateTime.now());
        jvSnapshots.add(jvSnap);
        final CustomPageImpl<JvSnapshotDTO> jvSnapshot = new CustomPageImpl<>(jvSnapshots);
        Mockito.when(auditoriaFeignClient.listarPorEntidade(Mockito.anyString(), Mockito.anyString(), Mockito.anyInt(), Mockito.anyString()))
            .thenReturn(jvSnapshot);
        Mockito.when(auditoriaFeignClient.carregarDiferenca(Mockito.anyString(), Mockito.anyString(), Mockito.anyString()))
            .thenReturn(criarCampos());
        AuditoriaFiltroDTO filtro = criarFiltro();
        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL, "listar-por-entidade"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(filtro)))
            .andExpect(status().isOk());
    }

    private AuditoriaFiltroDTO criarFiltro() {
        AuditoriaFiltroDTO filtro = new AuditoriaFiltroDTO();
        filtro.setId("1");
        filtro.setAuditoria(AuditoriaEnum.USUARIO);
        return filtro;
    }

    private Map<String, Object> criarCampos() {
        Map<String, Object> campo = new HashMap<>();
        campo.put("nome", "usuario");
        return campo;
    }
}
