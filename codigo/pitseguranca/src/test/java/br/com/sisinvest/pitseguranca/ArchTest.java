package br.com.sisinvest.pitseguranca;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {

        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("br.com.sisinvest.pitseguranca");

        noClasses()
            .that()
                .resideInAnyPackage("br.com.sisinvest.pitseguranca.service..")
            .or()
                .resideInAnyPackage("br.com.sisinvest.pitseguranca.repository..")
            .should().dependOnClassesThat()
                .resideInAnyPackage("..br.com.sisinvest.pitseguranca.web..")
        .because("Services and repositories should not depend on web layer")
        .check(importedClasses);
    }
}
