package br.com.sisinvest.pitseguranca.security;

import br.com.sisinvest.pitseguranca.service.enumeration.UsuarioPerfilEnum;
import org.springframework.security.test.context.support.WithSecurityContext;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@WithSecurityContext(factory = RunWithMockCustomUserSecurityContextFactory.class)
public @interface RunWithMockCustomUser {

    String username() default "usuariotcc@teste.com.br";
    long idUsuario() default 1L;
    String perfil() default "GESTOR";
    String tipoUsuario() default "I";
    UsuarioPerfilEnum perfilNome() default UsuarioPerfilEnum.GESTOR;
}
