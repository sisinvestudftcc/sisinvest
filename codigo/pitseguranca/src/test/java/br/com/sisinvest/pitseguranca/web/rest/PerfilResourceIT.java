package br.com.sisinvest.pitseguranca.web.rest;

import br.com.sisinvest.pitseguranca.PitsegurancaApp;
import br.com.sisinvest.pitseguranca.util.IntTestComum;
import br.com.sisinvest.pitseguranca.security.RunWithMockCustomUser;
import br.com.sisinvest.pitseguranca.service.dto.PerfilDTO;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWithMockCustomUser
@Transactional
@SpringBootTest(classes = PitsegurancaApp.class)
class PerfilResourceIT extends IntTestComum {

    private static final String BASE_URL = "/api/public/perfil";

    @Test
    void filtro() throws Exception {
        PerfilDTO filtro = new PerfilDTO();
        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(filtro)))
            .andExpect(status().isOk());
    }
}
