package br.com.sisinvest.pitprojeto.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ProjetoEquipamentoCamposDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String unidadesHabitacionais;
    private String unidadesImobiliarias;
    private String capacidadesMesas;
    private String tamanhoProjeto;
    private String numeroPavilhoes;
    private String numeroSalasReuniao;
    private String numeroAnualVisitantes;
    private String rendaAnual;
    private String gastoMedioVisitantes;
    private String capacidadePublico;
    private String capacidadeAtracao;
    private String outroEquipamento;

}
