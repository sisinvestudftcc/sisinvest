package br.com.sisinvest.pitprojeto.service.dto.filtro;

import br.com.sisinvest.pitprojeto.service.dto.StatusDTO;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class StatusFiltroDTO extends StatusDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private List<Long> notStatusList;

}
