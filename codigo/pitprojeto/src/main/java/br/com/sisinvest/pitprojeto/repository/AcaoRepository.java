package br.com.sisinvest.pitprojeto.repository;

import br.com.sisinvest.pitprojeto.domain.Acao;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

@JaversSpringDataAuditable
public interface AcaoRepository extends JpaRepository<Acao, Long>, JpaSpecificationExecutor<Acao> {

}
