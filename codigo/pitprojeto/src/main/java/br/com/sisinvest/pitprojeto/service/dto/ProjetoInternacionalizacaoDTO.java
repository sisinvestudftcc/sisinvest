package br.com.sisinvest.pitprojeto.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class ProjetoInternacionalizacaoDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private IdiomaDTO idioma;
    private String nome;
    private String descricao;
    private List<ProjetoInternacionalizacaoArquivoDTO> projetoInternacionalizacaoArquivos;
    private Boolean internacionalizacaoPadrao;

}
