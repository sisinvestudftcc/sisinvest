package br.com.sisinvest.pitprojeto.service;

import br.com.sisinvest.pitprojeto.repository.ValorProjetoRepository;
import br.com.sisinvest.pitprojeto.service.dto.ValorProjetoDTO;
import br.com.sisinvest.pitprojeto.service.mapper.ValorProjetoMapper;
import br.com.sisinvest.pitprojeto.domain.ValorProjeto;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class ValorProjetoService {

    private final ValorProjetoMapper mapper;
    private final ValorProjetoRepository repository;

    @Transactional(readOnly = true)
    public Page<ValorProjetoDTO> buscarTodos(Pageable pageable) {
        return repository.findAll(specFiltro(), pageable).map(mapper::toDto);
    }

    private Specification<ValorProjeto> specFiltro() {
        return (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();

            return cb.and(predicates.toArray(new Predicate[0]));
        };
    }

}
