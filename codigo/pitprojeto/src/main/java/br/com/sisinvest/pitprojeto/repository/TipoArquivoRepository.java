package br.com.sisinvest.pitprojeto.repository;

import br.com.sisinvest.pitprojeto.domain.TipoArquivo;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

@JaversSpringDataAuditable
public interface TipoArquivoRepository extends JpaRepository<TipoArquivo, Long>, JpaSpecificationExecutor<TipoArquivo> {

}
