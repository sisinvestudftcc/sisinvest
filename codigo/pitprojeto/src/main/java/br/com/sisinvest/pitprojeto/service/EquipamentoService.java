package br.com.sisinvest.pitprojeto.service;

import br.com.sisinvest.pitprojeto.repository.EquipamentoRepository;
import br.com.sisinvest.pitprojeto.service.dto.EquipamentoDTO;
import br.com.sisinvest.pitprojeto.service.dto.filtro.EquipamentoFiltroDTO;
import br.com.sisinvest.pitprojeto.service.mapper.EquipamentoMapper;
import br.com.sisinvest.pitprojeto.domain.Equipamento;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class EquipamentoService {

    private final EquipamentoMapper mapper;
    private final EquipamentoRepository repository;

    @Transactional(readOnly = true)
    public Page<EquipamentoDTO> buscarTodos(EquipamentoFiltroDTO filtro, Pageable pageable) {
        return repository.findAll(specFiltro(filtro), pageable).map(mapper::toDto);
    }

    private Specification<Equipamento> specFiltro(EquipamentoFiltroDTO filtro) {
        return (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();

            if(filtro.getGrupoEquipamento() != null) {
                predicates.add(cb.equal(root.get("grupoEquipamento"), filtro.getGrupoEquipamento()));
            }

            return cb.and(predicates.toArray(new Predicate[0]));
        };
    }

}
