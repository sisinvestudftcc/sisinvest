package br.com.sisinvest.pitprojeto.repository;

import br.com.sisinvest.pitprojeto.domain.ValorProjeto;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

@JaversSpringDataAuditable
public interface ValorProjetoRepository extends JpaRepository<ValorProjeto, Long>, JpaSpecificationExecutor<ValorProjeto> {

}
