package br.com.sisinvest.pitprojeto.service.dto.filtro;

import br.com.sisinvest.pitprojeto.service.dto.ProjetoDTO;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

@Getter
@Setter
public class ProjetoFiltroDTO extends ProjetoDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long usuarioId;

    private Long apenasProprios;

    private Long usuarioFavoritado;

    private Long projetoId;

    private String idioma;

    private String nome;

    private String cnpj;

    private String protocolo;

    private Long pais;

    private Long naturezaInvestimento;

    private Long grupoEquipamento;

    private Long equipamento;

    private Long modeloContrato;

    private String valorProjeto;

    private Long status;

    private Long usuarioStatus;

    private String estado;

    private String cidade;

    private List<Long> statusList;

    private List<Long> segmentoList;

    private List<Long> notStatusList;

    private Timestamp dataInicio;

    private Timestamp dataConclusao;

    private Boolean internacionalizacaoPadrao;

}
