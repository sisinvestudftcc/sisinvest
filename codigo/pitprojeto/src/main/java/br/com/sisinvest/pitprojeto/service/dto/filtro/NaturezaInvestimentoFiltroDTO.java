package br.com.sisinvest.pitprojeto.service.dto.filtro;

import br.com.sisinvest.pitprojeto.service.dto.NaturezaInvestimentoDTO;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class NaturezaInvestimentoFiltroDTO extends NaturezaInvestimentoDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

}
