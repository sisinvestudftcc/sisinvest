package br.com.sisinvest.pitprojeto.repository;

import br.com.sisinvest.pitprojeto.domain.EmpregosProjeto;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

@JaversSpringDataAuditable
public interface EmpregosProjetoRepository extends JpaRepository<EmpregosProjeto, Long>, JpaSpecificationExecutor<EmpregosProjeto> {

}
