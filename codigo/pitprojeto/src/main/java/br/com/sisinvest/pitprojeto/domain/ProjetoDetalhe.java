package br.com.sisinvest.pitprojeto.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;

@Getter
@Setter
@Entity
@Table(name = "TB_PROJETO_DETALHE")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ProjetoDetalhe implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final String SEQ_PROJETO_DETALHE = "SEQ_PROJETO_DETALHE";

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQ_PROJETO_DETALHE)
    @SequenceGenerator(name = SEQ_PROJETO_DETALHE, sequenceName = SEQ_PROJETO_DETALHE, allocationSize = 1)
    private Long id;

    @ManyToOne
    @JoinColumn(referencedColumnName = "ID", name = "PROJETO_ID")
    private Projeto projeto;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(referencedColumnName = "ID", name = "NATUREZA_INVESTIMENTO_ID")
    private NaturezaInvestimento naturezaInvestimento;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(referencedColumnName = "ID", name = "MODELO_CONTRATO_ID")
    private ModeloContrato modeloContrato;

    @Column(name = "VALOR_ESTIMADO")
    private String valorProjeto;

    @Column(name = "EMPREGOS_PROJETO")
    private String empregosProjeto;

    @Column(name = "DATA_INICIO")
    private Timestamp dataInicio;

    @Column(name = "DATA_CONCLUSAO")
    private Timestamp dataConclusao;

    @Column(name = "MODELO_CONTRATO_OUTRO")
    private String modeloContratoOutro;

    @Column(name = "METRAGEM_AREA_CONSTRUIDA")
    private String areaConstruida;

    @Column(name = "METRAGEM_AREA_ADQUIRIDA")
    private String areaTotal;

    @Column(name = "GRUPO_OPERADOR")
    private String grupoOperador;

}
