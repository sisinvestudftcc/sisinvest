package br.com.sisinvest.pitprojeto.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class PublicacaoPortalDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

}
