package br.com.sisinvest.pitprojeto.service;

import br.com.sisinvest.pitprojeto.repository.NaturezaInvestimentoRepository;
import br.com.sisinvest.pitprojeto.service.dto.NaturezaInvestimentoDTO;
import br.com.sisinvest.pitprojeto.service.mapper.NaturezaInvestimentoMapper;
import br.com.sisinvest.pitprojeto.domain.NaturezaInvestimento;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class NaturezaInvestimentoService {

    private final NaturezaInvestimentoMapper mapper;
    private final NaturezaInvestimentoRepository repository;

    @Transactional(readOnly = true)
    public Page<NaturezaInvestimentoDTO> buscarTodos(Pageable pageable) {
        return repository.findAll(specFiltro(), pageable).map(mapper::toDto);
    }

    private Specification<NaturezaInvestimento> specFiltro() {
        return (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();

            return cb.and(predicates.toArray(new Predicate[0]));
        };
    }

}
