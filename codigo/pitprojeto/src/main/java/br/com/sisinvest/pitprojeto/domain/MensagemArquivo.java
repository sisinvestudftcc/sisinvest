package br.com.sisinvest.pitprojeto.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "TB_MENSAGEM_ARQUIVO")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class MensagemArquivo implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final String SEQ_MENSAGEM_ARQUIVO = "SEQ_MENSAGEM_ARQUIVO";

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQ_MENSAGEM_ARQUIVO)
    @SequenceGenerator(name = SEQ_MENSAGEM_ARQUIVO, sequenceName = SEQ_MENSAGEM_ARQUIVO, allocationSize = 1)
    private Long id;

    @ManyToOne
    @JoinColumn(referencedColumnName = "ID", name = "MENSAGEM_ID")
    private Mensagem mensagem;

    @ManyToOne
    @JoinColumn(referencedColumnName = "ID", name = "ARQUIVO_ID")
    private Arquivo arquivo;


}
