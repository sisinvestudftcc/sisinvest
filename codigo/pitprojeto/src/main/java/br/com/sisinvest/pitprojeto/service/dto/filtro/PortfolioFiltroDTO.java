package br.com.sisinvest.pitprojeto.service.dto.filtro;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class PortfolioFiltroDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<Long> idList;

}
