package br.com.sisinvest.pitprojeto.service;

import br.com.sisinvest.pitprojeto.repository.GrupoEquipamentoRepository;
import br.com.sisinvest.pitprojeto.service.dto.GrupoEquipamentoDTO;
import br.com.sisinvest.pitprojeto.service.mapper.GrupoEquipamentoMapper;
import br.com.sisinvest.pitprojeto.domain.GrupoEquipamento;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class GrupoEquipamentoService {

    private final GrupoEquipamentoMapper mapper;
    private final GrupoEquipamentoRepository repository;

    @Transactional(readOnly = true)
    public Page<GrupoEquipamentoDTO> buscarTodos(Pageable pageable) {
        return repository.findAll(specFiltro(), pageable).map(mapper::toDto);
    }

    private Specification<GrupoEquipamento> specFiltro() {
        return (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();

            return cb.and(predicates.toArray(new Predicate[0]));
        };
    }

}
