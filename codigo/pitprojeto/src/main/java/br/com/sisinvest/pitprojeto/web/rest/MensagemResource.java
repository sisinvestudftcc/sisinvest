package br.com.sisinvest.pitprojeto.web.rest;

import br.com.sisinvest.pitprojeto.service.MensagemService;
import br.com.sisinvest.pitprojeto.service.dto.MensagemDTO;
import br.com.sisinvest.pitprojeto.service.dto.ModeracaoDTO;
import br.com.sisinvest.pitprojeto.service.dto.filtro.MensagemFiltroDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/mensagem")
public class MensagemResource {

    private final MensagemService service;

    @PostMapping("/filtrar")
    public ResponseEntity<Page<MensagemDTO>> buscarPorId(@RequestBody MensagemFiltroDTO filtro, Pageable pageable) {

        return ResponseEntity.ok(service.buscarTodas(filtro, pageable));

    }

    @PostMapping("/projeto/{id}")
    public ResponseEntity<Page<MensagemDTO>> buscarPorProjeto(@PathVariable Long id, @RequestBody MensagemFiltroDTO filtro, Pageable pageable) {

        filtro.setProjeto(id);

        return ResponseEntity.ok(service.buscarPorProjeto(filtro, pageable));


    }

    @PostMapping(value = "/comentar/{projetoId}", consumes = "multipart/form-data")
    public ResponseEntity<Long> comentar(@RequestParam("arquivosUpload") MultipartFile[] arquivosUpload, @PathVariable Long projetoId, @ModelAttribute ModeracaoDTO moderacaoDTO) throws IOException, JSONException {

        moderacaoDTO.setArquivos(Arrays.asList(arquivosUpload));

        return ResponseEntity.ok(service.comentar(projetoId, moderacaoDTO));

    }

    @PostMapping(value = "/editar/{id}", consumes = "multipart/form-data")
    public ResponseEntity<Long> editar(@RequestParam("arquivosUpload") MultipartFile[] arquivosUpload, @PathVariable Long id, @ModelAttribute ModeracaoDTO moderacaoDTO) throws IOException, JSONException {

        moderacaoDTO.setArquivos(Arrays.asList(arquivosUpload));

        return ResponseEntity.ok(service.editar(id, moderacaoDTO));

    }

    @PostMapping(value = "/responder/{id}", consumes = "multipart/form-data")
    public ResponseEntity<Long> responder(@RequestParam("arquivosUpload") MultipartFile[] arquivosUpload, @PathVariable Long id, @ModelAttribute ModeracaoDTO moderacaoDTO) throws IOException, JSONException {

        moderacaoDTO.setArquivos(Arrays.asList(arquivosUpload));

        return ResponseEntity.ok(service.responder(id, moderacaoDTO));

    }

    @PostMapping(value = "/excluir/{id}")
    public ResponseEntity<Long> responder(@PathVariable Long id) throws JSONException, IOException {

        return ResponseEntity.ok(service.excluir(id));

    }

}
