package br.com.sisinvest.pitprojeto.service.mapper;

import br.com.sisinvest.pitprojeto.domain.Mensagem;
import br.com.sisinvest.pitprojeto.service.dto.MensagemDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface MensagemDetalhadaMapper {

    @Mapping(target = "id")
    @Mapping(target = "dataCriacao")
    @Mapping(target = "dataAtualizacao")
    @Mapping(target = "protocolo")
    @Mapping(target = "projeto", source = "projeto")
    @Mapping(target = "tipoMensagem", source = "tipoMensagem")
    @Mapping(target = "status", source = "status")
    @Mapping(target = "arquivoList", source = "arquivoList")
    @Mapping(target = "respostaList", source = "respostaList")
    MensagemDTO toDto(Mensagem entity);

}
