package br.com.sisinvest.pitprojeto.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "TB_STATUS")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Status implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final Integer ATIVO = 1;
    public static final Integer INATIVO = 2;
    public static final Integer EXCLUIDO = 3;
    public static final Integer RASCUNHO = 4;
    public static final Integer SOB_ANALISE = 5;
    public static final Integer APROVADO = 6;
    public static final Integer REPROVADO = 7;
    public static final Integer ESCLARECIMENTO = 8;
    public static final Integer NAO_RESPONDIDO = 9;
    public static final Integer RESPONDIDO = 10;

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "NOME")
    private String nome;

}
