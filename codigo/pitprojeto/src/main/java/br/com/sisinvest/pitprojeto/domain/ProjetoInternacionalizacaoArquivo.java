package br.com.sisinvest.pitprojeto.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "TB_PROJETO_INTERNACIONALIZACAO_ARQUIVO")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ProjetoInternacionalizacaoArquivo implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final String SEQ_PROJETO_INTERNACIONALIZACAO_ARQUIVO = "SEQ_PROJETO_INTERNACIONALIZACAO_ARQUIVO";

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQ_PROJETO_INTERNACIONALIZACAO_ARQUIVO)
    @SequenceGenerator(name = SEQ_PROJETO_INTERNACIONALIZACAO_ARQUIVO, sequenceName = SEQ_PROJETO_INTERNACIONALIZACAO_ARQUIVO, allocationSize = 1)
    private Long id;

    @ManyToOne
    @JoinColumn(referencedColumnName = "ID", name = "PROJETO_INTERNACIONALIZACAO_ID")
    private ProjetoInternacionalizacao projetoInternacionalizacao;

    @ManyToOne
    @JoinColumn(referencedColumnName = "ID", name = "ARQUIVO_ID")
    private Arquivo arquivo;

    @ManyToOne
    @JoinColumn(referencedColumnName = "ID", name = "TIPO_ARQUIVO_ID")
    private TipoArquivo tipoArquivo;

}
