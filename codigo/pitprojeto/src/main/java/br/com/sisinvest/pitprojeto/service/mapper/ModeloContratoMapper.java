package br.com.sisinvest.pitprojeto.service.mapper;

import br.com.sisinvest.pitprojeto.domain.ModeloContrato;
import br.com.sisinvest.pitprojeto.service.dto.ModeloContratoDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ModeloContratoMapper {

    @Mapping(target = "id")
    ModeloContratoDTO toDto(ModeloContrato entity);

}
