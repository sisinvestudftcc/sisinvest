package br.com.sisinvest.pitprojeto.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.sql.Timestamp;

@Getter
@Setter
public class ProjetoDetalheDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private Timestamp dataInicio;
    private Timestamp dataConclusao;
    private NaturezaInvestimentoDTO naturezaInvestimento;
    private ModeloContratoDTO modeloContrato;
    private String modeloContratoOutro;
    private String valorProjeto;
    private String empregosProjeto;
    private String areaConstruida;
    private String areaTotal;
    private String grupoOperador;

}
