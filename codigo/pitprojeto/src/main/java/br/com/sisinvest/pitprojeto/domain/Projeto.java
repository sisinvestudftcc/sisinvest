package br.com.sisinvest.pitprojeto.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.GeneratedValue;
import javax.persistence.OneToOne;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.CascadeType;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "TB_PROJETO")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Projeto implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final String SEQ_PROJETO = "SEQ_PROJETO";
    public static final String PROTOCOLO_INICIO = "PRJ";

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQ_PROJETO)
    @SequenceGenerator(name = SEQ_PROJETO, sequenceName = SEQ_PROJETO, allocationSize = 1)
    private Long id;

    @Column(name = "PROTOCOLO")
    private String protocolo;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(referencedColumnName = "ID", name = "STATUS_ID")
    private Status status;

    @Column(name = "DATA_CRIACAO")
    private Timestamp dataCriacao;

    @Column(name = "DATA_ATUALIZACAO")
    private Timestamp dataAtualizacao;

    @Column(name = "USUARIO_ID")
    private Long usuario;

    @Column(name = "PUBLICACAO_PORTAL_ID")
    private Long publicacaoPortal;

    @OneToMany(mappedBy = "projeto", targetEntity = ProjetoEmpresa.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ProjetoEmpresa> projetoEmpresaList;

    @OneToMany(mappedBy = "projeto", targetEntity = ProjetoInternacionalizacao.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ProjetoInternacionalizacao> projetoInternacionalizacaoList;

    @OneToMany(mappedBy = "projeto", targetEntity = ProjetoEquipamento.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ProjetoEquipamento> projetoEquipamentoList;

    @OneToMany(mappedBy = "projeto", targetEntity = ProjetoDetalhe.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ProjetoDetalhe> projetoDetalheList;

    @OneToMany(mappedBy = "projeto", targetEntity = ProjetoLocalizacao.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ProjetoLocalizacao> projetoLocalizacaoList;

    @OneToMany(mappedBy = "projeto", targetEntity = ProjetoSegmento.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ProjetoSegmento> projetoSegmentoList;

    @OneToMany(mappedBy = "projeto", targetEntity = ProjetoLinhaTemporal.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ProjetoLinhaTemporal> projetoLinhaTemporalList;

    @OneToMany(mappedBy = "projeto", targetEntity = ProjetoDestaque.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ProjetoDestaque> projetoDestaqueList;

    @OneToMany(mappedBy = "projeto", targetEntity = ProjetoFavorito.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ProjetoFavorito> projetoFavoritoList;

    public Projeto() {
        this.projetoEmpresaList = new ArrayList<ProjetoEmpresa>();
        this.projetoInternacionalizacaoList = new ArrayList<ProjetoInternacionalizacao>();
        this.projetoEquipamentoList = new ArrayList<ProjetoEquipamento>();
        this.projetoDetalheList = new ArrayList<ProjetoDetalhe>();
        this.projetoLocalizacaoList = new ArrayList<ProjetoLocalizacao>();
        this.projetoSegmentoList = new ArrayList<ProjetoSegmento>();
        this.projetoLinhaTemporalList = new ArrayList<ProjetoLinhaTemporal>();
        this.projetoDestaqueList = new ArrayList<ProjetoDestaque>();
        this.projetoFavoritoList = new ArrayList<ProjetoFavorito>();
    }
}
