package br.com.sisinvest.pitprojeto.service.dto.formulario;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.sql.Blob;
import java.util.List;

@Getter
@Setter
public class InternacionalizacaoFormularioDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private Long idiomaId;

    private String nome;

    private String descricao;

    private List<Long> anexosRemovidos;

    private Boolean removerImagem;

    private Boolean internacionalizacaoPadrao;


}
