package br.com.sisinvest.pitprojeto.service.dto.formulario;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class LocalizacaoFormularioDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String latitude;

    private String longitude;

    private String cidade;

    private String estado;


}
