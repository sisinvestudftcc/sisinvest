package br.com.sisinvest.pitprojeto.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableJpaRepositories("br.com.sisinvest.pitprojeto.repository")
@EnableTransactionManagement
public class DatabaseConfiguration {
}
