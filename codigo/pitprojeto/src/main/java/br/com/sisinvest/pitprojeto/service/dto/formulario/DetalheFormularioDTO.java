package br.com.sisinvest.pitprojeto.service.dto.formulario;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

@Getter
@Setter
public class DetalheFormularioDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private Timestamp dataInicio;

    private Timestamp dataConclusao;

    private Long naturezaInvestimento;

    private Long modeloContrato;

    private String outroModeloContrato;

    private String valorProjeto;

    private String empregosProjeto;

    private String areaConstruida;

    private String areaTotal;

    private String grupoOperador;


}
