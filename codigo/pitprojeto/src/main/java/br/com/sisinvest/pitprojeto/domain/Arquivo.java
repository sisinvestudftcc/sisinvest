package br.com.sisinvest.pitprojeto.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.javers.core.metamodel.annotation.DiffIgnore;
import org.javers.core.metamodel.annotation.ShallowReference;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.GeneratedValue;
import javax.persistence.OneToOne;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.JoinColumn;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "TB_ARQUIVO")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@ShallowReference
public class Arquivo implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final String SEQ_ARQUIVO = "SEQ_ARQUIVO";

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQ_ARQUIVO)
    @SequenceGenerator(name = SEQ_ARQUIVO, sequenceName = SEQ_ARQUIVO, allocationSize = 1)
    private Long id;

    @Column(name = "NOME")
    private String nome;

    @Lob
    @Column(name = "ARQUIVO")
    @DiffIgnore
    private byte[] documento;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(referencedColumnName = "ID", name = "STATUS_ID")
    private Status status;

}
