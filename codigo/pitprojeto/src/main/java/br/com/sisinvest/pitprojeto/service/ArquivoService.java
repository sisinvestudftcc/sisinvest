package br.com.sisinvest.pitprojeto.service;

import br.com.sisinvest.pitprojeto.repository.ArquivoRepository;
import br.com.sisinvest.pitprojeto.repository.StatusRepository;
import br.com.sisinvest.pitprojeto.domain.Arquivo;
import br.com.sisinvest.pitprojeto.domain.Status;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
@Slf4j
@Transactional
@RequiredArgsConstructor
public class ArquivoService {

    private final ArquivoRepository repository;
    private final StatusRepository statusRepository;

    @Transactional(readOnly = true)
    public Arquivo renderizarPorId(Long id) {
        return repository.findById(id).orElse(new Arquivo());
    }

    public Arquivo criaNovoArquivo(MultipartFile arquivoMultipart) {

        Status statusAtivo = statusRepository.findById(Status.ATIVO.longValue())
            .orElse(new Status());

        Arquivo arquivo = new Arquivo();
        arquivo.setNome(arquivoMultipart.getOriginalFilename());
        arquivo.setStatus(statusAtivo);

        try {
            arquivo.setDocumento(arquivoMultipart.getBytes());
        } catch (IOException e) {
            log.debug("Falha ao cadastrar arquivo", e);
        }

        return repository.save(arquivo);

    }

}
