package br.com.sisinvest.pitprojeto.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "TB_PROJETO_LOCALIZACAO")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ProjetoLocalizacao implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final String SEQ_PROJETO_LOCALIZACAO = "SEQ_PROJETO_LOCALIZACAO";

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQ_PROJETO_LOCALIZACAO)
    @SequenceGenerator(name = SEQ_PROJETO_LOCALIZACAO, sequenceName = SEQ_PROJETO_LOCALIZACAO, allocationSize = 1)
    private Long id;

    @ManyToOne
    @JoinColumn(referencedColumnName = "ID", name = "PROJETO_ID")
    private Projeto projeto;

    @Column(name = "LATITUDE")
    private String latitude;

    @Column(name = "LONGITUDE")
    private String longitude;

    @Column(name = "CIDADE")
    private String cidade;

    @Column(name = "ESTADO")
    private String estado;

}
