package br.com.sisinvest.pitprojeto.service.dto.filtro;

import br.com.sisinvest.pitprojeto.service.dto.EstadoDTO;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class EstadoFiltroDTO extends EstadoDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

}
