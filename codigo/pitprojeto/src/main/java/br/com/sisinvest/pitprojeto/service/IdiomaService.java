package br.com.sisinvest.pitprojeto.service;

import br.com.sisinvest.pitprojeto.repository.IdiomaRepository;
import br.com.sisinvest.pitprojeto.service.dto.IdiomaDTO;
import br.com.sisinvest.pitprojeto.service.mapper.IdiomaMapper;
import br.com.sisinvest.pitprojeto.domain.Idioma;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class IdiomaService {

    private final IdiomaMapper mapper;
    private final IdiomaRepository repository;

    @Transactional(readOnly = true)
    public Page<IdiomaDTO> buscarTodos(Pageable pageable) {
        return repository.findAll(specFiltro(), pageable).map(mapper::toDto);
    }

    private Specification<Idioma> specFiltro() {
        return (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();

            return cb.and(predicates.toArray(new Predicate[0]));
        };
    }

}
