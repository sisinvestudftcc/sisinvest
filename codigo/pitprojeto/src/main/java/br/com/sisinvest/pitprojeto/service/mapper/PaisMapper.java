package br.com.sisinvest.pitprojeto.service.mapper;

import br.com.sisinvest.pitprojeto.domain.Pais;
import br.com.sisinvest.pitprojeto.service.dto.PaisDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface PaisMapper {

    @Mapping(target = "id")
    PaisDTO toDto(Pais entity);

}
