package br.com.sisinvest.pitprojeto.service;

import br.com.sisinvest.pitprojeto.repository.PaisRepository;
import br.com.sisinvest.pitprojeto.service.dto.PaisDTO;
import br.com.sisinvest.pitprojeto.service.mapper.PaisMapper;
import br.com.sisinvest.pitprojeto.domain.Pais;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class PaisService {

    private final PaisMapper mapper;
    private final PaisRepository repository;

    @Transactional(readOnly = true)
    public Page<PaisDTO> buscarTodos(Pageable pageable) {
        return repository.findAll(specFiltro(), pageable).map(mapper::toDto);
    }

    private Specification<Pais> specFiltro() {
        return (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();

            return cb.and(predicates.toArray(new Predicate[0]));
        };
    }

}
