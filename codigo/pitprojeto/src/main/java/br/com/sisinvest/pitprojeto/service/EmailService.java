package br.com.sisinvest.pitprojeto.service;

import br.com.sisinvest.pitprojeto.service.dto.UsuarioDTO;
import br.com.sisinvest.pitprojeto.service.dto.UsuarioEmailDTO;
import br.com.sisinvest.pitprojeto.domain.Mensagem;
import br.com.sisinvest.pitprojeto.domain.Projeto;
import br.com.sisinvest.pitprojeto.domain.ProjetoInternacionalizacao;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class EmailService {

    @Value("${application.mail.endereco-remetente}")
    private String remetente;
    @Value("${application.mail.nome-remetente}")
    private String nomeRemetente;

    private static final String GESTOR_ROLE = "GESTOR";

    private static final String STATUS_ATIVO = "ATIVO";

    public static final String URL = "localhost:4200";

    private static final String SAUDACAO = "<p><span>Prezado(a),</span></p>";

    private static final String CAMINHO_NOME_USUARIO = "/pitseguranca/api/usuario/";

    private static final String CAMINHO_LISTA_USUARIOS = "/pitseguranca/api/usuario/filtrar";

    private static final String CAMINHO_USUARIO_ID = "/pitseguranca/api/usuario/";

    private static final String CAMINHO_PROJETO = "/#/projetos/";

    private static final String ASSUNTO_INICIO_EMAIL = "[SISINVEST]";

    private static final String ASSINATURA_EMAIL = "</br><p><span>SISINVEST</span></p>" +
        "<br/><p>ATENÇÃO! Esta mensagem foi gerada e enviada automaticamente pelo sistema. Favor não responder.</p>";

    private static final String PROJETO_CADASTRADO = "Projeto cadastrado";

    private static final String PROJETO_MODERACAO = "Projeto em moderação";

    private static final String PROJETO_APROVADO = "Projeto aprovado";

    private static final String PROJETO_REPROVADO = "Projeto reprovado";

    private static final String PROJETO_RESTAURADO = "Projeto restaurado";

    private static final String PROJETO_EM_ESCLARECIMENTO = "Projeto em esclarecimento";

    private static final String PROJETO_RESPONDIDO = "Esclarecimento respondido";

    private static final String PROJETO_COMENTADO = "Comentário adicionado";

    private static final String PROJETO_COMENTARIO_REMOVIDO = "Comentário excluído";

    private static final String PROJETO_PUBLICADO = "Projeto publicado";

    private static final String PROJETO_DESPUBLICADO = "Projeto despublicado";

    private static final String PROJETO_DESTACADO = "Projeto destacado";

    private static final String PROJETO_DESTAQUE_REMOVIDO = "Destaque removido";

    private static final String PROJETO_MENSAGEM_ENVIADA = "Mensagem Enviada";

    private static final String PROJETO_MENSAGEM_RESPONDIDA = "Mensagem Respondida";

    private static final String PROJETO_MENSAGEM_EXCLUIDA = "Mensagem Excluída";

    private static final String PROJETO_RESPOSTA_EXCLUIDA = "Resposta Excluída";

    private static final String QUEBRA_LINHA = "<br/>";

    private static final String INICIO_PROJETO = "<p>O projeto ";

    private static final String INICIO_MENSAGEM = "<p>A mensagem ";

    private static final String INICIO_RESPOSTA_MENSAGEM = "<p>A resposta da mensagem ";

    private static final String FIM_LINHA = ".</p>";

    private static final String CONTENT_TYPE = "Content-type";

    private static final String APPLICATION_JSON = "application/json";

    private static final String UTF_8 = "UTF-8";

    @Autowired
    private Environment env;

    public final JavaMailSender emailSender;

    public String pegaUrlPorAmbiente() {

        String urlApi = "";

        HttpServletRequest currentRequest = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
            .getRequest();

        switch (currentRequest.getServerName()) {
            case URL:
                urlApi = env.getProperty("portal.hlog.urlApiSeguranca");
                break;
            default:
                urlApi = env.getProperty("portal.dev.urlApiSeguranca");
                break;
        }

        return urlApi;

    }


    public void cadastrarProjeto(Projeto projeto) {

        if (!projeto.getProjetoEmpresaList().isEmpty()) {

            String assuntoEmail = pegaAssunto(projeto, PROJETO_CADASTRADO);

            String baseUrl = pegaUrlPorAmbiente();

            String sb = SAUDACAO
                + "<p>Seu projeto foi cadastrado com sucesso e recebeu o seguinte protocolo: <b>" +
                projeto.getProtocolo() +
                "</b></p>" + QUEBRA_LINHA +
                stringParaAcessar(projeto, baseUrl) +
                QUEBRA_LINHA + ASSINATURA_EMAIL;


            if(projeto.getProjetoEmpresaList().get(0).getEmail() != null) {
                sendBasicEmail(
                    projeto.getProjetoEmpresaList().get(0).getEmail(),
                    assuntoEmail,
                    sb,
                    null
                );
            }

        }

    }


    public void enviarModeracao(Projeto projeto) throws IOException, JSONException {

        if (!projeto.getProjetoEmpresaList().isEmpty()) {

            String assuntoEmail = pegaAssunto(projeto, PROJETO_MODERACAO);

            String baseUrl = pegaUrlPorAmbiente();

            ProjetoInternacionalizacao[] projetoInternacionalizacao = pegaProjetoInternacionalizacao(projeto);

            String sb = SAUDACAO +
                INICIO_PROJETO +
                pegaLinkSemClique(projeto, baseUrl) +
                pegaNomeProtocolo(projeto, projetoInternacionalizacao[0]) +
                "</a> foi encaminhado para moderação." +
                "</p>" + QUEBRA_LINHA + ASSINATURA_EMAIL;

            sendBasicEmail(
                projeto.getProjetoEmpresaList().get(0).getEmail(),
                assuntoEmail,
                sb,
                pegaEmailGestores()
            );

        }

    }


    public void aprovar(Projeto projeto, String justificativa) throws IOException, JSONException {

        if (!projeto.getProjetoEmpresaList().isEmpty()) {

            String assuntoEmail = pegaAssunto(projeto, PROJETO_APROVADO);

            String baseUrl = pegaUrlPorAmbiente();

            ProjetoInternacionalizacao[] projetoInternacionalizacao = pegaProjetoInternacionalizacao(projeto);

            String sb = SAUDACAO +
                INICIO_PROJETO +
                pegaLinkSemClique(projeto, baseUrl) +
                pegaNomeProtocolo(projeto, projetoInternacionalizacao[0])
                + "</a> foi aprovado." +
                "</p></br><p>Segue análise do gestor:</p>" +
                "<p>" + justificativa + "</p>" +
                QUEBRA_LINHA + ASSINATURA_EMAIL;

            sendBasicEmail(
                projeto.getProjetoEmpresaList().get(0).getEmail(),
                assuntoEmail,
                sb,
                pegaEmailGestores()
            );

        }

    }

    private String pegaNomeProtocolo(Projeto projeto, ProjetoInternacionalizacao projetoInternacionalizacao) {
        return "<b>" +
            projetoInternacionalizacao.getNome() +
            "</b> - <b>" +
            projeto.getProtocolo() +
            "</b>";
    }


    public void reprovar(Projeto projeto, String justificativa) throws IOException, JSONException {

        if (!projeto.getProjetoEmpresaList().isEmpty()) {

            String assuntoEmail = pegaAssunto(projeto, PROJETO_REPROVADO);

            String baseUrl = pegaUrlPorAmbiente();

            ProjetoInternacionalizacao[] projetoInternacionalizacao = pegaProjetoInternacionalizacao(projeto);

            String sb = SAUDACAO +
                INICIO_PROJETO +
                pegaLinkSemClique(projeto, baseUrl) +
                pegaNomeProtocolo(projeto, projetoInternacionalizacao[0]) +
                "</a> foi reprovado." +
                "</p>" + QUEBRA_LINHA + "<p>Segue análise do gestor:</p>" +
                "<p>" + justificativa + "</p>" +
                QUEBRA_LINHA + ASSINATURA_EMAIL;

            sendBasicEmail(
                projeto.getProjetoEmpresaList().get(0).getEmail(),
                assuntoEmail,
                sb,
                pegaEmailGestores()
            );

        }

    }


    public void restaurar(Projeto projeto, String justificativa) throws IOException, JSONException {

        if (!projeto.getProjetoEmpresaList().isEmpty()) {

            String assuntoEmail = pegaAssunto(projeto, PROJETO_RESTAURADO);

            String baseUrl = pegaUrlPorAmbiente();

            ProjetoInternacionalizacao[] projetoInternacionalizacao = pegaProjetoInternacionalizacao(projeto);

            String sb = SAUDACAO +
                INICIO_PROJETO +
                pegaLinkSemClique(projeto, baseUrl) +
                pegaNomeProtocolo(projeto, projetoInternacionalizacao[0]) +
                "</a> foi restaurado." +
                "</p>" + QUEBRA_LINHA + "<p>Segue análise do gestor:</p>" +
                "<p>" + justificativa + "</p>" +
                QUEBRA_LINHA + ASSINATURA_EMAIL;

            sendBasicEmail(
                projeto.getProjetoEmpresaList().get(0).getEmail(),
                assuntoEmail,
                sb,
                pegaEmailGestores()
            );

        }

    }


    public void solicitarEsclarecimento(Projeto projeto, String justificativa) {

        if (!projeto.getProjetoEmpresaList().isEmpty()) {

            String assuntoEmail = pegaAssunto(projeto, PROJETO_EM_ESCLARECIMENTO);

            String baseUrl = pegaUrlPorAmbiente();

            ProjetoInternacionalizacao[] projetoInternacionalizacao = pegaProjetoInternacionalizacao(projeto);

            String sb = SAUDACAO +
                "<p>Houve uma solicitação de esclarecimento do projeto " +
                pegaNomeProtocolo(projeto, projetoInternacionalizacao[0]) +
                FIM_LINHA + QUEBRA_LINHA + justificativa + QUEBRA_LINHA +
                "<p>Para responder a essa solicitação de esclarecimento " + pegaLinkProjeto(projeto, baseUrl) + "</p>" +
                QUEBRA_LINHA + ASSINATURA_EMAIL;

            sendBasicEmail(
                projeto.getProjetoEmpresaList().get(0).getEmail(),
                assuntoEmail,
                sb,
                null
            );

        }

    }


    public void responderEsclarecimento(Projeto projeto, String justificativa) throws IOException, JSONException {

        if (!projeto.getProjetoEmpresaList().isEmpty()) {

            String assuntoEmail = pegaAssunto(projeto, PROJETO_RESPONDIDO);

            String baseUrl = pegaUrlPorAmbiente();

            ProjetoInternacionalizacao[] projetoInternacionalizacao = pegaProjetoInternacionalizacao(projeto);

            String sb = SAUDACAO +
                "<p>Os devidos esclarecimentos a respeito do projeto " +
                pegaNomeProtocolo(projeto, projetoInternacionalizacao[0]) +
                " foram enviados.</p>" + QUEBRA_LINHA + justificativa + QUEBRA_LINHA +
                "<p>Para prosseguir com o andamento deste projeto " + pegaLinkProjeto(projeto, baseUrl) + "</p>" +
                QUEBRA_LINHA + ASSINATURA_EMAIL;

            sendBasicEmail(
                null,
                assuntoEmail,
                sb,
                pegaEmailGestores()
            );

        }

    }

    public void comentar(Projeto projeto, String justificativa, Long usuarioId, Date dataComentario) throws IOException, JSONException {

        if (!projeto.getProjetoEmpresaList().isEmpty()) {

            String assuntoEmail = pegaAssunto(projeto, PROJETO_COMENTADO);

            String nomeUsuario = pegaNomeUsuario(usuarioId);

            String baseUrl = pegaUrlPorAmbiente();

            ProjetoInternacionalizacao[] projetoInternacionalizacao = pegaProjetoInternacionalizacao(projeto);

            String sb = SAUDACAO +
                INICIO_PROJETO +
                pegaNomeProtocolo(projeto, projetoInternacionalizacao[0]) +
                " foi comentado " +
                pegaAutorHora(dataComentario, nomeUsuario) + FIM_LINHA + QUEBRA_LINHA +
                justificativa + QUEBRA_LINHA +
                stringParaAcessar(projeto, baseUrl) +
                QUEBRA_LINHA + ASSINATURA_EMAIL;

            sendBasicEmail(
                null,
                assuntoEmail,
                sb,
                pegaEmailGestores()
            );

        }

    }

    public void enviarMensagem(Mensagem mensagem, Projeto projeto, String justificativa) throws IOException, JSONException {

        if (!projeto.getProjetoEmpresaList().isEmpty()) {

            String assuntoEmail = pegaAssunto(projeto, PROJETO_MENSAGEM_ENVIADA);

            String baseUrl = pegaUrlPorAmbiente();

            ProjetoInternacionalizacao[] projetoInternacionalizacao = pegaProjetoInternacionalizacao(projeto);

            String sb = SAUDACAO +
                "<p>Houve um envio de mensagem " +
                mensagem.getProtocolo() +
                " ao projeto " +
                pegaNomeProtocolo(projeto, projetoInternacionalizacao[0]) +
                FIM_LINHA + QUEBRA_LINHA +
                justificativa + QUEBRA_LINHA +
                stringResponderDuvida(projeto, baseUrl) +
                QUEBRA_LINHA + ASSINATURA_EMAIL;

            sendBasicEmail(
                projeto.getProjetoEmpresaList().get(0).getEmail(),
                assuntoEmail,
                sb,
                null
            );

        }

    }

    public void responderMensagem(Mensagem mensagem, Projeto projeto, String justificativa, UsuarioDTO usuarioDTO) throws IOException, JSONException {

        if (!projeto.getProjetoEmpresaList().isEmpty()) {

            String assuntoEmail = pegaAssunto(projeto, PROJETO_MENSAGEM_RESPONDIDA);

            String baseUrl = pegaUrlPorAmbiente();

            ProjetoInternacionalizacao[] projetoInternacionalizacao = pegaProjetoInternacionalizacao(projeto);

            String sb = SAUDACAO +
                "<p>Os devidos esclarecimentos a respeito da mensagem " +
                mensagem.getProtocolo() +
                " do projeto " +
                pegaNomeProtocolo(projeto, projetoInternacionalizacao[0]) +
                " foram enviados" +
                FIM_LINHA + QUEBRA_LINHA +
                justificativa + QUEBRA_LINHA +
                stringProsseguir(projeto, baseUrl) +
                QUEBRA_LINHA + ASSINATURA_EMAIL;

            String[] stringList = new String[]{usuarioDTO.getEmail()};

            sendBasicEmail(
                projeto.getProjetoEmpresaList().get(0).getEmail(),
                assuntoEmail,
                sb,
                stringList
            );

        }

    }

    public void excluirMensagem(Mensagem mensagem, Projeto projeto, UsuarioDTO usuarioDTO) throws IOException, JSONException {

        if (!projeto.getProjetoEmpresaList().isEmpty()) {

            String assuntoEmail = pegaAssunto(projeto, PROJETO_MENSAGEM_EXCLUIDA);

            String baseUrl = pegaUrlPorAmbiente();

            ProjetoInternacionalizacao[] projetoInternacionalizacao = pegaProjetoInternacionalizacao(projeto);

            Date dataComentario = new Timestamp(System.currentTimeMillis());

            String sb = SAUDACAO +
                INICIO_MENSAGEM +
                mensagem.getProtocolo() +
                " foi excluída do projeto " +
                pegaNomeProtocolo(projeto, projetoInternacionalizacao[0]) +
                pegaAutorHora(dataComentario, usuarioDTO.getNome()) + FIM_LINHA + QUEBRA_LINHA +
                stringParaAcessar(projeto, baseUrl) +
                QUEBRA_LINHA + ASSINATURA_EMAIL;

            sendBasicEmail(
                projeto.getProjetoEmpresaList().get(0).getEmail(),
                assuntoEmail,
                sb,
                null
            );

        }

    }

    public void excluirResposta(Mensagem mensagem, Projeto projeto, UsuarioDTO usuarioDTO) throws IOException, JSONException {

        if (!projeto.getProjetoEmpresaList().isEmpty()) {

            String assuntoEmail = pegaAssunto(projeto, PROJETO_RESPOSTA_EXCLUIDA);

            String baseUrl = pegaUrlPorAmbiente();

            ProjetoInternacionalizacao[] projetoInternacionalizacao = pegaProjetoInternacionalizacao(projeto);

            Date dataComentario = new Timestamp(System.currentTimeMillis());

            String sb = SAUDACAO +
                INICIO_RESPOSTA_MENSAGEM +
                mensagem.getProtocolo() +
                " foi excluída do projeto " +
                pegaNomeProtocolo(projeto, projetoInternacionalizacao[0]) +
                pegaAutorHora(dataComentario, usuarioDTO.getNome()) + FIM_LINHA + QUEBRA_LINHA +
                stringParaAcessar(projeto, baseUrl) +
                QUEBRA_LINHA + ASSINATURA_EMAIL;

            sendBasicEmail(
                projeto.getProjetoEmpresaList().get(0).getEmail(),
                assuntoEmail,
                sb,
                null
            );

        }

    }

    private String pegaAutorHora(Date dataComentario, String nomeUsuario) {

        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat hf = new SimpleDateFormat("HH:mm:ss");
        return " por <b>" + nomeUsuario
            + "</b> em <b>" + df.format(dataComentario)
            + "</b> às <b>" + hf.format(dataComentario)
            + "</b>";
    }


    public void removerComentario(Projeto projeto, String justificativa, Long usuarioId, Date dataComentario) throws IOException, JSONException {

        if (!projeto.getProjetoEmpresaList().isEmpty()) {

            String assuntoEmail = pegaAssunto(projeto, PROJETO_COMENTARIO_REMOVIDO);

            String nomeUsuario = pegaNomeUsuario(usuarioId);

            String baseUrl = pegaUrlPorAmbiente();

            ProjetoInternacionalizacao[] projetoInternacionalizacao = pegaProjetoInternacionalizacao(projeto);

            String sb = SAUDACAO +
                "<p>Um comentário foi excluído do projeto " +
                pegaNomeProtocolo(projeto, projetoInternacionalizacao[0]) + " " +
                pegaAutorHora(dataComentario, nomeUsuario)
                + FIM_LINHA + QUEBRA_LINHA +
                justificativa + QUEBRA_LINHA +
                stringParaAcessar(projeto, baseUrl) +
                QUEBRA_LINHA + ASSINATURA_EMAIL;

            sendBasicEmail(
                null,
                assuntoEmail,
                sb,
                pegaEmailGestores()
            );

        }

    }


    public void publicar(Projeto projeto, String linkPortal) throws IOException, JSONException {

        if (!projeto.getProjetoEmpresaList().isEmpty()) {

            String assuntoEmail = pegaAssunto(projeto, PROJETO_PUBLICADO);

            String baseUrl = pegaUrlPorAmbiente();

            ProjetoInternacionalizacao[] projetoInternacionalizacao = pegaProjetoInternacionalizacao(projeto);

            String sb = SAUDACAO +
                INICIO_PROJETO +
                pegaNomeProtocolo(projeto, projetoInternacionalizacao[0]) +
                " foi publicado e está disponível em " +
                pegaLinkPortal(linkPortal) +
                "</p>" + QUEBRA_LINHA + stringParaAcessar(projeto, baseUrl) +
                QUEBRA_LINHA + ASSINATURA_EMAIL;

            sendBasicEmail(
                projeto.getProjetoEmpresaList().get(0).getEmail(),
                assuntoEmail,
                sb,
                pegaEmailGestores()
            );

        }

    }


    public void despublicar(Projeto projeto, String justificativa, Long usuarioId, Date dataComentario) throws IOException, JSONException {

        if (!projeto.getProjetoEmpresaList().isEmpty()) {

            String assuntoEmail = pegaAssunto(projeto, PROJETO_DESPUBLICADO);

            String nomeUsuario = pegaNomeUsuario(usuarioId);

            String baseUrl = pegaUrlPorAmbiente();

            ProjetoInternacionalizacao[] projetoInternacionalizacao = pegaProjetoInternacionalizacao(projeto);
            String sb = SAUDACAO +
                INICIO_PROJETO +
                pegaNomeProtocolo(projeto, projetoInternacionalizacao[0]) +
                " foi despublicado "
                + pegaAutorHora(dataComentario, nomeUsuario)
                + FIM_LINHA + QUEBRA_LINHA +
                "<p>" + justificativa + "</p>" + QUEBRA_LINHA +
                stringParaAcessar(projeto, baseUrl) +
                QUEBRA_LINHA + ASSINATURA_EMAIL;

            sendBasicEmail(
                projeto.getProjetoEmpresaList().get(0).getEmail(),
                assuntoEmail,
                sb,
                pegaEmailGestores()
            );

        }

    }


    public void destacar(Projeto projeto, String linkPortal, Long usuarioId, Date dataComentario) throws IOException, JSONException {

        if (!projeto.getProjetoEmpresaList().isEmpty()) {

            String assuntoEmail = pegaAssunto(projeto, PROJETO_DESTACADO);

            String nomeUsuario = pegaNomeUsuario(usuarioId);

            String baseUrl = pegaUrlPorAmbiente();

            ProjetoInternacionalizacao[] projetoInternacionalizacao = pegaProjetoInternacionalizacao(projeto);

            String sb = SAUDACAO +
                INICIO_PROJETO +
                pegaNomeProtocolo(projeto, projetoInternacionalizacao[0]) +
                " foi destacado " + pegaAutorHora(dataComentario, nomeUsuario)
                + " e está disponível em " +
                pegaLinkPortal(linkPortal) +
                "</p>" + QUEBRA_LINHA +
                stringParaAcessar(projeto, baseUrl) +
                QUEBRA_LINHA + ASSINATURA_EMAIL;

            sendBasicEmail(
                projeto.getProjetoEmpresaList().get(0).getEmail(),
                assuntoEmail,
                sb,
                pegaEmailGestores()
            );

        }

    }

    public void removerDestaque(Projeto projeto, String justificativa, String linkPortal, Long usuarioId, Date dataComentario) throws IOException, JSONException {

        if (!projeto.getProjetoEmpresaList().isEmpty()) {

            String assuntoEmail = pegaAssunto(projeto, PROJETO_DESTAQUE_REMOVIDO);

            String nomeUsuario = pegaNomeUsuario(usuarioId);

            String baseUrl = pegaUrlPorAmbiente();

            ProjetoInternacionalizacao[] projetoInternacionalizacao = pegaProjetoInternacionalizacao(projeto);

            String sb = SAUDACAO +
                INICIO_PROJETO +
                pegaNomeProtocolo(projeto, projetoInternacionalizacao[0]) +
                " teve seu destaque removido " +
                pegaAutorHora(dataComentario, nomeUsuario)
                + "." + QUEBRA_LINHA + QUEBRA_LINHA +
                "<p>" + justificativa + "</p>" + QUEBRA_LINHA +
                "<p>Contudo o projeto permanece publicado e está disponível em " +
                pegaLinkPortal(linkPortal) +
                "</p>" + QUEBRA_LINHA +
                stringParaAcessar(projeto, baseUrl) +
                QUEBRA_LINHA + ASSINATURA_EMAIL;

            sendBasicEmail(
                null,
                assuntoEmail,
                sb,
                pegaEmailGestores()
            );

        }

    }

    private ProjetoInternacionalizacao[] pegaProjetoInternacionalizacao(Projeto projeto) {
        final ProjetoInternacionalizacao[] projetoInternacionalizacao = {new ProjetoInternacionalizacao()};

        projeto.getProjetoInternacionalizacaoList().forEach(projetoInternacionalizacao1 -> {
            if (projetoInternacionalizacao1.getInternacionalizacaoPadrao().equals(true)) {
                projetoInternacionalizacao[0] = projetoInternacionalizacao1;
            }
        });
        return projetoInternacionalizacao;
    }

    private void sendBasicEmail(String to, String subject, String text, String[] emailGestores) {
        try {
            log.info(String.format("Enviando e-mail de [%s] para [%s] com assunto: [%s] e encaminhando para [%s]", remetente, to, subject, Arrays.toString(emailGestores)));
            MimeMessage mail = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mail);

            if (to == null) {
                helper.setTo(emailGestores);
            } else {
                helper.setTo(to);
                if (emailGestores != null && emailGestores.length != 0) {
                    helper.setBcc(emailGestores);
                }
            }
            helper.setFrom(remetente, nomeRemetente);
            helper.setSubject(subject);
            helper.setText(text, true);

            emailSender.send(mail);
        } catch (MessagingException | UnsupportedEncodingException e) {
            log.debug(String.format("Falha ao enviar email para [%s] com assunto: [%s]", to, subject), e);
        }
    }

    private String stringParaAcessar(Projeto projeto, String baseUrl) {
        return "<p>Para acessar o projeto " + pegaLinkProjeto(projeto, baseUrl) + "</p>";
    }

    private String stringResponderDuvida(Projeto projeto, String baseUrl) {
        return "<p>Para responder a essa mensagem " + pegaLinkProjeto(projeto, baseUrl) + "</p>";
    }

    private String stringProsseguir(Projeto projeto, String baseUrl) {
        return "<p>Para prosseguir com o andamento deste projeto " + pegaLinkProjeto(projeto, baseUrl) + "</p>";
    }

    private String pegaLinkProjeto(Projeto projeto, String baseUrl) {
        return pegaLinkSemClique(projeto, baseUrl) + "clique aqui</a>";
    }

    private String pegaLinkSemClique(Projeto projeto, String baseUrl) {
        return "<a target=\"_blank\" href=\"" + baseUrl + CAMINHO_PROJETO + projeto.getId() + "\">";
    }

    private String pegaLinkPortal(String linkPortal) {
        return "<a href=\"" + linkPortal + "\" target=\"_blank\">" + linkPortal + "</a>";
    }

    private String pegaAssunto(Projeto projeto, String projetoModeracao) {
        List<String> assuntoStringList = new ArrayList<>();

        assuntoStringList.add(ASSUNTO_INICIO_EMAIL);
        assuntoStringList.add(projeto.getProtocolo());
        assuntoStringList.add(projetoModeracao);

        return String.join(" - ", assuntoStringList);
    }

    private String pegaNomeUsuario(Long usuarioId) throws IOException, JSONException {
        HttpClient client = getHttpClient();

        String url = pegaUrlPorAmbiente();

        HttpGet httpGet = new HttpGet(url + CAMINHO_NOME_USUARIO + usuarioId);

        httpGet.setHeader(CONTENT_TYPE, APPLICATION_JSON);

        HttpResponse response = client.execute(httpGet);

        String responseString = EntityUtils.toString(response.getEntity(), UTF_8);

        Gson gson = new GsonBuilder().create();

        JSONObject myObject = new JSONObject(responseString);

        UsuarioDTO usuarioDTO = gson.fromJson(myObject.toString(), UsuarioDTO.class);

        return usuarioDTO.getNome();
    }

    private HttpClient getHttpClient() {
        HttpServletRequest currentRequest = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
            .getRequest();

        Cookie[] cookies = currentRequest.getCookies();

        Cookie cookieAutenticacao = Arrays.stream(cookies)
            .filter(cookie -> cookie.getName().equals("jwt-token"))
            .collect(Collectors.toList())
            .get(0);

        CookieStore cookieStore = new BasicCookieStore();
        BasicClientCookie cookie = new BasicClientCookie(cookieAutenticacao.getName(), cookieAutenticacao.getValue());

        cookie.setDomain(currentRequest.getServerName());
        cookie.setPath("/");
        cookie.setSecure(false);

        cookieStore.addCookie(cookie);

        return HttpClientBuilder.create().setDefaultCookieStore(cookieStore).build();
    }

    public String[] pegaEmailGestores() throws IOException, JSONException {

        HttpClient client = getHttpClient();

        String url = pegaUrlPorAmbiente();

        HttpPost httpPost = new HttpPost(url + CAMINHO_LISTA_USUARIOS + "?page=0&size=999999&sort=nome,ASC");

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode node = mapper.createObjectNode();

        List<String> arrayList = new ArrayList<String>();

        arrayList.add(STATUS_ATIVO);

        ArrayNode array = mapper.valueToTree(arrayList);

        node.put("perfilEnum", GESTOR_ROLE);
        node.put("status", STATUS_ATIVO);
        node.putArray("listStatus").addAll(array);

        String json = node.toString();

        StringEntity entity = new StringEntity(json, ContentType.APPLICATION_JSON);

        httpPost.setEntity(entity);

        httpPost.setHeader(CONTENT_TYPE, APPLICATION_JSON);

        HttpResponse response = client.execute(httpPost);

        String responseString = EntityUtils.toString(response.getEntity(), UTF_8);

        Gson gson = new GsonBuilder().create();

        JSONObject myObject = new JSONObject(responseString);

        UsuarioEmailDTO usuarioEmailDTO = gson.fromJson(myObject.toString(), UsuarioEmailDTO.class);

        String[] arrayString = new ArrayList<String>().toArray(new String[0]);

        if(usuarioEmailDTO.getContent() != null) {
            arrayString = usuarioEmailDTO.getContent()
                .stream()
                .map(usuarioDTO -> usuarioDTO.getEmail())
                .collect(Collectors.toList())
                .toArray(new String[0]);
        }

        return arrayString;

    }

    public UsuarioDTO pegaUsuarioPorEmail(String email) throws IOException, JSONException {

        HttpClient client = getHttpClient();

        String url = pegaUrlPorAmbiente();

        HttpPost httpPost = new HttpPost(url + CAMINHO_LISTA_USUARIOS + "?page=0&size=999999&sort=nome,ASC");

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode node = mapper.createObjectNode();

        node.put("email", email);

        String json = node.toString();

        StringEntity entity = new StringEntity(json, ContentType.APPLICATION_JSON);

        httpPost.setEntity(entity);

        httpPost.setHeader(CONTENT_TYPE, APPLICATION_JSON);

        HttpResponse response = client.execute(httpPost);

        String responseString = EntityUtils.toString(response.getEntity(), UTF_8);

        Gson gson = new GsonBuilder().create();

        JSONObject myObject = new JSONObject(responseString);

        UsuarioEmailDTO usuarioEmailDTO = gson.fromJson(myObject.toString(), UsuarioEmailDTO.class);

        List<UsuarioDTO> arrayString = new ArrayList<UsuarioDTO>();

        if(usuarioEmailDTO.getContent() != null) {
            arrayString = new ArrayList<>(usuarioEmailDTO.getContent());
        }

        return arrayString.isEmpty() ? new UsuarioDTO() : arrayString.get(0);

    }

    public UsuarioDTO pegaUsuarioPorId(Long id) throws IOException, JSONException {

        HttpClient client = getHttpClient();

        String url = pegaUrlPorAmbiente();

        HttpGet httpPost = new HttpGet(url + CAMINHO_USUARIO_ID + id);

        httpPost.setHeader(CONTENT_TYPE, APPLICATION_JSON);

        HttpResponse response = client.execute(httpPost);

        String responseString = EntityUtils.toString(response.getEntity(), UTF_8);

        Gson gson = new GsonBuilder().create();

        JSONObject myObject = new JSONObject(responseString);

        UsuarioDTO usuarioEmailDTO = gson.fromJson(myObject.toString(), UsuarioDTO.class);

        return usuarioEmailDTO == null ? new UsuarioDTO() : usuarioEmailDTO;

    }

}
