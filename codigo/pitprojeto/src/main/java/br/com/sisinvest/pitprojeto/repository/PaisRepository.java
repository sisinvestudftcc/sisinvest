package br.com.sisinvest.pitprojeto.repository;

import br.com.sisinvest.pitprojeto.domain.Pais;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

@JaversSpringDataAuditable
public interface PaisRepository extends JpaRepository<Pais, Long>, JpaSpecificationExecutor<Pais> {

}
