package br.com.sisinvest.pitprojeto.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "TB_IDIOMA")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Idioma implements Serializable {

    public static final String SIGLA_PADRAO = "pt-BR";

    public static final String IDENTIFICADOR_PORTUGUES = "portuguese";
    public static final String IDENTIFICADOR_ESPANHOL = "spanish";
    public static final String IDENTIFICADOR_INGLES = "english";

    public static final Long PORTUGUES = 3L;

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "NOME")
    private String nome;

    @Column(name = "SIGLA")
    private String sigla;

    @Column(name = "PORTAL_IDENTIFICADOR")
    private String portalIdentificador;

    @Column(name = "PORTAL_CATEGORIA_ID")
    private Integer portalCategoria;

}
