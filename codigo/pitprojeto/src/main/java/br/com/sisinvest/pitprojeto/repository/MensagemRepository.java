package br.com.sisinvest.pitprojeto.repository;

import br.com.sisinvest.pitprojeto.domain.Mensagem;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

@JaversSpringDataAuditable
public interface MensagemRepository extends JpaRepository<Mensagem, Long>, JpaSpecificationExecutor<Mensagem> {


    @Query(value = "SELECT nextval('\"SEQ_MENSAGEM_PROTOCOLO\"')", nativeQuery = true)
    public Long pegaProximoProtocolo();

}
