package br.com.sisinvest.pitprojeto.service;

import br.com.sisinvest.pitprojeto.domain.Idioma_;
import br.com.sisinvest.pitprojeto.domain.ProjetoDetalhe_;
import br.com.sisinvest.pitprojeto.domain.ProjetoEmpresa_;
import br.com.sisinvest.pitprojeto.domain.ProjetoFavorito_;
import br.com.sisinvest.pitprojeto.domain.ProjetoInternacionalizacao_;
import br.com.sisinvest.pitprojeto.domain.ProjetoLocalizacao_;
import br.com.sisinvest.pitprojeto.domain.ProjetoSegmento_;
import br.com.sisinvest.pitprojeto.domain.Projeto_;
import br.com.sisinvest.pitprojeto.repository.AcaoRepository;
import br.com.sisinvest.pitprojeto.repository.ArquivoRepository;
import br.com.sisinvest.pitprojeto.repository.IdiomaRepository;
import br.com.sisinvest.pitprojeto.repository.ModeloContratoRepository;
import br.com.sisinvest.pitprojeto.repository.NaturezaInvestimentoRepository;
import br.com.sisinvest.pitprojeto.repository.PaisRepository;
import br.com.sisinvest.pitprojeto.repository.ProjetoDestaqueRepository;
import br.com.sisinvest.pitprojeto.repository.ProjetoInternacionalizacaoRepository;
import br.com.sisinvest.pitprojeto.repository.ProjetoLinhaTemporalRepository;
import br.com.sisinvest.pitprojeto.repository.ProjetoRepository;
import br.com.sisinvest.pitprojeto.repository.SegmentoRepository;
import br.com.sisinvest.pitprojeto.repository.StatusRepository;
import br.com.sisinvest.pitprojeto.repository.TipoArquivoRepository;
import br.com.sisinvest.pitprojeto.service.dto.ModeracaoDTO;
import br.com.sisinvest.pitprojeto.service.dto.ProjetoDTO;
import br.com.sisinvest.pitprojeto.service.dto.ProjetoInternacionalizacaoArquivoDTO;
import br.com.sisinvest.pitprojeto.service.dto.filtro.ProjetoFiltroDTO;
import br.com.sisinvest.pitprojeto.service.dto.formulario.InternacionalizacaoFormularioDTO;
import br.com.sisinvest.pitprojeto.service.dto.formulario.ProjetoFormularioDTO;
import br.com.sisinvest.pitprojeto.service.mapper.ProjetoMapper;
import br.com.sisinvest.pitprojeto.domain.Acao;
import br.com.sisinvest.pitprojeto.domain.Arquivo;
import br.com.sisinvest.pitprojeto.domain.Idioma;
import br.com.sisinvest.pitprojeto.domain.ModeloContrato;
import br.com.sisinvest.pitprojeto.domain.NaturezaInvestimento;
import br.com.sisinvest.pitprojeto.domain.Pais;
import br.com.sisinvest.pitprojeto.domain.Projeto;
import br.com.sisinvest.pitprojeto.domain.ProjetoDetalhe;
import br.com.sisinvest.pitprojeto.domain.ProjetoEmpresa;
import br.com.sisinvest.pitprojeto.domain.ProjetoEmpresaTelefone;
import br.com.sisinvest.pitprojeto.domain.ProjetoFavorito;
import br.com.sisinvest.pitprojeto.domain.ProjetoInternacionalizacao;
import br.com.sisinvest.pitprojeto.domain.ProjetoInternacionalizacaoArquivo;
import br.com.sisinvest.pitprojeto.domain.ProjetoLinhaTemporal;
import br.com.sisinvest.pitprojeto.domain.ProjetoLinhaTemporalArquivo;
import br.com.sisinvest.pitprojeto.domain.ProjetoLocalizacao;
import br.com.sisinvest.pitprojeto.domain.ProjetoSegmento;
import br.com.sisinvest.pitprojeto.domain.Segmento;
import br.com.sisinvest.pitprojeto.domain.Status;
import br.com.sisinvest.pitprojeto.domain.TipoArquivo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.configurationprocessor.json.JSONException;
import br.gov.nuvem.comum.microsservico.web.rest.errors.ParametrizedMessageException;
import lombok.RequiredArgsConstructor;
import org.apache.http.auth.AuthenticationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@Transactional
@RequiredArgsConstructor
public class ProjetoService {

    private final ProjetoMapper mapper;
    private final ProjetoRepository repository;
    private final ProjetoLinhaTemporalRepository projetoLinhaTemporalRepository;
    private final ProjetoDestaqueRepository projetoDestaqueRepository;
    private final PaisRepository paisRepository;
    private final NaturezaInvestimentoRepository naturezaInvestimentoRepository;
    private final ModeloContratoRepository modeloContratoRepository;
    private final SegmentoRepository segmentoRepository;
    private final IdiomaRepository idiomaRepository;
    private final StatusRepository statusRepository;
    private final AcaoRepository acaoRepository;
    private final ProjetoInternacionalizacaoRepository projetoInternacionalizacaoRepository;
    private final TipoArquivoRepository tipoArquivoRepository;
    private final ArquivoRepository arquivoRepository;
    private final EmailService emailService;
    private final ArquivoService arquivoService;

    public final String ERRO_TITLE = "error.title";
    public final String STATUS_FIELD = "status";
    public final String USUARIO_FIELD = "usuario";

    @Transactional(readOnly = true)
    public Page<ProjetoDTO> buscarTodos(ProjetoFiltroDTO filtro, Pageable pageable) {
        return repository.findAll(specFiltro(filtro), pageable).map(mapper::toDto);
    }

    @Transactional(readOnly = true)
    public ProjetoDTO buscarPorId(Long id) {
        Projeto projeto = findById(id);
        ProjetoDTO projetoDTO = mapper.toDto(projeto);

        projetoDTO.getInternacionalizacao().forEach(internacionalizacaoDTO -> {
            projeto.getProjetoInternacionalizacaoList().forEach(internacionalizacao -> {
                if (internacionalizacao.getId().equals(internacionalizacaoDTO.getId())) {
                    List<ProjetoInternacionalizacaoArquivoDTO> listArquivos = new ArrayList<ProjetoInternacionalizacaoArquivoDTO>();
                    internacionalizacao.getProjetoInternacionalizacaoArquivoList().forEach(arquivo -> {
                        ProjetoInternacionalizacaoArquivoDTO projetoInternacionalizacaoArquivoDTO = new ProjetoInternacionalizacaoArquivoDTO();

                        projetoInternacionalizacaoArquivoDTO.setId(arquivo.getId());
                        projetoInternacionalizacaoArquivoDTO.setTipoArquivo(arquivo.getTipoArquivo().getId());
                        projetoInternacionalizacaoArquivoDTO.setArquivoId(arquivo.getArquivo().getId());
                        projetoInternacionalizacaoArquivoDTO.setArquivo(arquivo.getArquivo().getNome());

                        listArquivos.add(projetoInternacionalizacaoArquivoDTO);
                    });

                    internacionalizacaoDTO.setProjetoInternacionalizacaoArquivos(listArquivos);
                }
            });
        });

        return projetoDTO;
    }

    private Projeto findById(Long id) {
        return repository.findById(id).orElseThrow(this::projetoInexistenteException);
    }

    private ParametrizedMessageException projetoInexistenteException() {
        return new ParametrizedMessageException("projeto.nao.encontrado", ERRO_TITLE);
    }

    public ProjetoDTO salvar(ProjetoFormularioDTO projetoFormularioDTO) throws IOException, JSONException {

        Projeto projeto = new Projeto();

        if (projetoFormularioDTO.getId() != null) {
            projeto = repository.findById(projetoFormularioDTO.getId())
                .orElse(new Projeto());
        }

        Status status = null;

        if (projetoFormularioDTO.getStatus() != null) {

            status = statusRepository.findById(projetoFormularioDTO.getStatus())
                .orElse(new Status());

            if (projetoFormularioDTO.getStatus() != Status.RASCUNHO.longValue() || projeto.getId() == null) {
                projeto.setStatus(status);
            }

            if (status.getId().equals(Status.SOB_ANALISE.longValue())) {
                ProjetoLinhaTemporal projetoLinhaTemporalStatus = new ProjetoLinhaTemporal();

                projetoLinhaTemporalStatus.setProjeto(projeto);
                projetoLinhaTemporalStatus.setStatus(status);
                projetoLinhaTemporalStatus.setDataCriacao(new Timestamp(System.currentTimeMillis()));
                projetoLinhaTemporalStatus.setUsuario(projetoFormularioDTO.getUsuarioId());

                projeto.getProjetoLinhaTemporalList().add(projetoLinhaTemporalStatus);
            }
        } else if (projeto.getId() == null) {
            status = statusRepository.findById(Status.RASCUNHO.longValue())
                .orElse(new Status());
            projeto.setStatus(status);
        }

        if (projeto.getProtocolo() == null) {

            Long protocoloId = repository.pegaProximoProtocolo();

            String protocolo = Projeto.PROTOCOLO_INICIO + Calendar.getInstance().get(Calendar.YEAR) + String.format("%04d", protocoloId);

            projeto.setProtocolo(protocolo);

        }

        if (projeto.getId() == null) {

            projeto.setDataCriacao(new Timestamp(System.currentTimeMillis()));
            projeto.setUsuario(projetoFormularioDTO.getUsuarioId());

        }

        projeto.setDataAtualizacao(new Timestamp(System.currentTimeMillis()));

        criaProjetoLocalizacao(projetoFormularioDTO, projeto);
        criaProjetoEmpresa(projetoFormularioDTO, projeto);
        criaProjetoDetalhe(projetoFormularioDTO, projeto);
        criaProjetoSegmento(projetoFormularioDTO, projeto);
        criaProjetoInternacionalizacao(projetoFormularioDTO, projeto);

        Projeto projetoSave = repository.save(projeto);

        if (projetoFormularioDTO.getId() == null) {
            emailService.cadastrarProjeto(projeto);
        }

        if (status != null && status.getId().equals(Status.SOB_ANALISE.longValue())) {
            emailService.enviarModeracao(projeto);
        }

        return mapper.toDto(projetoSave);
    }

    private void criaProjetoLocalizacao(ProjetoFormularioDTO projetoFormularioDTO, Projeto projeto) {
        ProjetoLocalizacao projetoLocalizacao = new ProjetoLocalizacao();

        if (!projeto.getProjetoLocalizacaoList().isEmpty()) {
            projetoLocalizacao = projeto.getProjetoLocalizacaoList().get(0);
        } else {
            projetoLocalizacao.setProjeto(projeto);
        }

        projetoLocalizacao.setLatitude(projetoFormularioDTO.getLocalizacao().getLatitude());
        projetoLocalizacao.setLongitude(projetoFormularioDTO.getLocalizacao().getLongitude());
        projetoLocalizacao.setCidade(projetoFormularioDTO.getLocalizacao().getCidade());
        projetoLocalizacao.setEstado(projetoFormularioDTO.getLocalizacao().getEstado());

        if (projeto.getProjetoLocalizacaoList().isEmpty()) {
            projeto.getProjetoLocalizacaoList().add(projetoLocalizacao);
        }
    }

    private void criaProjetoEmpresa(ProjetoFormularioDTO projetoFormularioDTO, Projeto projeto) {

        ProjetoEmpresa projetoEmpresa = new ProjetoEmpresa();

        List<String> telefonesCadastrados = new ArrayList<>();
        List<ProjetoEmpresaTelefone> telefonesRemover = new ArrayList<>();

        if (!projeto.getProjetoEmpresaList().isEmpty()) {
            projetoEmpresa = projeto.getProjetoEmpresaList().get(0);

            projetoEmpresa.getProjetoEmpresaTelefoneList().forEach(telefone -> {
                telefonesCadastrados.add(telefone.getTelefone());
            });
        } else {
            projetoEmpresa.setProjeto(projeto);
        }

        projetoEmpresa.setRazaoSocial(projetoFormularioDTO.getEmpresa().getRazaoSocial());
        projetoEmpresa.setCnpj(projetoFormularioDTO.getEmpresa().getCnpj());

        if (projetoFormularioDTO.getEmpresa().getPais() != null) {
            Pais pais = paisRepository.findById(projetoFormularioDTO.getEmpresa().getPais())
                .orElse(new Pais());
            projetoEmpresa.setPais(pais);
        }

        projetoEmpresa.setProprietario(projetoFormularioDTO.getEmpresa().getProprietario());
        projetoEmpresa.setResponsavel(projetoFormularioDTO.getEmpresa().getResponsavel());
        projetoEmpresa.setUrl(projetoFormularioDTO.getEmpresa().getSite());
        projetoEmpresa.setEmail(projetoFormularioDTO.getEmpresa().getEmail());

        ProjetoEmpresa finalProjetoEmpresa = projetoEmpresa;

        if (!projetoFormularioDTO.getEmpresa().getTelefoneList().isEmpty()) {
            projetoFormularioDTO.getEmpresa().getTelefoneList().forEach(telefone -> {

                if (!telefonesCadastrados.contains(telefone)) {
                    ProjetoEmpresaTelefone projetoEmpresaTelefone = new ProjetoEmpresaTelefone();
                    projetoEmpresaTelefone.setProjetoEmpresa(finalProjetoEmpresa);
                    projetoEmpresaTelefone.setTelefone(telefone);

                    finalProjetoEmpresa.getProjetoEmpresaTelefoneList().add(projetoEmpresaTelefone);
                }

            });
        } else {
            finalProjetoEmpresa.getProjetoEmpresaTelefoneList().clear();
        }

        telefonesCadastrados.forEach(telefone -> {
            if (!projetoFormularioDTO.getEmpresa().getTelefoneList().contains(telefone)) {
                finalProjetoEmpresa.getProjetoEmpresaTelefoneList().forEach(telefoneLista -> {
                    if (telefoneLista.getTelefone().equals(telefone)) {
                        telefonesRemover.add(telefoneLista);
                    }
                });
            }
        });

        telefonesRemover.forEach(telefone -> {
            finalProjetoEmpresa.getProjetoEmpresaTelefoneList().remove(telefone);
        });

        if (projeto.getProjetoEmpresaList().isEmpty()) {
            projeto.getProjetoEmpresaList().add(finalProjetoEmpresa);
        }
    }

    private void criaProjetoDetalhe(ProjetoFormularioDTO projetoFormularioDTO, Projeto projeto) {

        ProjetoDetalhe projetoDetalhe = new ProjetoDetalhe();

        if (!projeto.getProjetoDetalheList().isEmpty()) {
            projetoDetalhe = projeto.getProjetoDetalheList().get(0);
        } else {
            projetoDetalhe.setProjeto(projeto);
        }

        projetoDetalhe.setProjeto(projeto);
        projetoDetalhe.setDataInicio(projetoFormularioDTO.getDetalhe().getDataInicio());
        projetoDetalhe.setDataConclusao(projetoFormularioDTO.getDetalhe().getDataConclusao());

        if (projetoFormularioDTO.getDetalhe().getNaturezaInvestimento() != null) {
            NaturezaInvestimento naturezaInvestimento = naturezaInvestimentoRepository.findById(projetoFormularioDTO.getDetalhe().getNaturezaInvestimento())
                .orElse(new NaturezaInvestimento());
            projetoDetalhe.setNaturezaInvestimento(naturezaInvestimento);
        }

        if (projetoFormularioDTO.getDetalhe().getModeloContrato() != null) {
            ModeloContrato modeloContrato = modeloContratoRepository.findById(projetoFormularioDTO.getDetalhe().getModeloContrato())
                .orElse(new ModeloContrato());
            projetoDetalhe.setModeloContrato(modeloContrato);

            if (projetoFormularioDTO.getDetalhe().getModeloContrato().equals(ModeloContrato.OUTROS_ID.longValue())) {
                projetoDetalhe.setModeloContratoOutro(projetoFormularioDTO.getDetalhe().getOutroModeloContrato());
            }
        }

        if (projetoFormularioDTO.getDetalhe().getValorProjeto() != null) {
            projetoDetalhe.setValorProjeto(projetoFormularioDTO.getDetalhe().getValorProjeto());
        }

        if (projetoFormularioDTO.getDetalhe().getEmpregosProjeto() != null) {
            projetoDetalhe.setEmpregosProjeto(projetoFormularioDTO.getDetalhe().getEmpregosProjeto());
        }

        projetoDetalhe.setAreaConstruida(projetoFormularioDTO.getDetalhe().getAreaConstruida());
        projetoDetalhe.setAreaTotal(projetoFormularioDTO.getDetalhe().getAreaTotal());
        projetoDetalhe.setGrupoOperador(projetoFormularioDTO.getDetalhe().getGrupoOperador());

        if (projeto.getProjetoDetalheList().isEmpty()) {
            projeto.getProjetoDetalheList().add(projetoDetalhe);
        }
    }

    private void criaProjetoSegmento(ProjetoFormularioDTO projetoFormularioDTO, Projeto projeto) {

        if (projetoFormularioDTO.getSegmentoList().isEmpty()) {
            projeto.getProjetoSegmentoList().clear();
        }

        List<ProjetoSegmento> segmentoRemover = new ArrayList<>();

        if (!projetoFormularioDTO.getSegmentoList().isEmpty()) {

            if (!projeto.getProjetoSegmentoList().isEmpty()) {
                projeto.getProjetoSegmentoList().forEach(projetoSegmento -> {
                    if (!projetoFormularioDTO.getSegmentoList().contains(projetoSegmento.getSegmento().getId())) {
                        segmentoRemover.add(projetoSegmento);
                    } else {
                        projetoFormularioDTO.getSegmentoList().remove(projetoSegmento.getSegmento().getId());
                    }
                });
            }

            projetoFormularioDTO.getSegmentoList().forEach(segmento -> {
                Segmento segmentoProjeto = segmentoRepository.findById(segmento)
                    .orElse(new Segmento());

                ProjetoSegmento projetoSegmento = new ProjetoSegmento();
                projetoSegmento.setProjeto(projeto);
                projetoSegmento.setSegmento(segmentoProjeto);

                projeto.getProjetoSegmentoList().add(projetoSegmento);
            });

            segmentoRemover.forEach(segmento -> {
                projeto.getProjetoSegmentoList().remove(segmento);
            });
        }
    }

    private void criaProjetoInternacionalizacao(ProjetoFormularioDTO projetoFormularioDTO, Projeto projeto) {
        if (!projetoFormularioDTO.getInternacionalizacaoList().isEmpty()) {
            List<Long> idiomasCadastrar = projetoFormularioDTO.getInternacionalizacaoList().stream().map(InternacionalizacaoFormularioDTO::getIdiomaId).collect(Collectors.toList());
            List<Long> idiomasCadastrados = new ArrayList<>();
            List<ProjetoInternacionalizacao> internacionalizacaoRemover = new ArrayList<>();

            if (!projeto.getProjetoInternacionalizacaoList().isEmpty()) {

                adicionaProjetoNecessitaRemover(projeto, idiomasCadastrar, idiomasCadastrados, internacionalizacaoRemover);

                projetoFormularioDTO.getInternacionalizacaoList().forEach(internacionalizacao -> {

                    atualizaInternacionalizacaoCadastrada(projeto, idiomasCadastrados, internacionalizacao);


                });

                internacionalizacaoRemover.forEach(internacionalizacao -> {
                    projeto.getProjetoInternacionalizacaoList().remove(internacionalizacao);
                });

            } else {
                projetoFormularioDTO.getInternacionalizacaoList().forEach(internacionalizacao -> {
                    cadastrarInternacionalizacaoIdioma(projeto, internacionalizacao);
                });
            }
        }

    }

    private void atualizaInternacionalizacaoCadastrada(Projeto projeto, List<Long> idiomasCadastrados, InternacionalizacaoFormularioDTO internacionalizacao) {
        if (idiomasCadastrados.contains(internacionalizacao.getIdiomaId())) {

            projeto.getProjetoInternacionalizacaoList().forEach(projetoIdioma -> {

                if (projetoIdioma.getIdioma().getId().equals(internacionalizacao.getIdiomaId())) {
                    projetoIdioma.setNome(internacionalizacao.getNome());
                    projetoIdioma.setDescricao(internacionalizacao.getDescricao());
                    removerArquivosNecessarios(internacionalizacao, projetoIdioma);
                }

            });

        } else {
            cadastrarInternacionalizacaoIdioma(projeto, internacionalizacao);
        }
    }

    private void adicionaProjetoNecessitaRemover(Projeto projeto, List<Long> idiomasCadastrar, List<Long> idiomasCadastrados, List<ProjetoInternacionalizacao> internacionalizacaoRemover) {
        projeto.getProjetoInternacionalizacaoList().forEach(projetoIdioma -> {
            if (!idiomasCadastrar.contains(projetoIdioma.getIdioma().getId())) {
                internacionalizacaoRemover.add(projetoIdioma);
            } else {
                idiomasCadastrados.add(projetoIdioma.getIdioma().getId());
            }
        });
    }

    private void removerArquivosNecessarios(InternacionalizacaoFormularioDTO internacionalizacao, ProjetoInternacionalizacao projetoIdioma) {

        List<ProjetoInternacionalizacaoArquivo> arquivosRemover = new ArrayList<>();

        if (internacionalizacao.getRemoverImagem()) {
            projetoIdioma.getProjetoInternacionalizacaoArquivoList().forEach(arquivo -> {
                if (arquivo.getTipoArquivo().getId().equals(TipoArquivo.IMAGEM)) {
                    arquivosRemover.add(arquivo);
                }
            });
        }

        if (!internacionalizacao.getAnexosRemovidos().isEmpty()) {
            projetoIdioma.getProjetoInternacionalizacaoArquivoList().forEach(arquivo -> {
                if (internacionalizacao.getAnexosRemovidos().contains(arquivo.getId())) {
                    arquivosRemover.add(arquivo);
                }
            });
        }

        arquivosRemover.forEach(arquivo -> {
            projetoIdioma.getProjetoInternacionalizacaoArquivoList().remove(arquivo);
        });
    }

    private void cadastrarInternacionalizacaoIdioma(Projeto projeto, InternacionalizacaoFormularioDTO internacionalizacao) {
        ProjetoInternacionalizacao projetoInternacionalizacao = new ProjetoInternacionalizacao();

        Idioma idioma = idiomaRepository.findById(internacionalizacao.getIdiomaId())
            .orElse(new Idioma());

        projetoInternacionalizacao.setProjeto(projeto);
        projetoInternacionalizacao.setNome(internacionalizacao.getNome());
        projetoInternacionalizacao.setDescricao(internacionalizacao.getDescricao());
        projetoInternacionalizacao.setIdioma(idioma);
        projetoInternacionalizacao.setInternacionalizacaoPadrao(internacionalizacao.getInternacionalizacaoPadrao());

        projeto.getProjetoInternacionalizacaoList().add(projetoInternacionalizacao);
    }

    public Long cadastrarAnexos(Long idInternacionalizacao, Long tipoArquivo, MultipartFile[] arquivosUpload) {

        TipoArquivo tipoArquivoObjeto = tipoArquivoRepository.findById(tipoArquivo)
            .orElse(new TipoArquivo());

        ProjetoInternacionalizacao projetoInternacionalizacao = projetoInternacionalizacaoRepository.findById(idInternacionalizacao)
            .orElse(new ProjetoInternacionalizacao());

        Status status = statusRepository.findById(Status.ATIVO.longValue())
            .orElse(new Status());

        Arrays.stream(arquivosUpload).forEach(arquivoMultipart -> {

            try {

                Arquivo arquivo = new Arquivo();
                arquivo.setNome(arquivoMultipart.getOriginalFilename());
                arquivo.setStatus(status);
                arquivo.setDocumento(arquivoMultipart.getBytes());

                arquivo = arquivoRepository.save(arquivo);

                ProjetoInternacionalizacaoArquivo projetoInternacionalizacaoArquivo = new ProjetoInternacionalizacaoArquivo();
                projetoInternacionalizacaoArquivo.setArquivo(arquivo);
                projetoInternacionalizacaoArquivo.setTipoArquivo(tipoArquivoObjeto);
                projetoInternacionalizacaoArquivo.setProjetoInternacionalizacao(projetoInternacionalizacao);

                projetoInternacionalizacao.getProjetoInternacionalizacaoArquivoList().add(projetoInternacionalizacaoArquivo);

            } catch (IOException e) {
                log.debug("Falha ao cadastrar arquivo", e);
            }

        });

        projetoInternacionalizacaoRepository.save(projetoInternacionalizacao);

        return idInternacionalizacao;

    }

    public Long aprovar(Long id, ModeracaoDTO moderacaoDTO) throws IOException, JSONException {

        Projeto projeto = new Projeto();

        if (id != null) {
            projeto = repository.findById(id)
                .orElse(new Projeto());
        }

        if (projeto.getStatus().getId() != Status.SOB_ANALISE.longValue()) {
            throw this.statusIncorretoException();
        }

        Status status = statusRepository.findById(Status.APROVADO.longValue())
            .orElse(new Status());

        Acao acao = acaoRepository.findById(Acao.APROVAR.longValue())
            .orElse(new Acao());

        cadastraAcaoStatus(moderacaoDTO, projeto, status, acao);

        emailService.aprovar(projeto, moderacaoDTO.getJustificativa());

        return id;

    }

    public Long reprovar(Long id, ModeracaoDTO moderacaoDTO) throws IOException, JSONException {

        Projeto projeto = new Projeto();

        if (id != null) {
            projeto = repository.findById(id)
                .orElse(new Projeto());
        }

        if (!projeto.getProjetoDestaqueList().isEmpty()) {
            projeto.getProjetoDestaqueList().clear();
            repository.save(projeto);
        }

        Status status = statusRepository.findById(Status.REPROVADO.longValue())
            .orElse(new Status());

        Acao acao = acaoRepository.findById(Acao.REPROVAR.longValue())
            .orElse(new Acao());

        cadastraAcaoStatus(moderacaoDTO, projeto, status, acao);

        emailService.reprovar(projeto, moderacaoDTO.getJustificativa());

        repository.save(projeto);

        return id;

    }

    public Long restaurar(Long id, ModeracaoDTO moderacaoDTO) throws IOException, JSONException {

        Projeto projeto = new Projeto();

        if (id != null) {
            projeto = repository.findById(id)
                .orElse(new Projeto());
        }

        if (projeto.getStatus().getId() != Status.REPROVADO.longValue()) {
            throw this.statusIncorretoException();
        }

        Status status = statusRepository.findById(Status.RASCUNHO.longValue())
            .orElse(new Status());

        Acao acao = acaoRepository.findById(Acao.RESTAURAR.longValue())
            .orElse(new Acao());

        cadastraAcaoStatus(moderacaoDTO, projeto, status, acao);

        emailService.restaurar(projeto, moderacaoDTO.getJustificativa());

        return id;

    }

    public Long comentar(Long id, ModeracaoDTO moderacaoDTO) throws IOException, JSONException {

        Projeto projeto = new Projeto();

        if (id != null) {
            projeto = repository.findById(id)
                .orElse(new Projeto());
        }

        Status status = new Status();

        Acao acao = acaoRepository.findById(Acao.COMENTAR.longValue())
            .orElse(new Acao());

        cadastraAcaoStatus(moderacaoDTO, projeto, status, acao);

        emailService.comentar(projeto, moderacaoDTO.getJustificativa(), moderacaoDTO.getUsuario(), new Timestamp(System.currentTimeMillis()));

        repository.save(projeto);
        return id;

    }

    public Long solicitarEsclarecimento(Long id, ModeracaoDTO moderacaoDTO) throws IOException, AuthenticationException {

        Projeto projeto = new Projeto();

        if (id != null) {
            projeto = repository.findById(id)
                .orElse(new Projeto());
        }

        if (projeto.getStatus().getId().equals(Status.REPROVADO.longValue())) {
            throw this.statusIncorretoException();
        }

        if (!projeto.getProjetoDestaqueList().isEmpty()) {
            projeto.getProjetoDestaqueList().clear();
            repository.save(projeto);
        }

        Status status = statusRepository.findById(Status.ESCLARECIMENTO.longValue())
            .orElse(new Status());

        Acao acao = acaoRepository.findById(Acao.SOLICITAR_ESCLARECIMENTO.longValue())
            .orElse(new Acao());

        cadastraAcaoStatus(moderacaoDTO, projeto, status, acao);

        if (projeto.getPublicacaoPortal() != null) {
            projeto.setPublicacaoPortal(null);
        }

        emailService.solicitarEsclarecimento(projeto, moderacaoDTO.getJustificativa());

        repository.save(projeto);
        return id;

    }

    public Long responderEsclarecimento(Long id, ModeracaoDTO moderacaoDTO) throws IOException, JSONException {

        Projeto projeto = new Projeto();

        if (id != null) {
            projeto = repository.findById(id)
                .orElse(new Projeto());
        }

        if (projeto.getStatus().getId() != Status.ESCLARECIMENTO.longValue()) {
            throw this.statusIncorretoException();
        }

        Status status = statusRepository.findById(Status.SOB_ANALISE.longValue())
            .orElse(new Status());

        Acao acao = acaoRepository.findById(Acao.RESPONDER_ESCLARECIMENTO.longValue())
            .orElse(new Acao());

        cadastraAcaoStatus(moderacaoDTO, projeto, status, acao);

        emailService.responderEsclarecimento(projeto, moderacaoDTO.getJustificativa());

        repository.save(projeto);
        return id;

    }

    public Long removerComentario(Long linhaTemporalId) throws IOException, JSONException {

        ProjetoLinhaTemporal comentario = projetoLinhaTemporalRepository.findById(linhaTemporalId)
            .orElse(new ProjetoLinhaTemporal());

        emailService.removerComentario(
            comentario.getProjeto(),
            comentario.getComentario(),
            comentario.getUsuario(),
            new Timestamp(System.currentTimeMillis())
        );

        projetoLinhaTemporalRepository.deleteById(linhaTemporalId);

        return linhaTemporalId;

    }

    public Long favoritar(Long id, Long usuarioId) {

        Projeto projeto = new Projeto();

        if (id != null) {
            projeto = repository.findById(id)
                .orElse(new Projeto());
        }

        if (!projeto.getProjetoFavoritoList().isEmpty()) {
            projeto.getProjetoFavoritoList().forEach(projetoFavorito -> {
                if (projetoFavorito.getUsuario().equals(usuarioId)) {
                    throw this.projetoFavoritadoException();
                }
            });
        }

        ProjetoFavorito projetoFavorito = new ProjetoFavorito();
        projetoFavorito.setProjeto(projeto);
        projetoFavorito.setUsuario(usuarioId);

        projeto.getProjetoFavoritoList().add(projetoFavorito);

        repository.save(projeto);

        return id;

    }

    public Long desfavoritar(Long id, Long usuarioId) {

        Projeto projeto = new Projeto();

        if (id != null) {
            projeto = repository.findById(id)
                .orElse(new Projeto());
        }

        final ProjetoFavorito[] projetoFavorito = {null};

        if (!projeto.getProjetoFavoritoList().isEmpty()) {
            projeto.getProjetoFavoritoList().forEach(projetoFavoritoObjeto -> {
                if (projetoFavoritoObjeto.getUsuario().equals(usuarioId)) {
                    projetoFavorito[0] = projetoFavoritoObjeto;
                }
            });
        }

        projeto.getProjetoFavoritoList().remove(projetoFavorito[0]);

        repository.save(projeto);

        return id;

    }

    private ParametrizedMessageException statusIncorretoException() {
        return new ParametrizedMessageException("projeto.status.incorreto", ERRO_TITLE);
    }

    private ParametrizedMessageException projetoFavoritadoException() {
        return new ParametrizedMessageException("projeto.favoritado", ERRO_TITLE);
    }

    private void cadastraAcaoStatus(ModeracaoDTO moderacaoDTO, Projeto projeto, Status status, Acao acao) {

        if (acao.getId() != null && moderacaoDTO.getJustificativa() != null) {
            ProjetoLinhaTemporal projetoLinhaTemporalAcao = new ProjetoLinhaTemporal();

            projetoLinhaTemporalAcao.setProjeto(projeto);
            projetoLinhaTemporalAcao.setAcao(acao);
            projetoLinhaTemporalAcao.setDataCriacao(new Timestamp(System.currentTimeMillis()));
            projetoLinhaTemporalAcao.setComentario(moderacaoDTO.getJustificativa());
            projetoLinhaTemporalAcao.setUsuario(moderacaoDTO.getUsuario());

            if (moderacaoDTO.getArquivos() != null && !moderacaoDTO.getArquivos().isEmpty()) {

                moderacaoDTO.getArquivos().forEach(arquivoMultipart -> {

                        Arquivo arquivo = arquivoService.criaNovoArquivo(arquivoMultipart);

                        ProjetoLinhaTemporalArquivo projetoLinhaTemporalArquivo = new ProjetoLinhaTemporalArquivo();
                        projetoLinhaTemporalArquivo.setProjetoLinhaTemporal(projetoLinhaTemporalAcao);
                        projetoLinhaTemporalArquivo.setArquivo(arquivo);

                        projetoLinhaTemporalAcao.getProjetoLinhaTemporalArquivoList().add(projetoLinhaTemporalArquivo);
                    }
                );

            }

            projeto.setDataAtualizacao(new Timestamp(System.currentTimeMillis()));
            projeto.getProjetoLinhaTemporalList().add(projetoLinhaTemporalAcao);
        }

        if (status.getId() != null) {
            projeto.setStatus(status);

            ProjetoLinhaTemporal projetoLinhaTemporalStatus = new ProjetoLinhaTemporal();

            projetoLinhaTemporalStatus.setProjeto(projeto);
            projetoLinhaTemporalStatus.setStatus(status);
            projetoLinhaTemporalStatus.setDataCriacao(new Timestamp(System.currentTimeMillis()));
            projetoLinhaTemporalStatus.setUsuario(moderacaoDTO.getUsuario());

            projeto.setDataAtualizacao(new Timestamp(System.currentTimeMillis()));
            projeto.getProjetoLinhaTemporalList().add(projetoLinhaTemporalStatus);
        }

    }

    private Specification<Projeto> specFiltro(ProjetoFiltroDTO filtro) {
        return (root, query, cb) -> {

            List<Predicate> predicates = new ArrayList<>();
            Join<Projeto, ProjetoInternacionalizacao> projetoInternacionalizacaoJoin = root.join(Projeto_.projetoInternacionalizacaoList, JoinType.LEFT);
            Join<Projeto, ProjetoEmpresa> projetoEmpresaJoin = root.join(Projeto_.projetoEmpresaList, JoinType.LEFT);
            Join<Projeto, ProjetoDetalhe> projetoDetalheJoin = root.join(Projeto_.projetoDetalheList, JoinType.LEFT);
            Join<Projeto, ProjetoLocalizacao> projetoLocalizacaoJoin = root.join(Projeto_.projetoLocalizacaoList, JoinType.LEFT);
            Join<ProjetoInternacionalizacao, Idioma> idiomaJoin = projetoInternacionalizacaoJoin.join(ProjetoInternacionalizacao_.idioma, JoinType.LEFT);

            specStatus(filtro, root, predicates);

            specUsuarioFavoritado(filtro, root, cb, predicates);

            specNotStatus(filtro, root, cb, predicates);

            specIdioma(filtro, cb, predicates, idiomaJoin);

            specProtocolo(filtro, root, cb, predicates);

            specNome(filtro, cb, predicates, projetoInternacionalizacaoJoin);

            specInternacionalizacaoPadrao(filtro, cb, predicates, projetoInternacionalizacaoJoin);

            specCnpj(filtro, cb, predicates, projetoEmpresaJoin);

            specPais(filtro, cb, predicates, projetoEmpresaJoin);

            specNaturezaInvestimento(filtro, cb, predicates, projetoDetalheJoin);

            specModeloContrato(filtro, cb, predicates, projetoDetalheJoin);

            specValorProjeto(filtro, cb, predicates, projetoDetalheJoin);

            specEstado(filtro, cb, predicates, projetoLocalizacaoJoin);

            specCidade(filtro, cb, predicates, projetoLocalizacaoJoin);

            specRascunho(filtro, root, cb, predicates);

            specDataInicio(filtro, cb, predicates, projetoDetalheJoin);

            specDataConclusao(filtro, cb, predicates, projetoDetalheJoin);

            specSegmento(filtro, root, query, cb, predicates);

            specUsuario(filtro, root, cb, predicates);

            specProjetosProprios(filtro, root, cb, predicates);

            return cb.and(predicates.toArray(new Predicate[0]));
        };
    }

    private void specProjetosProprios(ProjetoFiltroDTO filtro, Root<Projeto> root, CriteriaBuilder cb, List<Predicate> predicates) {
        if (filtro.getApenasProprios() != null) {

            predicates.add(
                cb.equal(root.get(USUARIO_FIELD), filtro.getApenasProprios())
            );

        }
    }

    private void specUsuario(ProjetoFiltroDTO filtro, Root<Projeto> root, CriteriaBuilder cb, List<Predicate> predicates) {
        if (filtro.getUsuarioId() != null || (filtro.getUsuarioStatus() != null && filtro.getStatus() == null)) {

            predicates.add(
                cb.or(
                    cb.and(
                        cb.equal(root.get(STATUS_FIELD), Status.RASCUNHO),
                        cb.equal(root.get(USUARIO_FIELD), filtro.getUsuarioId() != null ? filtro.getUsuarioId() : filtro.getUsuarioStatus())
                    ),
                    cb.notEqual(root.get(STATUS_FIELD), Status.RASCUNHO)
                )
            );

        }
    }

    private void specSegmento(ProjetoFiltroDTO filtro, Root<Projeto> root, CriteriaQuery<?> query, CriteriaBuilder cb, List<Predicate> predicates) {
        if (filtro.getSegmentoList() != null) {

            List<Predicate> predicateList = new ArrayList<>();

            Subquery<ProjetoSegmento> projetoSegmentoSubquery = query.subquery(ProjetoSegmento.class);
            Root<ProjetoSegmento> segmentoRoot = projetoSegmentoSubquery.from(ProjetoSegmento.class);

            filtro.getSegmentoList().forEach(segmento -> {
                predicateList.add(
                    cb.equal(segmentoRoot.get(ProjetoSegmento_.segmento), segmento)
                );
            });

            projetoSegmentoSubquery.select(segmentoRoot.get("projeto"));

            projetoSegmentoSubquery.where(
                cb.or(predicateList.toArray(new Predicate[0]))
            );

            predicates.add(cb.in(root.get("id")).value(projetoSegmentoSubquery));

        }
    }

    private void specDataConclusao(ProjetoFiltroDTO filtro, CriteriaBuilder cb, List<Predicate> predicates, Join<Projeto, ProjetoDetalhe> projetoDetalheJoin) {
        if (filtro.getDataConclusao() != null) {
            predicates.add(cb.lessThanOrEqualTo(projetoDetalheJoin.get(ProjetoDetalhe_.dataConclusao), filtro.getDataConclusao()));
        }
    }

    private void specDataInicio(ProjetoFiltroDTO filtro, CriteriaBuilder cb, List<Predicate> predicates, Join<Projeto, ProjetoDetalhe> projetoDetalheJoin) {
        if (filtro.getDataInicio() != null) {
            predicates.add(cb.greaterThanOrEqualTo(projetoDetalheJoin.get(ProjetoDetalhe_.dataInicio), filtro.getDataInicio()));
        }
    }

    private void specRascunho(ProjetoFiltroDTO filtro, Root<Projeto> root, CriteriaBuilder cb, List<Predicate> predicates) {
        if (filtro.getStatus() != null) {

            if (filtro.getStatus().equals(Status.RASCUNHO.longValue()) && filtro.getUsuarioStatus() != null) {
                predicates.add(cb.and(
                    cb.equal(root.get(STATUS_FIELD), Status.RASCUNHO),
                    cb.equal(root.get(USUARIO_FIELD), filtro.getUsuarioStatus())
                ));
            } else {
                predicates.add(cb.equal(root.get(STATUS_FIELD), filtro.getStatus()));
            }

        }
    }

    private void specCidade(ProjetoFiltroDTO filtro, CriteriaBuilder cb, List<Predicate> predicates, Join<Projeto, ProjetoLocalizacao> projetoLocalizacaoJoin) {
        if (filtro.getCidade() != null) {
            predicates.add(cb.equal(projetoLocalizacaoJoin.get(ProjetoLocalizacao_.cidade), filtro.getCidade()));
        }
    }

    private void specEstado(ProjetoFiltroDTO filtro, CriteriaBuilder cb, List<Predicate> predicates, Join<Projeto, ProjetoLocalizacao> projetoLocalizacaoJoin) {
        if (filtro.getEstado() != null) {
            predicates.add(cb.equal(projetoLocalizacaoJoin.get(ProjetoLocalizacao_.estado), filtro.getEstado()));
        }
    }

    private void specValorProjeto(ProjetoFiltroDTO filtro, CriteriaBuilder cb, List<Predicate> predicates, Join<Projeto, ProjetoDetalhe> projetoDetalheJoin) {
        if (filtro.getValorProjeto() != null) {
            predicates.add(cb.equal(projetoDetalheJoin.get(ProjetoDetalhe_.valorProjeto), filtro.getValorProjeto()));
        }
    }

    private void specModeloContrato(ProjetoFiltroDTO filtro, CriteriaBuilder cb, List<Predicate> predicates, Join<Projeto, ProjetoDetalhe> projetoDetalheJoin) {
        if (filtro.getModeloContrato() != null) {
            predicates.add(cb.equal(projetoDetalheJoin.get(ProjetoDetalhe_.modeloContrato), filtro.getModeloContrato()));
        }
    }

    private void specNaturezaInvestimento(ProjetoFiltroDTO filtro, CriteriaBuilder cb, List<Predicate> predicates, Join<Projeto, ProjetoDetalhe> projetoDetalheJoin) {
        if (filtro.getNaturezaInvestimento() != null) {
            predicates.add(cb.equal(projetoDetalheJoin.get(ProjetoDetalhe_.naturezaInvestimento), filtro.getNaturezaInvestimento()));
        }
    }

    private void specPais(ProjetoFiltroDTO filtro, CriteriaBuilder cb, List<Predicate> predicates, Join<Projeto, ProjetoEmpresa> projetoEmpresaJoin) {
        if (filtro.getPais() != null) {
            predicates.add(cb.equal(projetoEmpresaJoin.get(ProjetoEmpresa_.pais), filtro.getPais()));
        }
    }

    private void specCnpj(ProjetoFiltroDTO filtro, CriteriaBuilder cb, List<Predicate> predicates, Join<Projeto, ProjetoEmpresa> projetoEmpresaJoin) {
        if (filtro.getCnpj() != null) {
            predicates.add(cb.equal(projetoEmpresaJoin.get(ProjetoEmpresa_.cnpj), filtro.getCnpj()));
        }
    }

    private void specNome(ProjetoFiltroDTO filtro, CriteriaBuilder cb, List<Predicate> predicates, Join<Projeto, ProjetoInternacionalizacao> projetoInternacionalizacaoJoin) {
        if (filtro.getNome() != null) {
            predicates.add(cb.like(cb.lower(projetoInternacionalizacaoJoin.get(ProjetoInternacionalizacao_.nome)), "%" + filtro.getNome().toLowerCase() + "%"));
        }
    }

    private void specInternacionalizacaoPadrao(ProjetoFiltroDTO filtro, CriteriaBuilder cb, List<Predicate> predicates, Join<Projeto, ProjetoInternacionalizacao> projetoInternacionalizacaoJoin) {
        if (filtro.getInternacionalizacaoPadrao() != null) {
            predicates.add(cb.equal(projetoInternacionalizacaoJoin.get(ProjetoInternacionalizacao_.internacionalizacaoPadrao), filtro.getInternacionalizacaoPadrao()));
        }
    }

    private void specProtocolo(ProjetoFiltroDTO filtro, Root<Projeto> root, CriteriaBuilder cb, List<Predicate> predicates) {
        if (filtro.getProtocolo() != null) {
            predicates.add(cb.equal(root.get("protocolo"), filtro.getProtocolo()));
        }
    }

    private void specIdioma(ProjetoFiltroDTO filtro, CriteriaBuilder cb, List<Predicate> predicates, Join<ProjetoInternacionalizacao, Idioma> idiomaJoin) {
        if (filtro.getIdioma() != null) {
            predicates.add(cb.equal(idiomaJoin.get(Idioma_.sigla), filtro.getIdioma()));
        }
    }

    private void specNotStatus(ProjetoFiltroDTO filtro, Root<Projeto> root, javax.persistence.criteria.CriteriaBuilder cb, List<Predicate> predicates) {
        if (filtro.getNotStatusList() != null) {
            filtro.getNotStatusList().forEach(status -> {
                predicates.add(cb.notEqual(root.get(STATUS_FIELD), status));
            });
        }
    }

    private void specUsuarioFavoritado(ProjetoFiltroDTO filtro, Root<Projeto> root, javax.persistence.criteria.CriteriaBuilder cb, List<Predicate> predicates) {
        if (filtro.getUsuarioFavoritado() != null) {
            Join<Projeto, ProjetoFavorito> projetoFavoritoJoin = root.join(Projeto_.projetoFavoritoList, JoinType.INNER);
            predicates.add(cb.equal(projetoFavoritoJoin.get(ProjetoFavorito_.USUARIO), filtro.getUsuarioFavoritado()));

            if (filtro.getProjetoId() != null) {
                predicates.add(cb.equal(root.get("id"), filtro.getProjetoId()));
            }
        }
    }

    private void specStatus(ProjetoFiltroDTO filtro, Root<Projeto> root, List<Predicate> predicates) {
        if (filtro.getStatusList() != null) {
            Expression<String> parentExpression = root.get(Projeto_.STATUS);
            predicates.add(parentExpression.in(filtro.getStatusList()));
        }
    }

}
