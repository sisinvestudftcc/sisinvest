package br.com.sisinvest.pitprojeto.service.mapper;

import br.com.sisinvest.pitprojeto.domain.Segmento;
import br.com.sisinvest.pitprojeto.service.dto.SegmentoDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface SegmentoMapper {

    @Mapping(target = "id")
    SegmentoDTO toDto(Segmento entity);

}
