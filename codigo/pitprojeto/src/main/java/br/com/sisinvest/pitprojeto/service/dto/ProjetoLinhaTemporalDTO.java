package br.com.sisinvest.pitprojeto.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

@Getter
@Setter
public class ProjetoLinhaTemporalDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private StatusDTO status;
    private AcaoDTO acao;
    private Timestamp dataCriacao;
    private String comentario;
    private Long usuario;
    private List<ProjetoLinhaTemporalArquivoDTO> linhaTemporalArquivoList;

}
