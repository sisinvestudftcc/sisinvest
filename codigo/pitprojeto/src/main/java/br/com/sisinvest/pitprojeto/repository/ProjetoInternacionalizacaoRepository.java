package br.com.sisinvest.pitprojeto.repository;

import br.com.sisinvest.pitprojeto.domain.ProjetoInternacionalizacao;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

@JaversSpringDataAuditable
public interface ProjetoInternacionalizacaoRepository extends JpaRepository<ProjetoInternacionalizacao, Long>, JpaSpecificationExecutor<ProjetoInternacionalizacao> {

}
