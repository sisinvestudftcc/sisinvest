package br.com.sisinvest.pitprojeto.service.mapper;

import br.com.sisinvest.pitprojeto.domain.EmpregosProjeto;
import br.com.sisinvest.pitprojeto.service.dto.EmpregosProjetoDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface EmpregosProjetoMapper {

    @Mapping(target = "id")
    EmpregosProjetoDTO toDto(EmpregosProjeto entity);

}
