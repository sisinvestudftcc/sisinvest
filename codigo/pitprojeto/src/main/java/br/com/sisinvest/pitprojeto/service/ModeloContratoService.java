package br.com.sisinvest.pitprojeto.service;

import br.com.sisinvest.pitprojeto.repository.ModeloContratoRepository;
import br.com.sisinvest.pitprojeto.service.dto.ModeloContratoDTO;
import br.com.sisinvest.pitprojeto.service.mapper.ModeloContratoMapper;
import br.com.sisinvest.pitprojeto.domain.ModeloContrato;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class ModeloContratoService {

    private final ModeloContratoMapper mapper;
    private final ModeloContratoRepository repository;

    @Transactional(readOnly = true)
    public Page<ModeloContratoDTO> buscarTodos(Pageable pageable) {
        return repository.findAll(specFiltro(), pageable).map(mapper::toDto);
    }

    private Specification<ModeloContrato> specFiltro() {
        return (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();

            return cb.and(predicates.toArray(new Predicate[0]));
        };
    }

}
