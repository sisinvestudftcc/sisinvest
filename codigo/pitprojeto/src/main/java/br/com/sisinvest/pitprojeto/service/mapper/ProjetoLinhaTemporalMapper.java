package br.com.sisinvest.pitprojeto.service.mapper;

import br.com.sisinvest.pitprojeto.domain.ProjetoLinhaTemporal;
import br.com.sisinvest.pitprojeto.service.dto.ProjetoLinhaTemporalDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ProjetoLinhaTemporalMapper {

    @Mapping(target = "id")
    @Mapping(target = "linhaTemporalArquivoList", source = "projetoLinhaTemporalArquivoList")
    ProjetoLinhaTemporalDTO toDto(ProjetoLinhaTemporal entity);

}
