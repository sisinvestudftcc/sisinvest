package br.com.sisinvest.pitprojeto.service.mapper;

import br.com.sisinvest.pitprojeto.domain.GrupoEquipamento;
import br.com.sisinvest.pitprojeto.service.dto.GrupoEquipamentoDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface GrupoEquipamentoMapper {

    @Mapping(target = "id")
    GrupoEquipamentoDTO toDto(GrupoEquipamento entity);

}
