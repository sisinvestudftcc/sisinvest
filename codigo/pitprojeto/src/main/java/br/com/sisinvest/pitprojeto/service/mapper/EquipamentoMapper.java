package br.com.sisinvest.pitprojeto.service.mapper;

import br.com.sisinvest.pitprojeto.domain.Equipamento;
import br.com.sisinvest.pitprojeto.service.dto.EquipamentoDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface EquipamentoMapper {

    @Mapping(target = "id")
    @Mapping(target = "grupoEquipamento", source = "grupoEquipamento")
    EquipamentoDTO toDto(Equipamento entity);

}
