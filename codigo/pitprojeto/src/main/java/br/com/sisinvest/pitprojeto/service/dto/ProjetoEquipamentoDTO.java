package br.com.sisinvest.pitprojeto.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ProjetoEquipamentoDTO extends ProjetoEquipamentoCamposDTO implements Serializable{

    private static final long serialVersionUID = 1L;

    private Long id;
    private GrupoEquipamentoDTO grupoEquipamento;
    private EquipamentoDTO equipamento;

}
