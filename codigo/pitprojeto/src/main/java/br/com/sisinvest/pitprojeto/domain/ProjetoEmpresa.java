package br.com.sisinvest.pitprojeto.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "TB_PROJETO_EMPRESA")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ProjetoEmpresa implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final String SEQ_PROJETO_EMPRESA = "SEQ_PROJETO_EMPRESA";

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQ_PROJETO_EMPRESA)
    @SequenceGenerator(name = SEQ_PROJETO_EMPRESA, sequenceName = SEQ_PROJETO_EMPRESA, allocationSize = 1)
    private Long id;

    @ManyToOne
    @JoinColumn(referencedColumnName = "ID", name = "PROJETO_ID")
    private Projeto projeto;

    @OneToMany(mappedBy = "projetoEmpresa", targetEntity = ProjetoEmpresaTelefone.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ProjetoEmpresaTelefone> projetoEmpresaTelefoneList;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(referencedColumnName = "ID", name = "PAIS_ID")
    private Pais pais;

    @Column(name = "RAZAO_SOCIAL")
    private String razaoSocial;

    @Column(name = "CNPJ")
    private String cnpj;

    @Column(name = "PROPRIETARIO")
    private String proprietario;

    @Column(name = "RESPONSAVEL")
    private String responsavel;

    @Column(name = "URL")
    private String url;

    @Column(name = "EMAIL")
    private String email;

    public ProjetoEmpresa() {
        this.projetoEmpresaTelefoneList = new ArrayList<ProjetoEmpresaTelefone>();
    }
}
