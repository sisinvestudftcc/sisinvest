package br.com.sisinvest.pitprojeto.repository;

import br.com.sisinvest.pitprojeto.domain.ProjetoDestaque;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

@JaversSpringDataAuditable
public interface ProjetoDestaqueRepository extends JpaRepository<ProjetoDestaque, Long>, JpaSpecificationExecutor<ProjetoDestaque> {

    @Query(value = "SELECT max(\"ORDEM\") + 1 from \"TB_PROJETO_DESTAQUE\"", nativeQuery = true)
    public Integer pegaUltimaOrdem();

    @Query(value = "SELECT \"ID\" from \"TB_PROJETO_DESTAQUE\" WHERE \"ORDEM\" > ?1 ORDER BY \"ORDEM\" ASC LIMIT 1", nativeQuery = true)
    public Long pegaProximoProjetoIdOrdem(Integer ordem);

    @Query(value = "SELECT \"ID\" from \"TB_PROJETO_DESTAQUE\" WHERE \"ORDEM\" < ?1 ORDER BY \"ORDEM\" DESC LIMIT 1", nativeQuery = true)
    public Long pegaAnteriorProjetoIdOrdem(Integer ordem);

}
