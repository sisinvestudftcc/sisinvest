package br.com.sisinvest.pitprojeto.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "TB_MENSAGEM")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Mensagem implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final String SEQ_MENSAGEM = "SEQ_MENSAGEM";

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQ_MENSAGEM)
    @SequenceGenerator(name = SEQ_MENSAGEM, sequenceName = SEQ_MENSAGEM, allocationSize = 1)
    private Long id;

    @Column(name = "DESCRICAO")
    private String descricao;

    @Column(name = "PROTOCOLO")
    private String protocolo;

    @ManyToOne
    @JoinColumn(referencedColumnName = "ID", name = "PROJETO_ID")
    private Projeto projeto;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(referencedColumnName = "ID", name = "TIPO_MENSAGEM_ID")
    private TipoMensagem tipoMensagem;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(referencedColumnName = "ID", name = "STATUS_ID")
    private Status status;

    @ManyToOne
    @JoinColumn(referencedColumnName = "ID", name = "MENSAGEM_PAI_ID")
    private Mensagem mensagemPai;

    @Column(name = "DATA_CRIACAO")
    private Timestamp dataCriacao;

    @Column(name = "DATA_ATUALIZACAO")
    private Timestamp dataAtualizacao;

    @OneToMany(mappedBy = "mensagem", targetEntity = MensagemArquivo.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<MensagemArquivo> arquivoList;

    @OneToMany(mappedBy = "mensagemPai", targetEntity = Mensagem.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Mensagem> respostaList;

    @Column(name = "USUARIO_ID")
    private Long usuario;

    public Mensagem() {
        this.arquivoList = new ArrayList<MensagemArquivo>();
    }

    public Timestamp getDataCriacao() {
        return this.dataCriacao != null ? (Timestamp) this.dataCriacao.clone() : null;
    }

    public Mensagem setDataCriacao(Timestamp date) {
        if(date != null) {
            this.dataCriacao = (Timestamp) date.clone();
        }
        return this;
    }

    public Timestamp getDataAtualizacao() {
        return this.dataAtualizacao != null ? (Timestamp) this.dataAtualizacao.clone() : null;
    }

    public Mensagem setDataAtualizacao(Timestamp date) {
        if(date != null) {
            this.dataAtualizacao = (Timestamp) date.clone();
        }
        return this;
    }

}
