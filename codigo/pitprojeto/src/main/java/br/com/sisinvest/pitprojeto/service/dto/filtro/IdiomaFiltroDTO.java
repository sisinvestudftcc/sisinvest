package br.com.sisinvest.pitprojeto.service.dto.filtro;

import br.com.sisinvest.pitprojeto.service.dto.IdiomaDTO;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class IdiomaFiltroDTO extends IdiomaDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

}
