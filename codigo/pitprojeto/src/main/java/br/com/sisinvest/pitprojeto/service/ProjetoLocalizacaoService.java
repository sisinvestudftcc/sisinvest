package br.com.sisinvest.pitprojeto.service;

import br.com.sisinvest.pitprojeto.repository.ProjetoLocalizacaoRepository;
import br.com.sisinvest.pitprojeto.service.dto.CidadeDTO;
import br.com.sisinvest.pitprojeto.service.dto.EstadoDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class ProjetoLocalizacaoService {

    private final ProjetoLocalizacaoRepository repository;

    @Transactional(readOnly = true)
    public List<EstadoDTO> buscarTodosEstados() {
        List<String> stringList = repository.pegaListaEstado();

        List<EstadoDTO> estadoDTOList = new ArrayList<>();

        stringList.forEach(string -> {
            EstadoDTO estadoDTO = new EstadoDTO();
            estadoDTO.setId(string);
            estadoDTO.setNome(string);

            estadoDTOList.add(estadoDTO);
        });

        return estadoDTOList;
    }


    @Transactional(readOnly = true)
    public List<CidadeDTO> buscarTodasCidades(String estado) {
        List<String> stringList = repository.pegaListaCidade(estado);

        List<CidadeDTO> cidadeDTOList = new ArrayList<>();

        stringList.forEach(string -> {
            CidadeDTO estadoDTO = new CidadeDTO();
            estadoDTO.setId(string);
            estadoDTO.setNome(string);

            cidadeDTOList.add(estadoDTO);
        });

        return cidadeDTOList;
    }

}
