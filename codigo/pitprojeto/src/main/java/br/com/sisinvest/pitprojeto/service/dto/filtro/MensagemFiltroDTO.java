package br.com.sisinvest.pitprojeto.service.dto.filtro;

import br.com.sisinvest.pitprojeto.service.dto.ProjetoDTO;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

@Getter
@Setter
public class MensagemFiltroDTO extends ProjetoDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long status;

    private Long projeto;

    private String protocolo;

    private Long tipoMensagem;

    private String nomeProjeto;

    private Long usuario;

    private Long usuarioMensagem;

    private Timestamp dataInicio;

    private Timestamp dataConclusao;

    private List<Long> segmentoList;

    public Timestamp getDataInicio() {
        return this.dataInicio != null ? (Timestamp) this.dataInicio.clone() : null;
    }

    public MensagemFiltroDTO setDataInicio(Timestamp date) {
        if(date != null) {
            this.dataInicio = (Timestamp) date.clone();
        }
        return this;
    }

    public Timestamp getDataConclusao() {
        return this.dataConclusao != null ? (Timestamp) this.dataConclusao.clone() : null;
    }

    public MensagemFiltroDTO setDataConclusao(Timestamp date) {
        if(date != null) {
            this.dataConclusao = (Timestamp) date.clone();
        }
        return this;
    }


}
