package br.com.sisinvest.pitprojeto.service.dto.formulario;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class EmpresaFormularioDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String razaoSocial;

    private String cnpj;

    private Long pais;

    private String nacionalidade;

    private String proprietario;

    private String responsavel;

    private String site;

    private String email;

    private List<String> telefoneList;


}
