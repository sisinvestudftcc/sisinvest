package br.com.sisinvest.pitprojeto.service.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class ModeracaoDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String justificativa;
    private Long usuario;
    private List<MultipartFile> arquivos;
    private List<Long> anexosRemovidos;

}
