package br.com.sisinvest.pitprojeto.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "TB_PROJETO_EQUIPAMENTO")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ProjetoEquipamento implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final String SEQ_PROJETO_EQUIPAMENTO = "SEQ_PROJETO_EQUIPAMENTO";

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQ_PROJETO_EQUIPAMENTO)
    @SequenceGenerator(name = SEQ_PROJETO_EQUIPAMENTO, sequenceName = SEQ_PROJETO_EQUIPAMENTO, allocationSize = 1)
    private Long id;

    @ManyToOne
    @JoinColumn(referencedColumnName = "ID", name = "PROJETO_ID")
    private Projeto projeto;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(referencedColumnName = "ID", name = "GRUPO_EQUIPAMENTO_ID")
    private GrupoEquipamento grupoEquipamento;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(referencedColumnName = "ID", name = "EQUIPAMENTO_ID")
    private Equipamento equipamento;

    @Column(name = "UNIDADES_HABITACIONAIS")
    private String unidadesHabitacionais;

    @Column(name = "UNIDADES_IMOBILIARIAS")
    private String unidadesImobiliarias;

    @Column(name = "CAPACIDADES_MESAS")
    private String capacidadesMesas;

    @Column(name = "TAMANHO_PROJETO")
    private String tamanhoProjeto;

    @Column(name = "NUMERO_PAVILHOES")
    private String numeroPavilhoes;

    @Column(name = "NUMERO_SALAS_REUNIAO")
    private String numeroSalasReuniao;

    @Column(name = "NUMERO_ANUAL_VISITANTES")
    private String numeroAnualVisitantes;

    @Column(name = "RENDA_ANUAL")
    private String rendaAnual;

    @Column(name = "GASTO_MEDIO_VISITANTES")
    private String gastoMedioVisitantes;

    @Column(name = "CAPACIDADE_PUBLICO")
    private String capacidadePublico;

    @Column(name = "CAPACIDADE_ATRACAO")
    private String capacidadeAtracao;

    @Column(name = "OUTRO_EQUIPAMENTO")
    private String outroEquipamento;

}
