package br.com.sisinvest.pitprojeto.service;

import br.com.sisinvest.pitprojeto.repository.ProjetoLinhaTemporalRepository;
import br.com.sisinvest.pitprojeto.service.dto.ProjetoLinhaTemporalDTO;
import br.com.sisinvest.pitprojeto.service.mapper.ProjetoLinhaTemporalMapper;
import br.com.sisinvest.pitprojeto.domain.ProjetoLinhaTemporal;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class ProjetoLinhaTemporalService {

    private final ProjetoLinhaTemporalMapper mapper;
    private final ProjetoLinhaTemporalRepository repository;

    public Page<ProjetoLinhaTemporalDTO> buscarPorProjeto(Long projetoId, Pageable pageable) {
        return repository.findAll(specFiltro(projetoId), pageable).map(mapper::toDto);
    }

    private Specification<ProjetoLinhaTemporal> specFiltro(Long projetoId) {
        return (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();

            if (projetoId != null) {
                predicates.add(cb.equal(root.get("projeto"), projetoId));
            }

            return cb.and(predicates.toArray(new Predicate[0]));
        };
    }

}
