package br.com.sisinvest.pitprojeto.service;

import br.com.sisinvest.pitprojeto.repository.ProjetoRepository;
import br.com.sisinvest.pitprojeto.domain.Projeto;
import com.lowagie.text.DocumentException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class PortfolioService {

    private ProjetoRepository projetoRepository;

    private TemplateEngine templateEngine;

    @Autowired
    public PortfolioService(TemplateEngine templateEngine, ProjetoRepository projetoRepository) {
        this.templateEngine = templateEngine;
        this.projetoRepository = projetoRepository;
    }

    public ByteArrayOutputStream createPdf(Long[] idList) throws IOException, DocumentException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        if (idList.length != 0) {
            List<Projeto> projetoList = new ArrayList<>();

            for (Long id : idList) {
                Projeto projeto = projetoRepository.findById(id).orElse(new Projeto());

                projetoList.add(projeto);
            }

            Context ctx = new Context();
            ctx.setVariable("projetoList", projetoList);
            String processedHtml = templateEngine.process("portfolio", ctx);

            ITextRenderer renderer = new ITextRenderer();
            renderer.setDocumentFromString(processedHtml);
            renderer.layout();
            renderer.createPDF(bos, false);
            renderer.finishPDF();
        }

        return bos;
    }

}
