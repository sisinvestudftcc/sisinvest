package br.com.sisinvest.pitprojeto.service.mapper;

import br.com.sisinvest.pitprojeto.domain.Projeto;
import br.com.sisinvest.pitprojeto.service.dto.ProjetoDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ProjetoMapper {

    @Mapping(target = "usuario", source = "usuario")
    @Mapping(target = "statusId", source = "status.id")
    @Mapping(target = "statusNome", source = "status.nome")
    @Mapping(target = "internacionalizacao", source = "projetoInternacionalizacaoList")
    @Mapping(target = "empresas", source = "projetoEmpresaList")
    @Mapping(target = "localizacoes", source = "projetoLocalizacaoList")
    @Mapping(target = "detalhes", source = "projetoDetalheList")
    @Mapping(target = "segmentos", source = "projetoSegmentoList")
    @Mapping(target = "equipamentos", source = "projetoEquipamentoList")
    ProjetoDTO toDto(Projeto entity);

}
