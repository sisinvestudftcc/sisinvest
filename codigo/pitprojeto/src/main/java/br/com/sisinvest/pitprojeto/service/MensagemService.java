package br.com.sisinvest.pitprojeto.service;

import br.com.sisinvest.pitprojeto.domain.Mensagem_;
import br.com.sisinvest.pitprojeto.domain.ProjetoInternacionalizacao_;
import br.com.sisinvest.pitprojeto.domain.ProjetoSegmento_;
import br.com.sisinvest.pitprojeto.domain.Projeto_;
import br.com.sisinvest.pitprojeto.repository.MensagemRepository;
import br.com.sisinvest.pitprojeto.repository.ProjetoRepository;
import br.com.sisinvest.pitprojeto.repository.StatusRepository;
import br.com.sisinvest.pitprojeto.repository.TipoMensagemRepository;
import br.com.sisinvest.pitprojeto.service.dto.MensagemDTO;
import br.com.sisinvest.pitprojeto.service.dto.ModeracaoDTO;
import br.com.sisinvest.pitprojeto.service.dto.UsuarioDTO;
import br.com.sisinvest.pitprojeto.service.dto.filtro.MensagemFiltroDTO;
import br.com.sisinvest.pitprojeto.service.mapper.MensagemDetalhadaMapper;
import br.com.sisinvest.pitprojeto.service.mapper.MensagemMapper;
import br.com.sisinvest.pitprojeto.domain.Arquivo;
import br.com.sisinvest.pitprojeto.domain.Idioma;
import br.com.sisinvest.pitprojeto.domain.Mensagem;
import br.com.sisinvest.pitprojeto.domain.MensagemArquivo;
import br.com.sisinvest.pitprojeto.domain.Projeto;
import br.com.sisinvest.pitprojeto.domain.ProjetoInternacionalizacao;
import br.com.sisinvest.pitprojeto.domain.ProjetoSegmento;
import br.com.sisinvest.pitprojeto.domain.Status;
import br.com.sisinvest.pitprojeto.domain.TipoMensagem;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Service
@Slf4j
@Transactional
@RequiredArgsConstructor
public class MensagemService {

    private final EmailService emailService;
    private final MensagemMapper mensagemMapper;
    private final MensagemDetalhadaMapper mensagemDetalhadaMapper;
    private final MensagemRepository repository;
    private final ProjetoRepository projetoRepository;
    private final TipoMensagemRepository tipoMensagemRepository;
    private final StatusRepository statusRepository;
    private final ArquivoService arquivoService;

    public Page<MensagemDTO> buscarTodas(MensagemFiltroDTO filtro, Pageable pageable) {
        return repository.findAll(specFiltro(filtro), pageable).map(mensagemMapper::toDto);
    }

    public Page<MensagemDTO> buscarPorProjeto(MensagemFiltroDTO filtro, Pageable pageable) {
        return repository.findAll(specFiltro(filtro), pageable).map(mensagemDetalhadaMapper::toDto);
    }

    public Long comentar(Long projetoId, ModeracaoDTO moderacaoDTO) throws JSONException, IOException {

        Mensagem mensagem = new Mensagem();

        Projeto projeto = projetoRepository.findById(projetoId)
            .orElse(new Projeto());

        TipoMensagem tipoMensagem = tipoMensagemRepository.findById(TipoMensagem.MENSAGEM)
            .orElse(new TipoMensagem());

        Status statusNaoRespondido = statusRepository.findById(Status.NAO_RESPONDIDO.longValue())
            .orElse(new Status());

        criaMensagemPadrao(moderacaoDTO, mensagem, projeto, tipoMensagem, statusNaoRespondido);

        String protocoloFinal = montaProtocoloMensagem(projeto);

        mensagem.setProtocolo(protocoloFinal);

        emailService.enviarMensagem(mensagem, mensagem.getProjeto(), mensagem.getDescricao());

        repository.save(mensagem);

        return mensagem.getId();

    }

    private String montaProtocoloMensagem(Projeto projeto) {
        Long protocolo = repository.pegaProximoProtocolo();

        return projeto.getProtocolo() + "/" + StringUtils.leftPad(protocolo.toString(), 2, "0");
    }

    public Long editar(Long id, ModeracaoDTO moderacaoDTO) {

        Mensagem mensagem = repository.findById(id)
            .orElse(new Mensagem());

        criaMensagemPadrao(moderacaoDTO, mensagem, mensagem.getProjeto(), null, null);

        removeArquivosRemovidos(moderacaoDTO, mensagem);

        if(mensagem.getMensagemPai() != null) {
            mensagem.getMensagemPai().setDataAtualizacao(new Timestamp(System.currentTimeMillis()));
        }

        repository.save(mensagem);

        return mensagem.getId();

    }

    private void removeArquivosRemovidos(ModeracaoDTO moderacaoDTO, Mensagem mensagem) {
        List<MensagemArquivo> mensagemArquivosRemovidos = new ArrayList<>();

        if(moderacaoDTO.getAnexosRemovidos() != null && !moderacaoDTO.getAnexosRemovidos().isEmpty()) {
            moderacaoDTO.getAnexosRemovidos().forEach((Long anexoRemovido) -> {
                mensagem.getArquivoList().forEach((MensagemArquivo mensagemArquivo) -> {
                    if (mensagemArquivo.getArquivo().getId().equals(anexoRemovido)) {
                        mensagemArquivosRemovidos.add(mensagemArquivo);
                    }
                });
            });

            mensagemArquivosRemovidos.forEach(mensagemArquivo -> {
                mensagem.getArquivoList().remove(mensagemArquivo);
            });
        }
    }

    public Long responder(Long mensagemId, ModeracaoDTO moderacaoDTO) throws JSONException, IOException {

        Mensagem mensagem = repository.findById(mensagemId)
            .orElse(new Mensagem());

        Mensagem resposta = new Mensagem();

        TipoMensagem tipoMensagem = tipoMensagemRepository.findById(TipoMensagem.RESPOSTA)
            .orElse(new TipoMensagem());

        Status status = statusRepository.findById(Status.RESPONDIDO.longValue())
            .orElse(new Status());

        criaMensagemPadrao(moderacaoDTO, resposta, mensagem.getProjeto(), tipoMensagem, status);

        resposta.setMensagemPai(mensagem);

        mensagem.setDataAtualizacao(new Timestamp(System.currentTimeMillis()));
        mensagem.setStatus(status);

        repository.save(resposta);
        repository.save(mensagem);

        UsuarioDTO usuarioRespostaDTO = emailService.pegaUsuarioPorId(mensagem.getUsuario());

        emailService.responderMensagem(mensagem, mensagem.getProjeto(), moderacaoDTO.getJustificativa(), usuarioRespostaDTO);

        return resposta.getId();

    }

    public Long excluir(Long id) throws JSONException, IOException {
        Mensagem mensagem = repository.findById(id).orElse(new Mensagem());

        Mensagem mensagemPai = mensagem.getMensagemPai();

        if (mensagemPai != null) {

            Status status = statusRepository.findById(Status.NAO_RESPONDIDO.longValue()).orElse(new Status());

            mensagemPai.setDataAtualizacao(new Timestamp(System.currentTimeMillis()));
            mensagemPai.setStatus(status);

            mensagemPai.getRespostaList().remove(mensagem);

            repository.save(mensagemPai);

        }

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if(auth.getPrincipal() != null) {
            UsuarioDTO usuarioDTO = this.emailService.pegaUsuarioPorEmail(((User) auth.getPrincipal()).getUsername());

            if(mensagem.getTipoMensagem().getId().equals(TipoMensagem.MENSAGEM)) {
                emailService.excluirMensagem(mensagem, mensagem.getProjeto(), usuarioDTO);
            } else {
                emailService.excluirResposta(mensagem.getMensagemPai(), mensagem.getProjeto(), usuarioDTO);
            }
        }

        repository.delete(mensagem);

        return id;
    }

    private void criaMensagemPadrao(ModeracaoDTO moderacaoDTO, Mensagem mensagem, Projeto projeto, TipoMensagem tipoMensagem, Status status) {

        mensagem.setProjeto(projeto);
        mensagem.setDescricao(moderacaoDTO.getJustificativa());

        setaDataCriacao(mensagem);

        setaTipoMensagem(mensagem, tipoMensagem);

        setaStatus(mensagem, status);

        mensagem.setDataAtualizacao(new Timestamp(System.currentTimeMillis()));
        mensagem.setUsuario(moderacaoDTO.getUsuario());

        removeArquivosNecessarios(moderacaoDTO, mensagem);
        cadastraArquivosMensagem(moderacaoDTO, mensagem);

    }

    private void setaStatus(Mensagem mensagem, Status status) {
        if (status != null) {
            mensagem.setStatus(status);
        }
    }

    private void setaTipoMensagem(Mensagem mensagem, TipoMensagem tipoMensagem) {
        if (tipoMensagem != null) {
            mensagem.setTipoMensagem(tipoMensagem);
        }
    }

    private void setaDataCriacao(Mensagem mensagem) {
        if (mensagem.getId() == null) {
            mensagem.setDataCriacao(new Timestamp(System.currentTimeMillis()));
        }
    }

    private void cadastraArquivosMensagem(ModeracaoDTO moderacaoDTO, Mensagem mensagem) {
        if (moderacaoDTO.getArquivos() != null && !moderacaoDTO.getArquivos().isEmpty()) {

            moderacaoDTO.getArquivos().forEach(arquivoMultipart -> {
                Arquivo arquivo = arquivoService.criaNovoArquivo(arquivoMultipart);

                MensagemArquivo mensagemArquivo = new MensagemArquivo();
                mensagemArquivo.setMensagem(mensagem);
                mensagemArquivo.setArquivo(arquivo);

                mensagem.getArquivoList().add(mensagemArquivo);
            });

        }
    }

    private void removeArquivosNecessarios(ModeracaoDTO moderacaoDTO, Mensagem mensagem) {

        List<MensagemArquivo> arquivosRemover = pegaListaAnexosRemovidos(moderacaoDTO, mensagem);

        arquivosRemover.forEach(arquivo -> {
            mensagem.getArquivoList().remove(arquivo);
        });

    }

    private List<MensagemArquivo> pegaListaAnexosRemovidos(ModeracaoDTO moderacaoDTO, Mensagem mensagem) {

        List<MensagemArquivo> arquivosRemover = new ArrayList<>();

        if (moderacaoDTO.getAnexosRemovidos() != null && !moderacaoDTO.getAnexosRemovidos().isEmpty()) {
            mensagem.getArquivoList().forEach(arquivo -> {
                if (moderacaoDTO.getAnexosRemovidos().contains(arquivo.getId())) {
                    arquivosRemover.add(arquivo);
                }
            });
        }

        return arquivosRemover;
    }

    private Specification<Mensagem> specFiltro(MensagemFiltroDTO filtro) {
        return (root, query, cb) -> {

            List<Predicate> predicates = new ArrayList<>();

            specTipoMensagem(filtro, root, predicates);

            specStatus(filtro, root, predicates);

            specNomeProjeto(filtro, root, cb, predicates);

            specUsuario(filtro, root, cb, predicates);

            specUsuarioMensagem(filtro, root, cb, predicates);

            specSegmento(filtro, root, query, cb, predicates);

            specDataInicio(filtro, root, cb, predicates);

            specDataConclusao(filtro, root, cb, predicates);

            specProtocolo(filtro, root, cb, predicates);

            specProjeto(filtro, root, cb, predicates);


            return cb.and(predicates.toArray(new Predicate[0]));

        };
    }

    private void specTipoMensagem(MensagemFiltroDTO filtro, Root<Mensagem> root, List<Predicate> predicates) {
        if (filtro.getTipoMensagem() != null) {
            Expression<String> parentExpression = root.get(Mensagem_.TIPO_MENSAGEM);
            predicates.add(parentExpression.in(filtro.getTipoMensagem()));
        }
    }

    private void specStatus(MensagemFiltroDTO filtro, Root<Mensagem> root, List<Predicate> predicates) {
        if (filtro.getStatus() != null) {
            Expression<String> parentExpression = root.get(Mensagem_.STATUS);
            predicates.add(parentExpression.in(filtro.getStatus()));
        }
    }

    private void specNomeProjeto(MensagemFiltroDTO filtro, Root<Mensagem> root, CriteriaBuilder cb, List<Predicate> predicates) {
        Join<Mensagem, Projeto> projetoJoin = root.join(Mensagem_.projeto, JoinType.INNER);
        Join<Projeto, ProjetoInternacionalizacao> projetoInternacionalizacaoJoin = projetoJoin.join(Projeto_.projetoInternacionalizacaoList, JoinType.INNER);

        predicates.add(cb.equal(projetoInternacionalizacaoJoin.get(ProjetoInternacionalizacao_.idioma), Idioma.PORTUGUES));

        if (filtro.getNomeProjeto() != null) {
            predicates.add(cb.like(cb.lower(projetoInternacionalizacaoJoin.get(ProjetoInternacionalizacao_.nome)), "%" + filtro.getNomeProjeto().toLowerCase() + "%"));
        }
    }

    private void specUsuario(MensagemFiltroDTO filtro, Root<Mensagem> root, CriteriaBuilder cb, List<Predicate> predicates) {
        Join<Mensagem, Projeto> projetoJoin = root.join(Mensagem_.projeto, JoinType.INNER);

        if (filtro.getUsuario() != null) {
            predicates.add(cb.equal(projetoJoin.get(Projeto_.usuario), filtro.getUsuario()));
        }
    }

    private void specUsuarioMensagem(MensagemFiltroDTO filtro, Root<Mensagem> root, CriteriaBuilder cb, List<Predicate> predicates) {
        if (filtro.getUsuarioMensagem() != null) {
            predicates.add(cb.equal(root.get(Mensagem_.usuario), filtro.getUsuarioMensagem()));
        }
    }

    private void specSegmento(MensagemFiltroDTO filtro, Root<Mensagem> root, CriteriaQuery<?> query, CriteriaBuilder cb, List<Predicate> predicates) {
        if (filtro.getSegmentoList() != null) {

            Join<Mensagem, Projeto> projetoJoin = root.join(Mensagem_.projeto, JoinType.INNER);

            List<Predicate> predicateList = new ArrayList<>();

            Subquery<ProjetoSegmento> projetoSegmentoSubquery = query.subquery(ProjetoSegmento.class);
            Root<ProjetoSegmento> segmentoRoot = projetoSegmentoSubquery.from(ProjetoSegmento.class);

            filtro.getSegmentoList().forEach(segmento -> {
                predicateList.add(
                    cb.equal(segmentoRoot.get(ProjetoSegmento_.segmento), segmento)
                );
            });

            projetoSegmentoSubquery.select(segmentoRoot.get("projeto"));

            projetoSegmentoSubquery.where(
                cb.or(predicateList.toArray(new Predicate[0]))
            );

            predicates.add(cb.in(projetoJoin.get("id")).value(projetoSegmentoSubquery));

        }
    }

    private void specProtocolo(MensagemFiltroDTO filtro, Root<Mensagem> root, CriteriaBuilder cb, List<Predicate> predicates) {

        if (filtro.getProtocolo() != null) {

            predicates.add(cb.like(cb.lower(root.get(Mensagem_.PROTOCOLO)), "%" + filtro.getProtocolo().toLowerCase() + "%"));

        }

    }

    private void specProjeto(MensagemFiltroDTO filtro, Root<Mensagem> root, CriteriaBuilder cb, List<Predicate> predicates) {

        if (filtro.getProjeto() != null) {

            predicates.add(cb.equal(root.get(Mensagem_.projeto), filtro.getProjeto()));

        }

    }


    private void specDataInicio(MensagemFiltroDTO filtro, Root<Mensagem> root, CriteriaBuilder cb, List<Predicate> predicates) {
        if (filtro.getDataInicio() != null) {
            predicates.add(cb.greaterThanOrEqualTo(root.get(Mensagem_.DATA_CRIACAO), filtro.getDataInicio()));
        }
    }

    private void specDataConclusao(MensagemFiltroDTO filtro, Root<Mensagem> root, CriteriaBuilder cb, List<Predicate> predicates) {
        if (filtro.getDataConclusao() != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(filtro.getDataConclusao().getTime());
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.SECOND, 59);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.MILLISECOND, 999);

            long tempoFinal = calendar.getTimeInMillis();

            filtro.getDataConclusao().setTime(tempoFinal);

            predicates.add(cb.lessThanOrEqualTo(root.get(Mensagem_.DATA_CRIACAO), filtro.getDataConclusao()));
        }
    }

}
