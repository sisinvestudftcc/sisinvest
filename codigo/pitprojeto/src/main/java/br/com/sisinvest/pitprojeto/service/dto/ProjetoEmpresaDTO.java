package br.com.sisinvest.pitprojeto.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class ProjetoEmpresaDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String razaoSocial;
    private String cnpj;
    private PaisDTO pais;
    private String proprietario;
    private String responsavel;
    private String url;
    private String email;
    private List<ProjetoEmpresaTelefoneDTO> projetoEmpresaTelefoneList;

}
