package br.com.sisinvest.pitprojeto.service;

import br.com.sisinvest.pitprojeto.repository.EmpregosProjetoRepository;
import br.com.sisinvest.pitprojeto.service.dto.EmpregosProjetoDTO;
import br.com.sisinvest.pitprojeto.service.mapper.EmpregosProjetoMapper;
import br.com.sisinvest.pitprojeto.domain.EmpregosProjeto;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class EmpregosProjetoService {

    private final EmpregosProjetoMapper mapper;
    private final EmpregosProjetoRepository repository;

    @Transactional(readOnly = true)
    public Page<EmpregosProjetoDTO> buscarTodos(Pageable pageable) {
        return repository.findAll(specFiltro(), pageable).map(mapper::toDto);
    }

    private Specification<EmpregosProjeto> specFiltro() {
        return (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();

            return cb.and(predicates.toArray(new Predicate[0]));
        };
    }

}
