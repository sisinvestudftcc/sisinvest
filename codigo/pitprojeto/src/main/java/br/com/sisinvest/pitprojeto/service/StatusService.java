package br.com.sisinvest.pitprojeto.service;

import br.com.sisinvest.pitprojeto.repository.StatusRepository;
import br.com.sisinvest.pitprojeto.service.dto.StatusDTO;
import br.com.sisinvest.pitprojeto.service.dto.filtro.StatusFiltroDTO;
import br.com.sisinvest.pitprojeto.service.mapper.StatusMapper;
import br.com.sisinvest.pitprojeto.domain.Status;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class StatusService {

    private final StatusMapper mapper;
    private final StatusRepository repository;

    @Transactional(readOnly = true)
    public Page<StatusDTO> buscarTodos(StatusFiltroDTO filtro, Pageable pageable) {
        return repository.findAll(specFiltro(filtro), pageable).map(mapper::toDto);
    }

    private Specification<Status> specFiltro(StatusFiltroDTO filtro) {
        return (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();

            if (filtro.getNotStatusList() != null) {
                filtro.getNotStatusList().forEach(status -> {
                    predicates.add(cb.notEqual(root.get("id"), status));
                });
            }

            return cb.and(predicates.toArray(new Predicate[0]));
        };
    }

}
