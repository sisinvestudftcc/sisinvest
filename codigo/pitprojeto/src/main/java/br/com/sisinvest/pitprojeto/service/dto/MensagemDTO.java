package br.com.sisinvest.pitprojeto.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

@Getter
@Setter
public class MensagemDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String descricao;
    private String protocolo;

    private Long usuario;

    private StatusDTO status;
    private ProjetoDTO projeto;
    private TipoMensagemDTO tipoMensagem;
    private List<MensagemArquivoDTO> arquivoList;
    private List<MensagemDTO> respostaList;

    private Timestamp dataCriacao;
    private Timestamp dataAtualizacao;

    public Timestamp getDataCriacao() {
        return this.dataCriacao != null ? (Timestamp) this.dataCriacao.clone() : null;
    }

    public MensagemDTO setDataCriacao(Timestamp date) {
        if(date != null) {
            this.dataCriacao = (Timestamp) date.clone();
        }
        return this;
    }

    public Timestamp getDataAtualizacao() {
        return this.dataAtualizacao != null ? (Timestamp) this.dataAtualizacao.clone() : null;
    }

    public MensagemDTO setDataAtualizacao(Timestamp date) {
        if(date != null) {
            this.dataAtualizacao = (Timestamp) date.clone();
        }
        return this;
    }

}
