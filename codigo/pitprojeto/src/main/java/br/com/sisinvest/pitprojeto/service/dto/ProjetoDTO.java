package br.com.sisinvest.pitprojeto.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

@Getter
@Setter
public class ProjetoDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String protocolo;

    private Long usuario;
    private Integer statusId;
    private String statusNome;

    private Timestamp dataCriacao;
    private Timestamp dataAtualizacao;

    private List<ProjetoLocalizacaoDTO> localizacoes;
    private List<ProjetoDetalheDTO> detalhes;
    private List<ProjetoEmpresaDTO> empresas;
    private List<ProjetoInternacionalizacaoDTO> internacionalizacao;
    private List<ProjetoEquipamentoDTO> equipamentos;
    private List<ProjetoSegmentoDTO> segmentos;


}
