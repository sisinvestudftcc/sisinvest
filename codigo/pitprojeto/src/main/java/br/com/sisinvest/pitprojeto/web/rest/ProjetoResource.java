package br.com.sisinvest.pitprojeto.web.rest;

import br.com.sisinvest.pitprojeto.service.ProjetoLinhaTemporalService;
import br.com.sisinvest.pitprojeto.service.ProjetoService;
import br.com.sisinvest.pitprojeto.service.dto.ModeracaoDTO;
import br.com.sisinvest.pitprojeto.service.dto.ProjetoDTO;
import br.com.sisinvest.pitprojeto.service.dto.ProjetoLinhaTemporalDTO;
import br.com.sisinvest.pitprojeto.service.dto.filtro.ProjetoFiltroDTO;
import br.com.sisinvest.pitprojeto.service.dto.formulario.ProjetoFormularioDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.auth.AuthenticationException;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/projeto")
public class ProjetoResource {

    private final ProjetoService service;
    private final ProjetoLinhaTemporalService projetoLinhaTemporalService;

    @GetMapping("/projeto/{id}")
    public ResponseEntity<ProjetoDTO> buscarPorId(@PathVariable Long id) {

        return ResponseEntity.ok(service.buscarPorId(id));

    }

    @PostMapping("/projeto/{id}/linha-temporal")
    public ResponseEntity<Page<ProjetoLinhaTemporalDTO>> buscarLinhaTemporal(@PathVariable Long id, Pageable pageable) {

        return ResponseEntity.ok(projetoLinhaTemporalService.buscarPorProjeto(id, pageable));

    }

    @PostMapping("/filtrar")
    public ResponseEntity<Page<ProjetoDTO>> listarProjetos(@RequestBody ProjetoFiltroDTO filtro, Pageable pageable) {

        return ResponseEntity.ok(service.buscarTodos(filtro, pageable));

    }

    @PostMapping("/salvar")
    public ResponseEntity<ProjetoDTO> salvarProjetos(@RequestBody ProjetoFormularioDTO projetoFormularioDTO) throws IOException, JSONException {

        return ResponseEntity.ok(service.salvar(projetoFormularioDTO));

    }

    @PostMapping(value = "/upload-arquivos/{idInternacionalizacao}/{tipoArquivo}", consumes = "multipart/form-data")
    public ResponseEntity<Long> cadastrarProjetos(@RequestParam("arquivosUpload") MultipartFile[] arquivosUpload, @PathVariable Long idInternacionalizacao, @PathVariable Long tipoArquivo) {

        return ResponseEntity.ok(service.cadastrarAnexos(idInternacionalizacao, tipoArquivo, arquivosUpload));

    }

    @PostMapping(value = "/aprovar/{id}")
    public ResponseEntity<Long> aprovarProjeto(@PathVariable Long id, @RequestBody ModeracaoDTO moderacaoDTO) throws IOException, JSONException {

        return ResponseEntity.ok(service.aprovar(id, moderacaoDTO));

    }

    @PostMapping(value = "/reprovar/{id}")
    public ResponseEntity<Long> reprovarProjeto(@PathVariable Long id, @RequestBody ModeracaoDTO moderacaoDTO) throws IOException, JSONException {

        return ResponseEntity.ok(service.reprovar(id, moderacaoDTO));

    }

    @PostMapping(value = "/restaurar/{id}")
    public ResponseEntity<Long> restaurarProjeto(@PathVariable Long id, @RequestBody ModeracaoDTO moderacaoDTO) throws IOException, JSONException {

        return ResponseEntity.ok(service.restaurar(id, moderacaoDTO));

    }

    @PostMapping(value = "/comentar/{id}", consumes = "multipart/form-data")
    public ResponseEntity<Long> comentar(@RequestParam("arquivosUpload") MultipartFile[] arquivosUpload, @PathVariable Long id, @ModelAttribute ModeracaoDTO moderacaoDTO) throws IOException, JSONException {

        moderacaoDTO.setArquivos(Arrays.asList(arquivosUpload));

        return ResponseEntity.ok(service.comentar(id, moderacaoDTO));

    }

    @PostMapping(value = "/solicitar-esclarecimento/{id}", consumes = "multipart/form-data")
    public ResponseEntity<Long> solicitarEsclarecimento(@RequestParam("arquivosUpload") MultipartFile[] arquivosUpload, @PathVariable Long id, @ModelAttribute ModeracaoDTO moderacaoDTO) throws IOException, AuthenticationException {

        moderacaoDTO.setArquivos(Arrays.asList(arquivosUpload));

        return ResponseEntity.ok(service.solicitarEsclarecimento(id, moderacaoDTO));

    }

    @PostMapping(value = "/responder-esclarecimento/{id}", consumes = "multipart/form-data")
    public ResponseEntity<Long> responderEsclarecimento(@RequestParam("arquivosUpload") MultipartFile[] arquivosUpload, @PathVariable Long id, @ModelAttribute ModeracaoDTO moderacaoDTO) throws IOException, JSONException {

        moderacaoDTO.setArquivos(Arrays.asList(arquivosUpload));

        return ResponseEntity.ok(service.responderEsclarecimento(id, moderacaoDTO));

    }

    @PostMapping(value = "/remover-comentario/{linhaTemporalId}")
    public ResponseEntity<Long> removerComentario(@PathVariable Long linhaTemporalId) throws IOException, JSONException {

        return ResponseEntity.ok(service.removerComentario(linhaTemporalId));

    }

    @PostMapping(value = "/favoritar/{id}/{usuarioId}")
    public ResponseEntity<Long> favoritar(@PathVariable Long id, @PathVariable Long usuarioId) {

        return ResponseEntity.ok(service.favoritar(id, usuarioId));

    }

    @PostMapping(value = "/desfavoritar/{id}/{usuarioId}")
    public ResponseEntity<Long> desfavoritar(@PathVariable Long id, @PathVariable Long usuarioId) {

        return ResponseEntity.ok(service.desfavoritar(id, usuarioId));

    }

}
