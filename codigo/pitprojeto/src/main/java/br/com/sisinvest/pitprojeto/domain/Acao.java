package br.com.sisinvest.pitprojeto.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "TB_ACAO")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Acao implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final Integer SOLICITAR_ESCLARECIMENTO = 1;
    public static final Integer COMENTAR = 2;
    public static final Integer APROVAR = 3;
    public static final Integer REPROVAR = 4;
    public static final Integer RESTAURAR = 5;
    public static final Integer RESPONDER_ESCLARECIMENTO = 6;
    public static final Integer DESPUBLICAR = 7;
    public static final Integer REMOVER_DESTAQUE = 8;

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "NOME")
    private String nome;

}
