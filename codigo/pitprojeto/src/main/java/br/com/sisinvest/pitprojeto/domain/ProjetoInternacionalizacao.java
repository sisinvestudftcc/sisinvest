package br.com.sisinvest.pitprojeto.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "TB_PROJETO_INTERNACIONALIZACAO")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ProjetoInternacionalizacao implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final String SEQ_PROJETO_INTERNACIONALIZACAO = "SEQ_PROJETO_INTERNACIONALIZACAO";

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQ_PROJETO_INTERNACIONALIZACAO)
    @SequenceGenerator(name = SEQ_PROJETO_INTERNACIONALIZACAO, sequenceName = SEQ_PROJETO_INTERNACIONALIZACAO, allocationSize = 1)
    private Long id;

    @ManyToOne
    @JoinColumn(referencedColumnName = "ID", name = "PROJETO_ID")
    private Projeto projeto;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(referencedColumnName = "ID", name = "IDIOMA_ID")
    private Idioma idioma;

    @Column(name = "NOME")
    private String nome;

    @Column(name = "DESCRICAO")
    private String descricao;

    @Column(name = "INTERNACIONALIZACAO_PADRAO")
    private Boolean internacionalizacaoPadrao;

    @OneToMany(mappedBy = "projetoInternacionalizacao", targetEntity = ProjetoInternacionalizacaoArquivo.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ProjetoInternacionalizacaoArquivo> projetoInternacionalizacaoArquivoList;

    public ProjetoInternacionalizacao() {
        this.projetoInternacionalizacaoArquivoList = new ArrayList<ProjetoInternacionalizacaoArquivo>();
    }
}
