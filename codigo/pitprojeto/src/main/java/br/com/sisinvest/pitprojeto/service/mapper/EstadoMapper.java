package br.com.sisinvest.pitprojeto.service.mapper;

import br.com.sisinvest.pitprojeto.service.dto.EstadoDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface EstadoMapper {

    @Mapping(target = "id")
    EstadoDTO toDto(Object entity);

}
