package br.com.sisinvest.pitprojeto.repository;

import br.com.sisinvest.pitprojeto.domain.ModeloContrato;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

@JaversSpringDataAuditable
public interface ModeloContratoRepository extends JpaRepository<ModeloContrato, Long>, JpaSpecificationExecutor<ModeloContrato> {

}
