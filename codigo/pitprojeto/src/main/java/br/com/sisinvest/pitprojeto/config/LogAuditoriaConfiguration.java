package br.com.sisinvest.pitprojeto.config;

import br.com.sisinvest.pitprojeto.utils.SecurityUtil;
import br.gov.nuvem.auditoria.producer.config.LogAuditoriaAutoConfiguration;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.stream.binder.kafka.BinderHeaderMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.support.KafkaHeaderMapper;

@Configuration
@RequiredArgsConstructor
@AutoConfigureBefore(LogAuditoriaAutoConfiguration.class)
@ConditionalOnProperty(value = "auditoria.enabled", matchIfMissing = true)
public class LogAuditoriaConfiguration implements SecurityUtil {

    @Bean("kafkaBinderHeaderMapper")
    public KafkaHeaderMapper kafkaBinderHeaderMapper() {
        BinderHeaderMapper mapper = new BinderHeaderMapper();
        mapper.setEncodeStrings(true);
        return mapper;
    }
}
