package br.com.sisinvest.pitprojeto.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class UsuarioEmailDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<UsuarioDTO> content;

}
