package br.com.sisinvest.pitprojeto.service.dto.formulario;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class ProjetoFormularioDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private Long status;

    private Long usuarioId;

    private LocalizacaoFormularioDTO localizacao;

    private EmpresaFormularioDTO empresa;

    private DetalheFormularioDTO detalhe;

    private List<Long> segmentoList;

    private List<InternacionalizacaoFormularioDTO> internacionalizacaoList;


}
