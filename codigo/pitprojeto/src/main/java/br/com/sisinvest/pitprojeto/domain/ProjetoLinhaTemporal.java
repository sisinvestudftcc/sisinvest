package br.com.sisinvest.pitprojeto.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "TB_PROJETO_LINHA_TEMPORAL")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ProjetoLinhaTemporal implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final String SEQ_PROJETO_LINHA_TEMPORAL = "SEQ_PROJETO_LINHA_TEMPORAL";

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQ_PROJETO_LINHA_TEMPORAL)
    @SequenceGenerator(name = SEQ_PROJETO_LINHA_TEMPORAL, sequenceName = SEQ_PROJETO_LINHA_TEMPORAL, allocationSize = 1)
    private Long id;

    @ManyToOne
    @JoinColumn(referencedColumnName = "ID", name = "PROJETO_ID")
    private Projeto projeto;

    @ManyToOne
    @JoinColumn(referencedColumnName = "ID", name = "STATUS_ID")
    private Status status;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(referencedColumnName = "ID", name = "ACAO_ID")
    private Acao acao;

    @Column(name = "DATA_CRIACAO")
    private Timestamp dataCriacao;

    @Column(name = "COMENTARIO")
    private String comentario;

    @Column(name = "USUARIO_ID")
    private Long usuario;

    @OneToMany(mappedBy = "projetoLinhaTemporal", targetEntity = ProjetoLinhaTemporalArquivo.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ProjetoLinhaTemporalArquivo> projetoLinhaTemporalArquivoList;

    public ProjetoLinhaTemporal() {
        this.projetoLinhaTemporalArquivoList = new ArrayList<ProjetoLinhaTemporalArquivo>();
    }
}
