package br.com.sisinvest.pitprojeto.service.dto.filtro;

import br.com.sisinvest.pitprojeto.service.dto.CidadeDTO;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class CidadeFiltroDTO extends CidadeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String estado;

}
