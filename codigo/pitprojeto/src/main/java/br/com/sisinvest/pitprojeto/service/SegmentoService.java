package br.com.sisinvest.pitprojeto.service;

import br.com.sisinvest.pitprojeto.repository.SegmentoRepository;
import br.com.sisinvest.pitprojeto.service.dto.SegmentoDTO;
import br.com.sisinvest.pitprojeto.service.mapper.SegmentoMapper;
import br.com.sisinvest.pitprojeto.domain.Segmento;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class SegmentoService {

    private final SegmentoMapper mapper;
    private final SegmentoRepository repository;

    @Transactional(readOnly = true)
    public Page<SegmentoDTO> buscarTodos(Pageable pageable) {
        return repository.findAll(specFiltro(), pageable).map(mapper::toDto);
    }

    private Specification<Segmento> specFiltro() {
        return (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();

            return cb.and(predicates.toArray(new Predicate[0]));
        };
    }

}
