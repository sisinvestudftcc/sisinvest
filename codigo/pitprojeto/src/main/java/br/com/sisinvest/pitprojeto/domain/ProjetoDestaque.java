package br.com.sisinvest.pitprojeto.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.GeneratedValue;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "TB_PROJETO_DESTAQUE")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ProjetoDestaque implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final String SEQ_PROJETO_DESTAQUE = "SEQ_PROJETO_DESTAQUE";

    public static final Integer LIMITE_MAXIMO_PROJETOS = 5;

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQ_PROJETO_DESTAQUE)
    @SequenceGenerator(name = SEQ_PROJETO_DESTAQUE, sequenceName = SEQ_PROJETO_DESTAQUE, allocationSize = 1)
    private Long id;

    @ManyToOne
    @JoinColumn(referencedColumnName = "ID", name = "PROJETO_ID")
    private Projeto projeto;

    @Column(name = "ORDEM")
    private Integer ordem;


}
