package br.com.sisinvest.pitprojeto.repository;

import br.com.sisinvest.pitprojeto.domain.ProjetoLocalizacao;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

@JaversSpringDataAuditable
public interface ProjetoLocalizacaoRepository extends JpaRepository<ProjetoLocalizacao, Long>, JpaSpecificationExecutor<ProjetoLocalizacao> {

    @Query(value = "SELECT DISTINCT pl.estado AS nome FROM ProjetoLocalizacao pl ORDER BY pl.estado ASC", nativeQuery = false)
    public List<String> pegaListaEstado();

    @Query(value = "SELECT DISTINCT pl.cidade AS nome FROM ProjetoLocalizacao pl WHERE pl.estado = ?1 ORDER BY pl.cidade ASC", nativeQuery = false)
    public List<String> pegaListaCidade(String estado);

}
