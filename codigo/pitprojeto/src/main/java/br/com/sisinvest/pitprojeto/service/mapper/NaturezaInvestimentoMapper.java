package br.com.sisinvest.pitprojeto.service.mapper;

import br.com.sisinvest.pitprojeto.domain.NaturezaInvestimento;
import br.com.sisinvest.pitprojeto.service.dto.NaturezaInvestimentoDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface NaturezaInvestimentoMapper {

    @Mapping(target = "id")
    NaturezaInvestimentoDTO toDto(NaturezaInvestimento entity);

}
