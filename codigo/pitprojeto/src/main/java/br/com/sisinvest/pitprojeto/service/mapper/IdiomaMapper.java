package br.com.sisinvest.pitprojeto.service.mapper;

import br.com.sisinvest.pitprojeto.domain.Idioma;
import br.com.sisinvest.pitprojeto.service.dto.IdiomaDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface IdiomaMapper {

    @Mapping(target = "id")
    IdiomaDTO toDto(Idioma entity);

}
