package br.com.sisinvest.pitprojeto.service.mapper;

import br.com.sisinvest.pitprojeto.domain.ValorProjeto;
import br.com.sisinvest.pitprojeto.service.dto.ValorProjetoDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ValorProjetoMapper {

    @Mapping(target = "id")
    ValorProjetoDTO toDto(ValorProjeto entity);

}
