package br.com.sisinvest.pitprojeto.web.rest;

import br.com.sisinvest.pitprojeto.domain.Arquivo;
import br.com.sisinvest.pitprojeto.service.ArquivoService;
import br.com.sisinvest.pitprojeto.service.EmpregosProjetoService;
import br.com.sisinvest.pitprojeto.service.EquipamentoService;
import br.com.sisinvest.pitprojeto.service.GrupoEquipamentoService;
import br.com.sisinvest.pitprojeto.service.IdiomaService;
import br.com.sisinvest.pitprojeto.service.ModeloContratoService;
import br.com.sisinvest.pitprojeto.service.NaturezaInvestimentoService;
import br.com.sisinvest.pitprojeto.service.PaisService;
import br.com.sisinvest.pitprojeto.service.PortfolioService;
import br.com.sisinvest.pitprojeto.service.ProjetoLocalizacaoService;
import br.com.sisinvest.pitprojeto.service.ProjetoService;
import br.com.sisinvest.pitprojeto.service.SegmentoService;
import br.com.sisinvest.pitprojeto.service.StatusService;
import br.com.sisinvest.pitprojeto.service.ValorProjetoService;
import br.com.sisinvest.pitprojeto.service.dto.CidadeDTO;
import br.com.sisinvest.pitprojeto.service.dto.EmpregosProjetoDTO;
import br.com.sisinvest.pitprojeto.service.dto.EquipamentoDTO;
import br.com.sisinvest.pitprojeto.service.dto.EstadoDTO;
import br.com.sisinvest.pitprojeto.service.dto.GrupoEquipamentoDTO;
import br.com.sisinvest.pitprojeto.service.dto.IdiomaDTO;
import br.com.sisinvest.pitprojeto.service.dto.ModeloContratoDTO;
import br.com.sisinvest.pitprojeto.service.dto.NaturezaInvestimentoDTO;
import br.com.sisinvest.pitprojeto.service.dto.PaisDTO;
import br.com.sisinvest.pitprojeto.service.dto.SegmentoDTO;
import br.com.sisinvest.pitprojeto.service.dto.StatusDTO;
import br.com.sisinvest.pitprojeto.service.dto.ValorProjetoDTO;
import br.com.sisinvest.pitprojeto.service.dto.filtro.CidadeFiltroDTO;
import br.com.sisinvest.pitprojeto.service.dto.filtro.EquipamentoFiltroDTO;
import br.com.sisinvest.pitprojeto.service.dto.filtro.StatusFiltroDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/public/dominio")
public class DominioResource {

    private final IdiomaService idiomaService;
    private final SegmentoService segmentoService;
    private final PaisService paisService;
    private final NaturezaInvestimentoService naturezaInvestimentoService;
    private final ModeloContratoService modeloContratoService;
    private final ValorProjetoService valorProjetoService;
    private final EmpregosProjetoService empregosProjetoService;
    private final GrupoEquipamentoService grupoEquipamentoService;
    private final EquipamentoService equipamentoService;
    private final StatusService statusService;
    private final ProjetoLocalizacaoService projetoLocalizacaoService;
    private final ArquivoService arquivoService;
    private final ProjetoService projetoService;
    private final PortfolioService portfolioService;

    @PostMapping("/idioma")
    public ResponseEntity<Page<IdiomaDTO>> listarIdiomas(Pageable pageable) {

        return ResponseEntity.ok(idiomaService.buscarTodos(pageable));

    }

    @PostMapping("/segmento")
    public ResponseEntity<Page<SegmentoDTO>> listarSegmentos(Pageable pageable) {

        return ResponseEntity.ok(segmentoService.buscarTodos(pageable));

    }

    @PostMapping("/pais")
    public ResponseEntity<Page<PaisDTO>> listarPaises(Pageable pageable) {

        return ResponseEntity.ok(paisService.buscarTodos(pageable));

    }

    @PostMapping("/natureza-investimento")
    public ResponseEntity<Page<NaturezaInvestimentoDTO>> listarNaturezasInvestimento(Pageable pageable) {

        return ResponseEntity.ok(naturezaInvestimentoService.buscarTodos(pageable));

    }

    @PostMapping("/modelo-contrato")
    public ResponseEntity<Page<ModeloContratoDTO>> listarModelosContrato(Pageable pageable) {

        return ResponseEntity.ok(modeloContratoService.buscarTodos(pageable));

    }

    @PostMapping("/valor-projeto")
    public ResponseEntity<Page<ValorProjetoDTO>> listarValoresProjeto(Pageable pageable) {

        return ResponseEntity.ok(valorProjetoService.buscarTodos(pageable));

    }

    @PostMapping("/emprego-projeto")
    public ResponseEntity<Page<EmpregosProjetoDTO>> listarEmpregosProjeto(Pageable pageable) {

        return ResponseEntity.ok(empregosProjetoService.buscarTodos(pageable));

    }

    @PostMapping("/grupo-equipamento")
    public ResponseEntity<Page<GrupoEquipamentoDTO>> listarGrupoEquipamento(Pageable pageable) {

        return ResponseEntity.ok(grupoEquipamentoService.buscarTodos(pageable));

    }

    @PostMapping("/equipamento")
    public ResponseEntity<Page<EquipamentoDTO>> listarEquipamento(@RequestBody EquipamentoFiltroDTO filtro, Pageable pageable) {

        return ResponseEntity.ok(equipamentoService.buscarTodos(filtro, pageable));

    }

    @PostMapping("/status")
    public ResponseEntity<Page<StatusDTO>> listarStatus(@RequestBody StatusFiltroDTO filtro, Pageable pageable) {

        return ResponseEntity.ok(statusService.buscarTodos(filtro, pageable));

    }

    @PostMapping("/estado")
    public ResponseEntity<Page<EstadoDTO>> listarEstado() {

        final Page<EstadoDTO> page = new PageImpl<>(projetoLocalizacaoService.buscarTodosEstados());

        return ResponseEntity.ok(page);

    }

    @PostMapping("/cidade")
    public ResponseEntity<Page<CidadeDTO>> listarCidade(@RequestBody CidadeFiltroDTO filtro) {

        final Page<CidadeDTO> page = new PageImpl<>(projetoLocalizacaoService.buscarTodasCidades(filtro.getEstado()));

        return ResponseEntity.ok(page);

    }

    @GetMapping("/visualizar-arquivo/{arquivoId}")
    public void visualizarArquivo(HttpServletResponse response, @PathVariable Long arquivoId) throws IOException {

        Arquivo arquivo = arquivoService.renderizarPorId(arquivoId);

        String contentType = "application/octet-stream";
        response.setContentType(contentType);
        OutputStream out = response.getOutputStream();
        ByteArrayInputStream in = new ByteArrayInputStream(arquivo.getDocumento());
        IOUtils.copy(in, out);
        out.close();
        in.close();

    }

    @GetMapping("/baixar-arquivo/{arquivoId}")
    public ResponseEntity<ByteArrayResource> baixarArquivo(@PathVariable Long arquivoId) {

        Arquivo arquivo = arquivoService.renderizarPorId(arquivoId);

        ByteArrayResource resource = new ByteArrayResource(arquivo.getDocumento());

        HttpHeaders header = new HttpHeaders();
        header.add(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=" + arquivo.getNome());
        header.add("Cache-Control", "no-cache, no-store, must-revalidate");
        header.add("Pragma", "no-cache");
        header.add("Expires", "0");

        return ResponseEntity.ok()
            .headers(header)
            .contentLength(resource.contentLength())
            .contentType(MediaType.APPLICATION_OCTET_STREAM)
            .body(resource);

    }

}
