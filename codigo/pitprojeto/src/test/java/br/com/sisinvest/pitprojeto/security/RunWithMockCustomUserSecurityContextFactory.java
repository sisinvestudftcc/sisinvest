package br.com.sisinvest.pitprojeto.security;

import br.com.sisinvest.pitprojeto.service.enumeration.UsuarioPerfilEnum;
import br.gov.nuvem.security.jwt.service.dto.UserDetailsDTO;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithSecurityContextFactory;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class RunWithMockCustomUserSecurityContextFactory implements WithSecurityContextFactory<RunWithMockCustomUser> {

    @Override
    public SecurityContext createSecurityContext(RunWithMockCustomUser customUser) {
        SecurityContext context = SecurityContextHolder.createEmptyContext();
        Map<String, Object> attributes = new HashMap<>();
        attributes.put("perfil", customUser.perfil());
        attributes.put("idUsuario", customUser.idUsuario());
        UserDetailsDTO userDetailsDTO = new UserDetailsDTO(customUser.username(), "******", getAuthorities(UsuarioPerfilEnum.GESTOR), attributes);
        Authentication auth = new UsernamePasswordAuthenticationToken(userDetailsDTO, "******", getAuthorities(UsuarioPerfilEnum.GESTOR));
        context.setAuthentication(auth);
        return context;
    }

    private Collection<? extends GrantedAuthority> getAuthorities(UsuarioPerfilEnum perfil) {
        return Collections.singletonList(new SimpleGrantedAuthority(rolePrefix(perfil.toString())));
    }

    private String rolePrefix(String role) {
        return String.format("ROLE_%s", role);
    }
}
