package br.com.sisinvest.pitprojeto.util;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.utility.DockerImageName;

import javax.persistence.EntityManager;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(initializers = {IntTestComum.Initializer.class})
public abstract class IntTestComum {

    private static final GenericContainer REDIS_CONTAINER;
    private static final KafkaContainer KAFKA_CONTAINER;

    static {
        REDIS_CONTAINER = new GenericContainer("redis:3.0.6").withExposedPorts(6379);
        REDIS_CONTAINER.start();
        KAFKA_CONTAINER = new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:5.4.3"));
        KAFKA_CONTAINER.start();
    }

    @Autowired
    private EntityManager em;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    protected MockMvc getMockMvc() {
        return mockMvc;
    }

    protected EntityManager getEm() {
        return em;
    }

    protected String buildUrl(String... params) {
        return String.join("/", params);
    }

    @BeforeEach
    void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
            .apply(SecurityMockMvcConfigurers.springSecurity()).build();
    }

    public static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        @Override
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertyValues.of(
                String.format("spring.redis.port=%s", REDIS_CONTAINER.getMappedPort(6379)),
                String.format("spring.redis.host=%s", REDIS_CONTAINER.getContainerIpAddress()),
                String.format("spring.kafka.bootstrap-servers=%s", KAFKA_CONTAINER.getBootstrapServers()))
                .applyTo(configurableApplicationContext.getEnvironment());
        }
    }
}

