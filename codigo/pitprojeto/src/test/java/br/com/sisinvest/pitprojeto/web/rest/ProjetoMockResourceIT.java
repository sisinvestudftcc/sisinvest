package br.com.sisinvest.pitprojeto.web.rest;

import br.com.sisinvest.pitprojeto.domain.Arquivo;
import br.com.sisinvest.pitprojeto.domain.Idioma;
import br.com.sisinvest.pitprojeto.domain.ModeloContrato;
import br.com.sisinvest.pitprojeto.domain.Projeto;
import br.com.sisinvest.pitprojeto.domain.ProjetoDestaque;
import br.com.sisinvest.pitprojeto.domain.ProjetoDetalhe;
import br.com.sisinvest.pitprojeto.domain.ProjetoEmpresa;
import br.com.sisinvest.pitprojeto.domain.ProjetoEmpresaTelefone;
import br.com.sisinvest.pitprojeto.domain.ProjetoFavorito;
import br.com.sisinvest.pitprojeto.domain.ProjetoInternacionalizacao;
import br.com.sisinvest.pitprojeto.domain.ProjetoInternacionalizacaoArquivo;
import br.com.sisinvest.pitprojeto.domain.ProjetoLinhaTemporal;
import br.com.sisinvest.pitprojeto.domain.ProjetoLocalizacao;
import br.com.sisinvest.pitprojeto.domain.ProjetoSegmento;
import br.com.sisinvest.pitprojeto.domain.Segmento;
import br.com.sisinvest.pitprojeto.domain.Status;
import br.com.sisinvest.pitprojeto.domain.TipoArquivo;
import br.com.sisinvest.pitprojeto.repository.ProjetoDestaqueRepository;
import br.com.sisinvest.pitprojeto.repository.ProjetoLinhaTemporalRepository;
import br.com.sisinvest.pitprojeto.repository.ProjetoRepository;
import br.com.sisinvest.pitprojeto.security.RunWithMockCustomUser;
import br.com.sisinvest.pitprojeto.service.EmailService;
import br.com.sisinvest.pitprojeto.service.dto.ModeracaoDTO;
import br.com.sisinvest.pitprojeto.service.dto.formulario.DetalheFormularioDTO;
import br.com.sisinvest.pitprojeto.service.dto.formulario.EmpresaFormularioDTO;
import br.com.sisinvest.pitprojeto.service.dto.formulario.InternacionalizacaoFormularioDTO;
import br.com.sisinvest.pitprojeto.service.dto.formulario.LocalizacaoFormularioDTO;
import br.com.sisinvest.pitprojeto.service.dto.formulario.ProjetoFormularioDTO;
import br.com.sisinvest.pitprojeto.util.IntTestComum;
import br.com.sisinvest.pitprojeto.PitprojetoApp;
import org.apache.commons.lang3.RandomUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWithMockCustomUser
@Transactional
@SpringBootTest(classes = PitprojetoApp.class)
class ProjetoMockResourceIT extends IntTestComum {

    private static final String BASE_URL = "/api/projeto";

    @MockBean
    private ProjetoRepository projetoRepository;

    @MockBean
    private ProjetoLinhaTemporalRepository projetoLinhaTemporalRepository;

    @MockBean
    private EmailService emailService;

    @MockBean
    private ProjetoDestaqueRepository projetoDestaqueRepository;

    @BeforeEach
    public void setup() throws IOException {

        Projeto projeto = new Projeto();

        Status status = new Status();
        status.setId(Status.SOB_ANALISE.longValue());

        projeto.setStatus(status);

        ProjetoEmpresa projetoEmpresa = new ProjetoEmpresa();
        projetoEmpresa.setEmail("teste@teste.com.br");

        ProjetoFavorito projetoFavorito = new ProjetoFavorito();
        projetoFavorito.setUsuario(1L);

        projeto.getProjetoEmpresaList().add(projetoEmpresa);
        projeto.getProjetoFavoritoList().add(projetoFavorito);

        ProjetoDestaque projetoDestaque = new ProjetoDestaque();

        List<ProjetoDestaque> projetoDestaqueList = new ArrayList<>();
        projetoDestaqueList.add(projetoDestaque);

        Mockito.when(projetoRepository.findById(Mockito.anyLong())).thenReturn(java.util.Optional.of(projeto));
        Mockito.when(projetoDestaqueRepository.findAll(Mockito.isA(Sort.class))).thenReturn(projetoDestaqueList);

    }

    @Test
    void cadastrar() throws Exception {
        ProjetoFormularioDTO projetoFormularioDTO = new ProjetoFormularioDTO();
        projetoFormularioDTO.setStatus(Status.SOB_ANALISE.longValue());
        projetoFormularioDTO.setUsuarioId(1L);


        LocalizacaoFormularioDTO localizacaoFormularioDTO = new LocalizacaoFormularioDTO();
        localizacaoFormularioDTO.setLatitude("123");
        localizacaoFormularioDTO.setLongitude("123");
        localizacaoFormularioDTO.setCidade("123");
        localizacaoFormularioDTO.setEstado("123");

        EmpresaFormularioDTO empresaFormularioDTO = new EmpresaFormularioDTO();
        empresaFormularioDTO.setCnpj("76838107000175");
        empresaFormularioDTO.setRazaoSocial("Teste");
        empresaFormularioDTO.setPais(1L);
        empresaFormularioDTO.setProprietario("Teste");
        empresaFormularioDTO.setResponsavel("Teste");
        empresaFormularioDTO.setSite("teste.com");
        empresaFormularioDTO.setEmail("teste@teste.com");
        empresaFormularioDTO.setTelefoneList(Collections.singletonList("1592391923"));

        DetalheFormularioDTO detalheFormularioDTO = new DetalheFormularioDTO();
        detalheFormularioDTO.setDataInicio(new Timestamp(System.currentTimeMillis()));
        detalheFormularioDTO.setDataConclusao(new Timestamp(System.currentTimeMillis()));
        detalheFormularioDTO.setNaturezaInvestimento(1L);
        detalheFormularioDTO.setModeloContrato(ModeloContrato.OUTROS_ID.longValue());
        detalheFormularioDTO.setOutroModeloContrato("Teste");
        detalheFormularioDTO.setValorProjeto("100.000");
        detalheFormularioDTO.setEmpregosProjeto("100");
        detalheFormularioDTO.setAreaConstruida("123");
        detalheFormularioDTO.setAreaTotal("123");
        detalheFormularioDTO.setGrupoOperador("123");

        InternacionalizacaoFormularioDTO internacionalizacaoFormularioDTO = new InternacionalizacaoFormularioDTO();
        internacionalizacaoFormularioDTO.setNome("123");
        internacionalizacaoFormularioDTO.setDescricao("123");
        internacionalizacaoFormularioDTO.setIdiomaId(1L);
        internacionalizacaoFormularioDTO.setRemoverImagem(false);
        internacionalizacaoFormularioDTO.setAnexosRemovidos(new ArrayList<>());

        projetoFormularioDTO.setLocalizacao(localizacaoFormularioDTO);
        projetoFormularioDTO.setEmpresa(empresaFormularioDTO);
        projetoFormularioDTO.setDetalhe(detalheFormularioDTO);
        projetoFormularioDTO.setSegmentoList(Collections.singletonList(1L));
        projetoFormularioDTO.setInternacionalizacaoList(Collections.singletonList(internacionalizacaoFormularioDTO));

        Mockito.when(projetoRepository.pegaProximoProtocolo()).thenReturn(1L);

        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL, "salvar"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(projetoFormularioDTO)))
            .andExpect(status().isOk());
    }

    @Test
    void atualizar() throws Exception {
        ProjetoFormularioDTO projetoFormularioDTO = new ProjetoFormularioDTO();
        projetoFormularioDTO.setStatus(Status.SOB_ANALISE.longValue());
        projetoFormularioDTO.setUsuarioId(1L);
        projetoFormularioDTO.setId(1L);

        LocalizacaoFormularioDTO localizacaoFormularioDTO = new LocalizacaoFormularioDTO();
        localizacaoFormularioDTO.setLatitude("123");
        localizacaoFormularioDTO.setLongitude("123");
        localizacaoFormularioDTO.setCidade("123");
        localizacaoFormularioDTO.setEstado("123");

        EmpresaFormularioDTO empresaFormularioDTO = new EmpresaFormularioDTO();
        empresaFormularioDTO.setCnpj("76838107000175");
        empresaFormularioDTO.setRazaoSocial("Teste");
        empresaFormularioDTO.setPais(1L);
        empresaFormularioDTO.setProprietario("Teste");
        empresaFormularioDTO.setResponsavel("Teste");
        empresaFormularioDTO.setSite("teste.com");
        empresaFormularioDTO.setEmail("teste@teste.com");
        empresaFormularioDTO.setTelefoneList(Collections.singletonList("1592391923"));

        DetalheFormularioDTO detalheFormularioDTO = new DetalheFormularioDTO();
        detalheFormularioDTO.setDataInicio(new Timestamp(System.currentTimeMillis()));
        detalheFormularioDTO.setDataConclusao(new Timestamp(System.currentTimeMillis()));
        detalheFormularioDTO.setNaturezaInvestimento(1L);
        detalheFormularioDTO.setModeloContrato(ModeloContrato.OUTROS_ID.longValue());
        detalheFormularioDTO.setOutroModeloContrato("Teste");
        detalheFormularioDTO.setValorProjeto("100.000");
        detalheFormularioDTO.setEmpregosProjeto("100");
        detalheFormularioDTO.setAreaConstruida("123");
        detalheFormularioDTO.setAreaTotal("123");
        detalheFormularioDTO.setGrupoOperador("123");

        InternacionalizacaoFormularioDTO internacionalizacaoFormularioDTO = new InternacionalizacaoFormularioDTO();
        internacionalizacaoFormularioDTO.setNome("123");
        internacionalizacaoFormularioDTO.setDescricao("123");
        internacionalizacaoFormularioDTO.setIdiomaId(2L);
        internacionalizacaoFormularioDTO.setRemoverImagem(true);
        internacionalizacaoFormularioDTO.setAnexosRemovidos(Collections.singletonList(1L));

        projetoFormularioDTO.setLocalizacao(localizacaoFormularioDTO);
        projetoFormularioDTO.setEmpresa(empresaFormularioDTO);
        projetoFormularioDTO.setDetalhe(detalheFormularioDTO);
        projetoFormularioDTO.setSegmentoList(Collections.singletonList(1L));
        projetoFormularioDTO.setInternacionalizacaoList(Collections.singletonList(internacionalizacaoFormularioDTO));

        Mockito.when(projetoRepository.pegaProximoProtocolo()).thenReturn(1L);

        Projeto projeto = new Projeto();

        ProjetoLocalizacao projetoLocalizacao = new ProjetoLocalizacao();

        ProjetoEmpresaTelefone projetoEmpresaTelefone = new ProjetoEmpresaTelefone();
        projetoEmpresaTelefone.setTelefone("123");

        ProjetoEmpresa projetoEmpresa = new ProjetoEmpresa();
        projetoEmpresa.getProjetoEmpresaTelefoneList().add(projetoEmpresaTelefone);

        ProjetoDetalhe projetoDetalhe = new ProjetoDetalhe();

        ProjetoSegmento projetoSegmento = new ProjetoSegmento();
        projetoSegmento.setSegmento(new Segmento());

        Idioma idioma = new Idioma();
        idioma.setId(1L);
        idioma.setNome("Teste");
        idioma.setSigla("en-US");

        ProjetoInternacionalizacao projetoInternacionalizacao = new ProjetoInternacionalizacao();
        projetoInternacionalizacao.setIdioma(idioma);
        projetoInternacionalizacao.setNome("123");
        projetoInternacionalizacao.setDescricao("123");

        projeto.getProjetoInternacionalizacaoList().add(projetoInternacionalizacao);

        Idioma idiomaNovo = new Idioma();
        idiomaNovo.setId(2L);
        idiomaNovo.setNome("Teste");
        idiomaNovo.setSigla(Idioma.SIGLA_PADRAO);

        Status status = new Status();
        status.setId(1L);
        status.setNome("teste");

        byte[] randomBytes = RandomUtils.nextBytes(20);

        Arquivo arquivo = new Arquivo();
        arquivo.setDocumento(randomBytes);
        arquivo.setNome("teste");
        arquivo.setId(1L);
        arquivo.setStatus(status);

        TipoArquivo tipoArquivo = new TipoArquivo();
        tipoArquivo.setId(1L);
        tipoArquivo.setNome("Teste");

        ProjetoInternacionalizacaoArquivo projetoInternacionalizacaoArquivo = new ProjetoInternacionalizacaoArquivo();
        projetoInternacionalizacaoArquivo.setArquivo(arquivo);
        projetoInternacionalizacaoArquivo.setId(1L);
        projetoInternacionalizacaoArquivo.setTipoArquivo(tipoArquivo);

        ProjetoInternacionalizacao projetoPortugues = new ProjetoInternacionalizacao();
        projetoPortugues.setIdioma(idiomaNovo);
        projetoPortugues.setNome("123");
        projetoPortugues.setDescricao("123");
        projetoPortugues.getProjetoInternacionalizacaoArquivoList().add(projetoInternacionalizacaoArquivo);

        projeto.getProjetoInternacionalizacaoList().add(projetoPortugues);

        List<ProjetoSegmento> projetoSegmentoList = new ArrayList<>();
        projetoSegmentoList.add(projetoSegmento);

        projeto.setProjetoLocalizacaoList(Collections.singletonList(projetoLocalizacao));
        projeto.setProjetoEmpresaList(Collections.singletonList(projetoEmpresa));
        projeto.setProjetoDetalheList(Collections.singletonList(projetoDetalhe));
        projeto.setProjetoSegmentoList(projetoSegmentoList);

        Mockito.when(projetoRepository.findById(Mockito.anyLong())).thenReturn(java.util.Optional.of(projeto));

        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL, "salvar"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(projetoFormularioDTO)))
            .andExpect(status().isOk());
    }

    @Test
    void buscarPorId() throws Exception {
        Projeto projeto = new Projeto();

        Idioma idioma = new Idioma();
        idioma.setId(1L);
        idioma.setNome("Teste");
        idioma.setSigla(Idioma.SIGLA_PADRAO);

        Status status = new Status();
        status.setId(1L);
        status.setNome("teste");

        byte[] randomBytes = RandomUtils.nextBytes(20);

        Arquivo arquivo = new Arquivo();
        arquivo.setDocumento(randomBytes);
        arquivo.setNome("teste");
        arquivo.setId(1L);
        arquivo.setStatus(status);

        TipoArquivo tipoArquivo = new TipoArquivo();
        tipoArquivo.setId(1L);
        tipoArquivo.setNome("Teste");

        ProjetoInternacionalizacaoArquivo projetoInternacionalizacaoArquivo = new ProjetoInternacionalizacaoArquivo();
        projetoInternacionalizacaoArquivo.setArquivo(arquivo);
        projetoInternacionalizacaoArquivo.setTipoArquivo(tipoArquivo);

        ProjetoInternacionalizacao projetoInternacionalizacao = new ProjetoInternacionalizacao();
        projetoInternacionalizacao.setId(1L);
        projetoInternacionalizacao.setNome("teste");
        projetoInternacionalizacao.setDescricao("teste");
        projetoInternacionalizacao.setIdioma(idioma);
        projetoInternacionalizacao.getProjetoInternacionalizacaoArquivoList().add(projetoInternacionalizacaoArquivo);

        projeto.setStatus(status);
        projeto.setProjetoInternacionalizacaoList(Collections.singletonList(projetoInternacionalizacao));

        Mockito.when(projetoRepository.findById(Mockito.anyLong())).thenReturn(java.util.Optional.of(projeto));

        this.getMockMvc().perform(MockMvcRequestBuilders.get(buildUrl(BASE_URL, "projeto/1"))
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
    }

    @Test
    void aprovar() throws Exception {
        ModeracaoDTO moderacaoDTO = new ModeracaoDTO();
        moderacaoDTO.setJustificativa("Teste");
        moderacaoDTO.setUsuario(1L);

        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL, "aprovar/1"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(moderacaoDTO)))
            .andExpect(status().isOk());
    }

    @Test
    void aprovarAprovado() throws Exception {
        ModeracaoDTO moderacaoDTO = new ModeracaoDTO();
        moderacaoDTO.setJustificativa("Teste");
        moderacaoDTO.setUsuario(1L);

        Projeto projeto = new Projeto();

        Status status = new Status();
        status.setId(Status.APROVADO.longValue());

        projeto.setStatus(status);

        Mockito.when(projetoRepository.findById(Mockito.anyLong())).thenReturn(java.util.Optional.of(projeto));

        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL, "aprovar/1"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(moderacaoDTO)))
            .andExpect(status().isBadRequest());
    }

    @Test
    void reprovar() throws Exception {

        ModeracaoDTO moderacaoDTO = new ModeracaoDTO();
        moderacaoDTO.setJustificativa("Teste");
        moderacaoDTO.setUsuario(1L);

        Projeto projeto = new Projeto();

        Status status = new Status();
        status.setId(Status.APROVADO.longValue());

        projeto.setStatus(status);

        projeto.getProjetoDestaqueList().add(new ProjetoDestaque());

        Mockito.when(projetoRepository.findById(Mockito.anyLong())).thenReturn(java.util.Optional.of(projeto));

        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL, "reprovar/1"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(moderacaoDTO)))
            .andExpect(status().isOk());
    }

    @Test
    void restaurar() throws Exception {
        ModeracaoDTO moderacaoDTO = new ModeracaoDTO();
        moderacaoDTO.setJustificativa("Teste");
        moderacaoDTO.setUsuario(1L);

        Projeto projeto = new Projeto();

        Status status = new Status();
        status.setId(Status.REPROVADO.longValue());

        projeto.setStatus(status);

        Mockito.when(projetoRepository.findById(Mockito.anyLong())).thenReturn(java.util.Optional.of(projeto));

        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL, "restaurar/1"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(moderacaoDTO)))
            .andExpect(status().isOk());
    }

    @Test
    void restaurarAtivo() throws Exception {
        ModeracaoDTO moderacaoDTO = new ModeracaoDTO();
        moderacaoDTO.setJustificativa("Teste");
        moderacaoDTO.setUsuario(1L);

        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL, "restaurar/1"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(moderacaoDTO)))
            .andExpect(status().isBadRequest());
    }

    @Test
    void favoritar() throws Exception {
        ModeracaoDTO moderacaoDTO = new ModeracaoDTO();
        moderacaoDTO.setJustificativa("Teste");
        moderacaoDTO.setUsuario(1L);

        Projeto projeto = new Projeto();

        Status status = new Status();
        status.setId(Status.APROVADO.longValue());

        projeto.setStatus(status);

        ProjetoDestaque projetoDestaque = new ProjetoDestaque();
        projeto.getProjetoDestaqueList().add(projetoDestaque);

        Mockito.when(projetoRepository.findById(Mockito.anyLong())).thenReturn(java.util.Optional.of(projeto));

        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL, "favoritar/1/1"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(moderacaoDTO)))
            .andExpect(status().isOk());
    }

    @Test
    void desfavoritar() throws Exception {
        ModeracaoDTO moderacaoDTO = new ModeracaoDTO();
        moderacaoDTO.setJustificativa("Teste");
        moderacaoDTO.setUsuario(1L);

        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL, "desfavoritar/1/1"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(moderacaoDTO)))
            .andExpect(status().isOk());
    }

    @Test
    void comentar() throws Exception {
        ModeracaoDTO moderacaoDTO = new ModeracaoDTO();
        moderacaoDTO.setJustificativa("Teste");
        moderacaoDTO.setUsuario(1L);

        this.getMockMvc().perform(MockMvcRequestBuilders.multipart(buildUrl(BASE_URL, "comentar/1"))
            .contentType(MediaType.MULTIPART_FORM_DATA)
            .content(TestUtil.convertObjectToJsonBytes(moderacaoDTO)))
            .andExpect(status().isOk());
    }

    @Test
    void removerComentario() throws Exception {
        ModeracaoDTO moderacaoDTO = new ModeracaoDTO();
        moderacaoDTO.setJustificativa("Teste");
        moderacaoDTO.setUsuario(1L);

        ProjetoLinhaTemporal projetoLinhaTemporal = new ProjetoLinhaTemporal();

        Mockito.when(projetoLinhaTemporalRepository.findById(Mockito.anyLong())).thenReturn(java.util.Optional.of(projetoLinhaTemporal));

        this.getMockMvc().perform(MockMvcRequestBuilders.multipart(buildUrl(BASE_URL, "remover-comentario/1"))
            .contentType(MediaType.MULTIPART_FORM_DATA)
            .content(TestUtil.convertObjectToJsonBytes(moderacaoDTO)))
            .andExpect(status().isOk());
    }

    @Test
    void solicitarEsclarecimento() throws Exception {
        ModeracaoDTO moderacaoDTO = new ModeracaoDTO();
        moderacaoDTO.setJustificativa("Teste");
        moderacaoDTO.setUsuario(1L);

        this.getMockMvc().perform(MockMvcRequestBuilders.multipart(buildUrl(BASE_URL, "solicitar-esclarecimento/1"))
            .contentType(MediaType.MULTIPART_FORM_DATA)
            .content(TestUtil.convertObjectToJsonBytes(moderacaoDTO)))
            .andExpect(status().isOk());
    }

    @Test
    void solicitarEsclarecimentoReprovado() throws Exception {
        ModeracaoDTO moderacaoDTO = new ModeracaoDTO();
        moderacaoDTO.setJustificativa("Teste");
        moderacaoDTO.setUsuario(1L);

        Projeto projeto = new Projeto();

        Status status = new Status();
        status.setId(Status.REPROVADO.longValue());

        projeto.setStatus(status);

        ProjetoDestaque projetoDestaque = new ProjetoDestaque();
        projeto.getProjetoDestaqueList().add(projetoDestaque);

        Mockito.when(projetoRepository.findById(Mockito.anyLong())).thenReturn(java.util.Optional.of(projeto));

        this.getMockMvc().perform(MockMvcRequestBuilders.multipart(buildUrl(BASE_URL, "solicitar-esclarecimento/1"))
            .contentType(MediaType.MULTIPART_FORM_DATA)
            .content(TestUtil.convertObjectToJsonBytes(moderacaoDTO)))
            .andExpect(status().isBadRequest());
    }

    @Test
    void solicitarEsclarecimentoComDestaque() throws Exception {
        ModeracaoDTO moderacaoDTO = new ModeracaoDTO();
        moderacaoDTO.setJustificativa("Teste");
        moderacaoDTO.setUsuario(1L);

        Projeto projeto = new Projeto();

        Status status = new Status();
        status.setId(Status.APROVADO.longValue());

        projeto.setStatus(status);

        ProjetoDestaque projetoDestaque = new ProjetoDestaque();
        projeto.getProjetoDestaqueList().add(projetoDestaque);

        projeto.setPublicacaoPortal(1L);

        Mockito.when(projetoRepository.findById(Mockito.anyLong())).thenReturn(java.util.Optional.of(projeto));

        this.getMockMvc().perform(MockMvcRequestBuilders.multipart(buildUrl(BASE_URL, "solicitar-esclarecimento/1"))
            .contentType(MediaType.MULTIPART_FORM_DATA)
            .content(TestUtil.convertObjectToJsonBytes(moderacaoDTO)))
            .andExpect(status().isOk());
    }

    @Test
    void responderEsclarecimento() throws Exception {
        ModeracaoDTO moderacaoDTO = new ModeracaoDTO();
        moderacaoDTO.setJustificativa("Teste");
        moderacaoDTO.setUsuario(1L);

        Projeto projeto = new Projeto();

        Status status = new Status();
        status.setId(Status.ESCLARECIMENTO.longValue());

        projeto.setStatus(status);

        Mockito.when(projetoRepository.findById(Mockito.anyLong())).thenReturn(java.util.Optional.of(projeto));

        this.getMockMvc().perform(MockMvcRequestBuilders.multipart(buildUrl(BASE_URL, "responder-esclarecimento/1"))
            .contentType(MediaType.MULTIPART_FORM_DATA)
            .content(TestUtil.convertObjectToJsonBytes(moderacaoDTO)))
            .andExpect(status().isOk());
    }

    @Test
    void responderEsclarecimentoAprovado() throws Exception {
        ModeracaoDTO moderacaoDTO = new ModeracaoDTO();
        moderacaoDTO.setJustificativa("Teste");
        moderacaoDTO.setUsuario(1L);

        Projeto projeto = new Projeto();

        Status status = new Status();
        status.setId(Status.APROVADO.longValue());

        projeto.setStatus(status);

        Mockito.when(projetoRepository.findById(Mockito.anyLong())).thenReturn(java.util.Optional.of(projeto));

        this.getMockMvc().perform(MockMvcRequestBuilders.multipart(buildUrl(BASE_URL, "responder-esclarecimento/1"))
            .contentType(MediaType.MULTIPART_FORM_DATA)
            .content(TestUtil.convertObjectToJsonBytes(moderacaoDTO)))
            .andExpect(status().isBadRequest());
    }

}
