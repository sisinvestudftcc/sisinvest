package br.com.sisinvest.pitprojeto.web.rest;

import br.com.sisinvest.pitprojeto.domain.Arquivo;
import br.com.sisinvest.pitprojeto.domain.Idioma;
import br.com.sisinvest.pitprojeto.domain.Status;
import br.com.sisinvest.pitprojeto.repository.ArquivoRepository;
import br.com.sisinvest.pitprojeto.repository.ProjetoLocalizacaoRepository;
import br.com.sisinvest.pitprojeto.security.RunWithMockCustomUser;
import br.com.sisinvest.pitprojeto.service.dto.filtro.CidadeFiltroDTO;
import br.com.sisinvest.pitprojeto.service.dto.filtro.EquipamentoFiltroDTO;
import br.com.sisinvest.pitprojeto.service.dto.filtro.IdiomaFiltroDTO;
import br.com.sisinvest.pitprojeto.service.dto.filtro.StatusFiltroDTO;
import br.com.sisinvest.pitprojeto.util.IntTestComum;
import br.com.sisinvest.pitprojeto.PitprojetoApp;
import org.apache.commons.lang3.RandomUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWithMockCustomUser
@Transactional
@SpringBootTest(classes = PitprojetoApp.class)
class DominioResourceIT extends IntTestComum {

    private static final String BASE_URL = "/api/public/dominio";

    @MockBean
    private ArquivoRepository arquivoRepository;

    @MockBean
    private ProjetoLocalizacaoRepository projetoLocalizacaoRepository;

    @BeforeEach
    public void setup() {
        Arquivo arquivo = new Arquivo();

        byte[] randomBytes = RandomUtils.nextBytes(20);

        Status status = new Status();
        status.setId(1L);
        status.setNome("Ativo");

        arquivo.setId(1L);
        arquivo.setStatus(status);
        arquivo.setNome("Teste.png");
        arquivo.setDocumento(randomBytes);

        ArrayList<String> stringList = new ArrayList<>();

        stringList.add("Teste");

        Mockito.when(arquivoRepository.findById(Mockito.anyLong())).thenReturn(java.util.Optional.of(arquivo));
        Mockito.when(projetoLocalizacaoRepository.pegaListaEstado()).thenReturn(stringList);
        Mockito.when(projetoLocalizacaoRepository.pegaListaCidade(Mockito.anyString())).thenReturn(stringList);
    }

    @Test
    void listarIdiomas() throws Exception {
        IdiomaFiltroDTO idiomaFiltroDTO = new IdiomaFiltroDTO();
        idiomaFiltroDTO.setNome("Português");
        idiomaFiltroDTO.setSigla(Idioma.SIGLA_PADRAO);

        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL, "idioma"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(idiomaFiltroDTO)))
            .andExpect(status().isOk());
    }

    @Test
    void listarSegmentos() throws Exception {
        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL, "segmento"))
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
    }

    @Test
    void listarPaises() throws Exception {
        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL, "pais"))
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
    }

    @Test
    void listarNaturezasInvestimento() throws Exception {
        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL, "natureza-investimento"))
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
    }

    @Test
    void listarModelosContrato() throws Exception {
        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL, "modelo-contrato"))
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
    }

    @Test
    void listarValoresProjeto() throws Exception {
        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL, "valor-projeto"))
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
    }

    @Test
    void listarEmpregosProjeto() throws Exception {
        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL, "emprego-projeto"))
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
    }

    @Test
    void listarGruposEquipamento() throws Exception {
        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL, "grupo-equipamento"))
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
    }

    @Test
    void listarEquipamento() throws Exception {
        EquipamentoFiltroDTO equipamentoFiltroDTO = new EquipamentoFiltroDTO();
        equipamentoFiltroDTO.setGrupoEquipamento(1L);
        equipamentoFiltroDTO.setNome("Teste");

        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL, "equipamento"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(equipamentoFiltroDTO)))
            .andExpect(status().isOk());
    }

    @Test
    void listarEquipamentoSemGrupo() throws Exception {
        EquipamentoFiltroDTO equipamentoFiltroDTO = new EquipamentoFiltroDTO();
        equipamentoFiltroDTO.setGrupoEquipamento(null);
        equipamentoFiltroDTO.setNome("Teste");

        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL, "equipamento"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(equipamentoFiltroDTO)))
            .andExpect(status().isOk());
    }

    @Test
    void listarStatus() throws Exception {
        StatusFiltroDTO statusFiltroDTO = new StatusFiltroDTO();
        ArrayList<Long> statusList = new ArrayList<>();
        statusList.add(1L);
        statusFiltroDTO.setNotStatusList(statusList);

        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL, "status"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(statusFiltroDTO)))
            .andExpect(status().isOk());
    }

    @Test
    void listarStatusSemFiltro() throws Exception {
        StatusFiltroDTO statusFiltroDTO = new StatusFiltroDTO();
        statusFiltroDTO.setNotStatusList(null);

        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL, "status"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(statusFiltroDTO)))
            .andExpect(status().isOk());
    }

    @Test
    void listarStatusSemValores() throws Exception {
        StatusFiltroDTO statusFiltroDTO = new StatusFiltroDTO();
        ArrayList<Long> statusList = new ArrayList<>();

        statusFiltroDTO.setNotStatusList(statusList);

        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL, "status"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(statusFiltroDTO)))
            .andExpect(status().isOk());
    }

    @Test
    void listarEstados() throws Exception {
        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL, "estado"))
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
    }

    @Test
    void listarCidade() throws Exception {
        CidadeFiltroDTO cidadeFiltroDTO = new CidadeFiltroDTO();
        cidadeFiltroDTO.setEstado("DF");

        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL, "cidade"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(cidadeFiltroDTO)))
            .andExpect(status().isOk());
    }

    @Test
    void visualizarArquivo() throws Exception {
        this.getMockMvc().perform(MockMvcRequestBuilders.get(buildUrl(BASE_URL, "visualizar-arquivo/1"))
            .contentType(MediaType.APPLICATION_OCTET_STREAM))
            .andExpect(status().isOk());
    }

    @Test
    void portfolio() throws Exception {
        this.getMockMvc().perform(MockMvcRequestBuilders.get(buildUrl(BASE_URL, "portfolio?idlist[]=1"))
            .contentType(MediaType.APPLICATION_OCTET_STREAM))
            .andExpect(status().isOk());
    }

    @Test
    void portfolioSemId() throws Exception {
        this.getMockMvc().perform(MockMvcRequestBuilders.get(buildUrl(BASE_URL, "portfolio?idlist[]="))
            .contentType(MediaType.APPLICATION_OCTET_STREAM))
            .andExpect(status().isOk());
    }

    @Test
    void baixarArquivo() throws Exception {
        this.getMockMvc().perform(MockMvcRequestBuilders.get(buildUrl(BASE_URL, "baixar-arquivo/1"))
            .contentType(MediaType.APPLICATION_OCTET_STREAM))
            .andExpect(status().isOk());

    }

}
