package br.com.sisinvest.pitprojeto.web.rest;

import br.com.sisinvest.pitprojeto.domain.Status;
import br.com.sisinvest.pitprojeto.security.RunWithMockCustomUser;
import br.com.sisinvest.pitprojeto.service.dto.filtro.ProjetoFiltroDTO;
import br.com.sisinvest.pitprojeto.util.IntTestComum;
import br.com.sisinvest.pitprojeto.PitprojetoApp;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.ArrayList;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWithMockCustomUser
@Transactional
@SpringBootTest(classes = PitprojetoApp.class)
class ProjetoResourceIT extends IntTestComum {

    private static final String BASE_URL = "/api/projeto";

    @Test
    void buscarLinhaTemporal() throws Exception {
        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL, "projeto/1/linha-temporal"))
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
    }

    @Test
    void filtrarProjetos() throws Exception {
        ProjetoFiltroDTO projetoFiltroDTO = new ProjetoFiltroDTO();

        ArrayList<Long> list = new ArrayList<>();
        list.add(1L);

        projetoFiltroDTO.setStatusList(list);
        projetoFiltroDTO.setNotStatusList(list);

        projetoFiltroDTO.setUsuarioFavoritado(1L);
        projetoFiltroDTO.setProjetoId(1L);

        projetoFiltroDTO.setIdioma("pt-BR");

        projetoFiltroDTO.setProtocolo("PRJ202000001");

        projetoFiltroDTO.setNome("Projeto");

        projetoFiltroDTO.setCnpj("56772421000195");

        projetoFiltroDTO.setPais(1L);

        projetoFiltroDTO.setNaturezaInvestimento(1L);

        projetoFiltroDTO.setModeloContrato(1L);

        projetoFiltroDTO.setValorProjeto("100.000");

        projetoFiltroDTO.setEstado("DF");

        projetoFiltroDTO.setCidade("Brasília");

        projetoFiltroDTO.setGrupoEquipamento(1L);

        projetoFiltroDTO.setStatus(Status.RASCUNHO.longValue());
        projetoFiltroDTO.setUsuarioStatus(1L);

        projetoFiltroDTO.setDataInicio(new Timestamp(System.currentTimeMillis()));
        projetoFiltroDTO.setDataConclusao(new Timestamp(System.currentTimeMillis()));

        projetoFiltroDTO.setSegmentoList(list);

        projetoFiltroDTO.setApenasProprios(1L);

        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL, "filtrar"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(projetoFiltroDTO)))
            .andExpect(status().isOk());
    }

    @Test
    void filtrarProjetosSemFiltro() throws Exception {
        ProjetoFiltroDTO projetoFiltroDTO = new ProjetoFiltroDTO();

        projetoFiltroDTO.setStatusList(null);
        projetoFiltroDTO.setNotStatusList(null);

        projetoFiltroDTO.setUsuarioFavoritado(null);
        projetoFiltroDTO.setProjetoId(null);

        projetoFiltroDTO.setIdioma(null);

        projetoFiltroDTO.setProtocolo(null);

        projetoFiltroDTO.setNome(null);

        projetoFiltroDTO.setCnpj(null);

        projetoFiltroDTO.setPais(null);

        projetoFiltroDTO.setNaturezaInvestimento(null);

        projetoFiltroDTO.setModeloContrato(null);

        projetoFiltroDTO.setValorProjeto(null);

        projetoFiltroDTO.setEstado(null);

        projetoFiltroDTO.setCidade(null);

        projetoFiltroDTO.setGrupoEquipamento(null);

        projetoFiltroDTO.setStatus(null);
        projetoFiltroDTO.setUsuarioStatus(null);

        projetoFiltroDTO.setDataInicio(null);
        projetoFiltroDTO.setDataConclusao(null);

        projetoFiltroDTO.setSegmentoList(null);

        projetoFiltroDTO.setApenasProprios(null);

        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL, "filtrar"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(projetoFiltroDTO)))
            .andExpect(status().isOk());
    }

    @Test
    void filtrarProjetosComRascunhoFavoritado() throws Exception {
        ProjetoFiltroDTO projetoFiltroDTO = new ProjetoFiltroDTO();

        projetoFiltroDTO.setStatus(Status.RASCUNHO.longValue());
        projetoFiltroDTO.setUsuarioStatus(null);
        projetoFiltroDTO.setUsuarioFavoritado(1L);
        projetoFiltroDTO.setProjetoId(null);

        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL, "filtrar"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(projetoFiltroDTO)))
            .andExpect(status().isOk());
    }

    @Test
    void filtrarProjetosComUsuario() throws Exception {
        ProjetoFiltroDTO projetoFiltroDTO = new ProjetoFiltroDTO();

        projetoFiltroDTO.setUsuarioId(1L);

        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL, "filtrar"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(projetoFiltroDTO)))
            .andExpect(status().isOk());
    }

    @Test
    void filtrarProjetosComUsuarioStatus() throws Exception {
        ProjetoFiltroDTO projetoFiltroDTO = new ProjetoFiltroDTO();

        projetoFiltroDTO.setUsuarioStatus(1L);
        projetoFiltroDTO.setStatus(null);

        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL, "filtrar"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(projetoFiltroDTO)))
            .andExpect(status().isOk());
    }

    @Test
    void filtrarProjetosSemRascunho() throws Exception {
        ProjetoFiltroDTO projetoFiltroDTO = new ProjetoFiltroDTO();

        projetoFiltroDTO.setUsuarioStatus(1L);
        projetoFiltroDTO.setStatus(Status.APROVADO.longValue());

        this.getMockMvc().perform(MockMvcRequestBuilders.post(buildUrl(BASE_URL, "filtrar"))
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(projetoFiltroDTO)))
            .andExpect(status().isOk());
    }

}
