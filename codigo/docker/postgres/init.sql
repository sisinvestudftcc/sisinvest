-- #### APLICAÇÃO #### --

-- ### Pit Service ### --
-- cria esquema
\connect pit;
CREATE SCHEMA pitservice;

-- cria role com privilégios para todas as operações
CREATE ROLE rpitservice;
ALTER ROLE rpitservice SET search_path TO pitservice, public;
GRANT ALL PRIVILEGES ON DATABASE pit TO rpitservice;
GRANT ALL PRIVILEGES ON SCHEMA pitservice TO rpitservice;

-- cria usuário geral da aplicação (terá privilégios para todas as operações)
CREATE USER pitservice WITH PASSWORD 'pitservice' IN ROLE rpitservice;

-- ### Pit Segurança ### --
-- cria esquema
CREATE SCHEMA pitseguranca;

-- cria role com privilégios para todas as operações
CREATE ROLE rpitseguranca;
ALTER ROLE rpitseguranca SET search_path TO pitseguranca, public;
GRANT ALL PRIVILEGES ON DATABASE pit TO rpitseguranca;
GRANT ALL PRIVILEGES ON SCHEMA pitseguranca TO rpitseguranca;

-- cria usuário geral da aplicação (terá privilégios para todas as operações)
CREATE USER pitseguranca WITH PASSWORD 'pitseguranca' IN ROLE rpitseguranca;

-- ### Pit Projeto ### --
-- cria esquema
CREATE SCHEMA pitprojeto;

-- cria role com privilégios para todas as operações
CREATE ROLE rpitprojeto;
ALTER ROLE rpitprojeto SET search_path TO pitprojeto, public;
GRANT ALL PRIVILEGES ON DATABASE pit TO rpitprojeto;
GRANT ALL PRIVILEGES ON SCHEMA pitprojeto TO rpitprojeto;

-- cria usuário geral da aplicação (terá privilégios para todas as operações)
CREATE USER pitprojeto WITH PASSWORD 'pitprojeto' IN ROLE rpitprojeto;
