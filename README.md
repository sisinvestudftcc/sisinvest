# SISINVEST - TCC UDF

O SISINVEST, Sistema de Investimentos, é um sistema de cadastro de projetos cujo objetivo é atender as necessidades de empreendedores que desejem receber investimentos para o planejamento e execução de projetos urbanísticos de pequeno porte, por meio da divulgação de seus projetos que poderão ser visualizados por investidores em potencial.

O objetivo principal do projeto é manter um sistema eletrônico capaz de permitir a divulgação de projetos por parte dos empreendedores, assim como o acompanhamento desse projeto pelos possíveis investidores, desde que o projeto tenha passado por uma moderação da área gestora.

O sistema possuirá a criação de um projeto, por meio de 3 módulos: Manter Projeto; Manter Usuários e Central de Comunicação.

Esses módulos estão relacionados ao Google para consulta dos dados de geolocalização dos projetos.

Ainda que façam parte das necessidades dos clientes, não farão parte do escopo do sistema: Manter domínios fixos do sistema, publicar os projetos externamente, pagamentos pela plataforma e divulgação do projeto em redes sociais.


### Estrutura de Arquivos

 - codigo = Código da aplicação
    - pitclient = Aplicação Angular
    - pitseguranca = Microserviço de Usuários
    - pitprojeto = Microserviço de Projetos
    - docker = Docker do Projeto
 - docs = Documentação do TCC
